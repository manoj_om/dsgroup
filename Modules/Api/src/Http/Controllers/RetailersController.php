<?php

namespace Api\Http\Controllers;

use App\Http\Controllers\Controllers;
use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;

use Api\Models\AgentsOtpModel;
use Api\Models\RetailersModel;

use App\Helpers;
use Api\Models\QrCodeModel;
use Api\Models\QrCodeLogModel;
use Illuminate\Routing\Controller as BaseController;
use App\MapDistributor;
use App\City;
use App\Distributor;
use App\Traits\CurlRequestTrait;

class RetailersController extends BaseController {

    use CurlRequestTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'user_key' => 'required',
        ]);
        if ($validator->fails()) {

            return response()->json([
                'status' => 0,
                'message' => $validator->errors()->first()
            ]);
        }
        $objUserkey = AgentsOtpModel::where(['agent_id' => $request->user_id])
                        ->where(['token_key' => $request->user_key])->first();
        if (isset($objUserkey->id)) {


            $result_list = RetailersModel::select('id', 'user_id', 'shop_name', 'shop_address', 'full_name', 'primary_mobile_number', 'latitudes', 'longitude', 'shop_image')
                            ->where(['user_id' => $request->user_id])->where(['is_fill' => 1])->orderBy('id', "DESC")->paginate(20);
            $array_retailer = [];
            $image_path = config('app.url') . '/uploads/retailers/shop_image/';
            foreach ($result_list as $retailer_val) {
                //$lat= 26.754347; //latitude
                //$lng= 81.001640; //longitude
                $address = '';
                if (isset($retailer_val->latitudes) && isset($retailer_val->longitude))
                    $address = Helpers\Helpers::getaddress($retailer_val->latitudes, $retailer_val->longitude);



                $array_retailer[] = array('id' => $retailer_val->id, 'user_id' => $retailer_val->user_id,
                    'shop_name' => $retailer_val->shop_name,
                    'shop_address' => $address,
                    'full_name' => $retailer_val->full_name, 'primary_mobile_number' => $retailer_val->primary_mobile_number,
                    'shop_image' => $retailer_val->shop_image ? $image_path . $retailer_val->shop_image : ''
                );
            }
            if ($result_list->lastPage()) {
                return Response()->json([
                            'status' => 1,
                            'last_page' => $result_list->lastPage(),
                            'data' => $array_retailer,
                ]);
            } else {
                return Response()->json([
                            'status' => 0,
                            'message' => "record not found"
                ]);
            }
        } else {
            return Response()->json([
                        'status' => 0,
                        'message' => "Invalid user key"
            ]);
        }
    }

    public function store(Request $request) {
        //781eb99b39961ba83ac1cd55c1cab6d7
         //228,227,226,225,224,221,220,219
//        RetailersModel::whereIn('id', [228,227,226,225,224,221,220,219])->delete();
//        die();
        $id = $request->id ? $request->id : 0;
        $validator = Validator::make($request->all(), [
                    'operation_type' => 'required|max:3',
        ]);
        if ($validator->fails()) {

            return response()->json([
                'status' => 0,
                'message' => $validator->errors()->first()
            ]);
        }

        
        //1 for primary,2 for secondry,3 for shop detail,4 for additional,5 for kyc ,6 for scan code
        switch ($request->operation_type) {

            case 1: {

                    $validator = Validator::make($request->all(), [
                                'user_id' => 'required',
                                'user_key' => 'required',
                                'full_name' => 'required|string|max:150',
                                'primary_mobile_number' => 'required|numeric',
                                //  'alternative_mobile_number'=>'unique:retailers',
                                'capture_retailer_image' => 'image | mimes:jpeg,jpg,png,gif|max:10000'
                    ]);

                    if ($validator->fails()) {

                        return response()->json([
                            'status' => 0,
                            'message' => $validator->errors()->first()
                        ]);

                    }
                    if($request->hasFile('capture_retailer_image')){
                        $fileExtension = $request->capture_retailer_image->getClientOriginalExtension();
                        $imageExtensions = ['jpg','png','jpeg','gif'];
                        if(!in_array($fileExtension,$imageExtensions))
                            return response()->json([
                                'status' => 0,
                                'message' => 'Invalid file type'
                            ]);
                    }
                    $objUserkey = AgentsOtpModel::where(['agent_id' => $request->user_id])
                                    ->where(['token_key' => $request->user_key])->first();
                    if (isset($objUserkey->id)) {


                        // Must not already exist in the `email` column of `users` table


                        if ($id) {


                            $objRetailerPrimaryInfo = RetailersModel::where(['id' => $id])
                                            ->where(['user_id' => $request->user_id])->first();
                            if (isset($objRetailerPrimaryInfo->id)) {
                                $resultObjMobileExist = RetailersModel::where(["is_fill" => 1])->where("id", '!=', $id)
                                                ->where(['primary_mobile_number' => $request->primary_mobile_number])->first();
                                if (isset($resultObjMobileExist->id)) {
                                    return Response()->json([
                                                'status' => 0,
                                                'message' => "Primary Mobile Number already exist",
                                    ]);
                                }
                                $objRetailerPrimaryInfo->full_name = $request->full_name;
                                $objRetailerPrimaryInfo->primary_mobile_number = $request->primary_mobile_number;
                                $objRetailerPrimaryInfo->alternative_mobile_number = $request->alternative_mobile_number;
                                $objRetailerPrimaryInfo->whats_app_number = $request->whats_app_number;
                                $objRetailerPrimaryInfo->primary_mobile_number = $request->primary_mobile_number;
                                $objRetailerPrimaryInfo->capture_retailer_image = '';
                                $objRetailerPrimaryInfo->p_device_type = $request->p_device_type ? $request->p_device_type : 0;
                                if ($request->hasfile('capture_retailer_image')) {

                                    $file = $request->file('capture_retailer_image');

                                    $ext = $file->getClientOriginalExtension();
                                    $filename = $request->user_id . time() . "_cri" . '.' . $ext;

                                    $file->move('uploads/retailers/primary', $filename);

                                    $objRetailerPrimaryInfo->capture_retailer_image = $filename;
                                }
                                if ($objRetailerPrimaryInfo->save()) {

                                    $objRetailerPrimaryInfo->dob = $objRetailerPrimaryInfo->dob ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->dob)) : '';
                                    $objRetailerPrimaryInfo->anniversay = $objRetailerPrimaryInfo->anniversay ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->anniversay)) : '';
                                    $objRetailerPrimaryInfo->first_kid_birthday = $objRetailerPrimaryInfo->first_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->first_kid_birthday)) : '';
                                    $objRetailerPrimaryInfo->second_kid_birthday = $objRetailerPrimaryInfo->second_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->second_kid_birthday)) : '';
                                    $objRetailerPrimaryInfo->third_kid_birthday = $objRetailerPrimaryInfo->third_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->third_kid_birthday)) : '';
                                    return Response()->json([
                                                'status' => 1,
                                                'message' => "Record updated",
                                                'data' => $objRetailerPrimaryInfo->toArray()
                                    ]);
                                } else {
                                    return Response()->json([
                                                'status' => 0,
                                                'message' => "Server side issue",
                                    ]);
                                }
                            }
                        } else {
                            $resultObjMobileExist = RetailersModel::where(["is_fill" => 1])
                                            ->where(['primary_mobile_number' => $request->primary_mobile_number])->first();
                            if (isset($resultObjMobileExist->id)) {
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Primary Mobile Number all ready exist",
                                ]);
                            }
                            $objRetailerPrimaryInfo = new RetailersModel();
                            $objRetailerPrimaryInfo->user_id = $request->user_id;
                            $objRetailerPrimaryInfo->full_name = $request->full_name;
                            $objRetailerPrimaryInfo->primary_mobile_number = $request->primary_mobile_number ? $request->primary_mobile_number : 0;
                            $objRetailerPrimaryInfo->alternative_mobile_number = $request->alternative_mobile_number ? $request->alternative_mobile_number : 0;
                            $objRetailerPrimaryInfo->whats_app_number = $request->whats_app_number ? $request->whats_app_number : 0;
                            $objRetailerPrimaryInfo->primary_mobile_number = $request->primary_mobile_number ? $request->primary_mobile_number : 0;
                            $objRetailerPrimaryInfo->capture_retailer_image = '';
                            $objRetailerPrimaryInfo->p_device_type = $request->p_device_type ? $request->p_device_type : 0;
                            if ($request->hasfile('capture_retailer_image')) {

                                $file = $request->file('capture_retailer_image');

                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id . time() . '.' . $ext;

                                $file->move('uploads/retailers/primary', $filename);

                                $objRetailerPrimaryInfo->capture_retailer_image = $filename;
                            }
                            if ($objRetailerPrimaryInfo->save()) {
                                $objRetailerPrimaryInfo->dob = $objRetailerPrimaryInfo->dob ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->dob)) : '';
                                $objRetailerPrimaryInfo->anniversay = $objRetailerPrimaryInfo->anniversay ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->anniversay)) : '';
                                $objRetailerPrimaryInfo->first_kid_birthday = $objRetailerPrimaryInfo->first_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->first_kid_birthday)) : '';
                                $objRetailerPrimaryInfo->second_kid_birthday = $objRetailerPrimaryInfo->second_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->second_kid_birthday)) : '';
                                $objRetailerPrimaryInfo->third_kid_birthday = $objRetailerPrimaryInfo->third_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->third_kid_birthday)) : '';

                                return Response()->json([
                                            'status' => 1,
                                            'message' => "Record updated",
                                            'data' => $objRetailerPrimaryInfo->toArray()
                                ]);
                            } else {
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Server side issue",
                                ]);
                            }
                        }
                    } else {
                        return Response()->json([
                                    'status' => 0,
                                    'message' => "Invalid user key"
                        ]);
                    }
                }
                break;
            case 2: {

                    // if($_SERVER['REMOTE_ADDR']=='192.168.1.52')
                    // {
                    $validator = Validator::make($request->all(), [
                                'user_id' => 'required',
                                'user_key' => 'required',
                                //'secondary_full_name'=>'string:max:200',
                                'secondary_mobile_number' => 'numeric',
                                'secondary_capture_retailer_image' => 'image | mimes:jpeg,jpg,png,gif|max:10000'
                    ]);
                    if ($id) {
                        $validator = Validator::make($request->all(), [
                                    'user_id' => 'required',
                                    'user_key' => 'required',
                                    //   'secondary_full_name'=>'string:max:200',
                                    //    'secondary_mobile_number'=>'numeric',
                                    'secondary_capture_retailer_image' => 'image | mimes:jpeg,jpg,png,gif|max:10000'
                        ]);
                    }
                    if ($validator->fails()) {

                        $error_msg = json_encode([
                            'status' => 0,
                            'message' => $validator->messages()
                        ]);
                        $arrayMessages = json_decode($error_msg);
                        foreach ($arrayMessages as $keyVal => $value) {
                            if ($keyVal == 'message') {
                                foreach ($value as $messagesValues) {
                                    foreach ($messagesValues as $messagesValue) {
                                        return Response()->json([
                                                    'status' => 0,
                                                    'message' => $messagesValue
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                    if($request->hasFile('secondary_capture_retailer_image')){
                        $fileExtension = $request->secondary_capture_retailer_image->getClientOriginalExtension();
                        $imageExtensions = ['jpg','png','jpeg','gif'];
                        if(!in_array($fileExtension,$imageExtensions))
                            return response()->json([
                                'status' => 0,
                                'message' => 'Invalid file type'
                            ]);
                    }
                    $objUserkey = AgentsOtpModel::where(['agent_id' => $request->user_id])
                                    ->where(['token_key' => $request->user_key])->first();
                    if (isset($objUserkey->id)) {


                        // Must not already exist in the `email` column of `users` table


                        if ($id) {

                            $resultObjMobileExist = RetailersModel::where(["is_fill" => 1])->where("id", '!=', $id)->where("id", '!=', 0)
                                            ->where(['secondary_mobile_number' => $request->secondary_mobile_number])->first();
                            if (isset($resultObjMobileExist->id) && isset($request->secondary_mobile_number)) {
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Secondary Mobile Number already exist",
                                ]);
                            }
                            $objRetailerPrimaryInfo = RetailersModel::where(['id' => $id])->where(['user_id' => $request->user_id])->first();
                            if (isset($objRetailerPrimaryInfo->id)) {

                                $objRetailerPrimaryInfo->secondary_full_name = $request->secondary_full_name;
                                $objRetailerPrimaryInfo->secondary_mobile_number = $request->secondary_mobile_number;
                                $objRetailerPrimaryInfo->secondary_capture_retailer_image = '';

                                if ($request->hasfile('secondary_capture_retailer_image')) {

                                    $file = $request->file('secondary_capture_retailer_image');

                                    $ext = $file->getClientOriginalExtension();
                                    $filename = $request->user_id . time() . "_sc" . '.' . $ext;

                                    $file->move('uploads/retailers/secondary', $filename);

                                    $objRetailerPrimaryInfo->secondary_capture_retailer_image = $filename;
                                }
                                if ($objRetailerPrimaryInfo->save()) {

                                    $objRetailerPrimaryInfo->dob = $objRetailerPrimaryInfo->dob ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->dob)) : '';
                                    $objRetailerPrimaryInfo->anniversay = $objRetailerPrimaryInfo->anniversay ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->anniversay)) : '';
                                    $objRetailerPrimaryInfo->first_kid_birthday = $objRetailerPrimaryInfo->first_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->first_kid_birthday)) : '';
                                    $objRetailerPrimaryInfo->second_kid_birthday = $objRetailerPrimaryInfo->second_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->second_kid_birthday)) : '';
                                    $objRetailerPrimaryInfo->third_kid_birthday = $objRetailerPrimaryInfo->third_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->third_kid_birthday)) : '';
                                    return Response()->json([
                                                'status' => 1,
                                                'message' => "Record updated",
                                                'data' => $objRetailerPrimaryInfo->toArray()
                                    ]);
                                } else {
                                    return Response()->json([
                                                'status' => 0,
                                                'message' => "Server side issue",
                                    ]);
                                }
                            }
                        } else {
                            $resultObjMobileExist = RetailersModel::where(["is_fill" => 1])
                                            ->where(['secondary_mobile_number' => $request->secondary_mobile_number])->first();
                            if (isset($resultObjMobileExist->id)) {
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Secondary Mobile Number already exist",
                                ]);
                            }
                            $objRetailerPrimaryInfo = new RetailersModel();
                            $objRetailerPrimaryInfo->user_id = $request->user_id;
                            $objRetailerPrimaryInfo->secondary_full_name = $request->secondary_full_name ? $request->secondary_full_name : '';
                            $objRetailerPrimaryInfo->secondary_mobile_number = $request->secondary_mobile_number ? $request->secondary_mobile_number : '';
                            $objRetailerPrimaryInfo->secondary_capture_retailer_image = '';

                            if ($request->hasfile('secondary_capture_retailer_image')) {

                                $file = $request->file('secondary_capture_retailer_image');

                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id . time() . "_sc" . '.' . $ext;

                                $file->move('uploads/retailers/secondary', $filename);

                                $objRetailerPrimaryInfo->secondary_capture_retailer_image = $filename;
                            }
                            if ($objRetailerPrimaryInfo->save()) {

                                $objRetailerPrimaryInfo->dob = $objRetailerPrimaryInfo->dob ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->dob)) : '';
                                $objRetailerPrimaryInfo->anniversay = $objRetailerPrimaryInfo->anniversay ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->anniversay)) : '';
                                $objRetailerPrimaryInfo->first_kid_birthday = $objRetailerPrimaryInfo->first_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->first_kid_birthday)) : '';
                                $objRetailerPrimaryInfo->second_kid_birthday = $objRetailerPrimaryInfo->second_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->second_kid_birthday)) : '';
                                $objRetailerPrimaryInfo->third_kid_birthday = $objRetailerPrimaryInfo->third_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->third_kid_birthday)) : '';
                                return Response()->json([
                                            'status' => 1,
                                            'message' => "Record updated",
                                            'data' => $objRetailerPrimaryInfo->toArray()
                                ]);
                            } else {
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Server side issue",
                                ]);
                            }
                        }
                    } else {
                        return Response()->json([
                                    'status' => 0,
                                    'message' => "Invalid user key"
                        ]);
                    }

                    //}
                }
                break;
            case 3: {
                    //if($_SERVER['REMOTE_ADDR']=='192.168.1.52')
                    // {
                    $validator = Validator::make($request->all(), [
                                'user_id' => 'required',
                                'user_key' => 'required',
                                'shop_type' => 'required',
                                'latitudes' => 'required',
                                'longitude' => 'required',
                                'shop_name' => 'required|string|max:200',
                                'shop_image' => 'required|image | mimes:jpeg,jpg,png,gif|max:10000'
                    ]);

                    if ($validator->fails()) {

                        $error_msg = json_encode([
                            'status' => 0,
                            'message' => $validator->messages()
                        ]);
                        $arrayMessages = json_decode($error_msg);
                        foreach ($arrayMessages as $keyVal => $value) {
                            if ($keyVal == 'message') {
                                foreach ($value as $messagesValues) {
                                    foreach ($messagesValues as $messagesValue) {
                                        return Response()->json([
                                                    'status' => 0,
                                                    'message' => $messagesValue
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                if($request->hasFile('shop_image')){
                    $fileExtension = $request->shop_image->getClientOriginalExtension();
                    $imageExtensions = ['jpg','png','jpeg','gif'];
                    if(!in_array($fileExtension,$imageExtensions))
                        return response()->json([
                            'status' => 0,
                            'message' => 'Invalid file type'
                        ]);
                }
                    $objUserkey = AgentsOtpModel::where(['agent_id' => $request->user_id])
                                    ->where(['token_key' => $request->user_key])->first();
                    if (isset($objUserkey->id)) {


                        // Must not already exist in the `email` column of `users` table


                        if ($id) {
                            $objRetailerPrimaryInfo = RetailersModel::where(['id' => $id])->where(['user_id' => $request->user_id])->first();
                            if (isset($objRetailerPrimaryInfo->id)) {
//                                 'latitudes'=>'required',
//                     'longitude'=>'required',
                                $objRetailerPrimaryInfo->shop_type = $request->shop_type;
                                $objRetailerPrimaryInfo->shop_name = $request->shop_name;
                                $objRetailerPrimaryInfo->latitudes = $request->latitudes;
                                $objRetailerPrimaryInfo->longitude = $request->longitude;
                                //php artisan migrate shop_address
                                $address = Helpers\Helpers::getaddress($request->latitudes, $request->longitude);
                                $cityState = Helpers\Helpers::getCityState($request->latitudes, $request->longitude);
                                $objRetailerPrimaryInfo->city = isset($cityState['city']) ? $cityState['city'] : '';
                                $objRetailerPrimaryInfo->state = isset($cityState['state']) ? $cityState['state'] : '';
                                $objRetailerPrimaryInfo->shop_address = $address ? $address : '';
                                $objRetailerPrimaryInfo->shop_image = '';


                                if ($request->hasfile('shop_image')) {

                                    $file = $request->file('shop_image');

                                    $ext = $file->getClientOriginalExtension();
                                    $filename = $request->user_id . time() . "_spi" . '.' . $ext;

                                    $file->move('uploads/retailers/shop_image', $filename);

                                    $objRetailerPrimaryInfo->shop_image = $filename;
                                }
                                if ($objRetailerPrimaryInfo->save()) {



                                    $objRetailerPrimaryInfo->dob = $objRetailerPrimaryInfo->dob ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->dob)) : '';
                                    $objRetailerPrimaryInfo->anniversay = $objRetailerPrimaryInfo->anniversay ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->anniversay)) : '';
                                    $objRetailerPrimaryInfo->first_kid_birthday = $objRetailerPrimaryInfo->first_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->first_kid_birthday)) : '';
                                    $objRetailerPrimaryInfo->second_kid_birthday = $objRetailerPrimaryInfo->second_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->second_kid_birthday)) : '';
                                    $objRetailerPrimaryInfo->third_kid_birthday = $objRetailerPrimaryInfo->third_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->third_kid_birthday)) : '';
                                    return Response()->json([
                                                'status' => 1,
                                                'message' => "Record updated",
                                                'data' => $objRetailerPrimaryInfo->toArray()
                                    ]);
                                } else {
                                    return Response()->json([
                                                'status' => 0,
                                                'message' => "Server side issue",
                                    ]);
                                }
                            }
                        } else {
                            $objRetailerPrimaryInfo = new RetailersModel();
                            $objRetailerPrimaryInfo->user_id = $request->user_id;
                            $objRetailerPrimaryInfo->shop_type = $request->shop_type ? $request->shop_type : 1;
                            $objRetailerPrimaryInfo->shop_name = $request->shop_name ? $request->shop_name : '';
                            $objRetailerPrimaryInfo->shop_address = $request->shop_address ? $request->shop_address : '';
                            $objRetailerPrimaryInfo->shop_image = '';

                            if ($request->hasfile('shop_image')) {

                                $file = $request->file('shop_image');

                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id . "_spi" . time() . '.' . $ext;

                                $file->move('uploads/retailers/shop_image', $filename);

                                $objRetailerPrimaryInfo->shop_image = $filename;
                            }
                            if ($objRetailerPrimaryInfo->save()) {

                                $objRetailerPrimaryInfo->dob = $objRetailerPrimaryInfo->dob ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->dob)) : '';
                                $objRetailerPrimaryInfo->anniversay = $objRetailerPrimaryInfo->anniversay ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->anniversay)) : '';
                                $objRetailerPrimaryInfo->first_kid_birthday = $objRetailerPrimaryInfo->first_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->first_kid_birthday)) : '';
                                $objRetailerPrimaryInfo->second_kid_birthday = $objRetailerPrimaryInfo->second_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->second_kid_birthday)) : '';
                                $objRetailerPrimaryInfo->third_kid_birthday = $objRetailerPrimaryInfo->third_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->third_kid_birthday)) : '';
                                return Response()->json([
                                            'status' => 1,
                                            'message' => "Record updated",
                                            'data' => $objRetailerPrimaryInfo->toArray()
                                ]);
                            } else {
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Server side issue",
                                ]);
                            }
                        }
                    } else {
                        return Response()->json([
                                    'status' => 0,
                                    'message' => "Invalid user key"
                        ]);
                    }
                    //}
                }
                break;
            case 4: {
//                           if($_SERVER['REMOTE_ADDR']=='192.168.1.52')
//            {
                    //echo  date("Y-m-d",strtotime($request->dob));die();
                    $validator = Validator::make($request->all(), [
                                'user_id' => 'required',
                                'user_key' => 'required',
                    ]);
                    if ($validator->fails()) {

                        $error_msg = json_encode([
                            'status' => 0,
                            'message' => $validator->messages()
                        ]);
                        $arrayMessages = json_decode($error_msg);
                        foreach ($arrayMessages as $keyVal => $value) {
                            if ($keyVal == 'message') {
                                foreach ($value as $messagesValues) {
                                    foreach ($messagesValues as $messagesValue) {
                                        return Response()->json([
                                                    'status' => 0,
                                                    'message' => $messagesValue
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                    $objUserkey = AgentsOtpModel::where(['agent_id' => $request->user_id])
                                    ->where(['token_key' => $request->user_key])->first();
                    if (isset($objUserkey->id)) {


                        // Must not already exist in the `email` column of `users` table


                        if ($id) {
                            $objRetailerPrimaryInfo = RetailersModel::where(['id' => $id])->where(['user_id' => $request->user_id])->first();
                            if (isset($objRetailerPrimaryInfo->id)) {
                                $objRetailerPrimaryInfo->dob = $request->dob ? date("Y-m-d", strtotime($request->dob)) : NULL;
                                $objRetailerPrimaryInfo->anniversay = $request->anniversay ? date("Y-m-d", strtotime($request->anniversay)) : NULL;
                                $objRetailerPrimaryInfo->first_kid_birthday = $request->first_kid_birthday ? date("Y-m-d", strtotime($request->first_kid_birthday)) : NULL;
                                $objRetailerPrimaryInfo->second_kid_birthday = $request->second_kid_birthday ? date("Y-m-d", strtotime($request->second_kid_birthday)) : NULL;
                                $objRetailerPrimaryInfo->third_kid_birthday = $request->third_kid_birthday ? date("Y-m-d", strtotime($request->third_kid_birthday)) : NULL;
                                if ($objRetailerPrimaryInfo->save()) {

                                    $objRetailerPrimaryInfo->dob = $objRetailerPrimaryInfo->dob ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->dob)) : '';
                                    $objRetailerPrimaryInfo->anniversay = $objRetailerPrimaryInfo->anniversay ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->anniversay)) : '';
                                    $objRetailerPrimaryInfo->first_kid_birthday = $objRetailerPrimaryInfo->first_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->first_kid_birthday)) : '';
                                    $objRetailerPrimaryInfo->second_kid_birthday = $objRetailerPrimaryInfo->second_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->second_kid_birthday)) : '';
                                    $objRetailerPrimaryInfo->third_kid_birthday = $objRetailerPrimaryInfo->third_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->third_kid_birthday)) : '';
                                    return Response()->json([
                                                'status' => 1,
                                                'message' => "Record updated",
                                                'data' => $objRetailerPrimaryInfo->toArray()
                                    ]);
                                } else {
                                    return Response()->json([
                                                'status' => 0,
                                                'message' => "Server side issue",
                                    ]);
                                }
                            }
                        } else {
                            return Response()->json([
                                        'status' => 0,
                                        'message' => "missing id",
                            ]);
                        }
                    } else {
                        return Response()->json([
                                    'status' => 0,
                                    'message' => "Invalid user key"
                        ]);
                    }
                    //  }
                }
                break;
            case 5: {
                    //
//                if($_SERVER['REMOTE_ADDR']=='192.168.1.52')
//                {

                    $validator = Validator::make($request->all(), [
                                'user_id' => 'required',
                                'user_key' => 'required',
                                'id' => 'required',
                                'establishment_name' => 'string|max:150',
                                'gst_number' => 'string|max:150',
                                'pan_number' => 'string|max:10',
                                'adhar_number' => 'string|max:13',
                                'gst_number_file' => 'image | mimes:jpeg,jpg,png,gif|max:10000',
                                'pan_number_file' => 'image | mimes:jpeg,jpg,png,gif|max:10000',
                                'adhar_number_file' => 'image | mimes:jpeg,jpg,png,gif|max:10000',
                    ]);

                    if ($validator->fails()) {

                        $error_msg = json_encode([
                            'status' => 0,
                            'message' => $validator->messages()
                        ]);
                        $arrayMessages = json_decode($error_msg);
                        foreach ($arrayMessages as $keyVal => $value) {
                            if ($keyVal == 'message') {
                                foreach ($value as $messagesValues) {
                                    foreach ($messagesValues as $messagesValue) {
                                        return Response()->json([
                                                    'status' => 0,
                                                    'message' => $messagesValue
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                    if($request->hasFile('gst_number_file')){
                        $fileExtension = $request->gst_number_file->getClientOriginalExtension();
                        $imageExtensions = ['jpg','png','jpeg','gif'];
                        if(!in_array($fileExtension,$imageExtensions))
                            return response()->json([
                                'status' => 0,
                                'message' => 'Invalid gst_number_file file type'
                            ]);
                    }
                    if($request->hasFile('pan_number_file')){
                        $fileExtension = $request->pan_number_file->getClientOriginalExtension();
                        $imageExtensions = ['jpg','png','jpeg','gif'];
                        if(!in_array($fileExtension,$imageExtensions))
                            return response()->json([
                                'status' => 0,
                                'message' => 'Invalid gst_number_file file type'
                            ]);
                    }
                    if($request->hasFile('adhar_number_file')){
                        $fileExtension = $request->adhar_number_file->getClientOriginalExtension();
                        $imageExtensions = ['jpg','png','jpeg','gif'];
                        if(!in_array($fileExtension,$imageExtensions))
                            return response()->json([
                                'status' => 0,
                                'message' => 'Invalid gst_number_file file type'
                            ]);
                    }
                    $objUserkey = AgentsOtpModel::where(['agent_id' => $request->user_id])
                                    ->where(['token_key' => $request->user_key])->first();
                    if (isset($objUserkey->id)) {


                        // Must not already exist in the `email` column of `users` table


                        if ($id) {
                            $objRetailerPrimaryInfo = RetailersModel::where(['id' => $id])->where(['user_id' => $request->user_id])->first();
                            if (isset($objRetailerPrimaryInfo->id)) {

                                $objRetailerPrimaryInfo->establishment_name = $request->establishment_name ? $request->establishment_name : '';
                                $objRetailerPrimaryInfo->gst_number = $request->gst_number ? $request->gst_number : '';
                                $objRetailerPrimaryInfo->pan_number = $request->pan_number ? $request->pan_number : '';
                                $objRetailerPrimaryInfo->adhar_number = $request->adhar_number ? $request->adhar_number : '';
                                //adhar_number
                                $objRetailerPrimaryInfo->gst_number_file = '';
                                $objRetailerPrimaryInfo->pan_number_file = '';
                                $objRetailerPrimaryInfo->adhar_number_file = '';

                                if ($request->hasfile('gst_number_file')) {

                                    $file = $request->file('gst_number_file');

                                    $ext = $file->getClientOriginalExtension();
                                    $filename = $request->user_id . time() . "_gnf" . '.' . $ext;

                                    $file->move('uploads/retailers/kyc', $filename);

                                    $objRetailerPrimaryInfo->gst_number_file = $filename;
                                }
                                if ($request->hasfile('pan_number_file')) {

                                    $file = $request->file('pan_number_file');

                                    $ext = $file->getClientOriginalExtension();
                                    $filename = $request->user_id . time() . "_pnf" . '.' . $ext;

                                    $file->move('uploads/retailers/kyc', $filename);

                                    $objRetailerPrimaryInfo->pan_number_file = $filename;
                                }
                                if ($request->hasfile('adhar_number_file')) {

                                    $file = $request->file('adhar_number_file');

                                    $ext = $file->getClientOriginalExtension();
                                    $filename = $request->user_id . time() . "_anf" . '.' . $ext;

                                    $file->move('uploads/retailers/kyc', $filename);

                                    $objRetailerPrimaryInfo->adhar_number_file = $filename;
                                }
                                if ($objRetailerPrimaryInfo->save()) {

                                    $objRetailerPrimaryInfo->dob = $objRetailerPrimaryInfo->dob ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->dob)) : '';
                                    $objRetailerPrimaryInfo->anniversay = $objRetailerPrimaryInfo->anniversay ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->anniversay)) : '';
                                    $objRetailerPrimaryInfo->first_kid_birthday = $objRetailerPrimaryInfo->first_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->first_kid_birthday)) : '';
                                    $objRetailerPrimaryInfo->second_kid_birthday = $objRetailerPrimaryInfo->second_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->second_kid_birthday)) : '';
                                    $objRetailerPrimaryInfo->third_kid_birthday = $objRetailerPrimaryInfo->third_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->third_kid_birthday)) : '';
                                    return Response()->json([
                                                'status' => 1,
                                                'message' => "Record updated",
                                                'data' => $objRetailerPrimaryInfo->toArray()
                                    ]);
                                } else {
                                    return Response()->json([
                                                'status' => 0,
                                                'message' => "Server side issue",
                                    ]);
                                }
                            }
                        }
                    } else {
                        return Response()->json([
                                    'status' => 0,
                                    'message' => "Invalid user key"
                        ]);
                    }

                    //}
                }

                break;
            case 6: {


                    $validator = Validator::make($request->all(), [
                                'user_id' => 'required',
                                'user_key' => 'required',
                                'id' => 'required',
                                'scan_code' => 'required|unique:retailers'
                    ]);

                    if ($validator->fails()) {

                        return response()->json([
                            'status' => 0,
                            'message' => $validator->errors()->first()
                        ]);
                    }
                     if (isset($request->scan_code)) {
                                        if (!Helpers\Helpers::barcodeScanner($request->scan_code)) {
                                            return Response()->json([
                                                        'status' => 0,
                                                        'message' => "Invalid barcode",
                                            ]);
                                        }

                                    }
                                    else if(isset($request->ds_group)) {
                                        if (!Helpers\Helpers::barcodeScanner($request->ds_group)) {
                                            return Response()->json([
                                                        'status' => 0,
                                                        'message' => "Invalid barcode",
                                            ]);
                                        }

                                    }
             
                    if ($request->paytm or 
                        $request->mobiqwik or 
                        $request->gpay or 
                        $request->phone_pe or 
                        $request->upi or 
                        $request->bharat_qr ) {
                            if (Helpers\Helpers::barcodeScanner($request->paytm) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid paytm barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->mobiqwik) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid mobiqwik barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->gpay) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid gpay barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->phone_pe) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid phone_pe barcode",
                                ]);
                            }
                             if (Helpers\Helpers::barcodeScanner($request->upi) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid upi barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->bharat_qr) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid bharat_qr barcode",
                                ]);
                            }
                            //

                    }
                    $objUserkey = AgentsOtpModel::where(['agent_id' => $request->user_id])
                                    ->where(['token_key' => $request->user_key])->first();
                    if ($id) {
                        $objRetailerPrimaryInfo = RetailersModel::where(['id' => $id])->where(['user_id' => $request->user_id])->first();
                        if (isset($objRetailerPrimaryInfo->id)) {




                            $objRetailerPrimaryInfo->scan_code = $request->scan_code ? $request->scan_code : '';
                            $objRetailerPrimaryInfo->is_fill = 1;
                            if ($objRetailerPrimaryInfo->save()) {
                                $messageBody = "Dear ".$objRetailerPrimaryInfo->full_name. ", you are successfully onboarded to O2R platform.";
                                $this->sendSms($messageBody,$objRetailerPrimaryInfo->id);
                                //Start scan code validation 20200307
                                if ($request->scan_code or 
                                        $request->paytm or 
                                        $request->mobiqwik or 
                                        $request->gpay or 
                                        $request->phone_pe or 
                                        $request->upi or 
                                        $request->bharat_qr or 
                                        $request->ds_group) {
                                    if (isset($request->scan_code)) {
                                        if (!Helpers\Helpers::barcodeScanner($request->scan_code)) {
                                            return Response()->json([
                                                        'status' => 0,
                                                        'message' => "Invalid barcode",
                                            ]);
                                        }

                                    }
                                    else if(isset($request->ds_group)) {
                                        if (!Helpers\Helpers::barcodeScanner($request->ds_group)) {
                                            return Response()->json([
                                                        'status' => 0,
                                                        'message' => "Invalid barcode",
                                            ]);
                                        }

                                    }
                                    // else if(isset($request->paytm)) {
                                    //     if (!Helpers\Helpers::barcodeScanner($request->paytm)) {
                                    //         return Response()->json([
                                    //                     'status' => 0,
                                    //                     'message' => "Invalid barcode",
                                    //         ]);
                                    //     }

                                    // }
                                    // else if(isset($request->mobiqwik)) {
                                    //     if (!Helpers\Helpers::barcodeScanner($request->mobiqwik)) {
                                    //         return Response()->json([
                                    //                     'status' => 0,
                                    //                     'message' => "Invalid barcode",
                                    //         ]);
                                    //     }

                                    // }
                                    // else if(isset($request->gpay)) {
                                    //     if (!Helpers\Helpers::barcodeScanner($request->gpay)) {
                                    //         return Response()->json([
                                    //                     'status' => 0,
                                    //                     'message' => "Invalid barcode",
                                    //         ]);
                                    //     }

                                    // }
                                    // else if(isset($request->phone_pe)) {
                                    //     if (!Helpers\Helpers::barcodeScanner($request->phone_pe)) {
                                    //         return Response()->json([
                                    //                     'status' => 0,
                                    //                     'message' => "Invalid barcode",
                                    //         ]);
                                    //     }

                                    // }
                                    // else if(isset($request->upi)) {
                                    //     if (!Helpers\Helpers::barcodeScanner($request->upi)) {
                                    //         return Response()->json([
                                    //                     'status' => 0,
                                    //                     'message' => "Invalid barcode",
                                    //         ]);
                                    //     }

                                    // }
                                    //  else if(isset($request->bharat_qr)) {
                                    //     if (!Helpers\Helpers::barcodeScanner($request->bharat_qr)) {
                                    //         return Response()->json([
                                    //                     'status' => 0,
                                    //                     'message' => "Invalid barcode",
                                    //         ]);
                                    //     }

                                    // }
                                    
                                    ////End scan code validation 20200307
                                    
                                    $this->retailersQrCode($request, $objRetailerPrimaryInfo->id, 1);
                                }
                                $geolocation = new \Api\Models\AgentsStartMyDayModel;
                                $geolocation->user_id = $request->user_id;
                                $geolocation->latitudes = $objRetailerPrimaryInfo->latitudes;
                                $geolocation->longitude = $objRetailerPrimaryInfo->longitude;
                                $geolocation->type = 'RETAILER';
                                $geolocation->address = $objRetailerPrimaryInfo->shop_address;
                                $geolocation->save();


                                $objRetailerPrimaryInfo->dob = $objRetailerPrimaryInfo->dob ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->dob)) : '';
                                $objRetailerPrimaryInfo->anniversay = $objRetailerPrimaryInfo->anniversay ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->anniversay)) : '';
                                $objRetailerPrimaryInfo->first_kid_birthday = $objRetailerPrimaryInfo->first_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->first_kid_birthday)) : '';
                                $objRetailerPrimaryInfo->second_kid_birthday = $objRetailerPrimaryInfo->second_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->second_kid_birthday)) : '';
                                $objRetailerPrimaryInfo->third_kid_birthday = $objRetailerPrimaryInfo->third_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->third_kid_birthday)) : '';
//                                $this->retailersQrCode($request, $objRetailerPrimaryInfo->id);
                                $this->mapRetailerToHub($objRetailerPrimaryInfo);

                                return Response()->json([
                                            'status' => 1,
                                            'message' => "Record updated",
                                            'data' => $objRetailerPrimaryInfo->toArray()
                                ]);
                            } else {
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Server side issue",
                                ]);
                            }
                        }
                    }
                }

                break;
            case 7: {
                    //used for all operation
                    $validator = Validator::make($request->all(), [
                                'user_id' => 'required',
                                'user_key' => 'required',
                                'id' => 'required',
                                //case 1
                                'full_name' => 'required|string|max:150',
                                'primary_mobile_number' => 'required|numeric',
                                //  'alternative_mobile_number'=>'numeric',
                                // 'whats_app_number'=>'numeric',
                                'capture_retailer_image' => 'image | mimes:jpeg,jpg,png,gif|max:10000',
                                //case 2
                                // 'secondary_full_name'=>'string:max:200',
                                //  'secondary_mobile_number'=>'numeric',
                                'secondary_capture_retailer_image' => 'image | mimes:jpeg,jpg,png,gif|max:10000',
                                //case 3
                                'shop_type' => 'required',
                                'latitudes' => 'required',
                                'longitude' => 'required',
                                'shop_name' => 'required|string|max:200',
                                'shop_image' => 'image | mimes:jpeg,jpg,png,gif|max:10000',
                                //case 4
                                //case 5
                                'establishment_name' => 'string|max:150',
                                'gst_number' => 'string|max:150',
                                'pan_number' => 'string|max:10',
                                'adhar_number' => 'string|max:13',
                                'gst_number_file' => 'image | mimes:jpeg,jpg,png,gif|max:10000',
                                'pan_number_file' => 'image | mimes:jpeg,jpg,png,gif|max:10000',
                                'adhar_number_file' => 'image | mimes:jpeg,jpg,png,gif|max:10000',
                                    //case 6
                    ]);

                    if ($validator->fails()) {
                        return response()->json([
                            'status' => 0,
                            'message' => $validator->errors()->first()
                        ]);

                    }
                if($request->hasFile('gst_number_file')){
                    $fileExtension = $request->gst_number_file->getClientOriginalExtension();
                    $imageExtensions = ['jpg','png','jpeg','gif'];
                    if(!in_array($fileExtension,$imageExtensions))
                        return response()->json([
                            'status' => 0,
                            'message' => 'Invalid gst_number_file file type'
                        ]);
                }
                if($request->hasFile('pan_number_file')){
                    $fileExtension = $request->pan_number_file->getClientOriginalExtension();
                    $imageExtensions = ['jpg','png','jpeg','gif'];
                    if(!in_array($fileExtension,$imageExtensions))
                        return response()->json([
                            'status' => 0,
                            'message' => 'Invalid gst_number_file file type'
                        ]);
                }
                if($request->hasFile('adhar_number_file')){
                    $fileExtension = $request->adhar_number_file->getClientOriginalExtension();
                    $imageExtensions = ['jpg','png','jpeg','gif'];
                    if(!in_array($fileExtension,$imageExtensions))
                        return response()->json([
                            'status' => 0,
                            'message' => 'Invalid gst_number_file file type'
                        ]);
                }
                    $objUserkey = AgentsOtpModel::where(['agent_id' => $request->user_id])
                                    ->where(['token_key' => $request->user_key])->first();
                    if (isset($objUserkey->id)) {
                        $objRetailerPrimaryInfo = RetailersModel::where(['id' => $id])->first();
                        if (isset($objRetailerPrimaryInfo->id)) {
                            //case 1
                            $resultObjMobileExist = RetailersModel::where(["is_fill" => 1])->where("id", '!=', $id)
                                            ->where(['primary_mobile_number' => $request->primary_mobile_number])->where('primary_mobile_number', '!=', "")->first();
                            if (isset($resultObjMobileExist->id)) {
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Primary Mobile Number already exist",
                                ]);
                            }
                            $objRetailerPrimaryInfo->full_name = $request->full_name ? $request->full_name : $objRetailerPrimaryInfo->full_name;
                            $objRetailerPrimaryInfo->primary_mobile_number = $request->primary_mobile_number ? $request->primary_mobile_number : $objRetailerPrimaryInfo->primary_mobile_number;
                            $objRetailerPrimaryInfo->alternative_mobile_number = $request->alternative_mobile_number ? $request->alternative_mobile_number : $objRetailerPrimaryInfo->alternative_mobile_number;
                            $objRetailerPrimaryInfo->whats_app_number = $request->whats_app_number ? $request->whats_app_number : $objRetailerPrimaryInfo->whats_app_number;
                            $objRetailerPrimaryInfo->primary_mobile_number = $request->primary_mobile_number ? $request->primary_mobile_number : $objRetailerPrimaryInfo->primary_mobile_number;
                            $objRetailerPrimaryInfo->capture_retailer_image = $objRetailerPrimaryInfo->capture_retailer_image ? $objRetailerPrimaryInfo->capture_retailer_image : '';
                            $objRetailerPrimaryInfo->p_device_type = $request->p_device_type ? $request->p_device_type : 0;
                            if ($request->hasfile('capture_retailer_image')) {

                                $file = $request->file('capture_retailer_image');

                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id . time() . "_cif" . '.' . $ext;

                                $file->move('uploads/retailers/primary', $filename);

                                $objRetailerPrimaryInfo->capture_retailer_image = $filename;
                            }
                            //case 2
                            $resultObjMobileExist = RetailersModel::where(["is_fill" => 1])->where("id", '!=', $id)
                                            ->where(['secondary_mobile_number' => $request->secondary_mobile_number])->where('secondary_mobile_number', '!=', "")->first();
                            if (isset($resultObjMobileExist->id)) {
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Secondary Mobile Number already exist",
                                ]);
                            }
                            $objRetailerPrimaryInfo->secondary_full_name = $request->secondary_full_name ? $request->secondary_full_name : $objRetailerPrimaryInfo->secondary_full_name;
                            $objRetailerPrimaryInfo->secondary_mobile_number = $request->secondary_mobile_number ? $request->secondary_mobile_number : $objRetailerPrimaryInfo->secondary_mobile_number;
                            $objRetailerPrimaryInfo->secondary_capture_retailer_image = $objRetailerPrimaryInfo->secondary_capture_retailer_image ? $objRetailerPrimaryInfo->secondary_capture_retailer_image : '';

                            if ($request->hasfile('secondary_capture_retailer_image')) {

                                $file = $request->file('secondary_capture_retailer_image');

                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id . time() . "_sc" . '.' . $ext;

                                $file->move('uploads/retailers/secondary', $filename);

                                $objRetailerPrimaryInfo->secondary_capture_retailer_image = $filename;
                            }
                            //case 3
                            $objRetailerPrimaryInfo->shop_type = $request->shop_type ? $request->shop_type : $objRetailerPrimaryInfo->shop_type;
                            $objRetailerPrimaryInfo->shop_name = $request->shop_name ? $request->shop_name : $objRetailerPrimaryInfo->shop_name;
                            $objRetailerPrimaryInfo->latitudes = $request->latitudes ? $request->latitudes : $objRetailerPrimaryInfo->latitudes;
                            $objRetailerPrimaryInfo->longitude = $request->longitude ? $request->longitude : $objRetailerPrimaryInfo->longitude;
                            //php artisan migrate shop_address
                            $address = Helpers\Helpers::getaddress($request->latitudes, $request->longitude);

                            $cityState = Helpers\Helpers::getCityState($request->latitudes, $request->longitude);
                            $objRetailerPrimaryInfo->city = isset($cityState['city']) ? $cityState['city'] : '';
                            $objRetailerPrimaryInfo->state = isset($cityState['state']) ? $cityState['state'] : '';
                            $objRetailerPrimaryInfo->shop_image = $objRetailerPrimaryInfo->shop_image ? $objRetailerPrimaryInfo->shop_image : '';
                            $objRetailerPrimaryInfo->shop_address = $request->shop_address;

                            if ($request->hasfile('shop_image')) {

                                $file = $request->file('shop_image');

                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id . time() . "_si" . '.' . $ext;

                                $file->move('uploads/retailers/shop_image', $filename);

                                $objRetailerPrimaryInfo->shop_image = $filename;
                            }
                            //case 4
                            $objRetailerPrimaryInfo->dob = $request->dob ? date("Y-m-d", strtotime($request->dob)) : $objRetailerPrimaryInfo->dob;
                            $objRetailerPrimaryInfo->anniversay = $request->anniversay ? date("Y-m-d", strtotime($request->anniversay)) : $objRetailerPrimaryInfo->anniversay;
                            $objRetailerPrimaryInfo->first_kid_birthday = $request->first_kid_birthday ? date("Y-m-d", strtotime($request->first_kid_birthday)) : $objRetailerPrimaryInfo->first_kid_birthday;
                            $objRetailerPrimaryInfo->second_kid_birthday = $request->second_kid_birthday ? date("Y-m-d", strtotime($request->second_kid_birthday)) : $objRetailerPrimaryInfo->second_kid_birthday;
                            $objRetailerPrimaryInfo->third_kid_birthday = $request->third_kid_birthday ? date("Y-m-d", strtotime($request->third_kid_birthday)) : $objRetailerPrimaryInfo->third_kid_birthday;
                            //case 5
                            $objRetailerPrimaryInfo->establishment_name = $request->establishment_name ? $request->establishment_name : $objRetailerPrimaryInfo->establishment_name;
                            $objRetailerPrimaryInfo->gst_number = $request->gst_number ? $request->gst_number : $objRetailerPrimaryInfo->gst_number;
                            $objRetailerPrimaryInfo->pan_number = $request->pan_number ? $request->pan_number : $objRetailerPrimaryInfo->pan_number;
                            $objRetailerPrimaryInfo->adhar_number = $request->adhar_number ? $request->adhar_number : $objRetailerPrimaryInfo->adhar_number;
                            //adhar_number
                            $objRetailerPrimaryInfo->gst_number_file = $objRetailerPrimaryInfo->gst_number_file ? $objRetailerPrimaryInfo->gst_number_file : '';
                            $objRetailerPrimaryInfo->pan_number_file = $objRetailerPrimaryInfo->pan_number_file ? $objRetailerPrimaryInfo->pan_number_file : '';
                            $objRetailerPrimaryInfo->adhar_number_file = $objRetailerPrimaryInfo->adhar_number_file ? $objRetailerPrimaryInfo->adhar_number_file : '';

                            if ($request->hasfile('gst_number_file')) {

                                $file = $request->file('gst_number_file');

                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id . time() . "_gnf" . '.' . $ext;

                                $file->move('uploads/retailers/kyc', $filename);

                                $objRetailerPrimaryInfo->gst_number_file = $filename;
                            }
                            if ($request->hasfile('pan_number_file')) {

                                $file = $request->file('pan_number_file');

                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id . time() . "_pnf" . '.' . $ext;

                                $file->move('uploads/retailers/kyc', $filename);

                                $objRetailerPrimaryInfo->pan_number_file = $filename;
                            }
                            if ($request->hasfile('adhar_number_file')) {

                                $file = $request->file('adhar_number_file');

                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id . time() . "_anf" . '.' . $ext;

                                $file->move('uploads/retailers/kyc', $filename);

                                $objRetailerPrimaryInfo->adhar_number_file = $filename;
                            }
                            //case 6
                            if (isset($request->scan_code)) {
                                if (!Helpers\Helpers::barcodeScanner($request->scan_code)) {
                                    return Response()->json([
                                                'status' => 0,
                                                'message' => "Invalid barcode",
                                    ]);
                                }
                                $objRetailerScanInfo = RetailersModel::where('id', '!=', $request->id)->where(['scan_code' => $request->scan_code])->first();
                                if (isset($objRetailerScanInfo->id)) {
                                    return Response()->json([
                                                'status' => 0,
                                                'message' => "Qr code already exist",
                                    ]);
                                }
                            }

                    if ($request->paytm or 
                        $request->mobiqwik or 
                        $request->gpay or 
                        $request->phone_pe or 
                        $request->upi or 
                        $request->bharat_qr ) {
                          if (Helpers\Helpers::barcodeScanner($request->paytm) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid paytm barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->mobiqwik) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid mobiqwik barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->gpay) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid gpay barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->phone_pe) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid phone_pe barcode",
                                ]);
                            }
                             if (Helpers\Helpers::barcodeScanner($request->upi) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid upi barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->bharat_qr) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid bharat_qr barcode",
                                ]);
                            }

                    }
                            $objRetailerPrimaryInfo->scan_code = $request->scan_code ? $request->scan_code : $objRetailerPrimaryInfo->scan_code;
                            if ($objRetailerPrimaryInfo->save()) {

                                if ($request->scan_code or $request->paytm or $request->mobiqwik or $request->gpay or $request->phone_pe or $request->upi or $request->bharat_qr or $request->ds_group) {
                                    $this->retailersQrCode($request, $objRetailerPrimaryInfo->id, 1);
                                }
                                $objRetailerPrimaryInfo->dob = $objRetailerPrimaryInfo->dob ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->dob)) : '';
                                $objRetailerPrimaryInfo->anniversay = $objRetailerPrimaryInfo->anniversay ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->anniversay)) : '';
                                $objRetailerPrimaryInfo->first_kid_birthday = $objRetailerPrimaryInfo->first_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->first_kid_birthday)) : '';
                                $objRetailerPrimaryInfo->second_kid_birthday = $objRetailerPrimaryInfo->second_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->second_kid_birthday)) : '';
                                $objRetailerPrimaryInfo->third_kid_birthday = $objRetailerPrimaryInfo->third_kid_birthday ? date("d-m-Y", strtotime($objRetailerPrimaryInfo->third_kid_birthday)) : '';
                                return Response()->json([
                                            'status' => 1,
                                            'message' => "Record updated",
                                            'data' => $objRetailerPrimaryInfo->toArray()
                                ]);
                            } else {
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Server side issue",
                                ]);
                            }
                        } else {
                            return Response()->json([
                                        'status' => 0,
                                        'message' => "Id not found",
                            ]);
                        }
                    } else {
                        return Response()->json([
                                    'status' => 0,
                                    'message' => "Invalid user key"
                        ]);
                    }
                }
            
                break;
            case 8: {
                 $validator = Validator::make($request->all(), [
                                'user_id' => 'required',
                                'user_key' => 'required',
                               
                                //case 1
                               
                                    //case 6
                    ]);

                    if ($validator->fails()) {
                        return response()->json([
                            'status' => 0,
                            'message' => $validator->errors()->first()
                        ]);

                    }
                $objUserkey = AgentsOtpModel::where(['agent_id' => $request->user_id])
                                    ->where(['token_key' => $request->user_key])->first();
                    if (isset($objUserkey->id)){     
                if ($request->paytm or 
                        $request->mobiqwik or 
                        $request->gpay or 
                        $request->phone_pe or 
                        $request->upi or 
                        $request->bharat_qr ) {
                          if (Helpers\Helpers::barcodeScanner($request->paytm) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid paytm barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->mobiqwik) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid mobiqwik barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->gpay) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid gpay barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->phone_pe) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid phone_pe barcode",
                                ]);
                            }
                             if (Helpers\Helpers::barcodeScanner($request->upi) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid upi barcode",
                                ]);
                            }
                            if (Helpers\Helpers::barcodeScanner($request->bharat_qr) 

                                    ) {
                                
                                return Response()->json([
                                            'status' => 0,
                                            'message' => "Invalid bharat_qr barcode",
                                ]);
                            }

                    }
                    return Response()->json([
                                            'status' => 1,
                                            'message' => "bar code valid",
                                ]);
            }else {
                return Response()->json([
                                    'status' => 0,
                                    'message' => "Invalid user key"
                        ]);
            }
            }
            break;
            default : {
                    return Response()->json([
                                'status' => 0,
                                'message' => "Invalid Operation type 1 for primary,2 for secondry,3 for shop detail,4 for additional,5 for kyc ,6 for scan code"
                    ]);
                }
        }
    }

    public function show(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'user_key' => 'required',
        ]);
        if ($validator->fails()) {

            return response()->json([
                'status' => 0,
                'message' => $validator->errors()->first()
            ]);

        }
        $objUserkey = AgentsOtpModel::where(['agent_id' => $request->user_id])
                        ->where(['token_key' => $request->user_key])->first();
        if (isset($objUserkey->id)) {


            $result_list = RetailersModel::where(['is_fill' => 1])->where(['id' => $request->id])->first();
            $array_retailer = [];
            $image_path = config('app.url') . '/uploads/retailers/';

            //$lat= 26.754347; //latitude
            //$lng= 81.001640; //longitude
            $address = '';
            if (isset($result_list->latitudes) && isset($result_list->longitude))
                $address = Helpers\Helpers::getaddress($result_list->latitudes, $result_list->longitude);
            $qr_code = $this->getRetailersQrCode($request->id);
            $primary_info = array('full_name' => $result_list->full_name ? $result_list->full_name : '',
                'primary_mobile_number' => $result_list->primary_mobile_number ? $result_list->primary_mobile_number : 0,
                'alternative_mobile_number' => $result_list->alternative_mobile_number ? $result_list->alternative_mobile_number : 0,
                'whats_app_number' => $result_list->whats_app_number ? $result_list->whats_app_number : 0,
                'p_device_type' => $result_list->p_device_type ? $result_list->p_device_type : '0',
                'capture_retailer_image' => $result_list->capture_retailer_image ? $image_path . "primary/" . $result_list->capture_retailer_image : '',
            );
            $secondary_info = array(
                'secondary_full_name' => $result_list->secondary_full_name ? $result_list->secondary_full_name : '',
                'secondary_mobile_number' => $result_list->secondary_mobile_number ? $result_list->secondary_mobile_number : 0,
                'secondary_capture_retailer_image' => $result_list->secondary_capture_retailer_image ? $image_path . "secondary/" . $result_list->secondary_capture_retailer_image : '',
            );

            $shop_info = array(
                'shop_image' => $result_list->shop_image ? $image_path . "shop_image/" . $result_list->shop_image : '',
                'shop_type' => $result_list->shop_type,
                'shop_name' => $result_list->shop_name,
                'latitudes' => $result_list->latitudes,
                'longitude' => $result_list->longitude,
                'shop_address' => $address
            );
            $additional = array(
                'dob' => $result_list->dob ? date('d-m-Y', strtotime($result_list->dob)) : '',
                'anniversay' => $result_list->anniversay ? date('d-m-Y', strtotime($result_list->anniversay)) : '',
                'first_kid_birthday' => $result_list
                ->first_kid_birthday ? date('d-m-Y', strtotime($result_list->first_kid_birthday)) : '',
                'second_kid_birthday' => $result_list
                ->second_kid_birthday ? date('d-m-Y', strtotime($result_list->second_kid_birthday)) : '',
                'third_kid_birthday' => $result_list
                ->third_kid_birthday ? date('d-m-Y', strtotime($result_list->third_kid_birthday)) : '',
                'establishment_name' => $result_list->establishment_name ? $result_list->establishment_name : ''
            );
            $kyc = array(
                'establishment_name' => $result_list->establishment_name ? $result_list->establishment_name : '',
                'gst_number' => $result_list->gst_number ? $result_list->gst_number : '',
                'pan_number' => $result_list
                ->pan_number ? $result_list->pan_number : '',
                'adhar_number' => $result_list
                ->adhar_number ? $result_list->adhar_number : '',
                'gst_number_file' => $result_list
                ->gst_number_file ? $image_path . "kyc/" . $result_list->gst_number_file : '',
                'pan_number_file' => $result_list
                ->pan_number_file ? $image_path . "kyc/" . $result_list->pan_number_file : '',
                'adhar_number_file' => $result_list
                ->pan_number_file ? $image_path . "kyc/" . $result_list->adhar_number_file : '',
            );
            $qr_detail = array(
                'scan_code' => $result_list
                ->scan_code ? $result_list->scan_code : '', 'qr_code' => $qr_code
            );
            $shopType = array(
                array('id' => '1', 'value' => 'General A'),
                array('id' =>  '2','value' => 'General B' ),
                array('id' =>  '3','value' => 'General C'),
                array('id' =>  '4','value' => 'Chemist A'),
                array('id' =>  '5','value' => 'Chemist B'),
                array('id' =>  '6','value' => 'Chemist C'),
                array('id' =>  '7','value' => 'Cosmetic'),
                array('id' =>  '8','value' => 'Paan A' ),
                array('id' =>  '9','value' => 'Paan B' ),
                array('id' => '10','value' => 'Paan C')

            );
            $memberType = array(

                array( 'id'=>'1','value' =>'Prime'),
                array( 'id'=>'2','value' => 'Non Prime'),
                array( 'id'=>'3','value' => 'Regular'),
            );

            return Response()->json([
                        'status' => 1,
                        //'data'  =>$result_list,
                        'primary_info' => $primary_info,
                        'secondary_info' => $secondary_info,
                        'shop_info' => $shop_info,
                        'additional' => $additional,
                        'kyc' => $kyc,
                        'qr_detail' => $qr_detail,
                        'shop_type' => $shopType,
                        'member_type' => $memberType
            ]);
        }else {
            return Response()->json([
                        'status' => 0,
                        'message' => "Invalid user key"
            ]);
        }
    }

    public function searchRetailers(Request $request) {


        $searchkey = $request->searchtxt;
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'user_key' => 'required',
        ]);

        if ($validator->fails()) {

            return Response()->json([
                'status' => 0,
                'message' => $validator->errors()->first(),
            ]);

        }
        $objUserkey = AgentsOtpModel::where(['agent_id' => $request->user_id])
                        ->where(['token_key' => $request->user_key])->first();
        if (isset($objUserkey->id)) {
            $shopType = \App\ShopType::where('name','LIKE','%'.$searchkey.'%')->where('status',0)->get()->toArray();
            if(!empty($shopType)){
                $ids = array_column($shopType,'id');
            }else{
                $ids = array();
            }
            $result_list = RetailersModel::select('id', 'user_id', 'shop_name', 'shop_address', 'full_name', 'primary_mobile_number', 'latitudes', 'longitude', 'shop_image')
                            ->where(['is_fill' => 1])
                            ->where(function ($query) use($searchkey,$ids) {

                        $query->Where('shop_name', 'like', '%' . $searchkey . '%')
                                ->orWhere('full_name','like','%' . $searchkey . '%')
                                ->orWhere('primary_mobile_number','like', '%' . $searchkey . '%')
                                ->orWhere('secondary_mobile_number', 'like', '%' . $searchkey . '%')
                                ->orWhere('scan_code', 'like', '%' . $searchkey . '%')
                                ->orWhereIn('shop_type',$ids)
                                ->orWhere('shop_address', 'like', '%' . $searchkey . '%');
                    })->orderBy('id', "DESC")->paginate(20);
            $array_retailer = [];
            $image_path = config('app.url') . '/uploads/retailers/shop_image/';
            foreach ($result_list as $retailer_val) {
                //$lat= 26.754347; //latitude
                //$lng= 81.001640; //longitude
                $address = '';
                if (isset($retailer_val->latitudes) && isset($retailer_val->longitude))
                    $address = Helpers\Helpers::getaddress($retailer_val->latitudes, $retailer_val->longitude);



                $array_retailer[] = array('id' => $retailer_val->id, 'user_id' => $retailer_val->user_id,
                    'shop_name' => $retailer_val->shop_name,
                    'shop_address' => $address,
                    'full_name' => $retailer_val->full_name, 'primary_mobile_number' => $retailer_val->primary_mobile_number ? $retailer_val->primary_mobile_number : 0,
                    'shop_image' => $retailer_val->shop_image ? $image_path . $retailer_val->shop_image : ''
                );
            }
            if ($result_list->lastPage()) {
                return Response()->json([
                            'status' => 1,
                            'last_page' => $result_list->lastPage(),
                            'data' => $array_retailer,
                ]);
            } else {
                return Response()->json([
                            'status' => 0,
                            'message' => "record not found"
                ]);
            }
        } else {
            return Response()->json([
                        'status' => 0,
                        'message' => "Invalid user key"
            ]);
        }
    }

    public function retailersQrCode($data_fill, $id, $flage = 0) {
        if (isset($data_fill->scan_code)) {
                                        if (!Helpers\Helpers::barcodeScanner($data_fill->scan_code)) {
                                            return Response()->json([
                                                        'status' => 0,
                                                        'message' => "Invalid barcode",
                                            ]);die();
                                        }

                                    }
                                    else if(isset($data_fill->ds_group)) {
                                        if (!Helpers\Helpers::barcodeScanner($data_fill->ds_group)) {
                                            return Response()->json([
                                                        'status' => 0,
                                                        'message' => "Invalid barcode",
                                            ]);
                                        }

                                    }
        $obj = QrCodeModel::where('user_id', '=', $id)->first();
        if (isset($obj->id)) {
            $obj->paytm = $data_fill->paytm;
            $obj->mobiqwik = $data_fill->mobiqwik;
            $obj->phone_pe = $data_fill->phone_pe;
            $obj->upi = $data_fill->upi;
            $obj->bharat_qr = $data_fill->bharat_qr;
            $obj->gpay = $data_fill->gpay;
            $obj->ds_group = $data_fill->scan_code;
        } else {
            $obj = new QrCodeModel();
            $obj->paytm = $data_fill->paytm;
            //retailer_id
            $obj->user_id = $id;
            $obj->mobiqwik = $data_fill->mobiqwik;
            $obj->phone_pe = $data_fill->phone_pe;
            $obj->upi = $data_fill->upi;
            $obj->bharat_qr = $data_fill->bharat_qr;
            $obj->ds_group = $data_fill->scan_code;
            $obj->gpay = $data_fill->gpay;
        }
        if ($obj->save() && $flage == 1) {
            $logObj = new QrCodeLogModel();
            $logObj->user_id = $obj->id;
            $logObj->paytm = $obj->paytm;
            $logObj->mobiqwik = $obj->mobiqwik;
            $logObj->phone_pe = $obj->phone_pe;
            $logObj->upi = $obj->upi;
            $logObj->bharat_qr = $obj->bharat_qr;
            $logObj->ds_group = $obj->ds_group;
            $logObj->gpay = $obj->gpay;
            if ($logObj->save()) {
                return $obj->id;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function getRetailersQrCode($user_id) {
        return QrCodeModel::where('user_id', '=', $user_id)->get()->toArray();
    }

    public function mapRetailerToHub($retaiInfo) {
//            print_r();exit;
        $map = MapDistributor::where('retailer_id', $retaiInfo->id)->get();
//        print_r($map);exit;
        if (count($map) > 0) {
            return false;
        } else {
//            echo "iii";die();
            $getRetailer = RetailersModel::where('id', $retaiInfo->id)->first();
            if ($getRetailer->city != '') {
                $findCityid = City::where('city', $retaiInfo->city)->first();
//               print_r($findCityid);exit;
                if ($findCityid) {
                    $findDis = Distributor::where('city', $findCityid->id)->first();
//                    print_r($findDis);exit;
                    if ($findDis) {
                        $mapping = MapDistributor::create([
                                    'retailer_id' => $getRetailer->id,
                                    'distributor_id' => $findDis->id,
                        ]);
                        if ($mapping) {
                            return true;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

}
