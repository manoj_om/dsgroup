<?php

namespace Api\Http\Controllers;

use App\Http\Controllers\Controllers;

use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Api\Models\ApiModel;
use Api\Models\AgentsStartMyDayModel;
use Illuminate\Support\Facades\DB;
use Api\Models\AgentsOtpModel;
use App\Helpers;
use Api\Models\AgentsDiscoverPlacesModel;
use App;

use Illuminate\Routing\Controller as BaseController;
class AgentsDiscoverPlaceController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'user_id' =>'required',
                    'user_key' =>'required',
                                        
                    ]);
         if ($validator->fails()) {
                  
            $error_msg= json_encode([
                 'status' => 0,
                 'message' => $validator->messages()
             ]);
             $arrayMessages=json_decode($error_msg);
            foreach($arrayMessages as $keyVal=>$value)
            {
                if($keyVal=='message')
                {
                    foreach($value as $messagesValues)
                    {
                        foreach($messagesValues as $messagesValue){
                            return Response()->json([
                              'status' => 0,
                              'message' => $messagesValue
                     ]);
                        }
                    }
                }

            }
        }
        $objUserkey=AgentsOtpModel::where(['agent_id'=>$request->user_id])
                    ->where(['token_key'=>$request->user_key])->first();
        if(isset($objUserkey->id))
        {
            $result=AgentsStartMyDayModel::where(['user_id'=>$request->user_id])->get();
            $dataArray=array();
            foreach($result as $row)
            {
                //$address=Helpers\Helpers::getaddress($row->latitudes,$row->longitude);
                $dataArray[]=array('id'=>$row->id,'latitudes'=>$row->latitudes,'longitude'=>$row->longitude,'address'=>$row->address);
            }
            return Response()->json([
                              'status' => 1,
                              'data' => $dataArray
                     ]);
            
        }else{
            return Response()->json([
                              'status' => 0,
                              'message' => "Invalid user key"
                     ]);
        }
       
        
    }
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'user_id' =>'required',
                    'user_key' =>'required',
                    'lattitude' =>'required',
                    'longitude' =>'required',
                    'remarks'   =>'required',
                                        
                    ]);
         if ($validator->fails()) {
                  
            $error_msg= json_encode([
                 'status' => 0,
                 'message' => $validator->messages()
             ]);
             $arrayMessages=json_decode($error_msg);
            foreach($arrayMessages as $keyVal=>$value)
            {
                if($keyVal=='message')
                {
                    foreach($value as $messagesValues)
                    {
                        foreach($messagesValues as $messagesValue){
                            return Response()->json([
                              'status' => 0,
                              'message' => $messagesValue
                     ]);
                        }
                    }
                }

            }
        }
        $objUserkey=AgentsOtpModel::where(['agent_id'=>$request->user_id])
                    ->where(['token_key'=>$request->user_key])->first();
        if(isset($objUserkey->id))
        {
            $obj=new AgentsDiscoverPlacesModel();
            $obj->lattitude=$request->lattitude;
            $obj->longitude=$request->longitude;
            $obj->remarks=$request->remarks;
            $obj->user_id=$request->user_id;
            $obj->location=Helpers\Helpers::getaddress($request->lattitude,$request->longitude);
            $cityState=Helpers\Helpers::getCityState($request->lattitude,$request->longitude);
           $obj->city=$cityState['city']?$cityState['city']:'';
            $obj->state=$cityState['state']?$cityState['state']:'';
            
           // die();
            if($obj->save()){
                 return Response()->json([
                              'status' => 1,
                              'message' => "Record Saved",
                              
                     ]);
            }
        }else{
            return Response()->json([
                              'status' => 0,
                              'message' => "Invalid user key"
                     ]);
        }
    }
 
    

    

}
