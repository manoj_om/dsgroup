<?php

namespace Api\Http\Controllers;

use App\Http\Controllers\Controllers;

use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Api\Models\ApiModel;
use Illuminate\Support\Facades\DB;
use Api\Models\AgentsOtpModel;
use App;
use App\Helpers;
use Illuminate\Routing\Controller as BaseController;
class AgentLoginController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'mobile_number' =>'required|max:15',
          // 'user_key' =>'required|string|unique:agents_onboardings|max:15',

       ]);


         if ($validator->fails()) {

           $error_msg= json_encode([
                'status' => 0,
                'message' => $validator->messages()
            ]);
            $arrayMessages=json_decode($error_msg);

           foreach($arrayMessages as $keyVal=>$value)
           {
               if($keyVal=='message')
               {
                   foreach($value as $messagesValues)
                   {
                       foreach($messagesValues as $messagesValue){
                           return Response()->json([
                             'status' => 0,
                             'message' => $messagesValue
                    ]);
                       }
                   }
               }

           }


       }

       $objAgentInfo=ApiModel::where(['mobile_number'=>$request->mobile_number,'is_approve'=>1])->first();

       if(isset($objAgentInfo->id))
       {

            if($objAgentInfo->activate != '0'){
                return Response()->json([
                    'status' => 0,
                    'message' => 'You have been blocked. Please contact DS Group admin.'
                ]);
            }
          $objOtp=new AgentsOtpModel();
          $objOtp->agent_id= $objAgentInfo->id;
          $objOtp->otp=rand(1000,10000);
          $objOtp->token_key=md5($objAgentInfo->email.time());
         // echo "hi....".config('api.smsurl');die();

          $objOtp->save();
          $message_body=urlencode('Your verification code is : '.$objOtp->otp);
          $message="&send_to=".$request->mobile_number."&msg=$message_body";
          Helpers\Helpers::sendSMS(config('api.smsurl').$message);
           return Response()->json([
                'status' => 1,
                'message' => "OTP genrated ",'mobile'=>$request->mobile_number,'user_key'=>$objOtp->token_key
            ]);

       }else {
            return Response()->json([
                             'status' => 0,
                             'message' => 'Mobile number not register'
                    ]);
       }

        die();

    }
    public function edit($id)
    {
        echo "hi..edit...";die();

    }
    public function create()
    {
        echo "hi..create...";die();
    }
    public function show(Request $request)
    {
          $validator = Validator::make($request->all(), [
                    'user_id' =>'required',
                    'user_key' =>'required',

                    ]);
         if ($validator->fails()) {

            $error_msg= json_encode([
                 'status' => 0,
                 'message' => $validator->messages()
             ]);
             $arrayMessages=json_decode($error_msg);
            foreach($arrayMessages as $keyVal=>$value)
            {
                if($keyVal=='message')
                {
                    foreach($value as $messagesValues)
                    {
                        foreach($messagesValues as $messagesValue){
                            return Response()->json([
                              'status' => 0,
                              'message' => $messagesValue
                     ]);
                        }
                    }
                }

            }
        }
        $objUserkey=AgentsOtpModel::where(['agent_id'=>$request->user_id])
                    ->where(['token_key'=>$request->user_key])->first();
        if(isset($objUserkey->id))
        {
            $userProfile= ApiModel::where(['id'=>$request->user_id])->first()->toArray();
            $userProfile['avtar']=$userProfile['avtar']?config('app.url').'/uploads/agents/'.$userProfile['avtar']:'';
            return Response()->json([
                             'status' => 1,
                             'data' => $userProfile
                    ]);

        }else {
             return Response()->json([
                              'status' => 0,
                              'message' => "Invalid user key"
                     ]);
        }
    }

    public function store(Request $request)
    {



    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'user_id' =>'required',
                    'user_key' =>'required',
                    'email' => 'required|email',
                    'address' =>'required',
                    'city'=>'required',
                    'state'=>'required',
                    'avtar' =>'image|mimes:jpeg,jpg,png,gif|max:10000',
                    ]);
      //echo "hi.....".Helpers\Helpers::barcodeScanner('SDUP1600000004O');die();
         if ($validator->fails()) {

            $error_msg= json_encode([
                 'status' => 0,
                 'message' => $validator->messages()
             ]);
            return response()->json([
                'status' => 0,
                'message' => $validator->errors()->first()
            ]);


        }
        $objUserkey=AgentsOtpModel::where(['agent_id'=>$request->user_id])
                    ->where(['token_key'=>$request->user_key])->first();
        if(isset($objUserkey->id))
        {
            $result=ApiModel::where(['id'=>$request->user_id])->first();
            $objAlreadyExitEmail=ApiModel::where(['email'=>$request->email])->where('id','!=',$request->user_id)->first();

           // $objAlreadyExitMobile=ApiModel::where(['mobile_number'=>$request->mobile_number])->where('id','!=',$request->user_id)->first();
            if(isset($objAlreadyExitEmail->id))
            {
                return Response()->json([
                'status' => 1,
                'message' => "Email Already used"
             ]);
            }
            if($request->hasFile('avtar')){
                $fileExtension = $request->avtar->getClientOriginalExtension();
                $imageExtensions = ['jpg','png','jpeg','gif'];
                if(!in_array($fileExtension,$imageExtensions))
                    return response()->json([
                        'status' => 0,
                        'message' => 'Invalid avtar file type'
                    ]);
            }
            $result->email=$request->email;
            //$result->mobile_number=$request->mobile_number;
            $result->address=$request->address;
            $result->avtar=$result->avtar;
            $result->city=$request->city?$request->city:'';
            $result->state=$request->state?$request->state:'';
            // uploads/agent
            if($request->hasfile('avtar')){
                 $file = $request->file('avtar');
                 $ext = $file->getClientOriginalExtension();
                 $filename = time().'.'.$ext;
                 $file->move('uploads/agents',$filename);
                 $result->avtar=$filename;

            }
            if($result->save()){
               $result->avtar=$result->avtar?config('app.url').'/uploads/agents/'.$result->avtar:'';
               return Response()->json([
                'status' => 1,
                'message' => "Record Saved",'user_key'=>$objUserkey->token_key,
                'data'=>$result
             ]);

       }
        }else {
             return Response()->json([
                              'status' => 0,
                              'message' => "Invalid user key"
                     ]);
        }

    }
    public function verifiedOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'otp' =>'required|max:15',
           'user_key' =>'required',

          // 'user_key' =>'required|string|unique:agents_onboardings|max:15',

       ]);


         if ($validator->fails()) {

           $error_msg= json_encode([
                'status' => 0,
                'message' => $validator->messages()
            ]);
            $arrayMessages=json_decode($error_msg);
           foreach($arrayMessages as $keyVal=>$value)
           {
               if($keyVal=='message')
               {
                   foreach($value as $messagesValues)
                   {
                       foreach($messagesValues as $messagesValue){
                           return Response()->json([
                             'status' => 0,
                             'message' => $messagesValue
                    ]);
                       }
                   }
               }

           }


       }
       $objOtpVarified=AgentsOtpModel::where(['otp'=>$request->otp])->where(['token_key'=>$request->user_key])->first();
       if(isset($objOtpVarified->agent_id)){
           $objAgentInfo=ApiModel::where(['id'=>$objOtpVarified->agent_id,'is_approve'=>1])->first();
           //$objAgentInfo->attributesToArray()
           if(isset($objAgentInfo->avtar)){
               $objAgentInfo->avtar=config('app.url').'/uploads/agents/'.$objAgentInfo->avtar;
           }
           return Response()->json([
                             'status' => 1,
                             'data' => $objAgentInfo->attributesToArray(),
                             'message' => "otp correct",
                             'user_key'=>$objOtpVarified->token_key
                    ]);

       }else {
           return Response()->json([
                             'status' => 0,
                             'message' => "Invalid Otp"
                    ]);
       }


    }





}
