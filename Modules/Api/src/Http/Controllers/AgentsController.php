<?php

namespace Api\Http\Controllers;

use App\Http\Controllers\Controllers;

use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Api\Models\ApiModel;
use Illuminate\Support\Facades\DB;
use Api\Models\AgentsOtpModel;
use Api\Models\RetailersModel;
use App;
use App\Helpers;
use App\attention_requireds;
use Illuminate\Routing\Controller as BaseController;
use Api\Models\AttentionRequiredsModel;



class AgentsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        echo "hi..index.123..";die();

    }
    public function edit($id)
    {
        echo "hi..edit...";die();

    }
    public function create()
    {
        echo "hi..create...";die();
    }
    public function show($id)
    {
         echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
           'email' => 'unique:agents_onboardings|nullable',
           'name' => 'required|string|max:50',
           'mobile_number' =>'required|string|unique:agents_onboardings|max:15',
           'adhar_number' =>'required|string|unique:agents_onboardings|max:15',
         //  'agency_name' =>'string|unique:agents_onboardings|max:150',
           'employee_code' =>'string|unique:agents_onboardings|max:15|nullable',
           'city'=>'required',
           'state'=>'required',

           'avtar' =>'image | mimes:jpeg,jpg,png,gif|required|max:10000',
       ]);

       if ($validator->fails()) {

            return response()->json([
                'status' => 0,
                'message' => $validator->errors()->first()
            ]);

       }
        if($request->hasFile('avtar')){
            $fileExtension = $request->avtar->getClientOriginalExtension();
            $imageExtensions = ['jpg','png','jpeg','gif'];
            if(!in_array($fileExtension,$imageExtensions))
                return response()->json([
                    'status' => 0,
                    'message' => 'Invalid file type'
                ]);
        }
       $result=new ApiModel();
       $result->name=$request->name;
       $result->email=$request->email;
       $result->mobile_number=$request->mobile_number;
       $result->adhar_number=$request->adhar_number;
       $result->agency_name=$request->agency_name;
       $result->employee_code=$request->employee_code;
       $result->address=$request->address;
       $result->user_id=0;
       $result->city=$request->city?$request->city:'';
        $result->state=$request->state?$request->state:'';
       $result->avtar='';
       // uploads/agent
       if($request->hasfile('avtar')){
            $file = $request->file('avtar');
            $ext = $file->getClientOriginalExtension();
            $filename = time().'.'.$ext;
            $file->move('uploads/agents',$filename);
            $result->avtar=$filename;
       }
       if($result->save()){

          $objOtp=new AgentsOtpModel();
          $objOtp->agent_id= $result->id;
          $objOtp->otp=rand(1000,10000);
          $message_body= urlencode("We have received your registration form and it is under approval. Request Id: 00".$result->id);
          $message="&send_to=".$request->mobile_number."&msg=$message_body";
          Helpers\Helpers::sendSMS(config('api.smsurl').$message);
          $objOtp->save();
           return Response()->json([
                'status' => 1,
                'message' => "Your request has been submitted and pending for approval"
            ]);

       }





    }
    public function attention(Request $request)
    {
                $validator = Validator::make($request->all(), [
                    'user_id' =>'required',
                    'user_key' =>'required',
                    'scan_code' =>'required',

                    ]);
      //echo "hi.....".Helpers\Helpers::barcodeScanner('SDUP1600000004O');die();
         if ($validator->fails()) {

            return response()->json([
                'status' => 0,
                'message' => $validator->errors()->first()
            ]);
         }
          $objUserkey=AgentsOtpModel::where(['agent_id'=>$request->user_id])
                            ->where(['token_key'=>$request->user_key])->first();
        if(isset($objUserkey->id))
        {
            $objAttentionRequireds=new AttentionRequiredsModel();
            $objAttentionRequireds->reason=$request->reason?$request->reason:'';
            $objAttentionRequireds->sku_name=$request->sku_name?$request->sku_name:'';
            //$objAttentionRequireds->status=0;
            if(isset($request->scan_code)){
                                if(!Helpers\Helpers::barcodeScanner($request->scan_code)){
                              return Response()->json([
                                          'status' => 0,
                                          'message' => "Invalid QR code",

                                 ]);
                            }
            }

            $result_retailer=RetailersModel::where(['scan_code'=>$request->scan_code])->first();

            if(isset($result_retailer->id))
            {
                $objAttentionRequireds->retailer_id=$result_retailer->id?$result_retailer->id:0;
                 $objAttentionRequireds->user_id=$request->user_id;
           $objAttentionRequireds->scan_code=$request->scan_code?$request->scan_code:'';
            if($objAttentionRequireds->save())
            {
                return Response()->json([
                              'status' => 1,
                              'message' => "Attention Request saved"
                     ]);
            }

            }else {
                return Response()->json([
                                          'status' => 0,
                                          'message' => "Retailer QR code not found",

                                 ]);
            }

        }else{
            return Response()->json([
                              'status' => 0,
                              'message' => "Invalid user key"
                     ]);
            }

    }





}
