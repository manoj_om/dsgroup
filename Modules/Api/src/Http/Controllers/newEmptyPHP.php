<?php
$validator = Validator::make($request->all(), [
                    'user_id' =>'required',
                    'user_key' =>'required',
                    'full_name'=>'required|string|max:150',
                    'primary_mobile_number'=>'required|numeric|unique:retailers',
                    'alternative_mobile_number'=>'numeric|unique:retailers',
                    'whats_app_number'=>'numeric',
                    'capture_retailer_image'=>'mimes:jpeg,jpg,png,gif|max:10000'
                    ]);
                    if($id)
                    {
                        $validator = Validator::make($request->all(), [
                        'user_id' =>'required',
                        'user_key' =>'required',
                        'full_name'=>'required|string|max:150',
                        'primary_mobile_number'=>'required|numeric',
                        'alternative_mobile_number'=>'numeric',
                        'whats_app_number'=>'numeric',
                        'capture_retailer_image'=>'mimes:jpeg,jpg,png,gif|max:10000'
                        ]);
                    }
                     if ($validator->fails()) {
                  
                        $error_msg= json_encode([
                             'status' => 0,
                             'message' => $validator->messages()
                         ]);
                         $arrayMessages=json_decode($error_msg);
                        foreach($arrayMessages as $keyVal=>$value)
                        {
                            if($keyVal=='message')
                            {
                                foreach($value as $messagesValues)
                                {
                                    foreach($messagesValues as $messagesValue){
                                        return Response()->json([
                                          'status' => 0,
                                          'message' => $messagesValue
                                 ]);
                                    }
                                }
                            }

                        }
                    }
                    $objUserkey=AgentsOtpModel::where(['agent_id'=>$request->user_id])
                            ->where(['token_key'=>$request->user_key])->first();
                    if(isset($objUserkey->id))
                    {
                        

                        // Must not already exist in the `email` column of `users` table
                        

                        if($id) {
                            $objRetailerPrimaryInfo=RetailersModel::where(['id'=>$id])->where(['user_id'=>$request->user_id])->first();
                            if(isset($objRetailerPrimaryInfo->id))
                            {
                               
                            $objRetailerPrimaryInfo->establishment_name=$request->establishment_name?$request->establishment_name:'';
                            $objRetailerPrimaryInfo->gst_number=$request->gst_number ? $request->gst_number:'';
                            $objRetailerPrimaryInfo->pan_number=$request->pan_number ? $request->pan_number:'';
                            $objRetailerPrimaryInfo->adhar_number=$request->adhar_number ? $request->adhar_number:'';
                            //adhar_number
                            $objRetailerPrimaryInfo->gst_number_file='';
                            $objRetailerPrimaryInfo->pan_number_file='';
                            $objRetailerPrimaryInfo->adhar_number_file='';
                            
                            if($request->hasfile('gst_number_file')){
                                
                                $file = $request->file('gst_number_file');
                                
                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id.time().'.'.$ext;
                                
                                $file->move('uploads/agents/retailers/kyc',$filename);
                               
                                $objRetailerPrimaryInfo->gst_number_file=$filename;
                                
                                
                           }
                           if($request->hasfile('pan_number_file')){
                                
                                $file = $request->file('pan_number_file');
                                
                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id.time().'.'.$ext;
                                
                                $file->move('uploads/agents/retailers/kyc',$filename);
                               
                                $objRetailerPrimaryInfo->pan_number_file=$filename;
                                
                                
                           }
                           if($request->hasfile('adhar_number_file')){
                                
                                $file = $request->file('adhar_number_file');
                                
                                $ext = $file->getClientOriginalExtension();
                                $filename = $request->user_id.time().'.'.$ext;
                                
                                $file->move('uploads/agents/retailers/kyc',$filename);
                               
                                $objRetailerPrimaryInfo->adhar_number_file=$filename;
                                
                                
                           }
                           if($objRetailerPrimaryInfo->save())
                            {
                               
                                  
                                   return Response()->json([
                                          'status' => 1,
                                          'message' => "Record updated",
                                          'data'    => $objRetailerPrimaryInfo->toArray()
                                 ]);
                            }else {
                                return Response()->json([
                                          'status' => 0,
                                          'message' => "Server side issue",
                                         
                                 ]);
                            }
                            
                            
                            }
                        
                        }
                        
                    }else{
                        return Response()->json([
                                          'status' => 0,
                                          'message' => "Invalid user key"
                                 ]);
                    }
                    
