<?php

namespace Api\Http\Controllers;

use App\Http\Controllers\Controllers;
use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;

use Api\Models\AgentsOtpModel;
use Api\Models\RetailersModel;

use App\Helpers;
use Api\Models\QrCodeModel;
use Api\Models\QrCodeLogModel;
use Illuminate\Routing\Controller as BaseController;
use App\MapDistributor;
use App\City;
use App\Distributor;
use App\Traits\CurlRequestTrait;

class HubController extends BaseController {

    //use CurlRequestTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $retailers=new MapDistributor();
        $retailers=MapDistributor::all();
    $retailerInfo=array();    
    foreach($retailers as $key=>$row){
        if(isset($row->retailer)){
        
        $retailerInfo[$key]=$row->retailer->toArray();
        $retailerInfo[$key]['distributor']=$row->distributorData->toArray();
        
        
        }
    }
 
     return Response()->json([
                            'status' => 1,
                            'data' => $retailerInfo,
                ]);
    
    }

    public function store(Request $request) {
         echo "hi...store...";die();
   }
   public function show(Request $request) {
         echo "hi...show...";die();
   }
}
