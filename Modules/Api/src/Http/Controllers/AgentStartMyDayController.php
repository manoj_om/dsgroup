<?php

namespace Api\Http\Controllers;

use App\Http\Controllers\Controllers;

use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Api\Models\ApiModel;
use Api\Models\AgentsStartMyDayModel;
use Illuminate\Support\Facades\DB;
use Api\Models\AgentsOtpModel;
use App\Helpers;
use App;

use Illuminate\Routing\Controller as BaseController;
class AgentStartMyDayController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'user_id' =>'required',
                    'user_key' =>'required',

                    ]);
         if ($validator->fails()) {
                  return response()->json([
                    'status' => 0,
                    'message' => $validator->errors()->first()
           ]);
        }
        $date = $request->date;
        $objUserkey=AgentsOtpModel::where(['agent_id'=>$request->user_id])
                    ->where(['token_key'=>$request->user_key])->first();
        if(isset($objUserkey->id))
        {
            $query = AgentsStartMyDayModel::where(['user_id'=>$request->user_id])
            ->where('latitudes','!=',0.0)->where('longitude','!=',0.0);
            if(!empty($date))
                $query->whereDate('created_at',$date);

            $result = $query->get();
            $dataArray=array();
            foreach($result as $row)
            {
                //$address=Helpers\Helpers::getaddress($row->latitudes,$row->longitude);
                $dataArray[]    =
                    array(
                        'id'=>$row->id,
                        'latitudes'=>$row->latitudes,
                        'longitude'=>$row->longitude,
                        'address'=>$row->address,
                        'type' => $row->type
                    );
            }
            return Response()->json([
                              'status' => 1,
                              'data' => $dataArray
                     ]);

        }else{
            return Response()->json([
                              'status' => 0,
                              'message' => "Invalid user key"
                     ]);
        }


    }

    public function startmyday(Request $request)
    {
//        echo $request->agent_id;
//        echo $request->latitudes;
//        echo $request->longitude;

        $validator = Validator::make($request->all(), [
           'agent_id' =>'required',
           'latitudes' =>'required',
           'longitude' =>'required'

       ]);
        if ($validator->fails()) {

           $error_msg= json_encode([
                'status' => 0,
                'message' => $validator->messages()
            ]);
            $arrayMessages=json_decode($error_msg);
           foreach($arrayMessages as $keyVal=>$value)
           {
               if($keyVal=='message')
               {
                   foreach($value as $messagesValues)
                   {
                       foreach($messagesValues as $messagesValue){
                           return Response()->json([
                             'status' => 0,
                             'message' => $messagesValue
                    ]);
                       }
                   }
               }

           }


       }
        $objAgentInfo=ApiModel::where(['id'=>$request->agent_id,'is_approve'=>1])->first();
        if(isset($objAgentInfo->id)){
            $objStartMyday= New AgentsStartMyDayModel();
            $objStartMyday->user_id=$objAgentInfo->id;
            $objStartMyday->latitudes=$request->latitudes;
            $objStartMyday->longitude=$request->longitude;
            $objStartMyday->address=Helpers\Helpers::getaddress($request->latitudes,$request->longitude);

            if($objStartMyday->save()){
                return Response()->json([
                             'status' => 1,
                             'message' => "record saved"
                    ]);
            }



        }
        return Response()->json([
                             'status' => 0,
                             'message' => "agent not exist"
                    ]);


    }



}
