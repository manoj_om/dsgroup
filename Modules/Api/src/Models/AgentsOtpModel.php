<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class AgentsOtpModel extends Model
{
    public $table = 'agents_otp';
    protected $fillable = ['agent_id','otp'];


}
