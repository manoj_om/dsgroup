<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class ApiModel extends Model
{
    public $table = 'agents_onboardings';
    protected $fillable = ['agency_name','name',
        'adhar_number','address','mobile_number','email','employee_code','avtar'];


}
