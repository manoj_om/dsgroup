<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class AgentsStartMyDayModel extends Model
{
    public $table = 'start_my_day';
    protected $fillable = ['user_id','latitudes','longitude','address','resolution'];

}
