<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class AgentsDiscoverPlacesModel extends Model
{
    protected $attributes = [
        'status' => 0
    ];
    public $table = 'discovered_places';


}
