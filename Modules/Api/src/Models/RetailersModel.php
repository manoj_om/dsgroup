<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RetailersModel extends Model
{

    public $table = 'retailers';
    public $shop_location='';

}
