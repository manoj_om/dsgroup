<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class QrCodeModel extends Model {

    public $table = 'retailers_qr_code';
    protected $fillable = [
        'user_id', 'paytm', 'mobiqwik', 'gpay', 'phone_pe', 'upi', 'bharat_qr', 'ds_group'
    ];
    
   

}
