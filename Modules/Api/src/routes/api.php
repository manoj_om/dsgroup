<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Route::resource('api', 'Api\Http\Controllers\AgentsController');
Route::group([
      'middleware' => 'customapi','requestLog'
    ], function() {

        Route::resource('api/agent/register', 'Api\Http\Controllers\AgentsController');
        Route::resource('api/agent/login', 'Api\Http\Controllers\AgentLoginController');
        Route::post('api/agent/login', 'Api\Http\Controllers\AgentLoginController@index');
        Route::post('api/agent/varified', 'Api\Http\Controllers\AgentLoginController@verifiedOtp');
        Route::post('api/agent/start-my-day', 'Api\Http\Controllers\AgentStartMyDayController@startmyday');
        Route::resource('api/retailer', 'Api\Http\Controllers\RetailersController');
        Route::post('api/retailer/detail', 'Api\Http\Controllers\RetailersController@show');
        Route::post('api/retailer/detail', 'Api\Http\Controllers\RetailersController@show');
        //searchRetailers
        Route::post('api/retailers/search', 'Api\Http\Controllers\RetailersController@searchRetailers');
        Route::post('api/agent/profile', 'Api\Http\Controllers\AgentLoginController@show');
        Route::post('api/agent/edit', 'Api\Http\Controllers\AgentLoginController@update');
        Route::post('api/agent/attention', 'Api\Http\Controllers\AgentsController@attention');
        Route::post('api/agent/heat-map', 'Api\Http\Controllers\AgentStartMyDayController@index');
        Route::resource('api/agent/discover-place', 'Api\Http\Controllers\AgentsDiscoverPlaceController');
        //add
        Route::post('api/agent/share-places', 'Api\Http\Controllers\AgentsDiscoverPlaceController@add');
        //('/search-agent', 'HomeController@searchInAgent')
        //used for hub
        

    });
    Route::resource('api/hub', 'Api\Http\Controllers\HubController');
    Route::get('/search-agent', function(){ return(redirect('home')); });


