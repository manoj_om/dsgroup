<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. ApiServiceProvider
 */
namespace Api;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
         $this->mergeConfigFrom(__DIR__ . '/config/api.php', 'api');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Load Routes
        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        
        // Load Template Management and Core Views


        // To load the migration file
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

    }
}

