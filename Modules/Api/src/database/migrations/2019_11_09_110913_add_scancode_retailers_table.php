<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddScancodeRetailersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        
        if(Schema::hasTable('retailers')){
           Schema::table('retailers', function(Blueprint $table) {
                if (!Schema::hasColumn('retailers', 'scan_code')) {
                    $table->text('scan_code')->nullable();
                }
                    
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
 if(Schema::hasTable('retailers')){
           Schema::table('retailers', function(Blueprint $table) {
                if (Schema::hasColumn('retailers', 'scan_code')) {
                    $table->dropColumn('scan_code');
                }
                    
                
            });
        }
    }

}
