<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetailersScanCodeTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        if(!Schema::hasTable('retailers_qr_code')){
           
        Schema::create('retailers_qr_code', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->text('paytm')->nullable();
            $table->text('mobiqwik')->nullable();
            $table->text('gpay')->nullable();
            $table->text('phone_pe')->nullable();
            $table->text('upi')->nullable();
            $table->text('bharat_qr')->nullable();
            $table->text('ds_group')->nullable();
            $table->timestamps();
        });
        }
         if(!Schema::hasTable('retailers_qr_code_log')){
           
        Schema::create('retailers_qr_code_log', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->text('paytm')->nullable();
            $table->text('mobiqwik')->nullable();
            $table->text('gpay')->nullable();
            $table->text('phone_pe')->nullable();
            $table->text('upi')->nullable();
            $table->text('bharat_qr')->nullable();
            $table->text('ds_group')->nullable();
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if(Schema::hasTable('retailers_qr_code')){
        Schema::dropIfExists('retailers_qr_code_log');
        }
    }

}
