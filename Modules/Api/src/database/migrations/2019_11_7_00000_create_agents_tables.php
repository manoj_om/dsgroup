<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
         if(!Schema::hasTable('agents_onboardings')){
         Schema::create('agents_onboardings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email',200)->unique();
            $table->string('name');
            $table->bigInteger('mobile_number')->unique();
            $table->string('adhar_number',200)->unique();
            $table->string('agency_name',200);
            $table->text('address');
            $table->string('employee_code',200)->unique();
            $table->string('user_id')->default(NULL);
            $table->string('avtar');
            $table->tinyInteger('is_approve')->default(0);
            $table->timestamps();
        });
        
        
        
    }
    if(!Schema::hasTable('agents_otp')){
            Schema::create('agents_otp', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('agent_id');
                $table->string('otp');
                $table->timestamps();
            });
    }
    
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         if(Schema::hasTable('agents_onboardings')){
            Schema::dropIfExists('agents_onboardings');
         }
    }
}
