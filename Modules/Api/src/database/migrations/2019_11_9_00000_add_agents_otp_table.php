<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgentsOtpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        if(Schema::hasTable('agents_otp')){
           Schema::table('agents_otp', function(Blueprint $table) {
                if (!Schema::hasColumn('agents_otp', 'token_key')) {
                    $table->text('token_key')->nullable();
                }
                    
                
            });
        }
        if(Schema::hasTable('agents_onboardings')){
           Schema::table('agents_onboardings', function(Blueprint $table) {
                if (!Schema::hasColumn('agents_onboardings', 'is_approve')) {
                    $table->tinyInteger('is_approve')->default(0);
                }
                    
                
            });
        }
        //'is_approve
         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         if(Schema::hasTable('agents_otp')){
           Schema::table('agents_otp', function(Blueprint $table) {
                if (Schema::hasColumn('agents_otp', 'token_key')) {
                    $table->dropColumn('token_key');
                }
                    
                
            });
        }
        if(Schema::hasTable('agents_onboardings')){
           Schema::table('agents_onboardings', function(Blueprint $table) {
                if (Schema::hasColumn('agents_onboardings', 'is_approve')) {
                    $table->dropColumn('is_approve');
                }
                    
                
            });
        }
    }
}
