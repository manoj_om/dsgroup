<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultValueRetailersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        if(Schema::hasTable('retailers')){
           Schema::table('retailers', function(Blueprint $table) {
                if (Schema::hasColumn('retailers', 'alternative_mobile_number')) {
                    $table->bigInteger('alternative_mobile_number')->nullable()->change();
                }
                 if (Schema::hasColumn('retailers', 'capture_retailer_image')) {
                    $table->text('capture_retailer_image')->nullable()->change();
                }
                 
                    
                
            });
        }
        
        //'is_approve
         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
if(Schema::hasTable('retailers')){
           Schema::table('retailers', function(Blueprint $table) {
                if (Schema::hasColumn('retailers', 'alternative_mobile_number')) {
                    $table->bigInteger('alternative_mobile_number')->change();
                }
                 if (Schema::hasColumn('retailers', 'capture_retailer_image')) {
                    $table->text('capture_retailer_image')->change();
                }
                
                    
                
            });
        }
    }
}
