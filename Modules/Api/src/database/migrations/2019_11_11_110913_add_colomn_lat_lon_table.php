<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColomnLatLonTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        
        if(Schema::hasTable('retailers')){
           Schema::table('retailers', function(Blueprint $table) {
                if (!Schema::hasColumn('retailers', 'latitudes')) {
                    $table->text('latitudes')->nullable();
                }
                if (!Schema::hasColumn('retailers', 'longitude')) {
                    $table->text('longitude')->nullable();
                }
                
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
 if(Schema::hasTable('retailers')){
     //p_device_type
           Schema::table('retailers', function(Blueprint $table) {
                if (Schema::hasColumn('retailers', 'latitudes')) {
                    $table->dropColumn('latitudes');
                }
                   if (!Schema::hasColumn('retailers', 'longitude')) {
                    $table->dropColumn('longitude') ;
                }
                
                    
                
            });
        }
    }

}
