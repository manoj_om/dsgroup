<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddColmnAddressStartMyDayTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        if(Schema::hasTable('start_my_day')){
           
        Schema::table('start_my_day', function (Blueprint $table) {
             if (!Schema::hasColumn('start_my_day', 'address')) {
                    $table->text('address')->nullable();
                }
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
          Schema::table('start_my_day', function (Blueprint $table) {
       if (Schema::hasColumn('start_my_day', 'address')) {
                    $table->dropColumn('address');
        }
         });
    }

}
