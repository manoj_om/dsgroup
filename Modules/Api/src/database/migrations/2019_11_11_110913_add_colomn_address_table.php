<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColomnAddressTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        
        if(Schema::hasTable('retailers')){
           Schema::table('retailers', function(Blueprint $table) {
                if (!Schema::hasColumn('retailers', 'shop_address')) {
                    $table->text('shop_address')->nullable();
                }
                
                
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
 if(Schema::hasTable('retailers')){
     //p_device_type
           Schema::table('retailers', function(Blueprint $table) {
                if (Schema::hasColumn('retailers', 'shop_address')) {
                    $table->dropColumn('shop_address');
                }
                  
                
                    
                
            });
        }
    }

}
