<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColmnCityStateRetailerTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        if(Schema::hasTable('retailers')){
           
        Schema::table('retailers', function (Blueprint $table) {
             if (!Schema::hasColumn('retailers', 'city')) {
                    $table->string('city')->nullable();
                }
              if (!Schema::hasColumn('retailers', 'state')) {
                    $table->string('state')->nullable();
                }   
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
         Schema::table('retailers', function (Blueprint $table) {
       if (Schema::hasColumn('retailers', 'city')) {
                    $table->dropColumn('city');
        }
       if (Schema::hasColumn('retailers', 'state')) {
                    $table->dropColumn('state');
        }
          });
    }

}
