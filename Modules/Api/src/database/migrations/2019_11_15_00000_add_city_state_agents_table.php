<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCityStateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        if(Schema::hasTable('agents_onboardings')){
           Schema::table('agents_onboardings', function(Blueprint $table) {
                if (!Schema::hasColumn('agents_onboardings', 'state')) {
                    $table->string('state')->nullable();
                }
                if (!Schema::hasColumn('agents_onboardings', 'city')) {
                    $table->string('city')->nullable();
                }
                    
                
            });
        }

        //'is_approve
         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if(Schema::hasTable('agents_onboardings')){
           Schema::table('agents_onboardings', function(Blueprint $table) {
               if (Schema::hasColumn('agents_onboardings', 'state')) {
                    $table->dropColumn('state');
                }
                if (Schema::hasColumn('agents_onboardings', 'city')) {
                    $table->dropColumn('city');
                }
                    
                
            });
        }
    }
}
