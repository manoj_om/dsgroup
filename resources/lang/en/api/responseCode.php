<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'success' => '200',
    'failed' =>  '403',
    'server_error' => '500',
    'unsupported_type' => '415',
    'method_not_allow' => '405',
    'not_found' => '404',
    'unauthorized' => '401',
    'bad_request' => '400', //validation failed
    'logout' => '503',

    'pushNotification' => [
        '2' => 'Your order #:number has been packed successfully.',
        '3' => 'Your order #:number has been dispatched successfully',
        '0' => 'Your order #:number has been cancelled successfully',
        '12'=> 'Your order has been placed successfully and your order number is #:number.Thank you for shopping with DS Group'
    ],
    'title' => [
        '2' => 'Your oder packed successfully.',
        '3' => 'Your order has been dispatched',
        '0' => 'You order has been cancelled',
        '12'=> 'Your order has been placed successfully'
    ]

];
