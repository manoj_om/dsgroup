<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'pushNotification' => [
        '2' => 'Your order #:number has been packed successfully.',
        '3' => 'Your order #:number has been dispatched successfully',
        '0' => 'Your order #:number has been cancelled successfully',
        '12'=> 'Your order has been placed successfully and your order number is #:number.Thank you for shopping with DS Group'
    ],
    'title' => [
        '2' => 'Your oder packed successfully.',
        '3' => 'Your order has been dispatched',
        '0' => 'You order has been cancelled',
        '12'=> 'Your order has been placed successfully'
    ],
    'webNotification' => [
        'api' => [
            'retailer' => [
                'CANCEL_ORDER' => 'You have cancelled an order number #:number successfully.',
                'PLACE_ORDER' => 'You have placed an order successfully. Your order number is #:number.',

            ],
            'distributor' => [
                'CANCEL_ORDER' => 'Retailer has cancelled an order number #:number successfully.',
                'PLACE_ORDER' => 'Retailer has placed a new order successfully. Your order number is #:number.',

            ],
            'admin' => [
                'CANCEL_ORDER' => 'Retailer has cancelled an order number #:number successfully.',
                'PLACE_ORDER' => 'Retailer has placed a new order successfully. Your order number is #:number.',

            ]
        ],
        'hub' => [
            'distributor' => [
                'CANCEL_ORDER' => 'You have cancelled order number #:number successfully for the retailer.',
                'PACKED_ORDER' => 'Your order number #:number has been packed successfully for the retailer.',
                'DISPATCH_ORDER' => 'Your order number #:number has been dispatched successfully for the retailer.'
            ],
            'retailer' => [
                'CANCEL_ORDER' => 'Your Distributor has cancelled your order number #:number successfully.',
                'PACKED_ORDER' => 'Your order number #:number has been packed successfully by the distributor.',
                'DISPATCH_ORDER' => 'Your order number #:number has been dispatched successfully by the distributor.'
            ],
            'admin' => [
                'CANCEL_ORDER' => 'Distributor has cancelled order number #:number successfully.',
                'PACKED_ORDER' => 'Order number #:number has been packed successfully by the Distributor.',
                'DISPATCH_ORDER' => 'Order number #:number has been dispatched successfully by the Distributor.'
            ],
            'stock' => [
                'THRESH_ONE' => 'Stock less product than threshold one #:product_name.',
                'THRESH_TWO' => 'Stock less product than threshold two #:product_name.',
                
            ],

        ],
        'web' => [
            'admin' => [
                'CANCEL_ORDER' => 'You have cancelled order number #:number successfully.',
                'PLACE_ORDER' => 'You have placed your order successfully. User number is #:number.',
                'PACKED_ORDER' => 'Order number #:number has been packed successfully.',
                'DISPATCH_ORDER' => 'Order number #:number has been dispatched successfully.'
            ],
            'distributor' => [
                'CANCEL_ORDER' => 'Admin has cancelled your order number #:number successfully.',
                'PLACE_ORDER' => 'Admin has placed your order successfully. Your order number is #:number.',
                'PACKED_ORDER' => 'Your order number #:number has been packed successfully by the admin.',
                'DISPATCH_ORDER' => 'Your order number #:number has been dispatched successfully by admin.'
            ],
            'retailer' => [
                'CANCEL_ORDER' => 'Admin has cancelled your order number #:number successfully.',
                'PLACE_ORDER' => 'Admin has placed your order successfully. Your order number is #:number.',
                'PACKED_ORDER' => 'Your order number #:number has been packed successfully.',
                'DISPATCH_ORDER' => 'Your order number #:number has been dispatched successfully.'
            ],

        ],
        'agent' => [
            'retailers' => [
                'PLACE_ORDER' => 'Agent has placed your order successfully. Your order number is #:number.',
            ],
            'distributor' => [
                'PLACE_ORDER' => 'Agent has placed an order successfully. And Order number is #:number.',
            ],
            'admin' => [
                'PLACE_ORDER' => 'Agent has placed an order successfully. And Order number is #:number.',
            ]
        ]
    ],
    'code' => [
        'PLACE_ORDER' => 'New order placed',
        'CANCEL_ORDER' => 'Cancellation of order',
        'PACKED_ORDER' => 'Packing of Order',
        'DISPATCH_ORDER' => 'Dispatch of Order',
        'THRESH_ONE' => 'Stock Threshold one Alert',
        'THRESH_TWO' => 'Stock Threshold two Alert',
    ]

];
