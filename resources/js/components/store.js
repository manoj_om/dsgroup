
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);


export const store = new Vuex.Store({
    state: {
        currentUser: '',
        product_category: '',
        product_subcategory: '',
        product: '',
        productName: '',
        carts : ''
    },
    mutations: {
        carts(state, payload) {
            state.carts = payload;
        },
        currentUser(state, payload) {
            state.currentUser = payload;
        },
        productCategory(state, payload) {

            state.product_category = payload;
        },
        productSubCategory(state, payload) {

            state.product_subcategory = payload;
        },
        productlist(state, payload) {

            state.product = payload;
        },
        productNamelist(state, payload) {

            state.productName = payload;
        },
    },

    getters: {
        getCurrentUser: state => {
            return state.currentUser;
        },
        currentCarts: state => {
            return state.carts;
        },
        productCategoryList: state => {
            return state.product_category;
        },
        productSubCategoryList: state => {
            return state.product_subcategory;
        },
        products: state => {
            return state.product;
        },
        productNameList: state => {
            return state.productName;
        }
    }
})



