/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');



//import Vue from 'vue/dist/vue.min.js';
//import Vue from 'vue/dist/vue.js';

// OR #2: make an alias in webpack configuration

import Vue from 'vue';

import BootstrapVue from 'bootstrap-vue';
import Vue2Filters from 'vue2-filters';
import VeeValidate from 'vee-validate';
import VuejsDialog from 'vuejs-dialog';
import VueSocketio from 'vue-socket.io';
const moment = require('moment');
// Main JS (in UMD format)

Vue.use(require('vue-moment'), {
    moment
});
import VueCountdownTimer from 'vuejs-countdown-timer';

import 'vuejs-dialog/dist/vuejs-dialog.min.css';
Vue.use(VeeValidate);
Vue.use(VuejsDialog);
Vue.use(VueCountdownTimer);
//import Vuex from 'vuex';
//
//  Vue.use(Vuex);
// Vue.config.devtools = true;

//import 'bootstrap/dist/css/bootstrap.css';
//import 'bootstrap-vue/dist/bootstrap-vue.css';
//import './custom.scss';
Vue.use(BootstrapVue);
Vue.use(Vue2Filters);
/**

 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('onboarding-agent-table', require('./components/onboard/OnboardAgentTableComponent.vue').default);
Vue.component('retailer-list', require('./components/retailer/RetailersTableComponent.vue').default);
Vue.component('onboarder-retailer-list', require('./components/retailer/OnboarderRetailersTableComponent.vue').default);
Vue.component('header-page', require('./components/partials/HeaderComponent.vue').default);
Vue.component('star-rating', require('./components/partials/ratingtable.vue').default);
Vue.component('my-tree', require('./components/partials/treelist.vue').default);
Vue.component('tree-item', require('./components/partials/childtree.vue').default);
Vue.component('my-tree-script', require('./components/agentpanel/treelist.vue').default);
Vue.component('tree-item-script', require('./components/agentpanel/childtree.vue').default);
Vue.component('graph-tree-item', require('./components/callscript/CallScriptTreeComponent.vue').default);

Vue.component('map-distributor', require('./components/partials/MapDistributorComponent.vue').default);
Vue.component('header-guard-page', require('./components/partials/HeaderGuardComponent.vue').default);
Vue.component('header-admin-panel', require('./components/partials/HeaderAdminPanelComponent.vue').default);
Vue.component('hub-header-page', require('./components/partials/HeaderHubComponent.vue').default);
Vue.component('image-upload', require('./components/partials/ImageComponent.vue').default);
Vue.component('hub-image-upload', require('./components/partials/HubImageComponent.vue').default);
Vue.component('agent-image-upload', require('./components/partials/AgentImageComponent.vue').default);
Vue.component('attention-page', require('./components/retailer/AttentionTableComponent.vue').default);
Vue.component('retailer-form', require('./components/retailer/RetailerFormComponent.vue').default);
Vue.component('retailer-order-calling', require('./components/retailer/OrderCallingComponent.vue').default);
Vue.component('retailer-assign-product', require('./components/retailer/AssignProductComponent.vue').default);
Vue.component('discover-page', require('./components/retailer/DiscoveredTableComponent.vue').default);
Vue.component('map-distributor', require('./components/retailer/MapDistributorComponent.vue').default);
Vue.component('map-agent', require('./components/retailer/MapAgentComponent.vue').default);
Vue.component('add-user-form', require('./components/users/AddUsersFormrComponent.vue').default);
Vue.component('user-table', require('./components/users/UsersTableComponent.vue').default);
Vue.component('add-hub-form', require('./components/users/AddHubUsersFormrComponent.vue').default);
Vue.component('hub-users-table', require('./components/users/HubUsersTableComponent.vue').default);
Vue.component('distributor-list', require('./components/distributor/DistributorTableComponent.vue').default);
Vue.component('test-table', require('./components/users/testcomponent.vue').default);
Vue.component('stock-quantity', require('./components/partials/stockvalue.vue').default);
Vue.component('hub-order-table', require('./components/Hub/OrderListComponent.vue').default);
Vue.component('order-detaile-table', require('./components/Hub/OrderDetailTableComponent.vue').default);
Vue.component('order-item-table', require('./components/manage_scheme/OrderItemTableComponent.vue').default);

Vue.component('cms-table', require('./components/cms/CMSListTableComponent.vue').default);
Vue.component('cms-add-form', require('./components/cms/AddsliderComponent.vue').default);
Vue.component('order-status-setting', require('./components/cms/OrderStatusSettingComponent.vue').default);
Vue.component('add-scheme-form', require('./components/scheme/AddCmsComponent.vue').default);
Vue.component('scheme-table', require('./components/scheme/SchemeListTableComponent.vue').default);
Vue.component('building-scheme-form', require('./components/manage_scheme/SchemeBuildingComponent.vue').default);
Vue.component('building-scheme-table', require('./components/manage_scheme/SchemeBuildingTableComponent.vue').default);
Vue.component('order-volume-form', require('./components/manage_scheme/OrderVolumeFormComponent.vue').default);
Vue.component('order-volume-table', require('./components/manage_scheme/OrderVolumeTableComponent.vue').default);
Vue.component('order-item-form', require('./components/manage_scheme/OrderItemFormComponent.vue').default);
Vue.component('order-item-discount-form', require('./components/manage_scheme/OrderItemDiscountFormComponent.vue').default);
Vue.component('order-item-discount-table', require('./components/manage_scheme/OrderItemDiscountTableComponent.vue').default);
Vue.component('product-launch-form', require('./components/manage_scheme/NewProductLaunchFormComponent.vue').default);
Vue.component('product-launch-table', require('./components/manage_scheme/NewProductLaunchTableComponent.vue').default);
Vue.component('sku-scheme-form', require('./components/manage_scheme/SkuPerCallSchemeFormComponent.vue').default);
Vue.component('sku-scheme-table', require('./components/manage_scheme/SkuSchemeTableComponent.vue').default);

Vue.component('product-master', require('./components/products/ProductMasterComponent.vue').default);
Vue.component('product', require('./components/products/ProductComponent.vue').default);
Vue.component('product-category', require('./components/products/ProductCategoryComponent.vue').default);
Vue.component('product-list', require('./components/products/ProductListComponent.vue').default);
Vue.component('product-sub-category', require('./components/products/ProductSubCategoryComponent.vue').default);
Vue.component('calling-agent-form', require('./components/callingAgent/AddCallingAgentComponent.vue').default);
Vue.component('calling-agent-table', require('./components/callingAgent/CallingAgentTableComponent.vue').default);
Vue.component('call-center-agent-table', require('./components/callcenter/AgentTableComponent.vue').default);
Vue.component('auth-call-center-agent-table', require('./components/callcenter/AuthAgentTableComponent.vue').default);
Vue.component('order-table', require('./components/order/OrderTableComponent.vue').default);
Vue.component('report-call-center', require('./components/report/callcenter.vue').default);
Vue.component('order-report', require('./components/report/orderreport.vue').default);
Vue.component('mobile-app-report', require('./components/report/mobileapp.vue').default);
Vue.component('hub-report', require('./components/report/hubreport.vue').default);
Vue.component('rider-report', require('./components/report/riderreport.vue').default);
Vue.component('scheme-report', require('./components/report/schemereport.vue').default);




/**
 * calling scrtipt
 */
Vue.component('call-script-form', require('./components/callscript/AddCallScrioptComponent.vue').default);
Vue.component('call-script-table', require('./components/callscript/CallScriptTableComponent.vue').default);



Vue.component('stock-list', require('./components/Hub/StockListComponent.vue').default);
Vue.component('panel-master', require('./components/agentpanel/MasterPanelComponent.vue').default);
Vue.component('retailer-panel-master', require('./components/retailerpanel/MasterPanelComponent.vue').default);
Vue.component('retailer-queue', require('./components/agentqueue/RetailerQueueListComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
