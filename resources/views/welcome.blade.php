@extends('layouts.loginapp')

@section('content')
<?php
$admin = true;
$agent = $hub = false;
if($errors->has('agent')){
    $agent = true;
    $admin = false;

}
if(isset($agent) && $agent){
    $agent = true;
    $admin = false;

}
if(isset($hub) && $hub){
    $hub = true;
    $admin = false;

}
if($errors->has('hub')){
    $hub = true;
    $admin = false;

}

?>

<div class="d-flex flex-wrap login-section h-100">
    <div class="col-sm-5 p-4 d-flex flex-wrap align-content-center light-bg login-left">
        <div class="col-12 login-inner">
            <img src="{{asset('images/login-img.png')}}" alt="login-img" class="img-fluid" />
        </div>
    </div>
    <div class="col-sm-7 p-5 d-flex flex-wrap align-content-center bg-grey full-height login-right">
        <div class="col-md-10 login-form">
            <div class="row">
                <div class="col-12 mb-4 login-logo">
                    <img src="{{asset('images/logo.svg')}}" alt="logo" class="logo" />
                </div>
            </div>
            <div class="row">
                <div class="col-12 mt-4 mb-4 loginFormtab">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs border-0 loginSelect">
                        <li class="m-0 nav-item">
                            <a class="border-0 h5 py-3 px-4 font-weight-light m-0 nav-link {{ $admin ? 'active' : '' }}" data-toggle="tab" href="#admin">Admin Login</a>
                        </li>
                        <li class="m-0 nav-item">
                            <a class="border-0 h5 py-3 px-4 font-weight-light m-0 nav-link {{ $hub ? 'active' : '' }}" data-toggle="tab" href="#hub">Hub Login</a>
                        </li>
                        <li class="m-0 nav-item">
                            <a class="border-0 h5 py-3 px-4 font-weight-light m-0 nav-link {{ $agent ? 'active' : '' }}" data-toggle="tab" href="#agent">Agent Login</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane container {{ $admin ? 'active' : '' }}" id="admin">
                            <div class="row d-inline-block w-100 form-section">
                                <div class="mt-3 mb-4 pt-4 d-inline-block form-title">
                                    <h3 class="theme-text font-weight-light">Admin Login</h3>
                                    <p class="mb-4">Welcome Back! Please login with your details.</p>
                                </div>
                                <form method="POST" class="form-horizontal" id="loginform" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                                    {{ csrf_field() }}
                                    @if ($errors->has('email'))
                                    <div class="help-block mb-2 error-msg1">
                                        {{ $errors->first('email') }}
                                        <i class="las la-times hide-error" onclick="$(this).parent().hide();"></i>
                                    </div>
                                    @endif
                                    @if ($errors->has('messege'))
                                    <div class="help-block mb-2 error-msg1">
                                        {{ $errors->first('messege') }}
                                        <i class="las la-times hide-error" onclick="$(this).parent().hide();"></i>
                                    </div>
                                    @endif
                                    <div class="position-relative mb-5 form-group">
                                        <span class="input-icon"><img src="{{asset('images/sent-mail.svg')}}" alt="sent-mail" /></span>
                                        <input type="email" class="pl-4 rounded-0 form-control" id="email" placeholder="EMAIL" value="{{ old('email') }}" name="email" required />
                                        <div class="position-absolute invalid-feedback-login">Please fill out this field.</div>
                                    </div>
                                    <div class="position-relative mb-5 form-group">
                                        <span class="input-icon"><img src="{{asset('images/security-on.svg')}}" alt="security-on" /></span>
                                        <input type="password" class="pl-4 rounded-0 form-control" id="password" placeholder="PASSWORD" name="password" required />
                                        <div class="position-absolute invalid-feedback-login">Please fill out this field.</div>
                                    </div>
                                    <div class="text-center d-flex flex-wrap justify-content-between align-items-center submit">
                                        <a title="" class="font-weight-light theme-text forgot-text" href="{{ route('password.request') }}">Forgot Password?</a>
                                        <button title="Submit" type="submit" class="rounded-circle btn theme-bg text-white submit-btn"><i class="display-5 las la-arrow-right"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane container {{ $hub ? 'active' : '' }}" id="hub">
                            <div class="row d-inline-block w-100 form-section">
                                <div class="mt-3 mb-4 pt-4 d-inline-block form-title">
                                    <h3 class="theme-text font-weight-light">Hub Login</h3>
                                    <p class="mb-4">Welcome Back! Please login with your details.</p>
                                </div>
                                <form method="POST" class="form-horizontal" id="loginformhub" action="{{ route('hublogin') }}" aria-label="{{ __('Login') }}">
                                    {{ csrf_field() }}
                                    @if ($errors->has('email'))
                                    <div class="help-block mb-2 error-msg1">
                                        {{ $errors->first('email') }}
                                        <i class="las la-times hide-error" onclick="$(this).parent().hide();"></i>
                                    </div>
                                    @endif
                                    @if ($errors->has('messege'))
                                    <div class="help-block mb-2 error-msg1">
                                        {{ $errors->first('messege') }}
                                        <i class="las la-times hide-error" onclick="$(this).parent().hide();"></i>
                                    </div>
                                    @endif

                                    <div class="position-relative mb-5 form-group">
                                        <span class="input-icon"><img src="{{asset('images/sent-mail.svg')}}" alt="sent-mail" /></span>
                                        <input type="email" class="pl-4 rounded-0 form-control" id="emailhub" placeholder="EMAIL" value="{{ old('email') }}" name="email" required />
                                        <div class="position-absolute invalid-feedback-login">Please fill out this field.</div>
                                    </div>

                                    <div class="position-relative mb-5 form-group">
                                        <span class="input-icon"><img src="{{asset('images/security-on.svg')}}" alt="security-on" /></span>
                                        <input type="password" class="pl-4 rounded-0 form-control" id="passwordhub" placeholder="PASSWORD" name="password" required />
                                        <div class="position-absolute invalid-feedback-login">Please fill out this field.</div>
                                    </div>
                                    <div class="text-center d-flex flex-wrap justify-content-between align-items-center submit">
                                        <a title="" class="font-weight-light theme-text forgot-text" href="{{ route('hub.password.request') }}">Forgot Password?</a>
                                        <button title="Submit" type="submit" class="rounded-circle btn theme-bg text-white submit-btn"><i class="display-5 las la-arrow-right"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane container {{ $agent ? 'active' : '' }}" id="agent">
                            <div class="row d-inline-block w-100 form-section">
                                <div class="mt-3 mb-4 pt-4 d-inline-block form-title">
                                    <h3 class="theme-text font-weight-light">Agent Login</h3>
                                    <p class="mb-4">Welcome Back! Please login with your details.</p>
                                </div>
                                <form method="POST" class="form-horizontal" id="loginformagent"action="{{ route('agentlogin') }}" aria-label="{{ __('Login') }}">
                                    {{ csrf_field() }}
                                    @if ($errors->has('email'))
                                    <div class="help-block mb-2 error-msg1">
                                        {{ $errors->first('email') }}
                                        <i class="las la-times hide-error" onclick="$(this).parent().hide();"></i>
                                    </div>
                                    @endif
                                    @if ($errors->has('messege'))
                                    <div class="help-block mb-2 error-msg1">
                                        {{ $errors->first('messege') }}
                                        <i class="las la-times hide-error" onclick="$(this).parent().hide();"></i>
                                    </div>
                                    @endif
                                    <div class="position-relative mb-5 form-group">
                                        <span class="input-icon"><img src="{{asset('images/sent-mail.svg')}}" alt="sent-mail" /></span>
                                        <input type="email" class="pl-4 rounded-0 form-control" id="emailagent" placeholder="EMAIL" value="{{ old('email') }}" name="email" required />
                                        <div class="position-absolute invalid-feedback-login">Please fill out this field.</div>
                                    </div>
                                    <div class="position-relative mb-5 form-group">
                                        <span class="input-icon"><img src="{{asset('images/security-on.svg')}}" alt="security-on" /></span>
                                        <input type="password" class="pl-4 rounded-0 form-control" id="passwordagent" placeholder="PASSWORD" name="password" required />
                                        <div class="position-absolute invalid-feedback-login">Please fill out this field.</div>
                                    </div>
                                    <div class="text-center d-flex flex-wrap justify-content-between align-items-center submit">
                                        <a title="" class="font-weight-light theme-text forgot-text" href="{{ url('agent/password/reset') }}">Forgot Password?</a>
                                        <button title="Submit" type="submit" class="rounded-circle btn theme-bg text-white submit-btn"><i class="display-5 las la-arrow-right"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>



@endsection
