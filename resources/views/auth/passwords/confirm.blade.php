@extends('layouts.loginapp')

@section('content')


<div class="container-fluid">
    <div class="row align-content-center login-section">
        <div class="col-sm-5 p-4 d-flex flex-wrap align-content-center light-bg full-height login-left">
            <div class="col-12 login-inner">
                <img src="{{asset('images/login-img.png')}}" alt="login-img" class="img-fluid" />
            </div>
        </div>
        <div class="col-sm-7 p-5 d-flex flex-wrap align-content-center bg-grey full-height login-right">
            <div class="col-md-10 login-form">
                <div class="col-12 titles-logo">
                    <h1 class="d-inline-block font-weight-bold mb-5 theme-text text-logo">O<span class="text-dark">2</span>R</h1>
                </div>
                <div class="col-12 form-section">
                    <div class="mt-5 mb-4 d-inline-block form-title">
                        <h3 class="theme-text font-weight-light">Login</h3>
                        <p>Welcome Back! Please login with your details.</p>
                    </div>
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form class="form-horizontal" method="POST" action="{{ route('password.confirm') }">
                        @csrf
                         <input type="hidden" name="token" value="{{ $token }}">
                        <div class="position-relative mb-5 form-group">
                            <span class="input-icon"><img src="{{asset('images/sent-mail.svg')}}" alt="sent-mail" /></span>
                    <input type="password" class="pl-4 rounded-0 input-border form-control" id="password" placeholder="password"  name="password" required autocomplete="current-password"/>
                    @error('password')
                    <span class="invalid-feedback-login" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>

                <div class="text-center submit">
                    <button type="submit" class="rounded-circle btn theme-bg text-white submit-btn"><i class="display-5 la la-long-arrow-alt-down"></i></button>
                    @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                    @endif
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
