@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Manage Hub Users'" :loader="true"></header-page>

    <add-hub-form></add-hub-form>

    <hub-users-table></hub-users-table>

</div>
<!-- end -->

@endsection
