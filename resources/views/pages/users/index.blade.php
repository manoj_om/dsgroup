@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Manage Users'" :loader="true"></header-page>

    <add-user-form></add-user-form>

    <user-table></user-table>

</div>
<!-- end -->

@endsection
