@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    @if(Auth::guard('agent')->check() == false)
     <header-page :pagetitle="'Calling Agent Status'" :loader="true"></header-page>
    @endif
    @if(Auth::guard('agent')->check() == true)
    <header-guard-page :pagetitle="'Calling Agent Status'"></header-guard-page>
     @endif

     <call-center-agent-table></call-center-agent-table>
</div>
<!-- end -->
@endsection
