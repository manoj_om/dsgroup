@extends('layouts.agentpanel')

@section('content')
<panel-master :retailerid="{{ $rid }}" :type="'agent'"></panel-master>

@endsection
