@extends('layouts.agent')
@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-guard-page :pagetitle="'Calling Agent Dashboard'"></header-guard-page>

    <div class="row mt-4 mb-4 align-items-center agent-dashboard">
        <div class="col-md-12 pd-x-2 agentDash">
            <h4 class="d-inline-block font-weight-light mb-3">Life Till Date</h4>
            <div class="row mt-2 dash-grid">
                <div class="col-md-4">
                    <div class="px-3 py-4 mb-4 d-flex flex-wrap align-items-center text-white grid-inner grid-agent">
                        <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico"><i class="h2 m-0 las la-phone"></i></div>
                        <div class="grid-agent-info">
                            <h3 class="mb-0 font-weight-bold">{{ $connectedCalls }}</h3>
                            <span class="w-100 m-0 font-weight-light">Connected Calls</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="px-3 py-4 mb-4 d-flex flex-wrap align-items-center text-white grid-inner grid-queue">
                        <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico"><i class="h2 m-0 la la-wallet"></i></div>
                        <div class="grid-queue-info">
                            <h3 class="mb-0 font-weight-bold">{{ $orders }}<!--<font class="units">Lac</font>--></h3>
                            <span class="w-100 m-0 font-weight-light">Orders value</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="px-3 py-4 mb-4 d-flex flex-wrap align-items-center text-white grid-inner grid-orders">
                        <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico"><i class="h2 m-0 lar la-clock"></i></div>
                        <div class="grid-orders-info">
                            <h3 class="mb-0 font-weight-bold">36 <font class="units">hours</font> 40 <font class="units">Min</font></h3>
                            <span class="w-100 m-0 font-weight-light">Average Time Per call</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="px-3 py-4 mb-4 d-flex flex-wrap align-items-center text-white grid-inner grid-orders">
                        <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico"><i class="h2 m-0 lar la-calendar-alt"></i></div>
                        <div class="grid-orders-info">
                            <h3 class="mb-0 font-weight-bold">126 <font class="units">hours</font></h3>
                            <span class="w-100 m-0 font-weight-light">Attendance this month</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="px-3 py-4 mb-4 d-flex flex-wrap align-items-center text-white grid-inner grid-agent">
                        <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico"><i class="h2 m-0 las la-shopping-cart"></i></div>
                        <div class="grid-agent-info">
                            <h3 class="mb-0 font-weight-bold">150</h3>
                            <span class="w-100 m-0 font-weight-light">Order Conversion</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-white px-3 py-4">
                <h6 class="mb-4">Daily - Total Number of Calls, Connected Calls and Orders</h6>
                <div id="ttlcalls" class="pt-3"></div>
            </div>
            <div class="bg-white px-3 py-4 mt-4">
                <h6 class="mb-4">Daily Average Order Value for Past 6 Days</h6>
                <div id="past6months" class="pt-3"></div>
            </div>
        </div>
    </div>
</div>
<!-- end -->
@endsection
@section('script')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script>
    Highcharts.chart('past6months', {
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['1 Jan', '2 Jan', '3 Jan', '4 Jan', '5 Jan', '6 Jan']
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
                name: 'Average Order Value',
                data: [10, 12, 5, 14.5, 18.4, 16]
            }],
        "colors": [
            "#fb5276"
        ]
    });

    Highcharts.chart('ttlcalls', {
         chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                '1 Jan',
                '2 Jan',
                '3 Jan',
                '4 Jan',
                '5 Jan',
                '6 Jan'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Calls',
            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5]

        },{
            name: 'Connected Calls',
            data: [40, 50, 60, 70, 80, 90]

        }, {
            name: 'Order',
            data: [ 176.0, 99, 71.5, 106.4, 129.2, 144.0]

        }],
        "colors": [
            "#fb5276",
            "#229802",
            "#0091d5"
        ]
    });
</script>
@endsection
