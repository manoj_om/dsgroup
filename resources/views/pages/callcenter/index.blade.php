@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
       @if(Auth::guard('agent')->check() == false)
    <header-page :pagetitle="'Call Setting'" :loader="false"></header-page>
    @endif
       @if(Auth::guard('agent')->check() == true)
    <header-guard-page :pagetitle="'Call Setting'"></header-guard-page>
    @endif
    <div class="row">
        <div class="col-md-12 pd-x-2 mt-4 call-setting">
            <form method="" action="" id="manage-user" class="bg-white mt-2 pb-5 rounded border light-border p-4 manage-user">
                <div class="row pt-3 pb-1 px-3">
                  <div class="col-md-9 fields field1">
                      <div class="col-12">
                          <label for="designation">Callcenter DID <sup class="text-danger font-weight-bold h6">*</sup></label>
                          <div class="row align-items-center">
                              <div class="col-md-10">
                                  <select name="min-order" class="w-100 px-3 pr-5 select-shadow rounded-lg light-border show-data">
                                      <option value="">Enter DID number of callcenter</option>
                                      <option value="">Open Order Page From Mobile Email</option>
                                  </select>
                              </div>
                              <div class="col-md-2 px-0">
                                  <button type="submit" class="h6 text-uppercase d-inline-block w-100 m-0 py-2 rounded text-white theme-bg border-btn save-btn">SAVE</button>
                              </div>
                          </div>
                      </div>
                      <div class="d-inline-block px-4 ml-1 mt-3 agreediv">
                          <label class="d-flex checklabel">
                              <input type="checkbox" name="selectcheck" id="selectall" class="checkbox-select"> <span class="checkmark"></span>
                              <small class="font-weight-light">I agree to change the existing DID</small>
                          </label>
                      </div>
                  </div>
                </div>
                <div class="row pt-3 pb-1 px-3">
                  <div class="col-md-9 fields field1">
                      <div class="col-12">
                          <label for="designation">Call Scheduling to Retailers <sup class="text-danger font-weight-bold h6">*</sup></label>
                          <div class="row align-items-center">
                              <div class="col-md-10">
                                  <select name="min-order" class="w-100 px-3 pr-5 select-shadow rounded-lg light-border show-data">
                                      <option value="">Manual Scheduling</option>
                                      <option value="">Open Order Page From Mobile Email</option>
                                  </select>
                              </div>
                              <div class="col-md-2 px-0">
                                  <button type="submit" class="h6 text-uppercase d-inline-block w-100 m-0 py-2 rounded text-white theme-bg border-btn save-btn">SAVE</button>
                              </div>
                          </div>
                      </div>
                      <div class="d-inline-block px-4 ml-1 mt-3 agreediv">
                          <label class="d-flex checklabel">
                              <input type="checkbox" name="selectcheck" id="selectall" class="checkbox-select"> <span class="checkmark"></span>
                              <small class="font-weight-light">I agree to change the scheduling setting</small>
                          </label>
                      </div>
                  </div>
                </div>
                <div class="row pt-3 pb-1 px-3">
                  <div class="col-md-9 fields field1">
                      <div class="col-12">
                          <label for="designation">Radiate API <sup class="text-danger font-weight-bold h6">*</sup></label>
                          <div class="row align-items-center">
                              <div class="col-md-10">
                                  <select name="min-order" class="w-100 px-3 pr-5 select-shadow rounded-lg light-border show-data">
                                      <option value="">Select API used for Radiate call center communication</option>
                                      <option value="">Open Order Page From Mobile Email</option>
                                  </select>
                              </div>
                              <div class="col-md-2 px-0">
                                  <button type="submit" class="h6 text-uppercase d-inline-block w-100 m-0 py-2 rounded text-white theme-bg border-btn save-btn">SAVE</button>
                              </div>
                          </div>
                      </div>
                      <div class="d-inline-block px-4 ml-1 mt-3 agreediv">
                          <label class="d-flex checklabel">
                              <input type="checkbox" name="selectcheck" id="selectall" class="checkbox-select"> <span class="checkmark"></span>
                              <small class="font-weight-light">I agree to change the API setting</small>
                          </label>
                      </div>
                  </div>
                </div>
                <div class="row pt-3 pb-1 px-3">
                  <div class="col-md-12">
                    <div class="col-12">
                      <h6 class="font-weight-normal mb-3">Activate Scheduling Process</h6>
                      <button class="h6 text-uppercase w-25 py-2 mr-2 d-inline-block rounded theme-text border-btn bg-transaprent cancel-btn">Reschedule Calls</button>
                      <small>System will start rescheduling process in a min</small>
                    </div>
                  </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end -->
@endsection
