@extends('layouts.agentpanel')

@section('content')

<panel-master :retailerid="{{ $rid }}" :type="'retailer'"></panel-master>
@endsection
