@extends('layouts.app')

@section('content')
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Retailers Queue List'" :loader="false"></header-page>
    <retailer-queue :isadmin="true"></retailer-queue>
</div>

@endsection
