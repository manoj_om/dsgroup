@extends('layouts.hub')

@section('content')

<?php // echo "<pre>"; print_r($product);exit;  ?>
<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list dashboard-page">

    <hub-header-page :pagetitle="'Stock Details'"></hub-header-page>
    <div class="row details">
        <div class="col-md-12 pd-x-2 pd-y-2 my-3 stock-details">
            <div class="p-4 bg-white">
                <div class="detailStock">
                    <div class="row">
                        <div class="col-md-3 pr-2 stock-view">
                            <span class="d-inline-block w-100 retail-view-profile border light-border p-1">

                                @if($product['avtar'] != '')
                                <img src="{{asset('images/product')}}/{{ $product['avtar'] }}" alt="Stock-view -image" class="img-fluid w-100 h-100 retail-view-profile-img">  
                                @else
                                <img src="{{asset('images/no-image.jpg')}}" alt="Stock-view -image" class="img-fluid w-100 h-100 retail-view-profile-img">
                                @endif
                            </span>
                        </div>
                        <div class="col-md-9 inner-stock">
                            <h5 class="mb-3 detail-name">{{ $product['product_name']['name'] }}</h5>
                            <div class="row mt-3">
                                <div class="col-sm-4 mb-1 info">
                                    <span class="details-heading">Manufacturer</span>
                                    @if($product['manufacturer'])
                                    <p class="mb-2 details">{{ $product['manufacturer'] }}</p>
                                    @else
                                    <p class="mb-2 details">N/A</p>
                                    @endif
                                </div>
                                <div class="col-sm-4 mb-1 info">
                                    <span class="details-heading">Brand</span>
                                    @if($product['brand_name'])
                                    <p class="mb-2 details">{{ $product['brand_name'] }}</p>
                                    @else
                                    <p class="mb-2 details">N/A</p>
                                    @endif
                                </div>
                                <div class="col-sm-4 mb-1 info">
                                    <span class="details-heading">SKU code</span>
                                    <p class="mb-2 details">{{ $product['sku_number'] }}</p>
                                </div>
                                <div class="col-sm-4 mb-1 info">
                                    <span class="details-heading">HSN Code</span>
                                    @if($product['hsn_code'])
                                    <p class="mb-2 details">{{ $product['hsn_code'] }}</p>
                                    @else
                                    <p class="mb-2 details">N/A</p>
                                    @endif
                                </div>
                                <div class="col-sm-4 mb-1 info">
                                    <span class="details-heading">EAN Code</span>
                                    @if($product['ean_code'])
                                    <p class="mb-2 details">{{ $product['ean_code'] }}</p>
                                    @else
                                    <p class="mb-2 details">N/A</p>
                                    @endif
                                </div>
                                <div class="col-sm-4 mb-1 info">
                                    <span class="details-heading">Status</span>
                                    @if($product['status'] == 0)
                                    <p class="mb-2 details">Active</p>
                                    @else
                                    <p class="mb-2 details">Inactive</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row price-info">
                                <div class="col-sm-4 mb-1 info">
                                    <span class="details-heading">DLP(Rs)</span>
                                    @if($product['dlp'])
                                    <p class="mb-2 details">{{ $product['dlp'] }}</p>
                                    @else
                                    <p class="mb-2 details">N/A</p>
                                    @endif
                                </div>
                                <div class="col-sm-4 mb-1 info">
                                    <span class="details-heading">RLP(Rs)</span>
                                    @if($product['rlp'])
                                    <p class="mb-2 details">{{ $product['rlp'] }}</p>
                                    @else
                                    <p class="mb-2 details">N/A</p>
                                    @endif
                                </div>
                                <div class="col-sm-4 mb-1 info">
                                    <span class="details-heading">MRP(Rs)</span>
                                    @if($product['mrp'])
                                    <p class="mb-2 details">{{ $product['mrp'] }}</p>
                                    @else
                                    <p class="mb-2 details">N/A</p>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12 mb-2 info">
                                <h6 class="font-weight-bold">Short Description</h6>
                                @if($product['short_description'])
                                <p class="m-0">{{ $product['short_description'] }}.</p>
                                @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                        </div>
                        <div class="row mt-3 border-top pt-3">
                            <div class="col-sm-3">
                                <span class="details-heading">Product Unit</span>
                                @if($product['unit_type']['name'])
                                <p class="mb-2 details">{{ $product['unit_type']['name'] }}</p>
                                 @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Packing Type</span>
                                @if($product['packing_type']['name'])
                                <p class="mb-2 details">{{ $product['packing_type']['name'] }}</p>
                                 @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Product Weight Type</span>
                                @if($product['weight_type']['name'])
                                <p class="mb-2 details"> {{ $product['weight_type']['name'] }} </p>
                                @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">No of unit in box</span>
                                @if($product['quantity'])
                                <p class="mb-2 details">{{ $product['quantity'] }}</p>
                                 @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Category</span>
                                @if($product['category']['name'])
                                <p class="mb-2 details">{{ $product['category']['name'] }}</p>
                                 @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Subcategory</span>
                                @if( $product['subcategory']['name'])
                                <p class="mb-2 details">{{ $product['subcategory']['name'] }}</p>
                                 @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Max order Qty</span>
                                @if($product['max_order_quantity'])
                                <p class="mb-2 details">{{ $product['max_order_quantity'] }}</p>
                                 @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Product Length</span>
                                @if($product['product_length'])
                                <p class="mb-2 details">{{ $product['product_length'] }}</p>
                                @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>

                            <div class="col-sm-3">
                                <span class="details-heading">Product Width</span>
                                @if($product['product_width'])
                                <p class="mb-2 details">{{ $product['product_width'] }}</p>
                                @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Product Height</span>
                                @if($product['product_height'])
                                <p class="mb-2 details">{{ $product['product_height'] }}</p>
                                 @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Shelf Life</span>
                                @if($product['shelf_life'])
                                <p class="mb-2 details">{{ $product['shelf_life'] }}</p>
                                 @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Customer Target</span>
                                @if($product['master_carton_gross_weight'])
                                <p class="mb-2 details">{{ $product['custmer_target'] }}</p>
                                  @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Master Carton Gross Weight</span>
                                @if($product['master_carton_gross_weight'])
                                <p class="mb-2 details">{{ $product['master_carton_gross_weight'] }}</p>
                                @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Master Carton Net Weight</span>
                                @if($product['master_carton_net_weight'])
                                <p class="mb-2 details">{{ $product['master_carton_net_weight'] }}</p>
                                 @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Master Carton Length</span>
                                @if($product['master_carton_length'])
                                <p class="mb-2 details">{{ $product['master_carton_length'] }}</p>
                                 @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Master Carton Width</span>
                                @if($product['master_carton_width'])
                                <p class="mb-2 details">{{ $product['master_carton_width'] }}</p>
                                @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading">Master Carton Height</span>
                                @if($product['master_carton_height'])
                                <p class="mb-2 details">{{ $product['master_carton_height'] }}</p>
                                 @else
                                <p class="mb-2 details">N/A</p>
                                @endif
                            </div>                        
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12 mb-2 info border-top pt-3 pb-2">
                                <h6 class="font-weight-bold">In Stock & Threshold Qunatity</h6>
                            </div>
                            <div class="col-md-4 mb-2 info">
                                <span class="details-heading">In Stock</span>                                    
                                <?php
                                $pr = 0;
                                if ($product['stock'] != null && $product['stock'] != '' && $product['stock']['in_stock'] != null) {
                                    $pr = $product['stock']['in_stock'];
                                }
                                ?>
                                <stock-quantity :productId="{{ $product['id'] }}" :type="1" :availability="{{ $pr }}" ></stock-quantity> 

                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading threshold-two" style="background-color: #ffc6c5">Threshold Quantity 1</span>                                    
                                <?php
                                $pr1 = 0;
                                if ($product['stock'] != null && $product['stock'] != '' && $product['stock']['threshold_quantity_one'] != null) {
                                    $pr1 = $product['stock']['threshold_quantity_one'];
                                }
                                ?>
                                <stock-quantity :productId="{{ $product['id'] }}" :type="2"  :availability="{{ $pr1 }}" ></stock-quantity> 

                            </div>
                            <div class="col-sm-3">
                                <span class="details-heading threshold-one" style="background-color: #ffc18f">Threshold Quantity 2</span>                                    
                                <?php
                                $pr2 = 0;
                                if ($product['stock'] != null && $product['stock'] != '' && $product['stock']['threshold_quantity_two'] != null) {
                                    $pr2 = $product['stock']['threshold_quantity_two'];
                                }
                                ?>
                                <stock-quantity :productId="{{ $product['id'] }}" :type="3" :availability="{{ $pr2 }}" ></stock-quantity> 

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- end -->

<!-- The Modal -->



@endsection
