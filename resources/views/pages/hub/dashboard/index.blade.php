@extends('layouts.hub')
<link href="https://code.highcharts.com/css/highcharts.css" rel="stylesheet"/>
@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list hub-dashboard-page">
    <hub-header-page :pagetitle="'Hub Manage Stock'"></hub-header-page>
    <div class="row mt-4 mb-4 align-items-center">
        <div class="col-md-12 pd-x-2">
        <h5 class="d-inline-block font-weight-light mb-3">Today Activity ({{ date('d M Y')}})</h5>
            <div class="row mt-2 hubDash-grid">
                <div class="col-md-3">
                    <div class="col-sm-12 px-3 py-4 d-flex flex-wrap align-items-center text-white grid-inner grid-agent">
                        <div class="d-flex align-items-center justify-content-center mr-2 rounded-circle grid-ico">
                            <i class="h2 la m-0 la-luggage-cart"></i>
                        </div>
                        <div class="d-grid grid-hub-info">
                            <h3 class="mb-0 font-weight-bold">{{$orderCount}}</h3>
                            <span class="w-100 m-0 font-weight-light">Order Received</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-sm-12 px-3 py-4 d-flex flex-wrap align-items-center text-white grid-inner grid-queue">
                        <div class="d-flex align-items-center justify-content-center mr-2 rounded-circle grid-ico">
                            <i class="h2 la m-0 la-luggage-cart"></i>
                        </div>
                        <div class="d-grid grid-hub-info">
                            <h3 class="mb-0 font-weight-bold">{{ $todayorderProcessed }}</h3>
                            <span class="w-100 m-0 font-weight-light">Order Processed</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-sm-12 px-3 py-4 d-flex flex-wrap align-items-center text-white grid-inner grid-orders">
                        <div class="d-flex align-items-center justify-content-center mr-2 rounded-circle grid-ico">
                            <i class="h2 la m-0 la la-wallet"></i>
                        </div>
                        <div class="d-grid grid-hub-info">
                            <h3 class="mb-0 font-weight-bold">{{ $totalPrice }}</h3>
                            <span class="w-100 m-0 font-weight-light">Order Amount</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-sm-12 px-3 py-4 d-flex flex-wrap align-items-center text-white grid-inner grid-collect">
                        <div class="d-flex align-items-center justify-content-center mr-2 rounded-circle grid-ico">
                            <i class="h2 la m-0 las la-rupee-sign"></i>
                        </div>
                        <div class="d-grid grid-hub-info">
                            <h3 class="mb-0 font-weight-bold">{{ $collectedAmount }}</h3>
                            <span class="w-100 m-0 font-weight-light">Amount Collected</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row mt-4 mb-4 align-items-center">
        <div class="col-md-12 pd-x-2">
            <div class="row mt-2">
                <div class="col-sm-6">
                    <div class="bg-white px-3 pt-4">
                        <h6 class="mb-3">Order Summary (Status Wise)</h6>
                        <div id="ordersummary" class="pt-3" style="height: 350px"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="bg-white px-3 pt-4">
                        <h6 class="mb-3">Order History(Last 6 Days)</h6>
                        <div id="ttlcalls" class="pt-3" style="height: 350px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-5">
      <div class="col-md-12 mt-4 pd-x-2">
        <div class="row mt-2 bottom-grid-sec">
          <div class="col-sm-4">
            <div class="col-12 px-2 py-3 d-flex flex-wrap align-items-center bg-white shadow bottom-grid">
              <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico">
                <i class="h2 m-0 la la-wallet"></i>
              </div>
              <div class="text-misc">
                    <h3 class="mb-0 font-weight-bold">{{ $totalOrdersPrice }} {{--<font class="units">Lac</font> --}}</h3>
                    <span class="w-100 m-0 font-weight-light">Order value (MTD)</span>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="col-12 px-2 py-3 d-flex flex-wrap align-items-center bg-white shadow bottom-grid">
              <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico">
                <i class="h2 la m-0 la-user-tie"></i>
              </div>
              <div class="text-misc">
                    <h3 class="mb-0 font-weight-bold">{{ $totalProcessed }}</h3>
                    <span class="w-100 m-0 font-weight-light">Order Processed (MTD)</span>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="col-12 px-2 py-3 d-flex flex-wrap align-items-center bg-white shadow bottom-grid">
              <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico">
                <i class="h2 la m-0 la-store-alt"></i>
              </div>
              <div class="text-misc">
                    <h3 class="mb-0 font-weight-bold">{{ $retailersAssigned }}</h3>
                    <span class="w-100 m-0 font-weight-light">Retailer Assigned</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--<div class="row">
        <div class="col-md-12 pd-x-2">
            <div class="col-12 p-0 inner">
                <h5 class="d-inline-block font-weight-light my-3">Recent Order Received</h5>
                <div class="border border-left-0 border-right-0 border-bottom-0 recent-orderTB">
                    <table cellspacing="0" class="table table-borderless mt-3 table-list ordered-list">
                        <thead>
                            <tr>
                                <th class="pl-4">Order Date</th>
                                <th>Order No</th>
                                <th>Call Center/Mobile App</th>
                                <th>Retailer</th>
                                <th>Mobile No.</th>
                                <th>Location</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="bg-white py-4 pl-4 border border-right-0 border-right-0 light-border"><span class="data">30-10-2019 <small class="text-success font-weight-bold">16:41</small></span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">DSG00254854</span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Asha Kumari/9563214578</span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Rajesh</span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">9532145678</span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Noida</span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Received</span></td>
                                <td class="bg-white p-3 border border-left-0 rounded-right light-border">
                                    <span class="actions">
                                        <a href="#" title="View" data-toggle="modal" data-target="#myModal" class="action-btn">
                                            <img src="http://localhost/o2r/public/images/view-icon.png" alt="view" class="action-icon">
                                        </a>
                                    </span>
                                </td>
                            </tr>
                            <tr class="separation"><td class="p-0">&nbsp;</td></tr>

                            <tr>
                                <td class="bg-white py-4 pl-4 border border-right-0 border-right-0 light-border"><span class="data">30-10-2019 <small class="text-success font-weight-bold">16:41</small></span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">DSG00254854</span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Asha Kumari/9563214578</span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Rajesh</span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">9532145678</span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Noida</span></td>
                                <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Received</span></td>
                                <td class="bg-white p-3 border border-left-0 rounded-right light-border">
                                    <span class="actions">
                                        <a href="#" title="View" data-toggle="modal" data-target="#myModal" class="action-btn">
                                            <img src="http://localhost/o2r/public/images/view-icon.png" alt="view" class="action-icon">
                                        </a>
                                    </span>
                                </td>
                            </tr>
                            <tr class="separation"><td class="p-0">&nbsp;</td></tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>-->
</div>
<!-- end -->

<!-- The Modal -->
@endsection
@section('script')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
    //Pie Chart ==> Retail Type Wise Order Value
    $(document).ready(function () {

        var options = {
            chart: {
                renderTo: 'ordersummary',
                styledMode: false
            },
            title: {
                text: ''
            },

            series: [{
                name: 'Order Summary',
                type: 'pie',
                allowPointSelect: true,
                keys: ['name', 'y', 'selected', 'sliced'],
                data: [],
                showInLegend: true
            }],
            "colors": [
                "#f26419",
                "#f6ae2d",
                "#0daffc",
                "#fb5276"
            ]
        };

        $.ajax({
            url: '{{url('hub/order/pie-chart')}}',
            type: 'GET',
            async: true,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: OnSuccess,
            error: OnError
        });

        function OnSuccess(data) {
            console.log(data);
            options.series[0].data = data;
            chart = new Highcharts.Chart(options);

        }

        function OnError(data) {
            console.log('fail');
        }
        var optionsChart = {

            chart: {
                renderTo : 'ttlcalls',
                type: 'column',
                styledMode: true
            },

            title: {
                text: ''
            },
            yAxis: [{
                className: 'highcharts-color-0',
                title: {
                    text: 'No. of Order'
                }
            }, {
                className: 'highcharts-color-1',
                opposite: true,
                title: {
                    text: 'Order Amount'
                }
            }],
            xAxis : {
                categories : []
            },

            plotOptions: {
                column: {
                    borderRadius: 5
                }
            }

        };
        $.ajax({
            url: '{{url('hub/order/last-sixday-pie-chart')}}',
            type: 'GET',
            async: true,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                console.log(data);
                optionsChart.series = data['yAxis'];
                optionsChart.xAxis.categories = data['xAxis'];
                chart = new Highcharts.Chart(optionsChart);
            },
            error: OnError
        });
    });

    /*Highcharts.chart('ttlcalls', {
         chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                '01/01',
                '02/01',
                '03/01',
                '04/01',
                '05/01',
                '06/01'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.valuePrefix}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: false,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Orders',
            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5]

        },{
            name: 'Price',
            data: [40, 50, 60, 70, 80, 90],
            tooltip: {
            valuePrefix: '$'
        }

        }],
        "colors": [
            "#fb5276",
            "#229802"
        ]
    });*/


</script>
@endsection
