@extends('layouts.hub')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
   
     <hub-header-page :pagetitle="'Hub Profile'"></hub-header-page>
      <div class="row position-relative profile-page hubProfile">
        <div class="col-12 pt-4 profile-inner">
            <div class="row pd-x-2 pt-3 position-relative">
              <div class="col-md-6 p-0 pt-3 d-flex flex-wrap text-center profile-detail">
                  <hub-image-upload></hub-image-upload>
                <span class="mt-5"><h5 class="text-white text-capitalize font-weight-normal">{{ Auth::user()->name }}</h5></span>
              </div>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="container">
          <div class="col-md-8 offset-md-2 hub-profile">
            <div class="row">
              <div class="col-6 pl-5 mb-4">
                <h6 class="m-0 text-grey-dark font-weight-light">Hub Name :</h6>
                <h5 class="text-body">{{ Auth::guard('hub')->user()->distributor_name}}</h5>
              </div>
              <div class="col-6 pl-5 mb-4">
                <h6 class="m-0 text-grey-dark font-weight-light">Mobile :</h6>
                <h5 class="text-body">{{ Auth::guard('hub')->user()->mobile }}</h5>
              </div>
              <div class="col-6 pl-5 mb-4">
                <h6 class="m-0 text-grey-dark font-weight-light">Email Id :</h6>
                <h5 class="text-body">{{ Auth::guard('hub')->user()->email }}</h5>
              </div>
              <div class="col-6 pl-5 mb-4">
                <h6 class="m-0 text-grey-dark font-weight-light">Hub Code :</h6>
                <h5 class="text-body">{{ Auth::guard('hub')->user()->distributor_code }}</h5>
              </div>
              <div class="col-6 pl-5 mb-4">
                <h6 class="m-0 text-grey-dark font-weight-light">PAN :</h6>
                <h5 class="text-body">{{ Auth::guard('hub')->user()->pan_card }}</h5>
              </div>
              <div class="col-6 pl-5 mb-4">
                <h6 class="m-0 text-grey-dark font-weight-light">GST :</h6>
                <h5 class="text-body">{{ Auth::guard('hub')->user()->gst_no }}</h5>
              </div>
              <div class="col-6 pl-5 mb-4">
                <h6 class="m-0 text-grey-dark font-weight-light">Aadhar No :</h6>
                <h5 class="text-body">{{ Auth::guard('hub')->user()->aadhar_no }}</h5>
              </div>
              <div class="col-6 pl-5 mb-4">
                <h6 class="m-0 text-grey-dark font-weight-light">Website :</h6>
                <h5 class="text-body">{{ Auth::guard('hub')->user()->website }}</h5>
              </div>
              <div class="col-6 pl-5 mb-4">
                <h6 class="m-0 text-grey-dark font-weight-light">Latitude :</h6>
                <h5 class="text-body">{{ Auth::guard('hub')->user()->latitude }}</h5>
              </div>
              <div class="col-6 pl-5 mb-4">
                <h6 class="m-0 text-grey-dark font-weight-light">Longitude :</h6>
                <h5 class="text-body">{{ Auth::guard('hub')->user()->longitude }}</h5>
              </div>
              <div class="col-md-12 pl-5 mb-4">
                <h6 class="m-0 text-grey-dark font-weight-light">Address :</h6>
                <h5 class="text-body">{{ Auth::guard('hub')->user()->address }}</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
<!-- end -->

@endsection
