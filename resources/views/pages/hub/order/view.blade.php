@extends('layouts.hub')

@section('content')
<?php // echo "<pre>"; print_r($data);exit;  ?>
<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list dashboard-page">

    <hub-header-page :pagetitle="'Order Details'"></hub-header-page>
     <?php $duration = new App\Orders;  ?>
    <div class="row">
        <div class="col-md-12 pd-x-2 mt-3 {{ $data['status'] == 0 ? 'cancelled' : '' }} ">
            <div class="bg-white px-3 pt-3">
                <table cellspacing="0" class="table layout-fixed table-borderless mt-3 border border-top-0 border-left-0 border-right-0 pb-3 ordered-list">
                    <thead>
                        <tr>
                            <th class="p-0">Order No :</th>
                            <th class="p-0">Order Date :</th>
                            <th class="p-0">Delivery Date/Time :</th>
                            <th class="p-0">Delivery Slot :</th>
                            <th class="p-0">Amount(RS) :</th>
                            <th class="p-0">Review :</th>
                            <th class="p-0">Status :</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="p-0">{{ $data['order_number'] }}</td>
                            <td class="p-0">{{ \Carbon\Carbon::parse($data['created_at'])->format('d/m/Y') }}</td>
                            <td class="p-0">{{ \Carbon\Carbon::parse($data['delivery_time'])->format('d/m/Y H:i') }}</td>
                            <td class="p-0">{{ $duration->findTimeSlot( $data['delivery_time']) }}</td>
                            <td class="p-0">{{round($data['total_amount'], 2)}}</td>
                            <td class="p-0">
                                <?php if($data['ratings'] != '' && $data['ratings'] != null) {  ?>
                                <star-rating value="{{ $data['ratings']['stars'] }}" disabled="true"></star-rating>
                                <?php } else {  ?>
                             
                                <star-rating value="0" disabled="true"></star-rating>
                                <?php } ?>
                                {{ $data['ratings']['comment'] }} 
                            </td>
                            <td class="p-0">@if($data['status'] == 1)
                                <p class="mb-1">Order Placed</p>
                                @elseif($data['status'] == 2)
                                <p class="mb-1">Packed</p>
                                @elseif($data['status'] == 3)
                                <p class="mb-1">Dispatched</p>
                                @elseif($data['status'] == 4)
                                <p class="mb-1">Delivered</p>
                                @elseif($data['status'] == 0)
                                <p class="mb-1 text-danger">Cancelled</p>
                                @endif</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="col-md-12 bg-white p-3 my-4 tracking">
                <div class="row order-status-outer">
                    <div class="col-sm-3 order-status-inner {{ $data['status']  >=  '1' ? 'status-done' : ''  }}">
                        <h6>Order Placed</h6>
                        <div class="status-circle"></div>
                    </div>
                   
                    <div class="col-sm-3 order-status-inner {{ $data['status'] >=  '2'  ? 'status-done' : ''  }} {{   $duration->timeDuration($data['packed_time'],$data['created_at'],1) && $data['status'] >=  '2' ? 'status-red' : '' }}">
                        <h6>Packed</h6>
                        <div class="status-circle"></div>
                    </div>
                    <div class="col-sm-3 order-status-inner {{ $data['status'] >=  '3'  ? 'status-done' : ''  }} {{ $duration->timeDuration($data['dispatch_time'],$data['packed_time'],2) && $data['status'] >=  '3' ? 'status-red' : '' }}">
                        <h6>Dispatched</h6>
                        <div class="status-circle"></div>
                    </div>
                    <div class="col-sm-3 order-status-inner {{ $data['status'] ==  '4'  ? 'status-done' : ''  }} {{ $duration->timeDuration($data['delivered_time'],$data['dispatch_time'],3) &&  $data['status'] >=  '4' ? 'status-red' : '' }}">
                        <h6>Delivered</h6>
                        <div class="status-circle"></div>
                    </div>
                </div>
            </div>
            
             @if($data['promocode'] != '')
            <div class="bg-white p-3 my-4">
                <div class="col-md-12">
                    <div class="row hub-manager-order">
                        <div class="col-md-4 mt-1 hub-order">
                            <div class="w-75 hubOrder-inner">
                                <h6 class="font-weight-bold mb-3">Promocode</h6>
                                <p class="mb-2 details">{{ $data['promocode'] }}</p>
                            </div>
                        </div>
                        <div class="col-md-4 mt-1 hub-order">
                            <div class="w-75 hubOrder-inner">
                                <h6 class="font-weight-bold mb-3">Promocode details</h6>
                                <p class="mb-2 details">{{ $data['promodetail']['message'] }}</p>
                            </div>
                        </div>
                        <div class="col-md-4 mt-1 hub-order">
                            <div class="w-75 hubOrder-inner">
                                <h6 class="font-weight-bold mb-3">Discount Amount</h6>
                                <p class="mb-2 details">{{$data['promodetail']['discount'] }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
              @endif
            <div class="bg-white p-3 my-4">
                <div class="col-md-12">
                    <div class="row hub-manager-order">
                        <div class="col-md-4 mt-1 hub-order">
                            <div class="w-75 hubOrder-inner">
                                <h6 class="font-weight-bold mb-3">Retailer</h6>
                                <span class="details-heading">Name</span>
                                <p class="mb-2 details">{{ $data['retailer']['full_name'] }}</p>
                                <span class="details-heading">Mobile Number</span>
                                <p class="mb-2 details">{{  $data['retailer']['primary_mobile_number'] }}</p>
                                <span class="details-heading">Location</span>
                                <p class="mb-2 details">{{ $data['retailer']['shop_address'] }}</p>
                                <span class="details-heading">Latitude & Longitude</span>
                                <p class="mb-2 details">{{ $data['retailer']['latitudes'] }} , {{  $data['retailer']['longitude'] }}</p>
                            </div>
                        </div>
                        <div class="col-md-4 hub-order">
                            <div class="w-75 m-auto hubOrder-inner">
                                <h6 class="font-weight-bold mb-3">Hub</h6>
                                <span class="details-heading">Name</span>
                                <p class="mb-2 details">{{ $data['retailer']['map_distributor']['distributor_data']['distributor_name']}}</p>
                                <span class="details-heading">Mobile Number</span>
                                <p class="mb-2 details">{{  $data['retailer']['map_distributor']['distributor_data']['mobile'] }}</p>
                                <span class="details-heading">Location</span>
                                <p class="mb-2 details">{{  $data['retailer']['map_distributor']['distributor_data']['address']}}</p>
                                 <span class="details-heading">Latitude & Longitude</span>
                                <p class=" details">{{ $data['retailer']['map_distributor']['distributor_data']['latitude'] }} , {{ $data['retailer']['map_distributor']['distributor_data']['longitude'] }}</p>
                            </div>
                        </div>
                        <div class="col-md-4 hub-order">
                            <div class="w-75 m-auto hubOrder-inner">
                                <h6 class="font-weight-bold mb-3">Delivery Agent</h6>
                                <span class="details-heading">Mobile Number</span>
                                <p class="mb-2 details">{{ $data['apiorder']['rider_phone']}}</p>
                                <span class="details-heading">Name</span>
                                <p class="mb-2 details">{{ $data['apiorder']['rider_name'] }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-white p-3 my-4">
                <order-detaile-table :orderdata="{{ json_encode($data) }}"></order-detaile-table>
            </div>
        </div>
    </div>
</div>
<!-- end -->





@endsection
