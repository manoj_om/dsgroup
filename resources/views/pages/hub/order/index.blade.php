@extends('layouts.hub')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list dashboard-page">
 <hub-header-page :pagetitle="'Hub Manage Order'" :loader="true"></hub-header-page>

 <hub-order-table></hub-order-table>
  </div>


@endsection
