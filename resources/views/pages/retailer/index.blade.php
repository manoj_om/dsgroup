@extends('layouts.app')

@section('content')

<retailer-list></retailer-list>
<!-- end -->

<script type="application/javascript">
    var singlelog = false;
    var show_url="<?php echo config('app.url'); ?><?php if ($_SERVER['HTTP_HOST'] == 'localhost') echo "public/"; ?>";
    window.addEventListener('DOMContentLoaded', function() {
    $("#is_state").html("<?php echo $state ? $state : '' ?>");
    $("#is_city").html();
   
    $("#is_address").html();
    jQuery(function($) {
  // Asynchronously Load the map API
    var script = document.createElement('script');
    script.src = "http://maps.googleapis.com/maps/api/js?callback=initMap&key=AIzaSyDvGdNTcLTe333c_bdBk7gjXg2Yi7RSsBA";
    document.body.appendChild(script);
    });
    
    });

function initMap(markers_id,infoWindowContent_id) {
    //undefined
  var t_markers_id= typeof markers_id ? markers_id:''; 
  var t_infoWindowContent_id= typeof infoWindowContent_id ? infoWindowContent_id:''; 
  var map;
  var bounds = new google.maps.LatLngBounds();
  var mapOptions = {
      mapTypeId: 'roadmap'
  };

  // Display a map on the page
  map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
  map.setTilt(45);

  // Multiple Markers
  if(!t_markers_id && !t_infoWindowContent_id){
  var markers = [

<?php foreach ($data as $reocrds) {
    if (isset($reocrds->latitudes) && isset($reocrds->longitude)) { ?>
                            ["<?php echo $reocrds->shop_address ? $reocrds->shop_address : ''; ?> <?php echo $reocrds->latitudes; ?> ",
                                "<?php echo $reocrds->latitudes ?>","<?php echo $reocrds->longitude ?>"],
                                  
    <?php }
} ?>

  ];

  // Info Window Content
  var infoWindowContent = [

<?php foreach ($data as $reocrds) {
    if (isset($reocrds->latitudes) && isset($reocrds->longitude)) { ?>
                             ['<div class="info_content">' +
                '<h3><?php echo ucwords($reocrds->full_name) ?></h3>' +
                '<p>Mobile Number <?php echo $reocrds->primary_mobile_number; ?></p>'+"<p>Shop Address:  <?php echo $reocrds->shop_address ? $reocrds->shop_address : ''; ?></p>" +'</div>'],
    <?php }
} ?>

  ];
     console.log(markers);
  }else {
       

        var markers = t_markers_id;
         console.log(markers);

        var infoWindowContent = infoWindowContent_id;
     }

  // Display multiple markers on a map
  var infoWindow = new google.maps.InfoWindow(), marker, i;

  // Loop through our array of markers & place each one on the map
  for( i = 0; i < markers.length; i++ ) {
      var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
      bounds.extend(position);
       map.setCenter(position);
      marker = new google.maps.Marker({
          position: position,
          map: map,
          
          title: markers[i][0]
      });

      // Allow each marker to have an info window
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
              infoWindow.setContent(infoWindowContent[i][0]);
              infoWindow.open(map, marker);
          }
      })(marker, i));

      // Automatically center the map fitting all markers on the screen
      map.fitBounds(bounds);
  }

  // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
  var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
       if(!singlelog){
           this.setZoom(3); 
       }
       if(singlelog){
            this.setZoom(12);
       }
       
     
      
      google.maps.event.removeListener(boundsListener);
  });

}

function show_map(cs_value,cs_id)
{
   
    if(cs_id == 'is_state'){
        $("#is_city").val('');
        $("#is_address").val('');
    }
    if(cs_id == 'is_city'){
       
        $("#is_address").val('');
    }
    singlelog = true;  
    $.ajax({

           type:'GET',

           url:show_url+'retailer/search-retailers-in-map',

           data:{city:$("#is_city").val(),state:$("#is_state").val(),location:$("#is_address").val()},

           success:function(data){

             // console.log(data.list);
             
         
             var arr = $.parseJSON(data);
             if(arr.code==200)
             {
                 if(cs_id == 'is_state'){
                      $("#is_city").html(arr.cityHtml);
                 }
                 if(cs_id == 'is_city'){
                      $("#is_address").html(arr.locationHtml);
                 }
                
               
                  var markers = [];
                  var infoWindowContent=[];
                   $.each(arr.list, function (index, value) {
                   var markers_in =[]   ; 
                   var infoWindowContent_in=[];
                   if($.trim(value.shop_address) && value.latitudes && value.longitude)
                   {
                   markers_in.push(value.shop_address); 
                   markers_in.push(value.latitudes);
                   markers_in.push(value.longitude);
                   //value.longitude
                   infoWindowContent_in.push('<div class="info_content">' +
       '<h3>'+value.full_name.substr(0,1).toUpperCase()+value.full_name.substr(1)+'</h3>' +
       '<p>Mobile Number '+value.primary_mobile_number+'</p>' +"<p>Shop Address: "+value.shop_address+"</p>"+'</div>');
                    markers.push(markers_in);
                    infoWindowContent.push(infoWindowContent_in);
                   }
                });
            
                 initMap(markers,infoWindowContent) ; 
             }
             
           }

        });
    
}

</script>

@endsection
