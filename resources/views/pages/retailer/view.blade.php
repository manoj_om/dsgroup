@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <!-- header row -->
    <?php // echo "<pre>"; print_r($data); ?>
    <header-page :pagetitle="'Retailers'" :activeClass="'#retailerlist'"></header-page>
    <!-- end -->

    <div class="row details">
        <div class="col-md-12 pd-x-2 pd-y-2 pt-3 details-inner-sec">
            <div class="p-4 bg-white">
                <div class="col-md-12 p-2 mb-5 detail-sec">
                    <div class="row">
                        <div class="col-md-6 left-section">
                            <div class="row">
                                <div class="col-md-6 pr-2 retail-view">
                                    <?php if ($data->shop_image != '') { ?>
                                        <span class="d-inline-block w-100 retail-view-profile border light-border rounded">
                                            <img src="{{asset('uploads/retailers/shop_image/')}}/{{ $data->shop_image }}" alt="retail-view-profile" class="img-fluid w-100 h-100 retail-view-profile-img rounded" />
                                        </span>
                                    <?php } else { ?>
                                        <img src="{{asset('images/no-image.jpg')}}" alt="reatil-detail-img" class="img-fluid" />
                                    <?php } ?>
                                </div>
                                <div class="col-md-6 inner-img">
                                    <h5 class="detail-name">{{ $data->shop_name }}</h5>
                                    <div class="d-flex my-3 location align-items-start">
                                        <img src="{{asset('images/location-icon.svg')}}" alt="location-icon" class="location-img mr-2 mt-1" width="15" />
                                        <div class="">
                                          <p class="mb-0 location-text">{{ $data->shop_address }}</p>
                                          <p class="mb-0">{{ $data->latitudes }} , {{ $data->longitude }}</p>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center mb-3 adhaar">
                                        <span class="mr-2 retail-info-ico adhar-ico">
                                            <img src="{{asset('images/adhar-icon.png')}}" alt="adhar-icon" class="adhaar-icon" />
                                        </span>
                                        @if($data->adhar_number)
                                        <span class="adhar-number">{{ $data->adhar_number }}</span>
                                        @else
                                         <span class="adhar-number">N/A</span>
                                         @endif
                                    </div>
                                    <div class="d-flex flex-wrap align-items-center icons-ico qr qr-code">
                                        <span><i class="h1 m-0 la la-qrcode"></i></span>
                                        <span>{{ $data->scan_code }}</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3 right-section">
                            <div class="row">
                                <map-distributor :retailer="{{ $data }}"></map-distributor>
                            </div>
                        </div>
                        <div class="col-md-3 right-section">
                            <div class="row">
                                <map-agent :retailer="{{ $data }}"></map-agent>
                            </div>
                        </div>
<!--                        <div class="col-md-4 pt-2 right-section">
                            <div class="row">
                                <a href="#" onclick="window.open('{{route('retailer-panel',$data->id)}}','mywindow','_blank','status=1,toolbar=0,height=200,width=500,left=500,top=500,fullscreen = 1,directories=no,titlebar=no')"  title="" class="sidebar-link" >Place Order</a>
                            </div>

                        </div>-->
                        @if($data->otherQrcode != '')
                        <div class="col-md-12 mt-3 qr-code-list">
                            <div class="row">
                                @if($data->otherQrcode->paytm)
                                <div class="col-md-2 border border-left-0 border-top-0 border-bottom-0 mt-2 qr-list">
                                    <div class="d-flex justify-content-center flex-wrap align-items-center icons-ico qr qr-code">
                                        <span class="other-qr"><img src="{{asset('images/qrimage/paytm-icon.png')}}" alt="retail-detail-img" class="img-fluid h-100" /></span>
                                        <span>{{ $data->otherQrcode->paytm }}</span>
                                    </div>
                                </div>
                                @endif
                                @if($data->otherQrcode->mobiqwik)
                                <div class="col-md-2 mt-2 border border-left-0 border-top-0 border-bottom-0 qr-list">
                                    <div class="d-flex justify-content-center flex-wrap align-items-center icons-ico qr qr-code">
                                        <span class="other-qr"><img src="{{asset('images/qrimage/mobikwik-icon.png')}}" alt="retail-detail-img" class="img-fluid h-100" /></span>
                                        <span>{{ $data->otherQrcode->mobiqwik }}</span>
                                    </div>
                                </div>
                                @endif
                                @if($data->otherQrcode->gpay)
                                <div class="col-md-2 mt-2 border border-left-0 border-top-0 border-bottom-0 qr-list">
                                    <div class="d-flex justify-content-center flex-wrap align-items-center icons-ico qr qr-code">
                                        <span class="other-qr"><img src="{{asset('images/qrimage/google-pay-icon.png')}}" alt="retail-detail-img" style="text-align: center" class="img-fluid h-100" /></span>
                                        <span>{{ $data->otherQrcode->gpay }}</span>
                                    </div>
                                </div>
                                @endif
                                @if($data->otherQrcode->bharat_qr)
                                <div class="col-md-2 mt-2 border border-left-0 border-top-0 border-bottom-0 qr-list">
                                    <div class="d-flex justify-content-center flex-wrap align-items-center icons-ico qr qr-code">
                                        <span class="other-qr"><img src="{{asset('images/qrimage/bharat-icon.jpg')}}" alt="retail-detail-img" class="img-fluid h-100" /></span>
                                        <span>{{ $data->otherQrcode->bharat_qr }}</span>
                                    </div>
                                </div>
                                @endif
                                @if($data->otherQrcode->phone_pe)
                                <div class="col-md-2 mt-2 border border-left-0 border-top-0 border-bottom-0 qr-list">
                                    <div class="d-flex justify-content-center flex-wrap align-items-center icons-ico qr qr-code">
                                        <span class="other-qr"><img src="{{asset('images/qrimage/phonepe-icon.png')}}" alt="retail-detail-img" class="img-fluid h-100" /></span>
                                        <span>{{ $data->otherQrcode->phone_pe }}</span>
                                    </div>
                                </div>
                                @endif
                                @if($data->otherQrcode->upi)
                                <div class="col-md-2 mt-2 qr-list">
                                    <div class="d-flex justify-content-center flex-wrap align-items-center icons-ico qr qr-code">
                                        <span class="other-qr"><img src="{{asset('images/qrimage/upi-icon.png')}}" alt="retail-detail-img" class="img-fluid h-100" /></span>
                                        <span>{{ $data->otherQrcode->upi }}</span>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 mt-4 contacts-row">
                    <div class="row">
                        <!-- primary contact -->
                        <div class="col-md-4 contact-grid">
                            <div class="col-12 p-3 rounded bg-white rounded grids">
                                <h5 class="mb-3 font-18">Primary Contact</h5>
                                <div class="d-flex details">
                                    <span class="col-md-3 p-0 retail-contact-img">
                                        <?php if ($data->capture_retailer_image != '') { ?>
                                            <img src="{{asset('uploads/retailers/primary/')}}/{{$data->capture_retailer_image}}" alt="contact-detail-img" class="w-100 h-100 img-fluid contact-img" />
                                        <?php } else { ?>
                                            <img src="{{asset('images/no-image.jpg')}}" alt="reatil-detail-img" class="img-fluid" />
                                        <?php } ?>
                                    </span>
                                    <span class="col-md-9 p-0 pl-3 retail-contact-detail">
                                        <h6>{{$data->full_name}}</h6>
                                        <p class="my-2 number"><img src="{{asset('images/phone-icon.svg')}}" alt="img-fluid phone-icon" class="phone-ico" /> <span class="number">{{$data->primary_mobile_number}} ,{{$data->alternative_mobile_number}}</span></p>
                                        <p class="m-0 watsapp"><img src="{{asset('images/whatsapp-icon.png')}}" alt="whatsapp-icon" class="whatsapp-ico" /> <span class="number">{{$data->whats_app_number}}</span></p>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- end -->

                        <!-- secondary contact -->
                        @if($data->secondary_full_name != '' && $data->secondary_mobile_number != '')
                        <div class="col-md-4 contact-grid" >
                            <div class="col-12 p-3 rounded bg-white rounded grids">
                                <h5 class="mb-3 font-18">Secondary Contact</h5>
                                <div class="d-flex details">
                                    <span class="col-md-3 p-0 retail-contact-img">
                                        <?php if ($data->secondary_capture_retailer_image != '') { ?>
                                            <img src="{{asset('uploads/retailers/secondary/')}}/{{$data->secondary_capture_retailer_image}}" alt="contact-detail-img" class="img-fluid profile-img" />
                                        <?php } else { ?>
                                            <img src="{{asset('images/no-image.jpg')}}" alt="reatil-detail-img" class="img-fluid" />
                                        <?php } ?>
                                    </span>
                                    <span class="col-md-9 p-0 pl-3 retail-contact-detail">
                                        <h6>{{ $data->secondary_full_name }}</h6>
                                        <p class="my-2 number"><img src="{{asset('images/phone-icon.svg')}}" alt="img-fluid phone-icon" class="phone-ico" /> <span class="number">{{ $data->secondary_mobile_number }}</span></p>

                                    </span>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="col-md-4 contact-grid" >
                            <div class="col-12 p-3 rounded bg-white rounded grids">
                                <h5 class="mb-3 font-18">Secondary Contact</h5>
                                <div class="d-flex align-items-center details">
                                    <p>N/A</p>
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- end -->

                        <!-- agent contact -->
                        @if($data->user_type == 1)
                        <div class="col-md-4 contact-grid" >
                            <div class="col-12 p-3 rounded bg-white rounded grids">
                                <h5 class="mb-3 font-18">Agent Contact</h5>
                                <div class="d-flex details">
                                    <span class="col-md-3 p-0 retail-contact-img">
                                        <?php if ($data->agantdata->avtar != '') { ?>
                                            <img src="{{asset("uploads/agents/")}}/{{ $data->agantdata->avtar }}" alt="contact-detail-img" class="img-fluid profile-img" />
                                        <?php } else { ?>
                                            <img src="{{asset('images/no-image.jpg')}}" alt="reatil-detail-img" class="img-fluid" />
                                        <?php } ?>
                                    </span>
                                    <span class="col-md-9 p-0 pl-3 retail-contact-detail">
                                        <h6>{{ $data->agantdata->name }}</h6>
                                        <p class="my-2 number"><img src="{{asset('images/phone-icon.svg')}}" alt="img-fluid phone-icon" class="phone-ico" /> <span class="number">{{ $data->agantdata->mobile_number }}</span></p>

                                    </span>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($data->user_type == 2)
                        <div class="col-md-4 contact-grid" >
                            <div class="col-12 p-3 rounded bg-white rounded grids">
                                <h5 class="mb-3 font-18">Admin Detail</h5>
                                <div class="d-flex details">
                                    <span class="col-md-3 p-0 retail-contact-img">
                                        <?php if (isset($data->agantdata->avtar) && $data->agantdata->avtar != '') { ?>
                                            <img src="{{asset("images/users/")}}/{{ $data->admin->avtar }}" alt="contact-detail-img" class="img-fluid profile-img" />
                                        <?php } else { ?>
                                            <img src="{{asset('images/no-image.jpg')}}" alt="reatil-detail-img" class="img-fluid" />
                                        <?php } ?>
                                    </span>
                                    <span class="col-md-9 p-0 pl-3 retail-contact-detail">
                                        <h6>{{ $data->admin->name }}</h6>
                                        <p class="my-2 number"><img src="{{asset('images/phone-icon.svg')}}" alt="img-fluid phone-icon" class="phone-ico" /> <span class="number">{{ $data->admin->mobile_number }}</span></p>
                                    </span>
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- end -->
                    </div>
                </div>
                <div class="col-md-12 mt-4 kyc-additional">
                    <!-- additional details -->
                    <div class="col-md-12 additional">
                        <h6 class="font-weight-normal">Additional Details</h6>
                        <div class="col-md-12 p-0 d-flex mt-3 list-details">
                            <span>
                                <p class="mb-1 font-weight-light kyc-label">DOB</p>
                                <?php if ($data->dob != null) { ?>
                                    <p class="mb-1">{{ date('d-m-Y', strtotime($data->dob)) }}</p>
                                <?php } else { ?>
                                    <p class="mb-1">N/A</p>
                                <?php } ?>

                            </span>
                            <span>
                                <p class="mb-1 font-weight-light kyc-label">Annivarsary Date</p>
                                <?php if ($data->anniversay != null) { ?>
                                    <p class="mb-1">{{ date('d-m-Y', strtotime($data->anniversay)) }}</p>
                                <?php } else { ?>
                                    <p class="mb-1">N/A</p>
                                <?php } ?>

                            </span>
                            <span>
                                <p class="mb-1 font-weight-light kyc-label">1st Kid DOB</p>
                                <?php if ($data->first_kid_birthday != null) { ?>
                                    <p class="mb-1">{{ date('d-m-Y', strtotime($data->first_kid_birthday)) }}</p>
                                <?php } else { ?>
                                    <p class="mb-1">N/A</p>
                                <?php } ?>

                            </span>
                            <span>
                                <p class="mb-1 font-weight-light kyc-label">2nd Kid DOB</p>
                                <?php if ($data->second_kid_birthday != null) { ?>

                                    <p class="mb-1">{{ date('d-m-Y', strtotime($data->second_kid_birthday)) }}</p>
                                <?php } else { ?>
                                    <p class="mb-1">N/A</p>
                                <?php } ?>

                            </span>
                        </div>
                    </div>
                    <!-- end -->

                    <!-- kyc details -->
                    <div class="col-md-12 mt-4 kyc">
                        <h6 class="font-weight-normal">KYC Details</h6>
                        <div class="col-md-12 p-0 d-flex mt-3 list-details">
                            <span class="pr-4">
                                <p class="mb-1 font-weight-light kyc-label">GST No.</p>
                                <?php if ($data->gst_number != null) { ?>
                                    <p class="mb-1">{{ $data->gst_number }}</p>
                                <?php } else { ?>
                                    <p class="mb-1">N/A</p>
                                <?php } ?>
                                <div class="mt-2 kyc-img">
                                    <?php if ($data->gst_number_file != null) { ?>
                                        <img src="{{asset("uploads/retailers/kyc/")}}/{{ $data->gst_number_file }}"  alt="pan-img" class="img-fluid w-100 h-100 pan-img" />
                                    <?php } else { ?>
                                        <img src="{{asset('images/no-image.jpg')}}" alt="reatil-detail-img" class="img-fluid" />
                                    <?php } ?>
                                </div>
                            </span>
                            <span class="pr-4">
                                <p class="mb-1 font-weight-light kyc-label">Pan No.</p>
                                
                                <?php if ($data->pan_number != null) { ?>
                                    <p class="mt-2 mb-0 text-center text-secondary">({{ $data->pan_number }})</p>
                                <?php } else { ?>
                                    <p class="mt-1">N/A</p>
                                <?php } ?>
                                    <div class="mt-2 kyc-img">
                                    <?php if ($data->pan_number_file != null) { ?>
                                        <img src="{{asset("uploads/retailers/kyc/")}}/{{ $data->pan_number_file }}"  alt="pan-img" class="img-fluid w-100 h-100 pan-img" />
                                    <?php } else { ?>
                                        <img src="{{asset('images/no-image.jpg')}}" alt="reatil-detail-img" class="img-fluid" />
                                    <?php } ?>
                                </div>
                            </span>
                            <span class="pr-4">
                                <p class="mb-1 font-weight-light kyc-label">Aadhar No.</p>
                                <?php if ($data->adhar_number != null) { ?>
                                    <p class="mb-1">{{ $data->adhar_number }}</p>
                                <?php } else { ?>
                                    <p class="mb-1">N/A</p>
                                <?php } ?>
                                <div class="mt-2 kyc-img">
                                    <?php if ($data->adhar_number_file != null) { ?>
                                        <img src="{{asset("uploads/retailers/kyc/")}}/{{ $data->adhar_number_file }}"  alt="pan-img" class="img-fluid w-100 h-100 pan-img" />
                                    <?php } else { ?>
                                        <img src="{{asset('images/no-image.jpg')}}" alt="reatil-detail-img" class="img-fluid" />
                                    <?php } ?>
                                </div>
                            </span>
                        </div>
                    </div>
                    <!-- end -->
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-md-12 pd-x-2">
            <div class="row align-items-center">
                <div class="col-md-4 list-orders">
                    <h6 class="font-weight-normal">List of Placed Orders</h6>
                </div>
            </div>
        </div>
    </div>

       <order-table retailerId="{{ $data->id }}"></order-table>
</div>

<!-- end -->
@endsection
