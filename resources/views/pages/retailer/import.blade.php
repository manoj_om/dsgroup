@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <!-- header row -->
  
    <header-page :pagetitle="'Retailers Import'" :activeClass="'#retailerlist'"></header-page>
    <!-- end -->
   @if(isset($errors) && count($errors) > 0)
   @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach
@endif

   
<div class="col-12 mt-3">
    <div class="col-md-12 px-3 mb-3 bg-white border rounded">
        @if (isset($failures))
    <div class="alert alert-danger" role="alert">
        <strong>Errors:</strong>

        <ul>
            @foreach ($failures as $failure)
            @foreach ($failure->errors() as $error)
            <li>{{ $error }}</li>
            @endforeach
            @endforeach
        </ul>
    </div>
    @endif
    <div class="my-3">
        <form action="{{ route('retailer.import') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-4 mb-3">
                    <label for="name">Choose File <sup class="text-danger font-weight-bold h6">*</sup></label>
                    <input type="file" name="file" id="file" class="w-100 input-shadow border light-border rounded px-3">
                </div>
            </div>
            <button class="primary-btn1 addresource">Import</button>
        </form>
    </div>
    </div>
</div>

    <!-- end -->
    @endsection
