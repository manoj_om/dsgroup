@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Calling Agent'" :loader="true"></header-page>

     <calling-agent-form></calling-agent-form>
    

     <calling-agent-table></calling-agent-table>
</div>



@endsection
