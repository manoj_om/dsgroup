
@extends('layouts.loginapp')

@section('content')
<div class="container-fluid">
    <div class="row align-content-center login-section">
        <div class="col-sm-5 p-4 d-flex flex-wrap align-content-center light-bg full-height login-left">
            <div class="col-12 login-inner">
                <img src="{{asset('images/login-img.png')}}" alt="login-img" class="img-fluid" />
            </div>
        </div>
        <div class="col-sm-7 p-5 d-flex flex-wrap align-content-center bg-grey full-height login-right">
            <div class="col-md-10 login-form">
                <div class="col-12 mb-4 login-logo">
                    <img src="{{asset('images/logo.svg')}}" alt="logo" class="logo" />
                </div>
                <div class="col-12 mt-3 d-inline-block form-section">
                    <div class="mt-5 mb-4 d-inline-block form-title">
                        <h3 class="theme-text  font-weight-light">Agent Login</h3>
                        <p class="mb-4">Welcome Back! Please login with your details.</p>
                    </div>
                        <form method="POST" class="form-horizontal" action="" aria-label="{{ __('Login') }}">
                        {{ csrf_field() }}
                        <div class="position-relative mb-5 form-group">
                            <span class="input-icon"><img src="{{asset('images/sent-mail.svg')}}" alt="sent-mail" /></span>
                            <input type="email" class="pl-4 rounded-0 input-border form-control" id="email" placeholder="EMAIL" value="{{ old('email') }}" name="email" required />
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                            <div class="position-absolute invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="position-relative mb-5 form-group">
                            <span class="input-icon"><img src="{{asset('images/security-on.svg')}}" alt="security-on" /></span>
                            <input type="password" class="pl-4 rounded-0 input-border form-control" id="password" placeholder="PASSWORD" name="password" required />
                            <div class="position-absolute invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="text-center submit">
                            <button type="submit" class="rounded-circle btn theme-bg text-white submit-btn">
                              <i class="display-5 la la-long-arrow-alt-down"></i>
                            </button>
                            <div class="d-inline-block w-100 mt-3"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- <div class="container">
 <div class="row justify-content-center">
 <div class="col-md-8">
 <div class="card">
 <div class="card-header"></div>

<div class="card-body">
 <form method="POST" action="">
 @csrf

<div class="form-group row">
 <label for="email" class="col-sm-4 col-form-label text-md-right"></label>

<div class="col-md-6">
 <input id="email" type="email" class="form-control" name="email" value="" required autofocus>

@if ($errors->has('email'))
 <span class="invalid-feedback">
 <strong></strong>
 </span>
 @endif
 </div>
 </div>

<div class="form-group row">
 <label for="password" class="col-md-4 col-form-label text-md-right"></label>

<div class="col-md-6">
 <input id="password" type="password" class="form-control" name="password" required>

@if ($errors->has('password'))
 <span class="invalid-feedback">
 <strong></strong>
 </span>
 @endif
 </div>
 </div>

<div class="form-group row">
 <div class="col-md-6 offset-md-4">
 <div class="checkbox">
 <label>
 <input type="checkbox" name="remember" >
 </label>
 </div>
 </div>
 </div>

<div class="form-group row mb-0">
 <div class="col-md-8 offset-md-4">
 <button type="submit" class="btn btn-primary">

 </button>

<a class="btn btn-link" href="">

 </a>
 </div>
 </div>
 </form>
 </div>
 </div>
 </div>
 </div>
 </div>-->

@endsection
