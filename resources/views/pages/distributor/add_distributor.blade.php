@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Hub Master'" :loader="false"></header-page>
    <div class="row mt-4 mb-4">

        <div class="col-md-12 pd-x-2">
            <div class="row align-items-center">
                <div class="col-md-4 attention-request">
                    <h4 class="font-weight-light">Add Hub</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 add-user-form open-form">
                    <form method="post" action="{{route('distributor.store')}}" id="" class="bg-white rounded border light-border p-4 manage-user">
                        {{ csrf_field() }}
                        <div class="row p-4">
                            <div class="col-md-4 mb-4">
                                <label for="distributor-name">Hub Name <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <input type="text" name="distributor_name" value="{{ old('distributor_name') }}" id="distributor_name" placeholder="Enter Here"  class="w-100 input-shadow border light-border rounded px-3" maxlength="30">
                                @error('distributor_name')
                                <div class="error-msg">
                                    <i  class="fa fa-warning"></i>
                                    <div class="error-msg">                                  
                                        <i  class="fa fa-warning"></i>   
                                        <span class="help is-danger">{{ $message }}</span>                                </div>
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4">
                                <label for="email">Hub Code <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <input type="text" value="{{ old('distributor_code') }}" name="distributor_code" id="distributor_code" placeholder="Enter Here"  class="w-100 input-shadow border light-border rounded px-3"  maxlength="20">
                                @error('distributor_code')
                                <div class="error-msg">                                  
                                    <i  class="fa fa-warning"></i>                                
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4">
                                <label for="mobile">Mobile <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <input type="text"  value="{{ old('mobile') }}" name="mobile" id="mobile" placeholder="Enter Here" class="w-100 input-shadow border light-border rounded px-3"  maxlength="12">
                                @error('mobile')
                                <div class="error-msg">  
                                    <i  class="fa fa-warning"></i>        
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4 mt-3">
                                <label for="email">Email <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <input type="email"   value="{{ old('email') }}" name="email" id="email" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded" maxlength="30">
                                @error('email')
                                <div class="error-msg">    
                                    <i  class="fa fa-warning"></i>               
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4 mt-3">
                                <label for="pan">PAN <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <input type="text" value="{{ old('pan_card') }}" name="pan_card" id="pan_card" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded" maxlength="10">
                                @error('pan_card')
                                <div class="error-msg">       
                                    <i  class="fa fa-warning"></i>         
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4 mt-3">
                                <label for="gst">GST <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <input type="text" value="{{ old('gst_no') }}" name="gst_no" id="gst_no" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded" maxlength="15">
                                @error('gst_no')
                                <div class="error-msg"> 
                                    <i  class="fa fa-warning"></i>         
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4 mt-3">
                                <label for="aadhar">Aadhar No. <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <input type="text" value="{{ old('aadhar_no') }}" name="aadhar_no" id="aadhar_no" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded" maxlength="12">
                                @error('aadhar_no')
                                <div class="error-msg">        
                                    <i  class="fa fa-warning"></i>        
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4 mt-3">
                                <label for="website">Website <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <input type="website" value="{{ old('website') }}" name="website" id="aadhar_no" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded" maxlength="50">
                                @error('website')
                                <div class="error-msg">    
                                    <i  class="fa fa-warning"></i>              
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4 mt-3">
                                <label for="state">State <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <select name="state" id="state" class="w-100 px-3 pr-5 select-shadow rounded-lg light-border show-data" onchange="show_city(this.value)">
                                    <option value="">Select</option>
                                    <?php foreach ($states as $row_state) { ?>
                                        <option value="<?php echo $row_state->id ?>"><?php echo $row_state->name ?></option>
                                    <?php } ?>
                                </select>
                                @error('state')
                                <div class="error-msg">        
                                    <i  class="fa fa-warning"></i>             
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4 mt-3">
                                <label for="city">City <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <select name="city" id="city" class="w-100 px-3 pr-5 select-shadow rounded-lg light-border show-data">
                                    <option value="">Select</option>
                                </select>
                                @error('city')
                                <div class="error-msg">       
                                    <i  class="fa fa-warning"></i>            
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4 mt-3">
                                <label for="latitude">Latitude <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <input type="text" value="{{ old('latitude') }}" name="latitude" id="latitude" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded">
                                @error('latitude')
                                <div class="error-msg">     
                                    <i  class="fa fa-warning"></i>        
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-4 mt-3">
                                <label for="longitude">Longitude <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <input type="text"  value="{{ old('longitude') }}"  name="longitude" id="longitude" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded">
                                @error('longitude')
                                <div class="error-msg">   
                                    <i  class="fa fa-warning"></i>      
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-4 mt-3">
                                <label for="add">Address <sup class="text-danger font-weight-bold h6">*</sup></label>
                                <textarea name="address" id="address" placeholder="Enter here" rows="3" class="w-100 px-3 py-3 input-shadow border light-border rounded">
                                {{ old('address') }}
                                </textarea>
                                @error('address')
                                <div class="error-msg">     
                                    <i  class="fa fa-warning"></i>       
                                    <span class="help is-danger">{{ $message }}</span>                                </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mt-4 text-center">
                                <div class="col-md-6 offset-md-3 margin-auto">
                                    <span class="h6 m-0">
                                        <button type="reset" onclick="locationTable()" class="h6 text-uppercase w-25 py-2 mr-2 d-inline-block rounded theme-text border-btn cancel-btn">Cancel</button>
                                    </span>
                                    <span class="h6 m-0">
                                        <button type="submit" class="h6 text-uppercase d-inline-block w-25 py-2 rounded text-white theme-bg border-btn save-btn">SAVE</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>
<script type="application/javascript">

   var show_url="<?php echo config('app.url'); ?><?php if ($_SERVER['HTTP_HOST'] == 'localhost') echo "public/"; ?>";
      function locationTable(){
          window.location.href = show_url+ "/distributor";
      }
    function show_city(state_val)
    {
    $.ajax({

    type:'GET',

    url:show_url+'getcity-by-state',

    data:{state:state_val},

    success:function(data){

    // console.log(data.list);
    $("#city").html(data);


    }

    });
    }
</script>
@endsection
