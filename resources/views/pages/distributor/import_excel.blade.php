@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Hub Master'" :loader="false"></header-page>
    <div class="row mt-4 mb-4">

      <div class="col-md-12 pd-x-2">
          <div class="row align-items-center">
              <div class="col-md-4 attention-request">
                    <h4 class="font-weight-light">Import Excel</h4>
              </div>
          </div>
          
          
         @if (isset($bug) && count($bug) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($bug as $failure)
                  <li>{{ $failure->errors() }}</li>
               @endforeach
            </ul>
         </div>
        @endif




          <div class="row">

              <div class="col-md-12 add-user-form open-form">
                  <form method="post" action="distributor-import" id="" class="bg-white rounded border light-border p-4 manage-user" enctype="multipart/form-data">
                     {{ csrf_field() }}
                      <div class="row p-4">
                          <div class="col-md-4 mb-4">
                            <label for="distributor-name">Excel<sup class="text-danger font-weight-bold h6">*</sup></label>
                            <input type="file" name="excel_file" id="excel_file" placeholder="Enter Here"  class="w-100 input-shadow border light-border rounded px-3" required="">
                          </div>

                      </div>
                     <div class="col-md-12 mt-4 text-center">
                              <div class="col-md-6 offset-md-3 margin-auto">

                                <span class="h6 m-0">
                                    <button type="submit" class="h6 text-uppercase d-inline-block w-25 py-2 rounded text-white theme-bg border-btn save-btn">SAVE</button>
                                </span>
                              </div>
                          </div>
                  </form>
              </div>
          </div>

      </div>
    </div>

</div>
<script type="application/javascript">


</script>
@endsection
