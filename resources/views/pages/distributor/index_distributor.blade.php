@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
  <header-page :pagetitle="'Hub Master'" :loader="true"></header-page>
    <div class="row mt-4 mb-4 align-items-center">
        <div class="col-md-12 pd-x-2">
           @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
            </div>
            @endif
            @if ($message = Session::get('bug'))
            <div class="alert alert-dange alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
            </div>
            @endif
            @if($errors->any())

              <div class = "alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{$errors->first()}}</strong>
            </div>
         @endif
        <distributor-list></distributor-list>
        </div>
    </div>
</div>
<!-- end -->

<!-- The Modal -->



@endsection
