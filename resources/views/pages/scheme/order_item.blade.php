@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Ordered Items'" :loader="true"></header-page>

    <order-item-form></order-item-form>   

    <order-item-table></order-item-table>

</div>

@endsection
