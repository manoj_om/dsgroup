@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Scheme Bundling'" :loader="true"></header-page>

    <building-scheme-form></building-scheme-form>

    <building-scheme-table></building-scheme-table>

</div>

@endsection
