@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Order Items Discount'" :loader="true"></header-page>

    <order-item-discount-form></order-item-discount-form>   

    <order-item-discount-table></order-item-discount-table>

</div>

@endsection
