@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'SKU-Line Sold Per Call'" :loader="true"></header-page>

    <sku-scheme-form></sku-scheme-form>   

    <sku-scheme-table></sku-scheme-table>

</div>

@endsection
