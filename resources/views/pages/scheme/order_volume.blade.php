@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Order Volume'" :loader="true"></header-page>

    <order-volume-form></order-volume-form>   

    <order-volume-table></order-volume-table>

</div>

@endsection
