@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'New Product Launch'" :loader="true"></header-page>

    <product-launch-form></product-launch-form>   

    <product-launch-table></product-launch-table>

</div>

@endsection
