@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Orders'" :loader="true"></header-page>
    <order-table></order-table>
</div>
<!-- end -->
@endsection
