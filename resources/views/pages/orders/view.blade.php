@extends('layouts.app')

@section('content')

<div class="col-md-9 toggle-switch bg-grey onboard-list dashboard-page">
<?php // echo "<pre>";print_r($orders['promodetail']);exit; ?>
    <header-page :pagetitle="'Orders'" :loader="false"></header-page>
<?php $duration = new App\Orders;  $prod = new App\Product;  ?>
    <div class="row details">
        <div class="col-md-12 pd-x-2 pt-3 stock-details {{ $orders['status'] == 0 ? 'cancelled' : '' }}">
            <div class="col-md-12 p-4 bg-white detailInfo">
                <div class="row price-info">
                    <div class="col-md-3 info">
                        <p class="mb-0 font-weight-light kyc-label">Order No</p>
                        <p class="mb-1">{{ strtoupper($orders['order_number']) }}</p>
                    </div>
                    <div class="col-md-3 info">
                        <p class="mb-0 font-weight-light kyc-label">Order Date</p>
                        <p class="mb-1">{{ date('d-m-Y H:i',strtotime($orders['created_at'])) }} </p>
                    </div>
                    <div class="col-md-3 info">
                        <p class="mb-0 font-weight-light kyc-label">Delivery Date/Time</p>
                        <p class="mb-1">{{ date('d-m-Y H:i',strtotime($orders['delivery_time'])) }} </p>
                    </div>
                    <div class="col-md-3 info">
                        <p class="mb-0 font-weight-light kyc-label">Delivery Slot</p>
                        <p class="mb-1">{{  $duration->findTimeSlot( $orders['delivery_time']) }} </p>
                    </div>
                    <div class="col-md-3 info">
                        <p class="mb-0 font-weight-light kyc-label">Order Amount</p>
                        <p class="mb-1"> {{ number_format(round($orders['total_amount'], 2), 2, '.', '')}}</p>
                    </div>
                    <div class="col-md-3 info">
                        <p class="mb-0 font-weight-light kyc-label">Review</p>
                        <?php if ($orders['ratings'] != '' && $orders['ratings'] != null) { ?>
                            <star-rating value="{{ $orders['ratings']['stars'] }}" disabled="true"></star-rating>
                        <?php } else { ?>

                            <star-rating value="0" disabled="true"></star-rating>
                        <?php } ?>
                        <p class="mb-1">{{ $orders['ratings']['comment'] }}</p>
                    </div>
                    <div class="col-md-3 info">
                        <p class="mb-0 font-weight-light kyc-label">Order Status</p>
                        @if($orders['status'] == 1)
                        <p class="mb-1">Order Placed</p>
                        @elseif($orders['status'] == 2)
                        <p class="mb-1">Packed</p>
                        @elseif($orders['status'] == 3)
                        <p class="mb-1">Dispatched</p>
                        @elseif($orders['status'] == 4)
                        <p class="mb-1">Delivered</p>
                        @elseif($orders['status'] == 0)
                        <p class="mb-1 text-danger">Cancelled</p>
                        @endif
                    </div>
                </div>
            </div>
            <?php $duration = new App\Orders; ?>
            <div class="col-md-12 bg-white p-3 my-4 tracking">
                <div class="row order-status-outer">
                    <div class="col-sm-3 order-status-inner {{ $orders['status']  >=  '1' ? 'status-done' : ''  }}">
                        <h6>Order Placed</h6>
                        <div class="status-circle"></div>
                    </div>

                    <div class="col-sm-3 order-status-inner {{ $orders['status'] >=  '2'  ? 'status-done' : ''  }}  {{ $duration->timeDuration($orders['packed_time'],$orders['created_at'],1) && $orders['status'] >=  '2' ? 'status-red' : '' }}">
                        <h6>Packed</h6>
                        <div class="status-circle"></div>
                    </div>
                    <div class="col-sm-3 order-status-inner {{ $orders['status'] >=  '3'  ? 'status-done' : ''  }} {{ $duration->timeDuration($orders['dispatch_time'],$orders['packed_time'],2)  && $orders['status'] >=  '3' ? 'status-red' : '' }}">
                        <h6>Dispatched</h6>
                        <div class="status-circle"></div>
                    </div>
                    <div class="col-sm-3 order-status-inner {{ $orders['status'] ==  '4'  ? 'status-done' : ''  }} {{ $duration->timeDuration($orders['delivered_time'],$orders['dispatch_time'],3) && $orders['status'] >=  '4' ? 'status-red' : '' }}">
                        <h6>Delivered</h6>
                        <div class="status-circle"></div>
                    </div>
                </div>
            </div>

            @if($orders['promocode'] != '')
            <div class="bg-white p-3 my-4">
                <div class="col-md-12">
                    <div class="row hub-manager-order">
                        <div class="col-md-4 mt-1 hub-order">
                            <div class="w-75 hubOrder-inner">
                                <h6 class="font-weight-bold mb-3">Promocode</h6>
                                <p class="mb-2 details">{{ $orders['promocode'] }}</p>
                            </div>
                        </div>
                        <div class="col-md-4 mt-1 hub-order">
                            <div class="w-75 hubOrder-inner">
                                <h6 class="font-weight-bold mb-3">Promocode details</h6>
                                <p class="mb-2 details">{{ $orders['promodetail']['message'] }}</p>
                            </div>
                        </div>
                        <div class="col-md-4 mt-1 hub-order">
                            <div class="w-75 hubOrder-inner">
                                <h6 class="font-weight-bold mb-3">Discount Amount</h6>
                                <p class="mb-2 details">{{$orders['promodetail']['discount'] }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-md-12 bg-white p-4 detailOrder">
                <div class="row Retail-info">
                    <div class="col-md-3 info">
                        <p class="mb-1 font-weight-light kyc-label">Retailer</p>
                        <p class="text-capitalize mb-1">{{ $orders['retailer']['full_name'] }}</p>
                        <p class="mb-1">{{ $orders['retailer']['primary_mobile_number'] }}</p>
                        <p class="text-capitalize mb-1">{{ $orders['retailer'] ['shop_address'] }}</p>
                       <p class="mb-2 details">{{ $orders['retailer']['latitudes'] }} , {{  $orders['retailer']['longitude'] }}</p>
                    </div>

                    <div class="col-md-4 hub-order">
                        <div class="w-75 m-auto hubOrder-inner">
                            <h6 class="font-weight-bold mb-3">Hub</h6>
                            <span class="details-heading">Name</span>
                            <p class="mb-2 details">{{ $orders['retailer']['map_distributor']['distributor_data']['distributor_name']}}</p>
                            <span class="details-heading">Mobile Number</span>
                            <p class="mb-2 details">{{  $orders['retailer']['map_distributor']['distributor_data']['mobile'] }}</p>
                            <span class="details-heading">Address</span>
                            <p class="mb-2 details">{{  $orders['retailer']['map_distributor']['distributor_data']['address']}}</p>
                            <span class="details-heading">Location</span>
                            <p class=" details">{{ $orders['retailer']['map_distributor']['distributor_data']['latitude'] }} , {{ $orders['retailer']['map_distributor']['distributor_data']['longitude'] }}</p>
                        </div>
                    </div> 
                    <div class="col-md-4 hub-order">
                        <div class="w-75 m-auto hubOrder-inner">
                            <h6 class="font-weight-bold mb-3">Delivery Agent</h6>
                            <span class="details-heading">Mobile Number</span>
                            <p class="mb-2 details">{{ $orders['apiorder']['rider_phone']}}</p>
                            <span class="details-heading">Name</span>
                            <p class="mb-2 details">{{ $orders['apiorder']['rider_name'] }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="bg-white p-3 my-4">
            <div class="row details stockTable">
                <div class="col-sm-12">
                    <h5>Product Details</h5>
                    <table cellspacing="0" cellpadding="8" border="1" class="mt-3 w-100 hub-ordered-detail layout-fixed">
                        <thead>
                            <tr>
                                <th width="50">SNo.</th>
                                <th>Product</th>
                                <th width="80">Rate(Rs)</th>
                                <th width="70">Weight</th>
                                <th width="70">Qty</th>
                                <th>Packed Qty</th>
                                <th>Delivered Qty</th>
                                <th>Discount(%)</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <?php // echo "<pre>";print_r($orders['order_items_view']);exit; ?>
                        <tbody>
                            @foreach($orders['order_items_view'] as $key=>$val)
                            <tr>
                                <td><span class="data">{{ $key + 1 }}</span></td>
                                <td><span class="data">{{ $val['products_detail']['product_name']['name']  }} <?php if ($val['products_detail']['package_unit'] != 1) { ?> {{  $val['products_detail']['unit_type']['name']   }} <?php } ?> {{ $val['products_detail']['packing_type']['name']  }} {{ $val['products_detail']['weight_type']['name']  }}</span></td>
                                <td><span class="data">{{ number_format(round($val['products_detail']['rlp'], 2), 2, '.', '') }}</span></td>
                                <td><span class="data">{{ $prod->findUnitWeightTypeName($val['products_detail']['package_unit']) }}</span></td>
                                <td><span class="data">{{ $val['quantity'] }}</span></td>
                                <td><span class="data">{{ $val['packed_quantity'] }}</span></td>
                                <td><span class="data">{{ $val['delivered_quantity'] }}</span></td>
                                <td><span class="data">{{ $orders['offer_discount_percentage'] }}</span></td>
                                @if($val['nature'] == 'FREE')
                                <td><span class="data">FREE</span></td>
                                @else
                                 <td><span class="data">{{ number_format(round(($val['products_detail']['rlp'] * $val['quantity']), 2), 2, '.', '') }}</span></td>
                                @endif
                            </tr>
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2" class="text-right font-weight-bold"></td>
                                <td></td>
                                <td  colspan="5" class="text-right font-weight-bold">Discount</td>
                                <td>{{ $orders['discount_amount'] }}</td>
                            </tr>
                            <tr>
                                <td colspan="7" class="text-right font-weight-bold"></td>
                                <td class="text-right font-weight-bold">Total Amount</td>
                                <td>{{ number_format(round($orders['total_amount'], 2), 2, '.', '')}}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>




</div>


<!-- end -->

<!-- The Modal -->



@endsection
