@extends('layouts.app')
@section('style')
<!--<link href="https://code.highcharts.com/css/highcharts.css" rel="stylesheet">-->
@endsection
@section('content')


<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list dashboard-page">
  <header-page :pagetitle="'Dashboard'" :loader="false"></header-page>
    <div class="row mt-4 mb-4 align-items-center">
      <div class="col-md-12 pd-x-2">
        <h4 class="d-inline-block font-weight-light mb-4">Calling Agent Daily Activity</h4>
        <div class="row mt-2 dash-grid">
          <div class="col-md-4">
            <div class="col-sm-12 px-3 py-4 d-flex flex-wrap align-items-center text-white grid-inner grid-agent">
              <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico">
                <i class="h2 la m-0 la-user-tie"></i>
              </div>
              <div class="grid-agent-info">
                <h3 class="mb-0 font-weight-bold">{{ $activeCallingAgent }}</h3>
                <span class="w-100 m-0 font-weight-light">Logged in Agents</span>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="col-sm-12 px-3 py-4 d-flex flex-wrap align-items-center text-white grid-inner grid-queue">
              <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico">
                <i class="h2 la m-0 la-users"></i>
              </div>
              <div class="grid-queue-info">

              <h3 class="mb-0 font-weight-bold">{{$nextDays[date('D')]}}</h3>
                <span class="w-100 m-0 font-weight-light">Current Queue</span>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="col-sm-12 px-3 py-4 d-flex flex-wrap align-items-center text-white grid-inner grid-orders">
              <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico">
                <i class="h2 la m-0 la-luggage-cart"></i>
              </div>
              <div class="grid-orders-info">
                <h3 class="mb-0 font-weight-bold">{{ $totalSales }}</h3>
                <span class="w-100 m-0 font-weight-light">Total Orders</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-4 mb-4 align-items-center">
        <div class="col-md-12 pd-x-2">
            <div class="row mt-2">
                <div class="col-sm-6">
                    <div class="bg-white px-3 pt-4">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h6>Call to Order Summary</h6>
                            {{-- <select class="custom-select graph-filter-select" id="" style="width: 150px;">
                                <option selected>Choose...</option>
                                <option value="Incoming">Incoming</option>
                                <option value="Outgoing">Outgoing</option>
                            </select> --}}
                        </div>
                        <div id="ordervalue" style="height: 300px;" class="pt-3"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="bg-white px-3 pt-4">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h6>Call to Order Ratio</h6>
                            {{-- <select class="custom-select graph-filter-select" id="" style="width: 150px;">
                                <option selected>Choose...</option>
                                <option value="Incoming">Incoming</option>
                                <option value="Outgoing">Outgoing</option>
                            </select> --}}
                        </div>
                        <div id="orderratio" style="height: 300px;" class="pt-3"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4 mb-4 align-items-center">
        <div class="col-md-12 pd-x-2">
            <div class="row mt-2">
                <div class="col-sm-12">
                    <div class="bg-white px-3 pt-4">
                        <h6 class="mb-4">Order & Delivery Ticket Size</h6>
                        <div id="ticketvalue" style="height: 300px;" class="pt-3"></div>
                    </div>
                </div>
                <!--<div class="col-sm-6">
                    <div class="bg-white px-3 pt-4">
                        <h6 class="mb-4">Average Ticket Ratio</h6>
                        <div id="ticketratio" style="height: 300px;" class="pt-3"></div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
    <div class="row mt-4 mb-4 align-items-center">
        <div class="col-md-12 pd-x-2">
            <div class="row mt-2">
                <div class="col-sm-12">
                    <div class="bg-white px-3 pt-4">
                      <h6 class="mb-4">Retail Type Wise Order Value</h6>
                      <div id="retailvalue" class="pt-3"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4 mb-4 retail-callCycle">
      <div class="col-md-12 mt-4 pd-x-2 callingCycle">
        <h4 data-target="collapse" class="d-flex flex-wrap align-items-center justify-content-between font-weight-light mb-0 pb-3 border border-top-0 border-right-0 border-left-0 accord">Retailer Calling Cycle Next 6 Days <i class="las la-plus"></i></h4>
        <div id="collapse">
          <div class="row week-grid">
            <div class="col-md-12 week-days">
              <ul class="list-group list-group-horizontal text-center justify-content-between pt-4 week-list">
                @foreach ($nextDays as $key => $day )
                <li class="border-0 list-group-item bg-transaprent p-0">
                    @if(isset($completeDays[$key]))
                 <label class="font-weight-light text-grey">{{ $completeDays[$key] }}</label>
                 @endif
                    <h3 class="bg-white shadow theme-text font-weight-bold d-flex align-items-center justify-content-center count">{{ $nextDays[$key] }}</h3>
                  </li>
                @endforeach


              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-md-12 mt-5 pd-x-2">
        <div class="row mt-2 bottom-grid-sec">
          <div class="col-sm-3">
            <div class="col-12 px-2 py-3 d-flex flex-wrap align-items-center bg-white shadow bottom-grid">
              <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico">
                <i class="h2 m-0 la la-wallet"></i>
              </div>
              <div class="text-misc">
                <h3 class="mb-0 font-weight-bold">{{ $totalSales }}</h3>
                <p class="m-0 font-weight-light">Life Time Sales</p>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="col-12 px-2 py-3 d-flex flex-wrap align-items-center bg-white shadow bottom-grid">
              <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico">
                <i class="h2 la m-0 la-user-tie"></i>
              </div>
              <div class="text-misc">
                <h3 class="mb-0 font-weight-bold">{{ $totalOnboard }}</h3>
                <p class="m-0 font-weight-light">Onboarding Agents</p>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="col-12 px-2 py-3 d-flex flex-wrap align-items-center bg-white shadow bottom-grid">
              <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico">
                <i class="h2 la m-0 la-store-alt"></i>
              </div>
              <div class="text-misc">
                <h3 class="mb-0 font-weight-bold">{{ $totalRetailers }}</h3>
                <p class="m-0 font-weight-light">Retailers Onboarded</p>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="col-12 px-2 py-3 d-flex flex-wrap align-items-center bg-white shadow bottom-grid">
              <div class="d-flex align-items-center justify-content-center mr-3 rounded-circle grid-ico">
                <i class="h2 la m-0 la-sitemap"></i>
              </div>
              <div class="text-misc">
                <h3 class="mb-0 font-weight-bold">{{ $totalDistributor }}</h3>
                <p class="m-0 font-weight-light">Distributors</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- end -->


@endsection

@section('script')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script>
    //Order Value
    var ordervalue = {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                '1 Jan',
                '2 Jan',
                '3 Jan',
                '4 Jan',
                '5 Jan',
                '6 Jan'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Call',
            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0]

        }, {
            name: 'Order',
            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5]

        }],
        "colors": [
            "#00bedd",
            "#fc6e03"
        ]
    };
    //Order Ratio
    var orderratio =
    {
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: []
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
                name: 'Call to Order Ratio',
                data: []
            }],
        "colors": [
            "#0daffc"
        ]
    };

    //Ticket Value
    var ticketvalue =  {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                // '1 Jan',
                // '2 Jan',
                // '3 Jan',
                // '4 Jan',
                // '5 Jan',
                // '6 Jan'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Order',
            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5]

        }, {
            name: 'Delivery Ticket Size',
            data: [ 176.0, 49.9, 71.5, 106.4, 129.2, 144.0]

        }],
        "colors": [
            "#253383",
            "#229802"
        ]
    };


    //Pie Chart ==> Retail Type Wise Order Value
    var retailvalue =  {
        chart: {
            styledMode: false
        },

        title: {
            text: ''
        },

        series: [{
                name:'Order Value',
            type: 'pie',
            allowPointSelect: true,
            keys: ['name', 'y', 'selected', 'sliced'],
            // data: [
            //     ['Branded Food Store', 29.9, false],
            //     ['General store-A', 71.5, false],
            //     ['General store-B', 106.4, false],
            //     ['General store-C', 129.2, false],
            //     ['Paan-A', 144.0, false],
            //     ['Paan-B', 176.0, false],
            //     ['Paan-C', 135.6, false]
            // ],
            showInLegend: true
        }]
    };
    function OnError(data) {
        console.log('fail');
    }

    $(document).ready(function(){
        $.ajax({
            url: '{{url('graph')}}',
            type: 'GET',
            async: true,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                console.log(data);
                //optionsChart.series = data['yAxis'];
                //optionsChart.xAxis.categories = data['xAxis'];
                //chart = new Highcharts.Chart(optionsChart);
                /**
                * order ratio
                **/
                //
                orderratio.xAxis.categories = data['xAxis'];
                orderratio.series =  data['orderRatio'];
                retailvalue.series[0].data = data['shopTypeGraph'];

                ticketvalue.xAxis.categories = data['delivered'];
                ticketvalue.series = data['call'].delivery;
                console.log('++++++++',ordervalue.series, data['call'].all);
                ordervalue.xAxis.categories = data['pending'];
                ordervalue.series = data['call'].all;

                // console.log(retailvalue.series[0].data,'.............',data['shopTypeGraph']);

                new Highcharts.chart('orderratio',orderratio );

                new Highcharts.chart('ordervalue', ordervalue);

                new Highcharts.chart('retailvalue',retailvalue);

                new Highcharts.chart('ticketvalue',ticketvalue);
            },
            error: OnError
        });
    })

</script>
@endsection
