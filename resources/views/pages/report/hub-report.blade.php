@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Hub Report'"></header-page>

    <hub-report></hub-report>
<!-- end -->


@endsection
