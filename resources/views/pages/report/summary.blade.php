@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Callers Summary'"></header-page>

    <div class="row mt-4 mb-4">
        <div class="col-md-12 pd-x-2">
            <div class="row align-items-center">
                <div class="col-md-3 pr-0 position-relative search-div">
                  <input type="text" name="search" id="search" value="" placeholder="Search Here" class="px-3 w-100 pr-5 rounded-lg light-border border search">
                  <i class="position-absolute la la-search"></i>
                </div>
                <div class="col-md-3 offset-md-6 position-relative date-div">
                  <span class="position-relative float-left w-100 date-input">
                    <input type="text" name="datefilter" placeholder="Date" class="px-3 w-100 rounded shadow light-border border date-picker">
                  </span>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-4">
      <div class="col-md-12 mt-3 pd-x-2">
          <div class="col-12 p-0 inner">
              <table cellspacing="0" class="table table-borderless table-list retailer-list">
                  <thead>
                      <tr>
                          <th class="pl-4">Telecaller</th>
                          <th class="text-center">Calls Attended</th>
                          <th class="text-center">Order Amount(Rs)</th>
                          <th class="text-center">Orders</th>
                          <th class="text-center">Avg. Order Value(Rs)</th>
                          <th class="text-center">Average Call Duration(Min)</th>
                          <th class="text-center pr-3">Conversion Rate(%)</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pl-4 border border-right-0 light-border">
                              <span class="data">Telecaller_name_1</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 text-center border border-right-0 border-left-0 light-border">
                              <span class="data">394</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 text-center border border-right-0 border-left-0 light-border">
                              <span class="data">4700</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 text-center border border-left-0 border-right-0 light-border">
                              <span class="data">8</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 text-center border border-left-0 border-right-0 light-border">
                              <span class="data">587.5</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 text-center border border-left-0 border-right-0 light-border">
                              <span class="data">10</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pr-3 text-center border border-left-0 light-border">
                              <span class="text-success data">2</span>
                          </td>
                      </tr>
                      <tr class="separation"><td class="p-0">&nbsp;</td></tr>

                      <tr>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pl-4 border border-right-0 light-border">
                              <span class="data">Telecaller_name_2</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 text-center border border-right-0 border-left-0 light-border">
                              <span class="data">394</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 text-center border border-right-0 border-left-0 light-border">
                              <span class="data">4700</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 text-center border border-left-0 border-right-0 light-border">
                              <span class="data">8</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 text-center border border-left-0 border-right-0 light-border">
                              <span class="data">617.5</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 text-center border border-left-0 border-right-0 light-border">
                              <span class="data">20</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pr-3 text-center border border-left-0 light-border">
                              <span class="text-success data">15</span>
                          </td>
                      </tr>
                      <tr class="separation"><td class="p-0">&nbsp;</td></tr>

                  </tbody>
              </table>
          </div>
      </div>

</div>
<!-- end -->


@endsection
