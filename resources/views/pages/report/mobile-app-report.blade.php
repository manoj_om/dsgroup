@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Mobile App Report'"></header-page>

    <mobile-app-report></mobile-app-report>
<!-- end -->


@endsection
