@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Scheme Report'"></header-page>

    <scheme-report></scheme-report>
<!-- end -->


@endsection
