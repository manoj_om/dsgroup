@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Rider Report'"></header-page>

    <rider-report></rider-report>
<!-- end -->


@endsection
