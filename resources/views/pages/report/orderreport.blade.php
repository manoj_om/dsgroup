@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Order Report'"></header-page>

    <order-report></order-report>
<!-- end -->


@endsection
