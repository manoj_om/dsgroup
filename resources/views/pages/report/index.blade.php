@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Call Status with Audio'" :loader="false"></header-page>

    <div class="row mt-4 mb-4">
        <div class="col-md-12 pd-x-2">
            <div class="row align-items-center">
                <div class="col-md-3 pr-0 position-relative search-div">
                  <input type="text" name="search" id="search" value="" placeholder="Search Here" class="px-3 w-100 pr-5 rounded-lg shadow light-border border search">
                  <i class="position-absolute la la-search"></i>
                </div>
                <div class="col-md-3 offset-md-6 position-relative date-div">
                  <span class="position-relative float-left w-100 date-input">
                    <input type="text" name="datefilter" value="" placeholder="Date" class="px-3 w-100 rounded shadow light-border border date-picker">
                  </span>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-4">
      <div class="col-md-12 mt-3 pd-x-2">
          <div class="col-12 p-0 inner">
              <table cellspacing="0" class="table table-borderless table-list retailer-list">
                  <thead>
                      <tr>
                          <th class="pl-4">Retailer</th>
                          <th>Mobile</th>
                          <th>Location</th>
                          <th>Queue Added</th>
                          <th>Call Start</th>
                          <th>Call End</th>
                          <th>Telecaller</th>
                          <th>Call Status</th>
                          <th>Order Status</th>
                          <th class="pr-3">Audio</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pl-4 border border-right-0 light-border">
                              <span class="data">Retailer name_1</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                              <span class="data">9532145678</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                              <span class="data">Sector 49, Gurgaon</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data">31-10-2019 <font class="text-success">17:15:12</font></span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="text-success data">17:15:12</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="text-danger data">17:25:12</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data">17:15:12</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data">Connected with TC</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data">Punched</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pr-3 text-center border border-left-0 light-border">
                              <span class="text-secondary data"><i class="fa fa-play"></i></span>
                          </td>
                      </tr>
                      <tr class="separation"><td class="p-0">&nbsp;</td></tr>

                      <tr>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pl-4 border border-right-0 light-border">
                              <span class="data">Retailer name_1</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                              <span class="data">9532145678</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                              <span class="data">Sector 49, Gurgaon</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data">31-10-2019 <font class="text-success">17:15:12</font></span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="text-success data">17:15:12</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="text-danger data">17:25:12</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data">17:15:12</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data">Call Drop</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data">NA</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pr-3 text-center border border-left-0 light-border">
                              <span class="text-secondary data"><i class="fa fa-play"></i></span>
                          </td>
                      </tr>
                      <tr class="separation"><td class="p-0">&nbsp;</td></tr>

                  </tbody>
              </table>
          </div>
      </div>

</div>
<!-- end -->

@endsection
