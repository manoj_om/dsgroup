@extends('layouts.agent')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
   <header-guard-page :pagetitle="'Agent Profile'"></header-guard-page>
      <div class="row position-relative profile-page">
        <div class="col-12 pt-4 profile-inner">
            <div class="row pd-x-2 pt-3 position-relative">
              <div class="col-md-6 p-0 pt-3 d-flex flex-wrap text-center profile-detail">
                  <agent-image-upload></agent-image-upload>
                <span class="mt-5"><h5 class="text-white text-capitalize font-weight-normal">{{ Auth::user()->name }}</h5></span>
              </div>
            </div>
        </div>
      </div>

      <div class="row">
        <div class="container">
          <div class="col-md-8 offset-md-2 user-details">
            <div class="row mt-4">
              <div class="col-6 pl-4 mb-2">
                <h6 class="m-0 text-grey-dark font-weight-light">Mobile:</h6>
                <h5 class="text-body">+91 {{ Auth::user()->mobile_number }}</h5>
              </div>
              <div class="col-6 pl-5 mb-2">
                <h6 class="m-0 text-grey-dark font-weight-light">Email Id:</h6>
                <h5 class="text-body">{{ Auth::user()->email }}</h5>
              </div>
              <div class="col-6 pl-4 mt-5">
                <h6 class="m-0 text-grey-dark font-weight-light">Department:</h6>
               
                 <h5 class="text-body">Call Center</h5>
                
              </div>
              <div class="col-6 pl-5 mt-5">
                <h6 class="m-0 text-grey-dark font-weight-light">Designation:</h6>
                 
                <h5 class="text-body">Call Agent</h5>
               
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
<!-- end -->









@endsection
