@extends('layouts.agent')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-guard-page :pagetitle="'Calling Agent Status'"></header-guard-page>
    <auth-call-center-agent-table></auth-call-center-agent-table>
</div>
<!-- end -->
@endsection
