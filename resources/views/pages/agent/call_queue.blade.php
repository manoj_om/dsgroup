@extends('layouts.agent')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    
     
    <header-guard-page :pagetitle="'Current Call Queue'"></header-guard-page>
      
    <div class="row mt-4">
      <div class="col-md-12 mt-3 pd-x-2">
          <div class="col-12 p-0 inner">
              <table cellspacing="0" class="table table-borderless table-list retailer-list">
                  <thead>
                      <tr>
                          <th class="pl-4">Retailer</th>
                          <th>Mobile</th>
                          <th>Location</th>
                          <th>Queue Added</th>
                          <th>Schedule Start & End Time</th>
                          <th class="pr-3">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pl-4 border border-right-0 light-border">
                              <span class="data">Retailer name_1</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                              <span class="data">9532145678</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                              <span class="data">Spaze i_tech ParkSector 49, Gurgaon</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data">31-10-2019 <font class="text-success">17:15:12</font></span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data"><font class="text-success">10:00 AM</font> - 17:45 PM</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pr-3 border border-left-0 light-border">
                              <span class="actions">
                                <a href="#" title="Reject" class="action-btn">
                                  <img src="{{asset('images/reject-icon.png')}}" alt="reject" class="action-icon">
                                </a>
                              </span>
                          </td>
                      </tr>
                      <tr class="separation"><td class="p-0">&nbsp;</td></tr>

                      <tr>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pl-4 border border-right-0 light-border">
                              <span class="data">Retailer name_1</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                              <span class="data">9532145678</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                              <span class="data">Spaze i_tech ParkSector 49, Gurgaon</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data">31-10-2019 <font class="text-success">17:15:12</font></span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data"><font class="text-success">10:00 AM</font> - 17:45 PM</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pr-3 border border-left-0 light-border">
                              <span class="actions">
                                <a href="#" title="Reject" class="action-btn">
                                  <img src="{{asset('images/reject-icon.png')}}" alt="reject" class="action-icon">
                                </a>
                              </span>
                          </td>
                      </tr>
                      <tr class="separation"><td class="p-0">&nbsp;</td></tr>

                      <tr>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pl-4 border border-right-0 light-border">
                              <span class="data">Retailer name_1</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                              <span class="data">9532145678</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                              <span class="data">Spaze i_tech ParkSector 49, Gurgaon</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data">31-10-2019 <font class="text-success">17:15:12</font></span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                              <span class="data"><font class="text-success">10:00 AM</font> - 17:45 PM</span>
                          </td>
                          <td data-toggle="modal" data-target="#myModal2" class="bg-white py-4 pr-3 border border-left-0 light-border">
                              <span class="actions">
                                <a href="#" title="Reject" class="action-btn">
                                  <img src="{{asset('images/reject-icon.png')}}" alt="reject" class="action-icon">
                                </a>
                              </span>
                          </td>
                      </tr>
                      <tr class="separation"><td class="p-0">&nbsp;</td></tr>

                  </tbody>
              </table>
          </div>
      </div>
  </div>
</div>
<!-- end -->
@endsection
