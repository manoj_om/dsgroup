@extends('layouts.agent')

@section('content')
<div class="col-md-12 toggle-switch bg-grey onboard-list">
    <header-guard-page :pagetitle="'Current Call Queue'"></header-guard-page>
    <retailer-queue :isadmin="false"></retailer-queue>
</div>
@endsection
