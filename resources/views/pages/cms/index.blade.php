@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Home Slider Banners'" :loader="true"></header-page>
     <cms-add-form></cms-add-form>
     <cms-table></cms-table>
</div>

@endsection
