@extends('layouts.app')

@section('content')
<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Order Status Setting'" :loader="false"></header-page>
     <order-status-setting></order-status-setting>
     
</div>

@endsection
