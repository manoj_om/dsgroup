@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Call Script'" :loader="true"></header-page>

    <call-script-form></call-script-form>

    <call-script-table></call-script-table>
      </div>

@endsection
