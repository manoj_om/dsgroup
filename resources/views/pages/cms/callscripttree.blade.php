@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Call Script Tree'" :loader="true"></header-page>
 
    <graph-tree-item :modaldata="{{ $data }}"></graph-tree-item>
      </div>

@endsection
