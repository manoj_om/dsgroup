@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Scheme & Promotions'" :loader="true"></header-page>

    <add-scheme-form></add-scheme-form>    
    
    <scheme-table></scheme-table>

    
</div>
<!-- end -->


@endsection
