@extends('layouts.app')

@section('content')


<?php $bug = false ;
if(isset($errors) && count($errors) > 0){
  $bug = true;  
}else{
   $bug = false; 
}  ?>
@error('excelfile')
<product-master messege="{{ $message }}" bug="{{ $bug }}"></product-master>

@else

<product-master bug="{{ $bug }}"></product-master>
@enderror

<div class="modal fade" id="errorpop">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close position-absolute close-modals" data-dismiss="modal">&times;</button>
            <!-- Modal body -->
            <div class="modal-body">
                @if (count($errors) > 0)
                <div class = "alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>



@endsection
