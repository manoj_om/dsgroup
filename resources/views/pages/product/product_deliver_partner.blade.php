@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
  <header-page :pagetitle="'Delivery Partner'"></header-page>

  <div class="row my-3">
    <div class="col-md-12 pd-x-2 mt-3">
        <div class="col-md-3 mb-5">
          <div class="row pr-0 position-relative search-div">
            <input type="text" name="searchkey" id="searchkey" placeholder="Search Here" class="px-3 w-100 w-100 pr-5 rounded-lg light-border border search">
            <i class="position-absolute la la-search"></i>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row inner">
            <table cellspacing="0" class="table table-borderless table-list retailer-list">
                <thead>
                    <tr>
                        <th class="pl-4">SNo.</th>
                        <th>Delivery Agent Name</th>
                        <th>Contact No</th>
                        <th>Email</th>
                        <th>Delivery Partner Name</th>
                        <th>Latitude</th>
                        <th>Longitude</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="bg-white py-4 pl-4 border border-right-0 light-border"><span class="data">1</span></td>
                        <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Tulsi Royal Khajoor</span></td>
                        <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">9632145789</span></td>
                        <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">test@gmail.com</span></td>
                        <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Deliverypartner1</span></td>
                        <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">24.21548</span></td>
                        <td class="bg-white p-3 pr-4 border border-left-0 rounded-right light-border"><span class="data">80.23549</span></td>
                    </tr>
                    <tr class="separation">
                        <td class="p-0">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="bg-white py-4 pl-4 border border-right-0 light-border"><span class="data">1</span></td>
                        <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Tulsi Royal Khajoor</span></td>
                        <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">9632145789</span></td>
                        <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">test@gmail.com</span></td>
                        <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">Deliverypartner1</span></td>
                        <td class="bg-white py-4 border border-right-0 border-left-0 light-border"><span class="data">24.21548</span></td>
                        <td class="bg-white p-3 pr-4 border border-left-0 rounded-right light-border"><span class="data">80.23549</span></td>
                    </tr>
                    <tr class="separation">
                        <td class="p-0">&nbsp;</td>
                    </tr>

                </tbody>
            </table>
        </div>
        </div>
    </div>
  </div>
</div>
<!-- end -->


@endsection
