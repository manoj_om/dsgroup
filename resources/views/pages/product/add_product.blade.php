@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Distributor Master'" :loader="false"></header-page>
    <div class="row mt-4 mb-4">
      <div class="col-md-12 pd-x-2">
          <div class="row align-items-center">
              <div class="col-md-4 attention-request">
                    <h4 class="font-weight-light">Add Distributor</h4>
              </div>
          </div>

          <div class="row">
              <div class="col-md-12 add-user-form open-form">
                  <form method="" action="" id="manage-user" class="bg-white rounded border light-border p-4 manage-user">
                      <div class="row p-4">
                          <div class="col-md-4 mb-4">
                            <label for="distributor-name">Distributor Name <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <input type="text" name="distributor-name" id="distributor-name" placeholder="Enter Here" class="w-100 input-shadow border light-border rounded px-3">
                          </div>
                          <div class="col-md-4 mb-4">
                            <label for="email">Distributor Code <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <input type="text" name="distributor-code" id="distributor-code" placeholder="Enter Here" class="w-100 input-shadow border light-border rounded px-3">
                          </div>
                          <div class="col-md-4 mb-4">
                            <label for="mobile">Mobile <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <input type="text" name="mobile" id="mobile" placeholder="Enter Here" class="w-100 input-shadow border light-border rounded px-3">
                          </div>
                          <div class="col-md-4 mb-4 mt-3">
                            <label for="email">Email <sup class="text-danger font-weight-bold h6">*</sup></label>
                              <input type="email" name="email" id="email" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded">
                          </div>
                          <div class="col-md-4 mb-4 mt-3">
                            <label for="pan">PAN <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <input type="text" name="pan" id="pan" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded">
                          </div>
                          <div class="col-md-4 mb-4 mt-3">
                            <label for="gst">GST <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <input type="text" name="gst" id="gst" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded">
                          </div>
                          <div class="col-md-4 mb-4 mt-3">
                            <label for="aadhar">Aadhar No. <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <input type="text" name="aadhar" id="aadhar" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded">
                          </div>
                          <div class="col-md-4 mb-4 mt-3">
                            <label for="website">Website <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <select name="website" id="website" class="w-100 px-3 pr-5 select-shadow rounded-lg light-border show-data">
                                <option value="">Select</option>
                                <option value="">Website 1</option>
                                <option value="">Website 2</option>
                                <option value="">Website 3</option>
                            </select>
                          </div>
                          <div class="col-md-4 mb-4 mt-3">
                            <label for="state">State <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <select name="state" id="state" class="w-100 px-3 pr-5 select-shadow rounded-lg light-border show-data">
                                <option value="">Select</option>
                                <option value="">State 1</option>
                                <option value="">State 2</option>
                                <option value="">State 3</option>
                            </select>
                          </div>
                          <div class="col-md-4 mb-4 mt-3">
                            <label for="city">City <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <select name="city" id="city" class="w-100 px-3 pr-5 select-shadow rounded-lg light-border show-data">
                                <option value="">Select</option>
                                <option value="">City 1</option>
                                <option value="">City 2</option>
                                <option value="">City 3</option>
                            </select>
                          </div>
                          <div class="col-md-4 mb-4 mt-3">
                            <label for="latitude">Latitude <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <input type="text" name="latitude" id="latitude" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded">
                          </div>
                          <div class="col-md-4 mb-4 mt-3">
                            <label for="longitude">Longitude <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <input type="text" name="longitude" id="longitude" placeholder="Enter here" class="w-100 px-3 input-shadow border light-border rounded">
                          </div>
                          <div class="col-md-12 mb-4 mt-3">
                            <label for="add">Address <sup class="text-danger font-weight-bold h6">*</sup></label>
                            <textarea name="add" id="add" placeholder="Enter here" rows="3" class="w-100 px-3 py-3 input-shadow border light-border rounded"></textarea>
                          </div>

                          <div class="col-md-12 mt-4 text-center">
                              <div class="col-md-6 offset-md-3 margin-auto">
                                  <button type="reset" class="h6 text-uppercase w-25 py-2 mr-2 d-inline-block rounded theme-text border-btn cancel-btn">Cancel</button>
                                  <button type="submit" class="h6 text-uppercase d-inline-block w-25 py-2 rounded text-white theme-bg border-btn save-btn">SAVE</button>
                              </div>
                          </div>
                      </div>
                  </form>
              </div>
          </div>

      </div>
    </div>

</div>
<!-- end -->


@endsection
