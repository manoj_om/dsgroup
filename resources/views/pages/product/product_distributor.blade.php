@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
  <header-page :pagetitle="'Distributor Master'" :loader="false"></header-page>
    <div class="row mt-4 mb-4 align-items-center">
        <div class="col-md-12 pd-x-2">
          <div class="row">
            <div class="col-md-3 pr-0 position-relative search-div">
              <input type="text" name="searchkey" id="searchkey" placeholder="Search Here" class="px-3 w-100 w-100 pr-5 rounded-lg light-border border search">
              <i class="position-absolute la la-search"></i>
            </div>
            <div class="col-md-8 d-flex flex-wrap justify-content-end text-right header-action-buttons">
                <div class="d-flex flex-wrap align-items-center checkbox-switch">
                    <label class="m-0">
                      <input type="checkbox" checked="checked" class="ios-switch green bigswitch">
                      <div>
                          <div></div>
                      </div>
                    </label>
                    <span class="ml-2 mt-1 theme-text">Active</span>
                </div>
                <div class="ml-4 add-user-btn">
                    <h6 class="m-0"><a href="{{route('add-product')}}" title="" class="d-inline-block w-100 py-2 px-4 rounded text-white theme-bg border-btn add-user-btn">ADD</a></h6>
                </div>
            </div>
          </div>

          <div class="row my-3">
              <div class="col-md-12 mt-5">
                  <div class="col-12 p-0 inner">
                      <table class="table table-borderless table-list retailer-list">
                          <thead>
                              <tr>
                                  <th class="position-relative pl-4">
                                    <label class="checklabel">
                                      <input type="checkbox" name="selectcheck" id="selectall" class="checkbox-select"> <span class="checkmark"></span>
                                    </label>
                                  </th>
                                  <th>SNo.</th>
                                  <th>Distributor Name</th>
                                  <th>Distributor Code</th>
                                  <th>Mobile</th>
                                  <th>Email</th>
                                  <th>GST</th>
                                  <th>City</th>
                                  <th>Status</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr style="cursor: pointer;">
                                  <td class="position-relative bg-white pl-4 border border-right-0 rounded-left light-border">
                                    <label class="checklabel">
                                      <input type="checkbox" name="selectcheck" id="selectall" class="checkbox-select">
                                      <input name="selectcheck" id="selectall" class="checkbox-select">
                                      <span class="checkmark"></span>
                                    </label>
                                  </td>
                                  <td data-toggle="modal" data-target="#myModal-distri" class="bg-white py-4 border border-left-0 border-right-0 light-border">
                                    <span class="data">1</span>
                                  </td>
                                  <td data-toggle="modal" data-target="#myModal-distri" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                                    <span class="data">Om Prakash Pandey</span>
                                  </td>
                                  <td data-toggle="modal" data-target="#myModal-distri" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                                    <span class="data">DIST01252</span>
                                  </td>
                                  <td data-toggle="modal" data-target="#myModal-distri" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                                    <span class="data">9532145678</span>
                                  </td>
                                  <td data-toggle="modal" data-target="#myModal-distri" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                                    <span class="data">omprakash@gmail.com</span>
                                  </td>
                                  <td data-toggle="modal" data-target="#myModal-distri" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                                    <span class="data">07AAECR2971C1Z</span>
                                  </td>
                                  <td data-toggle="modal" data-target="#myModal-distri" class="bg-white py-4 border border-right-0 border-left-0 light-border">
                                    <span class="data">Gurgaon</span>
                                  </td>
                                  <td class="bg-white py-4 border border-right-0 border-left-0 light-border">
                                    <span class="text-success data">Active</span>
                                  </td>
                              </tr>
                              <tr class="separation">
                                  <td class="p-0">&nbsp;</td>
                              </tr>

                          </tbody>
                      </table>
                  </div>
                  <div class="col-md-12 mt-4 text-center">
                      <div class="col-md-6 offset-md-3 margin-auto">
                        <span class="h6 m-0">
                          <a href="#" title="" class="text-uppercase w-25 py-2 mr-2 d-inline-block rounded theme-text border-btn cancel-btn">Cancel</a>
                        </span>
                        <span class="h6 m-0">
                          <a href="#" title="" class="text-uppercase d-inline-block w-25 py-2 rounded text-white theme-bg border-btn save-btn">SAVE</a>
                        </span>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>
<!-- end -->

<!-- The Modal -->
  <div class="modal fade" id="myModal-distri">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" data-dismiss="modal" class="close position-absolute close-modals">×</button>
        <!-- Modal body -->
        <div class="p-4 modal-body">
            <div class="p-2 row">
                <div class="col-md-12 distributor-modal">
                    <h4 class="font-weight-normal pt-1 pb-3">Namdharis Fresh</h4>
                    <ul class="list-group border border-top-0 border-right-0 border-left-0 distributor-view">
                        <li class="d-flex pt-0 mb-2 pb-3 px-0 list-group-item border-0">
                          <span class="icons-ico mr-4">
                            <img src="{{asset('images/location-icon.png')}}" alt="location-icon" class="icons">
                          </span>
                          <span id="company_address" class="w-100 font-weight-light text">Spaze iTechpark, Shop no 125, Sector 49, Gurugram, Haryana</span>
                        </li>
                        <li class="d-flex pt-0 mb-2 pb-3 px-0 list-group-item border-0">
                          <span class="icons-ico mr-4">
                            <img src="{{asset('images/web-icon.png')}}" alt="web-icon" class="icons">
                          </span>
                          <span id="company_website" class="w-100 font-weight-light text">www.dstrbtor.in</span>
                        </li>
                        <li class="d-flex pt-0 mb-2 pb-3 px-0 list-group-item border-0">
                          <span class="icons-ico mr-4">
                            <img src="{{asset('images/geo-location-icon.png')}}" alt="geo-location-icon" class="icons">
                          </span>
                          <span id="latitude_longitude" class="w-100 font-weight-light text">24.235218, 70.256692</span>
                        </li>
                        <li class="d-flex pt-0 mb-2 pb-3 px-0 list-group-item border-0">
                          <span class="icons-ico mr-4">
                            <img src="{{asset('images/phone-icon.svg')}}" alt="mail-icon" class="icons">
                          </span>
                          <span id="distri_contact" class="w-100 font-weight-light text">+91 9876543210, 952314587521</span>
                        </li>
                        <li class="d-flex pt-0 mb-2 pb-3 px-0 list-group-item border-0">
                          <span class="icons-ico mr-4">
                            <img src="{{asset('images/mail-icon.svg')}}" alt="mail-icon" class="icons">
                          </span>
                          <span id="distri_email" class="w-100 font-weight-light text">vipul.kumar@dsgroup.com</span>
                        </li>
                    </ul>
                    <div class="col-md-12 mt-2 pt-3 kyc-grid">
                      <div class="row">
                        <div class="pl-0 col-md-4">
                          <h4>GST:</h4>
                          <p>AVC01258VBDD02</p>
                        </div>
                        <div class="pl-5 border border-top-0 border-bottom-0 col-md-4">
                          <h4>PAN:</h4>
                          <p>AVC01258V</p>
                        </div>
                        <div class="pl-5 col-md-4">
                          <h4>Aadhar:</h4>
                          <p>96321456821</p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>


@endsection
