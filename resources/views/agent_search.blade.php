@extends('layouts.app')

@section('content')
<?php
$flage=1;

if($is_approve=='all') {
    $flage=-1;
}
?>
<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">


    <header-page :pagetitle="'Onboarding Agent'" :loader="false"></header-page>

    <div class="row mt-4 mb-4">
        <div class="col-md-12">
            <form name="search_form" id="search_form" method="post" action="{{ route('searchAgent') }}">
            <div class="row">
                <div class="col-md-4 pr-0 position-relative search-div">

                        @csrf
                        <?php if (isset($key) && $key != '') { ?>
                            <input type="text" class="px-3 w-100 w-100 pr-5 rounded-lg light-border border search" value="{{ $key }}" name="searchkey" id="searchkey" placeholder="Search Here" onkeyup="searchTable()">
                        <?php } else { ?>
                            <input type="text" class="px-3 w-100 w-100 pr-5 rounded-lg light-border border search" name="searchkey" id="search" placeholder="Search Here" onkeyup="searchTable()">
                        <?php } ?>

                        <i class="position-absolute la la-search" onclick="searchTable()"></i>

                </div>
                <div class="col-md-8 text-right filter-div">
                    <select name="is_approve" class="px-3 pr-5 rounded-lg light-border show-data" onchange="show_result(this.value);">
                        <option value="all" <?php if($is_approve=='all' and $flage==-1) { ?>selected = ""<?php } ?>>All</option>
                        <option value="0"  <?php if($is_approve==0 and $flage==1) { ?>selected=""<?php } ?>>Waiting For Approval</option>
                         <option value="1"  <?php if($is_approve=='1') { ?>selected=""<?php } ?>>Approved</option>
                         <option value="2"  <?php if($is_approve=='2') { ?>selected=""<?php } ?>>Rejected</option>
                    </select>
                </div>
            </div>
            </form>

            <div class="row">
                <div class="                col-md-12">
                    <div class="col-12 p-0 inner">
                        <table cellspacing="0" class="table table-borderless table-list agents-list">
                            <thead>
                                <tr>
                                    
                                    <th class="px-3">Name</th>
                                    <th class="px-3">Mobile Number</th>
                                    <th class="px-3">Aadhar Number</th>
                                    <th class="px-3">Agency Name</th>
                                    <th class="px-3 pr-4">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key=>$value)
                                <tr class="table-rows">
                                    <td class="bg-white p-3 pl-4 d-flex align-items-center border border-right-0 rounded-left light-border">
                                        <img src="uploads/agents/{{$value['avtar']}}" alt="list-image" class="mr-2 img-ico" />
                                        <span class="value">{{$value['name']}}</span>
                                    </td>
                                    <td class="bg-white p-3 border border-right-0 border-left-0 light-border">{{$value['mobile_number']}}</td>
                                    <td class="bg-white p-3 border border-right-0 border-left-0 light-border">{{$value['adhar_number']}}</td>
                                    <td class="bg-white p-3 border border-right-0 border-left-0 light-border">{{$value['agency_name']}}</td>
                                    <td class="bg-white p-3 pr-4 border border-left-0 rounded-right light-border">
                                        <span class="actions">
                                           <?php if ($value['is_approve'] == 0) { ?>
                                            <a href="#" title="View" class="action-btn" data-toggle="modal" onclick="showAgentDetail({{ json_encode($value) }})" data-target="#myModal">
                                                <img src="images/view-icon.png" alt="view" class="action-icon" />
                                            </a>
                                            <?php } else if($value['is_approve'] == 1) { ?>
                                             <a href="{{ route('approveView', [$value['id']]) }}" title="View" class="action-btn">
                                                <img src="images/view-icon.png" alt="view" class="action-icon" />
                                            </a>
                                            <?php } else if($value['is_approve'] == 2) { ?>
                                            <a href="#" title="View" class="action-btn" data-toggle="modal" onclick="showAgentDetail({{ json_encode($value) }})" data-target="#myModal">
                                                <img src="images/view-icon.png" alt="view" class="action-icon" />
                                            </a>
                                            <?php } ?>
                                        </span>
                                        <?php if ($value['is_approve'] == 0) { ?>
                                            <span class="actions">
                                                <a href="#" title="Approve" onclick="approveRequest({{ json_encode($value) }})" class="action-btn">
                                                    <img src="images/approve-icon.png" alt="approve" class="action-icon" />
                                                </a>
                                            </span>
                                            <span class="actions">
                                                <a href="#" title="Reject" onclick="RejectRequest({{ json_encode($value) }})" class="action-btn">
                                                    <img src="images/reject-icon.png" alt="reject" class="action-icon" />
                                                </a>
                                            </span>
                                        <?php } ?>
                                        <?php if ($value['is_approve'] == 1) { ?>
                                            <span class="actions">
                                                <i class="h5 m-0 align-middle la la-thumbs-up"></i>
                                            </span>
                                        <?php } ?>
                                        <?php if ($value['is_approve'] == 2) { ?>
                                            <span class="actions">
                                                <i class="h5 m-0 align-middle la la-thumbs-down"></i>
                                            </span>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr class="separation">
                                    <td class="p-0">&nbsp;</td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
<!--                    <div class="col-md-12 text-center">
                        <a href="{{ route('home') }}" title="" class="d-inline-block theme-bg text-white px-4 py-3 mt-3 mb-5 text-uppercase rounded backtolist">Back to list</a>
                    </div>-->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- end -->


<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <button type="button" class="close position-absolute close-modals" data-dismiss="modal">&times;</button>
            <!-- Modal body -->
            <div class="pt-5 px-4 modal-body">
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-4 pr-0">
                            <img id='agent_pic' src="images/detail-img.jpg" alt="detail"  class="w-100 img-fluid" />
                            <div class="d-flex flex-wrap align-items-center justify-content-between py-3 contact-info">
                                <span id="agent_name" class="h4 font-weight-normal mb-0 name">John Dore</span><span class="number"><i class="fa fa-phone"></i> <small id="agent_phone">+91 9876543210</small></span>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <ul class="list-group">
                                <li class="d-flex pt-0 mb-2 pb-3 list-group-item border-0">
                                    <span class="icons-ico mr-2">
                                        <img src="images/bag-icon.png" alt="" class="icons" />
                                    </span>
                                    <span id="company_name" class="text">Manpower Services India Pvt Ltd</span>
                                </li>
                                <li class="d-flex pt-0 mb-2 pb-3 list-group-item border-0">
                                    <span class="icons-ico mr-2">
                                        <img src="images/location-icon.png" alt="" class="icons" />
                                    </span>
                                    <span id="company_address" class="text">H.No 135, G Block, Sector 49, Gurgaon, Haryana 120018</span>
                                </li>
                                <li class="d-flex pt-0 mb-2 pb-3 list-group-item border-0">
                                    <span class="icons-ico mr-2">
                                        <img src="images/mail-icon.png" alt="" class="icons" />
                                    </span>
                                    <span id="agent_email" class="text">johndore1990@yahoo.com</span>
                                </li>
                                <li class="d-flex pt-0 mb-2 pb-3 list-group-item border-0">
                                    <span class="icons-ico mr-2">
                                        <img src="images/identity-icon.png" alt="" class="icons" />
                                    </span>
                                    <span id="agent_pancard" class="text">DS21053</span>
                                </li>
                                <li class="d-flex pt-0 mb-2 pb-3 list-group-item border-0">
                                    <span class="icons-ico mr-2">
                                        <img src="images/adhar-icon.png" alt="" class="icons" />
                                    </span>
                                    <span id="agent_adhaar" class="text">9621458750</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
     
var mainUrl = '<?php echo config('app.url'); ?>public';

    function searchTable(){

      if($("#searchkey").val() != ''){
           $('#search_form').submit();
        }else{
            alert('please enter key to search');
        }

    }
    function showAgentDetail(data){



    $("#agent_name").text(data.name);
    $("#agent_phone").text(data.mobile_number);
    $("#company_name").text(data.agency_name);
    $("#company_address").text(data.address);
    $("#agent_email").text(data.email);
    $("#agent_pancard").text(data.employee_code);
    $("#agent_adhaar").text(data.adhar_number);
    $("#agent_pic").attr("src", 'uploads/agents/' + data.avtar);
    }

    function approveRequest(data){

    var token = document.head.querySelector('meta[name="csrf-token"]');
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    // send contact form data.
    axios.get(mainUrl + '/approve-agent/'+data.id).then((response) => {
    if(response.data.code == 200){
    location.reload();
    }
    console.log(response)
    }).catch((error) => {

    console.log(error.response.data)
    });
    }
    function RejectRequest(data){
    var token = document.head.querySelector('meta[name="csrf-token"]');
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    // send contact form data.
    axios.get(mainUrl + '/reject-agent/'+data.id).then((response) => {
    if(response.data.code == 200){
    location.reload();
    }
    console.log(response)
    }).catch((error) => {

    console.log(error.response.data)
    });
    }

    function show_result(val_drop)
    {

        document.getElementById("search_form").submit();
    }
</script>




@endsection
