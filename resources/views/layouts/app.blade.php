<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Admin - DS Group</title>
        <link rel="icon" href="{{asset('images/favicon.png')}}" type="image/icon type">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/"> -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700&display=swap" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('css/line-awesome.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
        <script  src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/socket.io.js') }}"></script>
        @yield('style')

    </head>
    <body>
        <div id="app">
            <div class="container-fluid">
                <div class="row">
                    <!-- sidebar section -->
                    <div class="col-md-3 p-0 theme-bg toggle-switch sidebar-list">
                        @include('partials.sidebar')
                    </div>
                    @yield('content')
                </div>
            </div>
            </div>
            <!-- Scripts -->
            <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/"></script> -->
             <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/"></script>-->
            <script  src="{{ asset('js/bootstrap.min.js') }}"></script>
            <script  src="{{ asset('js/popper.min.js') }}"></script>
            <script  src="{{ asset('js/main.js') }}"></script>
            <script src="{{ asset('js/app.js') }}"></script>

            @yield('script')

    </body>
</html>
