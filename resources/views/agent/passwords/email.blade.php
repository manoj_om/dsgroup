@extends('layouts.loginapp')
@section('content')
<div class="d-flex flex-wrap login-section h-100">
    <div class="col-sm-5 p-4 d-flex flex-wrap align-content-center light-bg full-height login-left">
        <div class="col-12 login-inner">
            <img src="{{asset('images/login-img.png')}}" alt="login-img" class="img-fluid" />
        </div>
    </div>
    <div class="col-sm-7 p-5 d-flex flex-wrap align-content-center bg-grey full-height login-right">
        <div class="col-md-10 login-form">
            <div class="col-12 mb-4 login-logo">
                <img src="{{asset('images/logo.svg')}}" alt="logo" class="logo" />
            </div>
            <div class="col-12 form-section">
                <div class="mt-5 mb-5 d-inline-block form-title">
                    <h3 class="theme-text font-weight-light">{{ __('Reset Password') }}</h3>
                </div>
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ route('agent.password.email') }}">
                    @csrf
                    <div class="position-relative mb-5 form-group">
                        <span class="input-icon"><img src="{{asset('images/sent-mail.svg')}}" alt="sent-mail" /></span>
                        <input type="email" class="pl-4 rounded-0 input-border form-control" id="email" placeholder="EMAIL" value="{{ old('email') }}" name="email" required />
                        @error('email')
                        <span class="invalid-feedback-login" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="text-center submit">
                        <button title="Submit" type="submit" class="rounded-circle btn theme-bg text-white submit-btn">
                          <i class="display-5 las la-arrow-right"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection
