@extends('layouts.app')

@section('content')

<onboarding-agent-table></onboarding-agent-table>



<script type="application/javascript">
    jQuery(function($) {
  // Asynchronously Load the map API
    var script = document.createElement('script');
    script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyDvGdNTcLTe333c_bdBk7gjXg2Yi7RSsBA&sensor=false";

    document.body.appendChild(script);
    });
    var markers = [];
    var user_id = "";
    var date = "";
    var changeDate = false;
    $(document).on("change","#dateMap",function(){
        getOnBoard();
    });

    //$(document).ready(function () {

        function initMap(data) {
            console.log(data, 'myLatLong');
    	 var icon = {
    			    url: '{{ url("/images/googlemapicon.png") }}',
    			    scaledSize: new google.maps.Size(8, 8)
    			};

    		var service = new google.maps.DirectionsService;
    		var map = new google.maps.Map(document.getElementById('map_canvas_agent'));
    		
    		stations = [];
    		var address = [];
            var icons =[];
    		for(var i=0; i<data.length; i++) {
                console.log('lat', data[i].latitudes,data[i].longitude);
                if(data[i].type == 'AGENT'){
                    if(data.length-1 != i && data.length>50){
                        if(!(i%8)){
                            myLatLong=({lat: parseFloat(data[i].latitudes), lng: parseFloat(data[i].longitude)});
                            // list of points
                            stations.push(myLatLong);
                            address.push(data[i].address);
                           icons.push({marker : ''});
                        }
                    }else{
                        myLatLong=({lat: parseFloat(data[i].latitudes), lng: parseFloat(data[i].longitude) });
                            // list of points
                         stations.push(myLatLong);
                        address.push(data[i].address);
                       icons.push({marker : ''});
                    }
                }
                else{
                    myLatLong=({lat: parseFloat(data[i].latitudes), lng: parseFloat(data[i].longitude)});
                            // list of points
                    stations.push(myLatLong);
                     address.push(data[i].address);
                    icons.push({marker : icon});
                   
                }

            }
          
    		//console.log(myLatLong, 'myLatLong');

    	    // Zoom and center map automatically by stations (each station will be in visible map area)
    	    var lngs = stations.map(function(station) { return station.lng; });
    	    var lats = stations.map(function(station) { return station.lat; });
    	    map.fitBounds({
    	        west: Math.min.apply(null, lngs),
    	        east: Math.max.apply(null, lngs),
    	        north: Math.min.apply(null, lats),
    	        south: Math.max.apply(null, lats),
    	    });


    	    // Show stations on the map as markers
           console.log('stations');
           console.log(stations);
           
    	    //var marker;
    	    for (var i = 0; i < stations.length; i++) {
              
                if(icons[i].marker != ''){
                    markers[i] = new google.maps.Marker({
                        position: stations[i],
                        map: map,
                        title: address[i],
                        icon: ''
                    });
                }

    	    }


    	    // Divide route to several parts because max stations limit is 25 (23 waypoints + 1 origin + 1 destination)
    	    for (var i = 0, parts = [], max = 25 - 1; i < stations.length; i = i + max)
    	        parts.push(stations.slice(i, i + max + 1));

    	    // Service callback to process service results
    	    var service_callback = function(response, status) {
    	        if (status != 'OK') {
    	           // console.log('Directions request failed due to ' + status);
    	            return;
    	        }

    	        var renderer = new google.maps.DirectionsRenderer;
    	        renderer.setMap(map);
    	        renderer.setOptions({ suppressMarkers: true, preserveViewport: true });
    	        renderer.setDirections(response);
    	    };

    	    // Send requests to service to get route (for stations count <= 25 only one request will be sent)
    	    for (var i = 0; i < parts.length; i++) {
    	        // Waypoints does not include first station (origin) and last station (destination)
    	        var waypoints = [];
    	        for (var j = 1; j < parts[i].length - 1; j++)
    	            waypoints.push({location: parts[i][j], stopover: false});
    	        // Service options
    	        var service_options = {
    	            origin: parts[i][0],
    	            destination: parts[i][parts[i].length - 1],
    	            waypoints: waypoints,
    	            travelMode: 'WALKING'
    	        };
    	        // Send request
    	        service.route(service_options, service_callback);
    	    }

    	    google.maps.event.addListener(map, 'click', (function() {
    	        return function() {
    	            closeOnMapClick(i, map);
    	        }
    	    })());
    }





//   });
    function getOnBoard(){

        var id = $("#agent").val();
        var date = $("#dateMap").val()
        if(id == ""){
            changeDate = false;
            console.log('false', changeDate);
            return alert("Please choose retailer");
        }

        $.ajax({
        url: "{{url('/onboarding/agents/')}}/"+id ,
        method: 'POST',
        data:{
            date : date,
            _token : '{{ csrf_token() }}'
        },
        beforeSend: function() {
            // Show image container
            $("#loading").show();
        },
        success: function(data) {
            if(!data.list.length)
            {
                alert("No data found.")
            }
            initMap(data.list);
        },
        complete: function(data) {
            $("#loading").hide();
        }
    });
    }
</script>
<style>

#map {
height: 100%;
}
/* Optional: Makes the sample page fill the window. */
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

</style>

@endsection
