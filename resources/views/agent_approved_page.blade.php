@extends('layouts.app')

@section('content')

<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">

    <header-page :pagetitle="'Onboarding Agent'" :loader="false" :activeClass="'#agentList'"></header-page>

    <div class="col-md-12 pd-x-2 mt-4">
        <div class="row bg-white p-3 align-items-center">
            <div class="col-md-12 mb-4 agent-detail d-flex flex-wrap align-items-center justify-content-between">
                <h5 class="font-weight-normal detail-name">Agent details</h5>
                <a href="{{ route('onboarding') }}" title="" class="d-inline-block theme-bg text-white px-4 py-2 text-uppercase rounded backtolist">Back to list</a>
            </div>
            <div class="col-md-12 agentsdetail">
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-3 p-0 img-detail">
                                    <div class="detail-inner">
                                        <?php if ($data->avtar != '') { ?>
                                            <img src="{{asset('uploads/agents/')}}/{{$data->avtar}}" alt="detail-img" class="w-100 h-100 rounded-circle detailsimg" />
                                        <?php } else { ?>
                                            <img src="{{asset('images/detail-img.jpg')}}" alt="detail-img" class="w-100 h-100 rounded-circle detailsimg" />
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-md-8 pl-2 info-detail">
                                    <h6>{{ $data->name }}</h6>
                                    <p class="my-2 number"><span class="number">{{ $data->mobile_number}}</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 info-detail">
                            <span class="pr-4">
                                <p class="mb-1 font-weight-normal">Adhaar Number</p>
                                <h6>{{ $data->adhar_number}}</h6>
                            </span>
                        </div>
                        <div class="col-md-3 info-detail">
                            <span class="pr-4">
                                <p class="mb-1 font-weight-normal">Email Id:</p>
                                <h6>{{ $data->email}}</h6>
                            </span>
                        </div>
                        <div class="col-md-3 info-detail">
                            <p class="mb-1 font-weight-normal">Employee code:</p>
                            <h6>{{ $data->employee_code}}</h6>
                        </div>
                        <div class="col-md-4 mt-3 info-detail">
                            <p class="mb-1 font-weight-normal">Agency:</p>
                            <h6>{{$data->agency_name}}</h6>
                        </div>
                        <div class="col-md-6 mt-3 info-detail">
                            <p class="mb-1 font-weight-normal">Address:</p>
                            <h6>{{$data->address}} @if($data->city != '') {{$data->city}} ,@endif  @if($data->state != '') {{ $data->state}} @endif</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="row">
<!--            <div class="col-md-12 text-right pd-x-2">
                <form name="export" action="{{ route('onboarding-reatailer') }}" method="post">
                    @csrf
                    <input type="hidden" value="{{ $data->id }}" name="user_id">
                    <button type="submit"  name="button" class="d-flex align-items-center mb-4 float-right export-btn border-0 bg-transparent"><i class="la la-file-export h4 mb-0 mr-1"></i>EXPORT</button>
                </form>

            </div>-->
           
            <div class="col-md-12 text-center">
                
            </div>
        </div>

    </div>
    <onboarder-retailer-list v-bind:agentId="{{ $data->id }}" v-bind:onboardType="1"></onboarder-retailer-list>
    
</div>








@endsection
