

<aside class="theme-bg sidebar">
  <div class="py-2 text-center logo">
    <a href="{{route('agent-dashboard')}}" title="" class="sidebar-link">
      <span><img src="{{asset('images/dash-logo.svg')}}" alt="dashboard-logo" class="dash-logo" /></span>
    </a>
  </div>
  <ul class="list-group list-group-flush sidebar-menu">
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'agent/dashboard' ? 'active' : '' }}" id="agentList">
        <a href="{{route('agent-dashboard')}}" title="" class="sidebar-link">
        <i class="la la-user-tie"></i>Agent Dashboard
      </a>
    </li>
<!--    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'agent/panel' ? 'active' : '' }}" id="agentList">
        <a href="#"   title="" class="sidebar-link" >
        <i class="la la-user-tie"></i>Retailer Panel
      </a>
    </li>-->
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'agent/panel' ? 'active' : '' }}" id="agentList">
        <a href="{{route('retailer.queue')}}" title="" class="sidebar-link" >
        <i class="la la-user-tie"></i>Retailer Queue
      </a>
    </li>

   <!-- onclick="window.open('http://localhost/o2r/public/agent/panel','mywindow','status=1','toolbar=0')" --!>
    <!-- <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center has-submenu">
      <a href="javascript:void(0);" class="sidebar-link">
        <i class="la la-store-alt"></i>Retailer Panel
      </a>
      <div class="pl-4 submenu {{ Request::path() == 'agent/call/center' ? 'open' : '' || Request::path() == 'agent/call/status' ? 'open' : '' || Request::path() == 'agent/call/queue' ? 'open' : '' }}">
        <ul class="list-group list-unstyled">
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'agent/call/center' ? 'active' : ''  }}">
            <a href="{{route('agent-call-center')}}" title="">
              Call Setting
            </a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'agent/call/queue' ? 'active' : '' }}">
            <a href="{{route('agent-call-queue')}}" title="">
              Current Call Queue
            </a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'agent/call/status' ? 'active' : '' }}">
            <a href="{{route('agent-agent-status')}}" title="">
              Calling Agent Status
            </a>
          </li>
        </ul>
      </div>
    </li> -->
</ul>
</aside>
