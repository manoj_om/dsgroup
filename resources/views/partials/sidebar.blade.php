
@if(Auth::user()->role == 1)
<aside class="theme-bg sidebar">
  <div class="py-2 text-center logo">
    <a href="{{route('dashboard')}}" title="" class="sidebar-link">
      <span><img src="{{asset('images/dash-logo.svg')}}" alt="dashboard-logo" class="dash-logo" /></span>
    </a>
  </div>
  <ul class="list-group list-group-flush sidebar-menu">
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'dashboard' ? 'active' : ''  }}">
      <a href="{{route('dashboard')}}" title="" class="sidebar-link"><i class="la la-home"></i>Dashboard</a>
    </li>
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'onboarding/agents' ? 'active' : '' || request()->is('approve/agent/view/*') ? 'active' : ''}}" id="agentList">
      <a  href="{{route('onboarding')}}" title="" class="sidebar-link"><i class="la la-handshake"></i>Onboarding Agent</a>
    </li>
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'calling/agent' ? 'active' : '' }}">
      <a href="{{route('calling-agent')}}" title="" class="sidebar-link"><i class="la la-user-friends"></i>Calling Agent</a>
    </li>

    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center has-submenu {{ Request::path() == 'call/center' ? 'active' : '' || Request::path() == 'call/queue-list' ? 'active' : '' || Request::path() == 'call/status' ? 'active' : ''}}">
      <a href="javascript:void(0);" class="sidebar-link"><i class="la la-headset"></i>Call Center</a>

      <div class="submenu {{ Request::path() == 'call/center' ? 'open' : '' || Request::path() == 'call/status' ? 'open' : '' || Request::path() == 'call/queue-list' ? 'open' : '' }}">
        <ul class="list-group list-unstyled">
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'call/center' ? 'active' : ''  }}">
            <a href="{{route('call-center-queue')}}" title="">Call Setting</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'call/queue-list' ? 'active' : '' }}">
            <a href="{{route('call-queue')}}" title="">Current Call Queue</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'call/status' ? 'active' : '' }}">
            <a href="{{route('agent-status')}}" title="">Calling Agent Status</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center has-submenu  {{ Request::path() == 'retailer' ? 'active' : '' || Request::path() == 'retailer/attention/required' ? 'active' : '' || Request::path() == 'retailer/discovered/place' ? 'active' : '' ||  request()->is('view/retailer/data/*') ? 'active' : '' }}">
    <a href="javascript:void(0);" class="sidebar-link"><i class="la la-store-alt"></i>Retailers</a>
      <div class="submenu {{ Request::path() == 'retailer' ? 'open' : '' || Request::path() == 'retailer/attention/required' ? 'open' : '' || Request::path() == 'retailer/discovered/place' ? 'open' : '' || request()->is('view/retailer/data/*') ? 'open' : '' }}">
        <ul class="list-group list-unstyled">
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'retailer' ? 'active' : '' || request()->is('view/retailer/data/*') ? 'active' : '' }}">
            <a href="{{route('retailer')}}" title="">Retailers List</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'retailer/attention/required' ? 'active' : '' }}">
            <a href="{{route('attention')}}" title="">Attention Requested</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'retailer/discovered/place' ? 'active' : '' }}">
            <a href="{{route('discovered')}}" title="">Discovered Places</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'product/order' ? 'active' : '' || request()->is('order/view/*') ? 'active' : ''}}">
      <a href="{{route('orders')}}" title="" class="sidebar-link"><i class="la la-luggage-cart"></i>Orders</a>
    </li>
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center has-submenu {{ Request::path() == 'product/master' ? 'active' : '' || Request::path() == 'hub' ? 'active' : '' || Request::path() == 'product/delivery/partner' ? 'active' : '' ||   Request::path() == 'distributor/create' ? 'active' : '' ||   (request()->is('distributor*')) ? 'active' : ''  }}">
    <a href="javascript:void(0);" class="sidebar-link"><i class="la la-user-alt"></i>Masters</a>
      <div class="submenu {{ Request::path() == 'product/master' ? 'open' : '' || Request::path() == 'distributor' ? 'open' : '' || Request::path() == 'product/delivery/partner' ? 'open' : '' ||   Request::path() == 'distributor/create' ? 'open' : '' ||   (request()->is('distributor*')) ? 'open' : '' }}">
        <ul class="list-group list-unstyled">
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'product/master' ? 'active' : '' }}">
            <a href="{{ route('catalog') }}" title="">Product</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'distributor' ? 'active' : '' ||   Request::path() == 'distributor/create' ? 'active' : '' ||   (request()->is('distributor*')) ? 'active' : '' }}">
            <a href="{{url('distributor')}}"  title="">Hub</a>
          </li>
<!--          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'product/delivery/partner' ? 'active' : '' }}">
            <a href="{{route('catalog-delivery')}}" title="">Delievry Partner</a>
          </li>-->
        </ul>
      </div>
    </li>
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'manage/users' ? 'active' : ''  }}">
      <a href="{{ route('manage-user') }}" title="" class="sidebar-link"><i class="la la-user-cog"></i>Manage User</a>
    </li>
<!--    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'manage/users' ? 'active' : ''  }}">
      <a href="{{ route('manage-hub-users') }}" title="" class="sidebar-link"><i class="la la-user-cog"></i>Manage Hub User</a>
    </li>-->
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center has-submenu {{ Request::path() == 'report/order-report-page' ? 'active' : ''  || Request::path() == 'report/scheme-report-page' ? 'active' : '' || Request::path() == 'report/rider-report-page' ? 'active' : '' || Request::path() == 'report/hub-report-page' ? 'active' : '' || Request::path() == 'report/mobile-app-report-page' ? 'active' : '' || Request::path() == 'report/call-center-page' ? 'active' : '' || Request::path() == 'report/list' ? 'active' : '' || Request::path() == 'report/summary' ? 'active' : ''  }}">
    <a href="javascript:void(0);" class="sidebar-link"><i class="la la-chart-pie"></i>Report</a>
      <div class="submenu {{ Request::path() == 'report/order-report-page' ? 'active' : ''  || Request::path() == 'report/scheme-report-page' ? 'open' : '' || Request::path() == 'report/rider-report-page' ? 'open' : '' || Request::path() == 'report/hub-report-page' ? 'open' : '' || Request::path() == 'report/mobile-app-report-page' ? 'open' : '' || Request::path() == 'report/call-center-page' ? 'open' : '' || Request::path() == 'report/list' ? 'open' : '' || Request::path() == 'report/summary' ? 'open' : ''}}">
        <ul class="list-group list-unstyled">
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'report/list' ? 'active' : ''  }}">
            <a href="{{ route('list') }}" title="">Call Status with Audio</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'report/summary' ? 'active' : ''  }}" >
            <a href="{{ route('summary') }}" title="">Caller Summary</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'report/order-report-page' ? 'active' : ''  }}">
            <a href="{{ route('order-report') }}" title="">Order Report</a>
          </li>          
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'report/call-center-page' ? 'active' : ''  }}">
            <a href="{{ route('call-center') }}" title="">Call Center</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'report/mobile-app-report-page' ? 'active' : ''  }}">
            <a href="{{ route('mobile-app-report') }}" title="">Mobile App</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'report/hub-report-page' ? 'active' : ''  }}">
            <a href="{{ route('hub-report') }}" title="">Hub</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'report/rider-report-page' ? 'active' : ''  }}">
            <a href="{{ route('rider-report') }}" title="">Rider</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'report/scheme-report-page' ? 'active' : ''  }}">
            <a href="{{ route('scheme-report') }}" title="">Scheme</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center has-submenu {{ Request::path() == 'cms/index' ? 'active' : '' || Request::path() == 'cms/scheme' ? 'active' : '' || Request::path() == 'cms/order/status/setting' ? 'active' : '' || Request::path() == 'cms/callscript' ? 'active' : ''}}">
    <a href="javascript:void(0);" class="sidebar-link"><i class="la la-laptop"></i>CMS</a>
      <div class="submenu {{ Request::path() == 'cms/index' ? 'open' : '' || Request::path() == 'cms/scheme' ? 'open' : ''  || Request::path() == 'cms/order/status/setting' ? 'open' : '' || Request::path() == 'cms/callscript' ? 'open' : '' }}">
        <ul class="list-group list-unstyled">
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'cms/index' ? 'active' : '' }}">
            <a href="{{ route('index') }}" title="">Home Slider Banners</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'cms/scheme' ? 'active' : '' }}">
            <a href="{{ route('scheme') }}" title="">Schemes & Promotions</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'cms/callscript' ? 'active' : '' }}">
            <a href="{{ route('call-script') }}" title="">Call Script</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'cms/order/status/setting' ? 'active' : '' }}">
            <a href="{{ route('order-status-setting') }}" title="">Order Status Time Setting</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center has-submenu {{ Request::path() == 'scheme/setting' ? 'active' : '' || Request::path() == 'order/volume' ? 'active' : '' || Request::path() == 'ordered/items' ? 'active' : '' || Request::path() == 'sku/call/scheme' ? 'active' : ''  || Request::path() == 'new/product/launch' ? 'active' : '' || Request::path() == 'add/ordered/items/discount' ? 'active' : '' }}">
    <a href="javascript:void(0);" class="sidebar-link"><i class="la la-laptop"></i>Manage Scheme</a>
      <div class="manage_scheme submenu {{ Request::path() == 'scheme/setting' ? 'open' : '' || Request::path() == 'order/volume' ? 'open' : '' || Request::path() == 'ordered/items' ? 'open' : '' || Request::path() == 'sku/call/scheme' ? 'open' : '' || Request::path() == 'new/product/launch' ? 'open' : '' || Request::path() == 'add/ordered/items/discount' ? 'open' : '' }}">
        <ul class="list-group list-unstyled">
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'order/volume' ? 'active' : ''}}">
            <a href="{{ route('order-volume') }}" title="">Order Volume</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'ordered/items' ? 'active' : ''}}">
            <a href="{{ route('order-item') }}" title="">Ordered Items</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'sku/call/scheme' ? 'active' : ''}}">
            <a href="{{ route('sku-scheme') }}" title="">SKU-Line Sold Per Call</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'new/product/launch' ? 'active' : ''}}">
            <a href="{{ route('new-product') }}" title="">New Product Launch</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'add/ordered/items/discount' ? 'active' : ''}}">
            <a href="{{ route('ordered-discount') }}" title="">Ordered Items Discount</a>
          </li>
        </ul>
      </div>
    </li>
  </ul> 
</aside>
@endif
@if(Auth::user()->role == 2)
<aside class="theme-bg sidebar">
  <div class="py-4 text-center logo">
    <a href="#" title="" class="sidebar-link">
      <span><img src="{{asset('images/dash-logo.svg')}}" alt="dashboard-logo" class="dash-logo" /></span>
    </a>
  </div>
  <ul class="list-group list-group-flush sidebar-menu">
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center has-submenu {{ Request::path() == 'call/center' ? 'active' : '' || Request::path() == 'call/queue-list' ? 'active' : '' || Request::path() == 'call/status' ? 'active' : ''}}">
      <a href="javascript:void(0);" class="sidebar-link"><i class="la la-headset"></i>Call Center</a>
      <div class="submenu {{ Request::path() == 'call/center' ? 'open' : '' || Request::path() == 'call/status' ? 'open' : '' || Request::path() == 'call/queue-list' ? 'open' : '' }}">
        <ul class="list-group list-unstyled">
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'call/center' ? 'active' : ''  }}">
            <a href="{{route('call-center-queue')}}" title="">Call Setting</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'call/queue-list' ? 'active' : '' }}">
            <a href="{{route('call-queue')}}" title="">Current Call Queue</a>
          </li>
          <li class="mb-2 pl-2 pr-0 list-group-item inner-nav {{ Request::path() == 'call/status' ? 'active' : '' }}">
            <a href="{{route('agent-status')}}" title="">Calling Agent Status</a>
          </li>
        </ul>
      </div>
    </li> 
  </ul> 
</aside>
@endif