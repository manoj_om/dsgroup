

<aside class="theme-bg sidebar">
  <div class="py-2 text-center logo">
    <a href="{{route('hub-dashboard')}}" title="" class="sidebar-link">
    <span><img src="{{asset('images/dash-logo.svg')}}" alt="dashboard-logo" class="dash-logo" /></span>
       </a>
  </div>
  <ul class="list-group list-group-flush sidebar-menu">
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'hub/dashboard' ? 'active' : ''  }}" >
      <a href="{{ route('hub-dashboard') }}" title="" class="sidebar-link">
        <i class="la la-user-cog"></i>Dashboard
      </a>
    </li>
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'hub/order' ? 'active' : '' || request()->is('hub/order/view/*') ? 'active' : ''  }}" >
      <a href="{{ route('hub-order') }}" title="" class="sidebar-link">
        <i class="la la-luggage-cart"></i>Manage Order
      </a>
    </li>
    <li class="theme-bg list-group-item d-flex flex-wrap justify-content-between align-items-center {{ Request::path() == 'hub/stock' ? 'active' : '' || request()->is('hub/product/view/*') ? 'active' : ''  }}" >
      <a href="{{ route('hub-stock') }}" title="" class="sidebar-link">
        <i class="las la-coins"></i>Manage Stock
      </a>
    </li>
  </ul>
</aside>

