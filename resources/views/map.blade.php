@extends('layouts.app')

@section('content')
 
<!-- agent listing section -->
<div class="col-md-9 toggle-switch bg-grey onboard-list">
    <header-page :pagetitle="'Onboarding Agent'" :loader="false"></header-page>
    <div class="row mt-4 mb-4">
        <div class="col-md-12 pd-x-2">
            test
                     <div id="map_wrapper">
            <div id="map_canvas" class="mapping" style="min-height:400px;"></div>
        </div>
        </div>
    </div>
</div>
<!-- end -->


<!-- The Modal -->


<script type="application/javascript">

 var mainUrl = '<?php echo config('app.url'); ?>/public/';

    function searchTable(){
        if($("#searchkey").val() != ''){
           $('#search_form').submit();
        }else{
            alert('please enter key to search');
        }


    }
    function showAgentDetail(data){

    $("#agent_name").text(data.name);
    $("#agent_phone").text(data.mobile_number);
    $("#company_name").text(data.agency_name);
    $("#company_address").text(data.address);
    $("#agent_email").text(data.email);
    $("#agent_pancard").text(data.employee_code);
    $("#agent_adhaar").text(data.adhar_number);
    $("#agent_pic").attr("src", 'uploads/agents/' + data.avtar);
    }

    function approveRequest(data){
    var token = document.head.querySelector('meta[name="csrf-token"]');
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    // send contact form data.
    axios.get(mainUrl + '/approve-agent/'+data.id,).then((response) => {
    if(response.data.code == 200){
    location.reload();
    }
    console.log(response)
    }).catch((error) => {

    console.log(error.response.data)
    });
    }
    function RejectRequest(data){
    var token = document.head.querySelector('meta[name="csrf-token"]');
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    // send contact form data.
    axios.get(mainUrl + '/reject-agent/'+data.id,).then((response) => {
    if(response.data.code == 200){
    location.reload();
    }
    console.log(response)
    }).catch((error) => {

    console.log(error.response.data)
    });
    }
                jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "http://maps.googleapis.com/maps/api/js?callback=initialize&key=AIzaSyDvGdNTcLTe333c_bdBk7gjXg2Yi7RSsBA";
    document.body.appendChild(script);
});
function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
        <?php foreach($data as $reocrds) { if(isset($reocrds->latitudes) && isset($reocrds->longitude)){ ?>
                    ["London Eye, London <?php echo $reocrds->latitudes; ?> ",
                        "<?php echo $reocrds->latitudes ?>","<?php echo $reocrds->longitude ?>"],
                          
        <?php  } } ?>
        
//         ['London Eye, London', 51.503454,-0.119562],
//        ['Palace of Westminster, London', 51.499633,-0.124755]
    ];
                        
    // Info Window Content
    var infoWindowContent = [
         <?php foreach($data as $reocrds) { if(isset($reocrds->latitudes) && isset($reocrds->longitude)){ ?>
                     ['<div class="info_content">' +
        '<h3><?php echo $reocrds->full_name ?></h3>' +
        '<p>The London Eye is a giant \n\
Ferris wheel situated on the banks of the River Thames. The entire structure is 135 metres (443 ft) tall and the wheel has a diameter of 120 metres \n\
(394 ft).</p>' +        '</div>'],
         <?php  } } ?>
//        ['<div class="info_content">' +
//        '<h3>London Eye</h3>' +
//        '<p>The London Eye is a giant \n\
//Ferris wheel situated on the banks of the River Thames. The entire structure is 135 metres (443 ft) tall and the wheel has a diameter of 120 metres \n\
//(394 ft).</p>' +        '</div>'],
//     
//        ['<div class="info_content">' +
//        '<h3>Palace of Westminster</h3>' +
//        '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
//        '</div>']
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });
    
}

</script>

<style type="text/css">
    #map_wrapper {
    height: 400px;
}

#map_canvas {
    width: 100%;
    height: 100%;
}
</style>


@endsection
