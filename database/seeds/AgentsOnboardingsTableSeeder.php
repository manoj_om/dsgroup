<?php

use Illuminate\Database\Seeder;

class AgentsOnboardingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        factory(App\AgentsOnboardings::class, 2)->create()->each(function($u) {
            $u->retailadd()->save(factory(App\Retailer::class)->make());
        });
    }
}
