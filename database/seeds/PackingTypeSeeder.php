<?php

use Illuminate\Database\Seeder;

class PackingTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::unprepared('SET IDENTITY_INSERT [dbo].[packing_types] ON;');
        DB::table('packing_types')->insert([
            ['name' => 'IC','status'=>'1'],
            ['name' => 'Master Pouch','status'=>'1'],
            ['name' => 'Jar','status'=>'1'],
            ['name' => 'Tin','status'=>'1'],
            ['name' => 'Zipper','status'=>'1'],
            ['name' => 'Ladi','status'=>'1'],
            ['name' => 'Hanger','status'=>'1'],
            ['name' => 'Pouch','status'=>'1'],
            ['name' => 'Other- Add new','status'=>'0']

        ]);
        DB::unprepared('SET IDENTITY_INSERT [dbo].[packing_types] OFF;');
    }
}
