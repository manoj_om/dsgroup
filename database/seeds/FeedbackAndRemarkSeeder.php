<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class FeedbackAndRemarkSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::unprepared('SET IDENTITY_INSERT feedback_categories ON;');
        DB::table('feedback_categories')->insert([
            //

            ['id' => 1, 'name' => 'order', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => '1'],
            ['id' => 2, 'name' => 'rider', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => '1'],
            ['id' => 3, 'name' => 'retailer', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => '1'],
            ['id' => 4, 'name' => 'scheme', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => '1'],
            ['id' => 5, 'name' => 'Other ', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => '1'],
        ]);
        DB::unprepared('SET IDENTITY_INSERT feedback_categories OFF;');

        DB::unprepared('SET IDENTITY_INSERT feedback_remarks ON;');
        DB::table('feedback_remarks')->insert([
            ['id' => 1,'feedback_category_id' => 1, 'remark' => 'high prices', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 2,'feedback_category_id' => 1, 'remark' => 'received expired stock', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 3,'feedback_category_id' => 1, 'remark' => 'missing items', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 4,'feedback_category_id' => 1, 'remark' => 'last order not delivered', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 5,'feedback_category_id' => 2, 'remark' => 'late delivery', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => '1'],
            ['id' => 6,'feedback_category_id' => 2, 'remark' => 'didn’t accept coins, paytm etc.', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 7,'feedback_category_id' => 2, 'remark' => 'rider behaviour issue', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 8,'feedback_category_id' => 2, 'remark' => 'rider took extra money', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 9,'feedback_category_id' => 2, 'remark' => 'not delivering to location', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 10,'feedback_category_id' => 3, 'remark' => 'want to refer another retailer', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 11,'feedback_category_id' => 3, 'remark' => 'don’t want calls', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 12,'feedback_category_id' => 3, 'remark' => 'business closed', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 13,'feedback_category_id' => 3, 'remark' => 'address change', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 14,'feedback_category_id' => 3, 'remark' => 'phone number change', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 15,'feedback_category_id' => 4, 'remark' => 'received some different SKU', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 16,'feedback_category_id' => 4, 'remark' => 'completed scheme but still unqualified', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
            ['id' => 17,'feedback_category_id' => 4, 'remark' => 'scheme not delivered', 'created_by' => 1, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d'), 'status' => 1],
        ]);      
        DB:unprepared('SET IDENTITY_INSERT feedback_remarks OFF;');
} 


    }