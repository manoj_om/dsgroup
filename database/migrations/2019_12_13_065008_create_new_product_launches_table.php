<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewProductLaunchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_product_launches', function (Blueprint $table) {
           $table->bigIncrements('id');
            $table->text('scheme_name');
            $table->datetime('from_date');
            $table->datetime('to_date');
            $table->text('promocode');
            $table->bigInteger('minimum_order_value');
            $table->bigInteger('applicable_offer');
            $table->bigInteger('apply_discount');
            $table->bigInteger('fixed_amount_percentage');
            $table->bigInteger('free_secondary_product');
            $table->bigInteger('free_product_count');
            $table->text('applied_location')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_product_launches');
    }
}
