<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistributorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('distributors')){
        Schema::create('distributors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('distributor_name');
            $table->string('distributor_code',64)->unique();
            $table->bigInteger('mobile')->unique()->default(0);
            $table->string('email',200)->unique()->nullable();
            $table->string('pan_card',200)->nullable();
            $table->string('gst_no',200)->nullable();
            $table->string('aadhar_no',200)->nullable();
            $table->string('website',250)->nullable();
            $table->string('state',128)->nullable();
            $table->string('city',128)->nullable();
            $table->string('latitude',128)->nullable();
            $table->string('longitude',128)->nullable();
            $table->text('address',200)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        }
        if(!Schema::hasTable('map_distributors')){
        Schema::create('map_distributors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('distributor_id');
            $table->bigInteger('retailer_id');
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('distributors')){
        Schema::dropIfExists('distributors');
        }
        if(Schema::hasTable('map_distributors')){
        Schema::dropIfExists('map_distributors');
        }
    }
}
