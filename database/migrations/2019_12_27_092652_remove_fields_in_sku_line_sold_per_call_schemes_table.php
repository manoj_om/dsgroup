<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFieldsInSkuLineSoldPerCallSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sku_line_sold_per_call_schemes', function (Blueprint $table) {
          $table->dropColumn('primary_product');
          $table->dropColumn('secondary_product');
          $table->dropColumn('applicable_offer');
          $table->dropColumn('free_product_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sku_line_sold_per_call_schemes', function (Blueprint $table) {
          $table->bigInteger('primary_product');
          $table->bigInteger('secondary_product');
          $table->bigInteger('applicable_offer');
          $table->bigInteger('free_product_count');
        });
    }
}
