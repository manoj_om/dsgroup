<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderedItemsDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordered_items_discounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('scheme_name');
            $table->date('from_date');
            $table->date('to_date');
            $table->text('promocode');
            $table->bigInteger('frequency')->nullable();
            $table->bigInteger('shop_type');
            $table->bigInteger('primary_product');
            $table->bigInteger('primary_product_count');
            $table->bigInteger('free_secondary_product');
            $table->bigInteger('free_secondary_product_count');
            $table->bigInteger('apply_discount');
            $table->bigInteger('fixed_percentage');
            $table->bigInteger('max_time_applicability');
            $table->text('applied_location')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->bigInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordered_items_discounts');
    }
}
