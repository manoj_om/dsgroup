<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderedItemSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordered_item_schemes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('scheme_name');
            $table->datetime('from_date');
            $table->datetime('to_date');
            $table->integer('frequency')->nullable();
            $table->text('promocode');
            $table->bigInteger('free_secondary_product');
            $table->bigInteger('primary_product');
            $table->bigInteger('product_count');
            $table->bigInteger('free_product_count');
            $table->text('applied_location')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordered_item_schemes');
    }
}
