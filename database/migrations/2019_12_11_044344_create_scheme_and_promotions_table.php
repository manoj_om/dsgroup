<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchemeAndPromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheme_and_promotions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('scheme_name');
            $table->datetime('from_date');
            $table->datetime('to_date');
            $table->text('avatar');
            $table->text('eligibility');
            $table->text('description');
            $table->tinyInteger('status');
            $table->bigInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheme_and_promotions');
    }
}
