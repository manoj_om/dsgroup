<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsInSkuLineSoldPerCallSchemesTab extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('sku_line_sold_per_call_schemes', function (Blueprint $table) {
            $table->bigInteger('shop_type');
            $table->bigInteger('max_time_applicability');
            $table->bigInteger('frequency')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('sku_line_sold_per_call_schemes', function (Blueprint $table) {
            $table->dropColumn('shop_type');
            $table->dropColumn('max_time_applicability');
            $table->dropColumn('frequency');
        });
    }

}
