<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnAppliedLocationFromTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('new_product_launches', function (Blueprint $table) {
            $table->dropColumn('applied_location');
        });
        Schema::table('ordered_items_discounts', function (Blueprint $table) {
            $table->dropColumn('applied_location');
        });
        Schema::table('ordered_item_schemes', function (Blueprint $table) {
            $table->dropColumn('applied_location');
        });
        Schema::table('order_volume_schemes', function (Blueprint $table) {
            $table->dropColumn('applied_location');
        });
        Schema::table('sku_line_sold_per_call_schemes', function (Blueprint $table) {
            $table->dropColumn('applied_location');
        });
        Schema::table('building_schemes', function (Blueprint $table) {
            $table->dropColumn('applied_location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::table('new_product_launches', function (Blueprint $table) {
            $table->text('applied_location')->nullable();
        });
        Schema::table('ordered_items_discounts', function (Blueprint $table) {
            $table->text('applied_location')->nullable();
        });
        Schema::table('ordered_item_schemes', function (Blueprint $table) {
            $table->text('applied_location')->nullable();
        });
        Schema::table('order_volume_schemes', function (Blueprint $table) {
            $table->text('applied_location')->nullable();
        });
        Schema::table('sku_line_sold_per_call_schemes', function (Blueprint $table) {
            $table->text('applied_location')->nullable();
        });
        Schema::table('building_schemes', function (Blueprint $table) {
            $table->text('applied_location')->nullable();
        });
    }

}
