<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsOnboardingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('agents_onboardings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email',200)->unique();
            $table->string('name');
            $table->bigInteger('mobile_number')->default(0);
            $table->string('adhar_number',200)->default(0);
            $table->string('agency_name',200);
            $table->text('address');
            $table->string('employee_code',200)->nullable();
            $table->string('user_id');
            $table->text('avtar')->default(null);
            $table->tinyInteger('is_approve')->default(0);
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents_onboardings');
    }
}
