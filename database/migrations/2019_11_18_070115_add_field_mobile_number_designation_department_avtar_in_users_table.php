<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldMobileNumberDesignationDepartmentAvtarInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('users')){
           Schema::table('users', function(Blueprint $table) {
                if (!Schema::hasColumn('users', 'department')) {
                    $table->integer('department')->nullable();
                }
                if (!Schema::hasColumn('users', 'mobile_number')) {
                    $table->bigInteger('mobile_number')->unique();
                }
                if (!Schema::hasColumn('users', 'designation')) {
                    $table->text('designation')->nullable();
                }
                if (!Schema::hasColumn('users', 'avtar')) {
                    $table->text('avtar')->nullable();
                }
                    
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         
        if(Schema::hasTable('users')){
           Schema::table('users', function(Blueprint $table) {
                if (Schema::hasColumn('users', 'department')) {
                    $table->dropColumn('department');
                }
                if (Schema::hasColumn('users', 'mobile_number')) {
                    $table->dropColumn('mobile_number');
                }
                if (Schema::hasColumn('users', 'designation')) {
                    $table->dropColumn('designation');
                }
                if (Schema::hasColumn('users', 'avtar')) {
                    $table->dropColumn('avtar');
                }             
            });
        }
    }
}
