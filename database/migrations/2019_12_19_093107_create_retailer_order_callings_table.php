<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetailerOrderCallingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retailer_order_callings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('retailer_id');
            $table->text('calling_days');
            $table->text('order_limit');
            $table->time('start_time');
            $table->time('end_time');
            $table->text('calling_option');
            $table->text('source');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retailer_order_callings');
    }
}
