<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsInNewProductLaunchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_product_launches', function (Blueprint $table) {
             $table->bigInteger('shop_type');
             $table->bigInteger('frequency')->nullable();
             $table->bigInteger('max_time_applicability');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_product_launches', function (Blueprint $table) {
             $table->dropColumn('shop_type');
             $table->dropColumn('frequency');
             $table->dropColumn('max_time_applicability');
        });
    }
}
