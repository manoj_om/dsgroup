<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColomnsRetailersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        
        if(Schema::hasTable('retailers')){
           Schema::table('retailers', function(Blueprint $table) {
                if (!Schema::hasColumn('retailers', 'gst_number_file')) {
                    $table->text('gst_number_file')->nullable();
                }
                if (!Schema::hasColumn('retailers', 'pan_number_file')) {
                    $table->text('pan_number_file')->nullable();
                }
                if (!Schema::hasColumn('retailers', 'adhar_number_file')) {
                    $table->text('adhar_number_file')->nullable();
                }
                if (!Schema::hasColumn('retailers', 'p_device_type')) {
                    $table->tinyInteger('p_device_type')->nullable();
                }
                if (!Schema::hasColumn('retailers', 'scan_code')) {
                    $table->text('scan_code')->nullable();
                }
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
 if(Schema::hasTable('retailers')){
     //p_device_type
           Schema::table('retailers', function(Blueprint $table) {
                if (Schema::hasColumn('retailers', 'scan_code')) {
                    $table->dropColumn('scan_code');
                }
                   if (!Schema::hasColumn('retailers', 'gst_number_file')) {
                    $table->dropColumn('gst_number_file') ;
                }
                if (Schema::hasColumn('retailers', 'pan_number_file')) {
                    $table->dropColumn('pan_number_file') ;
                }
                if (Schema::hasColumn('retailers', 'adhar_number_file')) {
                    $table->dropColumn('adhar_number_file') ;
                }
                if (Schema::hasColumn('retailers', 'p_device_type')) {
                    $table->dropColumn('p_device_type');
                }
                    
                
            });
        }
    }

}
