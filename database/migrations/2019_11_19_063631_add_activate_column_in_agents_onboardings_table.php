<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddActivateColumnInAgentsOnboardingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     
        if(Schema::hasTable('agents_onboardings')){
           Schema::table('agents_onboardings', function(Blueprint $table) {
                if (!Schema::hasColumn('agents_onboardings', 'activate')) {
                    $table->tinyInteger('activate')->default(0);
                } 
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
        if(Schema::hasTable('agents_onboardings')){
           Schema::table('agents_onboardings', function(Blueprint $table) {
                if (Schema::hasColumn('agents_onboardings', 'activate')) {
                    $table->dropColumn('activate');
                }              
            });
        }
    }
}
