<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnsToNewProductLaunchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_product_launches', function (Blueprint $table) {
           $table->dropColumn('applicable_offer');
           $table->dropColumn('apply_discount');
           $table->dropColumn('free_secondary_product');
           $table->dropColumn('free_product_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_product_launches', function (Blueprint $table) {
           $table->bigInteger('applicable_offer');
           $table->bigInteger('apply_discount');
           $table->bigInteger('free_secondary_product');
           $table->bigInteger('free_product_count');
        });
    }
}
