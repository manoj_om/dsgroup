<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_orders', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('api_order_id')->unique();
            $table->integer('orders_id');
            $table->string("vendor_name");
            $table->string("vendor_contact_number");
            $table->string("vendor_address");
            $table->string("status");
            $table->text("cancellation_reason")->nullable();
            $table->string("order_time");
            $table->string("assigned_time")->nullable();
            $table->string("accepted_time")->nullable();
            $table->string("arrived_time")->nullable();
            $table->string("dispatched_time")->nullable();
            $table->string("delivered_time")->nullable();
            $table->string("cancelled_time")->nullable();
            $table->string("rider_name")->nullable();
            $table->string("rider_phone")->nullable();
            $table->string("vendor_pin")->nullable();
            $table->string("customer_pin")->nullable();
            $table->string("total_distance")->nullable();
            $table->string("total_cost")->nullable();
            $table->json("sku_list");
            $table->string("has_skus");
            $table->integer("rider_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_orders');
    }
}
