<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('order_number',200)->unique();
            $table->float('total_amount');
            $table->string('promocode')->nullable();
            $table->float('discount_amount')->default(0);
            $table->enum('status',[0,1,2,3,4])->default(1)->comment('0 => Cancelled,1=>Ordered & Approved,2=>Packed,3=>Shipped,4=>Delivered');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
