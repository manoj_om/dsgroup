<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnShopTypeInScheme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_volume_schemes', function (Blueprint $table) {
          $table->string('shop_type')->text()->change();
        });
        Schema::table('new_product_launches', function (Blueprint $table) {
          $table->string('shop_type')->text()->change();
        });
        Schema::table('ordered_item_schemes', function (Blueprint $table) {
           $table->string('shop_type')->text()->change();
        });
        Schema::table('ordered_items_discounts', function (Blueprint $table) {
          $table->string('shop_type')->text()->change();
        });
        Schema::table('sku_line_sold_per_call_schemes', function (Blueprint $table) {
          $table->string('shop_type')->text()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scheme', function (Blueprint $table) {
            //
        });
    }
}
