<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsInAttentionRequiredsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attention_requireds', function (Blueprint $table) {
            $table->text('resolution')->nullable();
        });
        Schema::table('discovered_places', function (Blueprint $table) {
            $table->text('resolution')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attention_requireds', function (Blueprint $table) {
            //
        });
    }
}
