<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsInProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->text('brand_name');
            $table->text('manufacturer');
            $table->string('hsn_code');
            $table->string('ean_code');
            $table->bigInteger('dlp');
            $table->bigInteger('max_order_quantity');
            $table->string('product_length');
            $table->string('product_width');
            $table->string('product_height');
            $table->string('shelf_life');
            $table->string('custmer_target');
            $table->string('master_carton_gross_weight');
            $table->string('master_carton_net_weight');
            $table->string('master_carton_length');
            $table->string('master_carton_width');
            $table->string('master_carton_height');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
