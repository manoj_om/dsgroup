<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusColumnInRetailersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if(Schema::hasTable('retailers')){
           Schema::table('retailers', function(Blueprint $table) {
                if (!Schema::hasColumn('retailers', 'status')) {
                    $table->tinyInteger('status')->default(0);
                } 
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('retailers')){
           Schema::table('retailers', function(Blueprint $table) {
                if (Schema::hasColumn('retailers', 'status')) {
                    $table->dropColumn('status');
                }              
            });
        }
    }
}
