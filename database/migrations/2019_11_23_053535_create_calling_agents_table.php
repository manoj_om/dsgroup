<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallingAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calling_agents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('created_by'); 
            $table->text('name');
            $table->text('serial_number');
            $table->text('executive_code');
            $table->text('mobile_number');
            $table->text('email');
            $table->text('supervisor_name');
            $table->text('user_name');
            $table->text('password');
            $table->dateTime('shift_start_time');
            $table->dateTime('shift_end_time');
            $table->text('caller_type');
            $table->text('avtar');
             $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calling_agents');
    }
}
