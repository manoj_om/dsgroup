<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkuLineSoldPerCallSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sku_line_sold_per_call_schemes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('scheme_name');
            $table->datetime('from_date');
            $table->datetime('to_date');
            $table->text('promocode');
            $table->bigInteger('primary_product');
            $table->bigInteger('secondary_product');
            $table->bigInteger('minimum_order_value');
            $table->bigInteger('applicable_offer');
            $table->bigInteger('apply_discount');
            $table->bigInteger('fixed_amount_percentage');
            $table->bigInteger('free_secondary_product');
            $table->bigInteger('free_product_count');
            $table->bigInteger('applied_location')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
        
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sku_line_sold_per_call_schemes');
    }
}
