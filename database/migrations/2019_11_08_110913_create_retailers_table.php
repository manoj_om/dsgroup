<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetailersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

       
           
        Schema::create('retailers', function (Blueprint $table) {
                       $table->increments('id');
            $table->bigInteger('user_id');
            $table->text('full_name')->nullable();
            $table->bigInteger('primary_mobile_number')->default(0);
            $table->bigInteger('alternative_mobile_number')->default(0);
            $table->bigInteger('whats_app_number')->default(0);
            $table->text('capture_retailer_image')->nullable();
            //secondary detail 
            $table->text('secondary_full_name')->nullable();
            $table->bigInteger('secondary_mobile_number')->nullable();
            $table->text('secondary_capture_retailer_image')->nullable();
            //shop detail
            $table->text('shop_image')->nullable();
            $table->tinyInteger('shop_type')->default(1);
            $table->text('shop_name')->nullable();
            
            $table->dateTime('dob')->nullable();
            $table->dateTime('anniversay')->nullable();
            $table->dateTime('first_kid_birthday')->nullable();
            $table->dateTime('second_kid_birthday')->nullable();
            $table->dateTime('third_kid_birthday')->nullable();
            //kyc
            $table->text('establishment_name')->nullable();
            
            $table->text('gst_number')->nullable();
            $table->text('pan_number')->nullable();
            $table->text('adhar_number')->nullable(); 
            $table->timestamps();
        });
        
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('retailers');
    }

}
