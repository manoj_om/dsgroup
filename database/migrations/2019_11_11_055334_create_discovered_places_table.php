<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscoveredPlacesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasTable('discovered_places')) {
            Schema::create('discovered_places', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('user_id');
                $table->text('location');
                $table->text('city');
                $table->text('state');
                $table->text('lattitude');
                $table->text('longitude');
                $table->text('remarks');
                $table->tinyInteger('status')->dafault(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasTable('discovered_places')) {
            Schema::dropIfExists('discovered_places');
        }
    }

}
