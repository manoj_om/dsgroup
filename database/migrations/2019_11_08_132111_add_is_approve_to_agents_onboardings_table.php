<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsApproveToAgentsOnboardingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('agents_onboardings', function (Blueprint $table) {
            if (!Schema::hasColumn('agents_onboardings', 'is_approve')) {
            $table->tinyInteger('is_approve')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agents_onboardings', function (Blueprint $table) {
           if (Schema::hasColumn('agents_onboardings', 'is_approve')) {
            $table->dropColumn('is_approve');
            }
        });
    }
}
