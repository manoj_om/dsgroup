<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order_id');
            $table->integer('retailer_id');
            $table->integer('hub_id');
            $table->float('total_amount');
            $table->integer('action_by');
            $table->string('action_user_gaurd');
            $table->text('data');
            $table->enum('status',[0,1,2,3,4])->default(1)->comment('0 => Cancelled,1=>Ordered & Approved,2=>Packed,3=>Shipped,4=>Delivered');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_logs');
    }
}
