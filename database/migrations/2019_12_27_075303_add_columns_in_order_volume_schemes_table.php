<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsInOrderVolumeSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_volume_schemes', function (Blueprint $table) {
           $table->bigInteger('shop_type');
           $table->bigInteger('free_ds_product_worth_mrp');
           $table->bigInteger('free_ds_product_worth_rlp');
           $table->bigInteger('max_time_applicability');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_volume_schemes', function (Blueprint $table) {
           $table->dropColumn('shop_type');
           $table->dropColumn('free_ds_product_worth_mrp');
           $table->dropColumn('free_ds_product_worth_rlp');
           $table->dropColumn('max_time_applicability');
        });
    }
}
