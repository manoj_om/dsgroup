<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsInAttentionRequiredsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('attention_requireds', function (Blueprint $table) {
           $table->bigInteger('feedback_id')->nullable();
           $table->bigInteger('user_type')->default(1);
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attention_requireds', function (Blueprint $table) {
            //
        });
    }
}
