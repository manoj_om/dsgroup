<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->integer('user_id');
            $table->integer('action_by');
            $table->string('action_user_gaurd');
            $table->json('data');
            $table->string('retailer_message');
            $table->string('hub_message');
            $table->string('admin_message');
            $table->enum('has_admin_view',[0,1])->default(0);
            $table->enum('has_admin_read',[0,1])->default(0);
            $table->enum('has_retailer_view',[0,1])->default(0);
            $table->enum('has_retailer_read',[0,1])->default(0);
            $table->enum('has_hub_view',[0,1])->default(0);
            $table->enum('has_hub_read',[0,1])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
