<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuildingSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('building_schemes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('scheme_name');
            $table->datetime('from_date');
            $table->bigInteger('primary_product_1');
            $table->bigInteger('primary_product_2');
            $table->bigInteger('secondary_product');
            $table->bigInteger('apply_discount');
            $table->text('fixed_amount');
            $table->text('promo_code');
            $table->bigInteger('applied_location');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('building_schemes');
    }
}
