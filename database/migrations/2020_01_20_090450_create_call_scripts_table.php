<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallScriptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_scripts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('shop_type');
            $table->bigInteger('retailer_type');
            $table->bigInteger('call_type');
            $table->bigInteger('script_level_type');
            $table->bigInteger('event_type');
            $table->longText('script');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_scripts');
    }
}
