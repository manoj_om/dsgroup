<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('serial_number');
            $table->integer('product_lists_id');
            $table->text('short_name');
            $table->text('imis_code');
            $table->text('sku_number');
            $table->text('package_unit');
            $table->text('packaging');
            $table->text('quantity');
            $table->text('weight');
            $table->text('min_order_by');
            $table->text('min_order');
            $table->integer('category');
            $table->integer('sub_category');
            $table->integer('created_by');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('products');
    }

}
