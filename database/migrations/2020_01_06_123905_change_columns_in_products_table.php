<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsInProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('short_description')->nullable()->change();
            $table->string('product_length')->nullable()->change();
            $table->string('product_width')->nullable()->change();
            $table->string('product_height')->nullable()->change();
            $table->string('shelf_life')->nullable()->change();
            $table->string('custmer_target')->nullable()->change();
            $table->string('master_carton_gross_weight')->nullable()->change();
            $table->string('master_carton_net_weight')->nullable()->change();
            $table->string('master_carton_length')->nullable()->change();
            $table->string('master_carton_length')->nullable()->change();
            $table->string('master_carton_width')->nullable()->change();
            $table->string('master_carton_height')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
