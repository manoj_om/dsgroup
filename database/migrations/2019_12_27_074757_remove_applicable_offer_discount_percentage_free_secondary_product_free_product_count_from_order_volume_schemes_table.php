<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveApplicableOfferDiscountPercentageFreeSecondaryProductFreeProductCountFromOrderVolumeSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_volume_schemes', function (Blueprint $table) {
            $table->dropColumn('applicable_offer');
            $table->dropColumn('apply_discount');
            $table->dropColumn('fixed_amount');
            $table->dropColumn('free_secondary_product');
            $table->dropColumn('free_product_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_volume_schemes', function (Blueprint $table) {
           $table->integer('applicable_offer');
           $table->integer('apply_discount');
           $table->integer('fixed_amount');
           $table->integer('free_secondary_product');
           $table->integer('free_product_count');
        });
    }
}
