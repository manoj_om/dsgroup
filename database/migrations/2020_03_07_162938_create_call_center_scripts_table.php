<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallCenterScriptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_center_scripts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('shop_type');
            $table->bigInteger('retailer_type');
            $table->bigInteger('call_type');
            $table->text('script');
            $table->tinyInteger('status');
            $table->bigInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_center_scripts');
    }
}
