<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderVolumeSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_volume_schemes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('scheme_name');
            $table->datetime('from_date');
            $table->datetime('to_date');
            $table->integer('frequency')->nullable();
            $table->bigInteger('order_count');
            $table->bigInteger('minimum_order_value');
            $table->text('promocode');
            $table->integer('applicable_offer');
            $table->integer('apply_discount');
            $table->text('fixed_amount');
            $table->bigInteger('free_secondary_product');
            $table->bigInteger('free_product_count');
            $table->string('applied_location')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_volume_schemes');
    }
}
