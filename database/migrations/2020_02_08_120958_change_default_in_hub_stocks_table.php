<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDefaultInHubStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hub_stocks', function (Blueprint $table) {
            $table->bigInteger('in_stock')->default(0)->change();
            $table->bigInteger('out_stock')->default(0)->change();
        });
        Schema::table('hub_stock_logs', function (Blueprint $table) {
            $table->bigInteger('in_stock')->default(0)->change();
            $table->bigInteger('out_stock')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hub_stocks', function (Blueprint $table) {
            //
        });
    }
}
