<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiSkuListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_sku_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('api_id');
            $table->integer('sku_id');
            $table->text('description')->default('N/A');
            $table->float('unit_cost');
            $table->integer('units');
            $table->integer('units_delivered')->default(0);
            $table->float('amount_collected')->default(0);
            $table->string('status')->nullable();
            $table->integer('api_order_id');
            $table->string('packing_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_sku_lists');
    }
}
