<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsInOrderedItemSchemes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordered_item_schemes', function (Blueprint $table) {
             $table->bigInteger('shop_type');  
             $table->bigInteger('max_time_applicability');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordered_item_schemes', function (Blueprint $table) {
             $table->dropColumn('shop_type');  
             $table->dropColumn('max_time_applicability');
        });
    }
}
