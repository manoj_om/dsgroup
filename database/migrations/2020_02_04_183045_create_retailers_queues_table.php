<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetailersQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retailers_queues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('retailer_id');
            $table->string('start_time');
            $table->string('end_time');
            $table->float('order_limit');
            $table->integer('status');
            $table->json('api_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retailers_queues');
    }
}
