<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPackedQuantityToOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            //
            $table->integer("packed_quantity")->nullable();
            $table->integer("delivered_quantity")->nullable();
            $table->float("offer_discount_percentage")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            //
            $table->dropColumn("packed_quantity");
            $table->dropColumn("delivered_quantity");
            $table->dropColumn("offer_discount_percentage");
        });
    }
}
