<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeNewFieldsInAttentionRequiredsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('attention_requireds', function (Blueprint $table) {
            $table->text('scan_code')->nullable()->change();;
            $table->text('reason')->nullable()->change();;
            $table->text('sku_name')->nullable()->change();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('attention_requireds', function (Blueprint $table) {
            //
        });
    }

}
