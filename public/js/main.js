//login form
//  $("#loginform").show();
//        $("#hublogin").hide();
//        $("#agentlogin").hide();
//
//function findUrl() {
//    var type = $("#usertype").val();
//    if (type == 1) {
//        $("#loginform").show();
//        $("#hublogin").hide();
//        $("#agentlogin").hide();
//    }
//    if (type == 2) {
//        $("#loginform").hide();
//        $("#hublogin").show();
//        $("#agentlogin").hide();
//    }
//    if (type == 3) {
//        $("#loginform").hide();
//        $("#hublogin").hide();
//        $("#agentlogin").show();
//    }
//}




// // --- login form validation js ---
// Disable form submissions if there are invalid fields
var list_of_element = [];
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('field-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();
// --- end ---


// --- custom js---
$(document).ready(function () {
    $('.ham-icon').click(function () {
        /* Act on the event */
        $('.toggle-switch').toggleClass('switch');
        $('.toggle-switch').find('.submenu').toggleClass('subswitch');
    });
    $('.sidebar-link', '.inner-sidebar-link').click(function () {
        /* Act on the event */
        $(this).parent().toggleClass('active');
        $(this).parent().siblings().removeClass('active');
    });
    $('.has-submenu').click(function () {
        $(this).toggleClass('has-submenu-active');
        $(this).siblings().removeClass('has-submenu-active');
        /* Act on the event */
        $(this).children('.submenu').toggleClass('open');
        //$(this).find('.arrow-icon').children('.la-angle-down').toggleClass('rotate');
        $(this).siblings().children('.submenu').removeClass('open');
        // $(this).siblings().find('.arrow-icon').children('.la-angle-down').removeClass('rotate');
    });

    // profile dropdown
    $('.top-profile').click(function () {
        /* Act on the event */
        if ($('.profile-drop').hasClass('open')) {
            $(this).find('.overlay').remove();
            $('.profile-drop').removeClass('open');
        } else {
            $('.profile-drop').addClass('open');
            $(this).append('<div class="overlay"></div>');
        }
    });
    $('.overlay').click(function () {
        $('.dropdown-menu').removeClass('open');
    });

    // add user form
    $('.addbtn').click(function () {
        $(this).parents('.tab-pane').find('.add-user-form').addClass('open-form');
    });
    $('.addresource').click(function () {
        $('.add-user-form').addClass('open-form');
    });

    // upload file input
    $('.upload-file-input').change(function (e) {
        $(this).siblings().find('.upload-text').text($(this)[0].files[0].name);
    });

    // Admin dashboard page accordian
    $("h4[data-target='collapse']").click(function () {
        /* Act on the event */
        $(this).toggleClass('rotate-Ico');
        $('#collapse').toggleClass('open');
    });

    // retail device add
    $('.has-device').click(function () {
        /* Act on the event */
        $(this).is(':checked')
        $('.radio-label').toggleClass('d-none');
    });


    $(document).on('click', '.ulListType ul li', function () {
        var currentText = $(this).text();
        $(this).parent().siblings('.customSelect').find('p').html('');
        $(this).parents('.ulListType').siblings('.customSelect').find('p').text(currentText);
    });
    $('.customSelect').on('click', function () {
        $('.ulListType').slideUp();
        $(this).siblings('.ulListType').slideToggle();
    });
    $(document).on('click', '.ulListType ul li', function () {
        $(this).parents('.ulListType').slideUp();
    });
    $('body').on('click', function (e) {
        if (!$(e.target).closest('.ulListType, .customSelect').length > 0) {
            $('.ulListType').slideUp();
        }
    });
    // end

});


// --- end ---


// --- map distributor input tags ---
(function ($) {
    "use strict";

    var defaultOptions = {
        tagClass: function (item) {
            return 'label label-info';
        },
        itemValue: function (item) {
            return item ? item.toString() : item;
        },
        itemText: function (item) {
            return this.itemValue(item);
        },
        itemTitle: function (item) {
            return null;
        },
        freeInput: true,
        addOnBlur: true,
        maxTags: undefined,
        maxChars: undefined,
        //confirmKeys: [13, 44],
        delimiter: ',',
        delimiterRegex: null,
        cancelConfirmKeysOnEmpty: true,
        onTagExists: function (item, $tag) {
            $tag.hide().fadeIn();
        },
        trimValue: false,
        allowDuplicates: false
    };

    /**
     * Constructor function
     */
    function TagsInput(element, options) {
        this.itemsArray = [];

        this.$element = $(element);
        this.$element.hide();

        this.isSelect = (element.tagName === 'SELECT');
        this.multiple = (this.isSelect && element.hasAttribute('multiple'));
        this.objectItems = options && options.itemValue;
        this.placeholderText = element.hasAttribute('placeholder') ? this.$element.attr('placeholder') : '';
        this.inputSize = Math.max(1, this.placeholderText.length);

        this.$container = $('<div class="bootstrap-tagsinput px-1 py-1 w-100 pr-4 rounded-lg light-border border tagsinput"></div>');
        this.$input = $('<input type="text" class="inputTag" name="searach_distributor" id="searach_distributor" placeholder="' + this.placeholderText + '"/>').appendTo(this.$container);

        this.$element.before(this.$container);

        this.build(options);
    }

    TagsInput.prototype = {
        constructor: TagsInput,

        /**
         * Adds the given item as a new tag. Pass true to dontPushVal to prevent
         * updating the elements val()
         */
        add: function (item, dontPushVal, options) {
            var self = this;

            if (self.options.maxTags && self.itemsArray.length >= self.options.maxTags)
                return;

            // Ignore falsey values, except false
            if (item !== false && !item)
                return;

            // Trim value
            if (typeof item === "string" && self.options.trimValue) {
                item = $.trim(item);
            }

            // Throw an error when trying to add an object while the itemValue option was not set
            if (typeof item === "object" && !self.objectItems)
                throw("Can't add objects when itemValue option is not set");

            // Ignore strings only containg whitespace
            if (item.toString().match(/^\s*$/))
                return;

            // If SELECT but not multiple, remove current tag
            if (self.isSelect && !self.multiple && self.itemsArray.length > 0)
                self.remove(self.itemsArray[0]);

            if (typeof item === "string" && this.$element[0].tagName === 'INPUT') {
                var delimiter = (self.options.delimiterRegex) ? self.options.delimiterRegex : self.options.delimiter;
                var items = item.split(delimiter);
                if (items.length > 1) {
                    for (var i = 0; i < items.length; i++) {
                        this.add(items[i], true);
                    }

                    if (!dontPushVal)
                        self.pushVal();
                    return;
                }
            }

            var itemValue = self.options.itemValue(item),
                    itemText = self.options.itemText(item),
                    tagClass = self.options.tagClass(item),
                    itemTitle = self.options.itemTitle(item);

            // Ignore items allready added
            var existing = $.grep(self.itemsArray, function (item) {
                return self.options.itemValue(item) === itemValue;
            })[0];
            if (existing && !self.options.allowDuplicates) {
                // Invoke onTagExists
                if (self.options.onTagExists) {
                    var $existingTag = $(".tag", self.$container).filter(function () {
                        return $(this).data("item") === existing;
                    });
                    self.options.onTagExists(item, $existingTag);
                }
                return;
            }

            // if length greater than limit
            if (self.items().toString().length + item.length + 1 > self.options.maxInputLength)
                return;

            // raise beforeItemAdd arg
            var beforeItemAddEvent = $.Event('beforeItemAdd', {item: item, cancel: false, options: options});
            self.$element.trigger(beforeItemAddEvent);
            if (beforeItemAddEvent.cancel)
                return;

            // register item in internal array and map
            self.itemsArray.push(item);

            // add a tag element

            // var $tag = $('<span class="tag ' + htmlEncode(tagClass) + (itemTitle !== null ? ('" title="' + itemTitle) : '') + '">' + htmlEncode(itemText) + '<span data-role="remove"></span></span>');
            // $tag.data('item', item);
            // self.findInputWrapper().before($tag);
            // $tag.after(' ');

            // add <option /> if item represents a value not present in one of the <select />'s options
            if (self.isSelect && !$('option[value="' + encodeURIComponent(itemValue) + '"]', self.$element)[0]) {
                var $option = $('<option selected>' + htmlEncode(itemText) + '</option>');
                $option.data('item', item);
                $option.attr('value', itemValue);
                self.$element.append($option);
            }

            if (!dontPushVal)
                self.pushVal();

            // Add class when reached maxTags
            if (self.options.maxTags === self.itemsArray.length || self.items().toString().length === self.options.maxInputLength)
                self.$container.addClass('bootstrap-tagsinput-max');

            self.$element.trigger($.Event('itemAdded', {item: item, options: options}));
        },

        /**
         * Removes the given item. Pass true to dontPushVal to prevent updating the
         * elements val()
         */
        remove: function (item, dontPushVal, options) {
            var self = this;

            if (self.objectItems) {
                if (typeof item === "object")
                    item = $.grep(self.itemsArray, function (other) {
                        return self.options.itemValue(other) == self.options.itemValue(item);
                    });
                else
                    item = $.grep(self.itemsArray, function (other) {
                        return self.options.itemValue(other) == item;
                    });

                item = item[item.length - 1];
            }

            if (item) {
                var beforeItemRemoveEvent = $.Event('beforeItemRemove', {item: item, cancel: false, options: options});
                self.$element.trigger(beforeItemRemoveEvent);
                if (beforeItemRemoveEvent.cancel)
                    return;

                $('.tag', self.$container).filter(function () {
                    return $(this).data('item') === item;
                }).remove();
                $('option', self.$element).filter(function () {
                    return $(this).data('item') === item;
                }).remove();
                if ($.inArray(item, self.itemsArray) !== -1)
                    self.itemsArray.splice($.inArray(item, self.itemsArray), 1);
            }

            if (!dontPushVal)
                self.pushVal();

            // Remove class when reached maxTags
            if (self.options.maxTags > self.itemsArray.length)
                self.$container.removeClass('bootstrap-tagsinput-max');

            self.$element.trigger($.Event('itemRemoved', {item: item, options: options}));
        },

        /**
         * Removes all items
         */
        removeAll: function () {
            var self = this;

            $('.tag', self.$container).remove();
            $('option', self.$element).remove();

            while (self.itemsArray.length > 0)
                self.itemsArray.pop();

            self.pushVal();
        },

        /**
         * Refreshes the tags so they match the text/value of their corresponding
         * item.
         */
        refresh: function () {
            var self = this;
            $('.tag', self.$container).each(function () {
                var $tag = $(this),
                        item = $tag.data('item'),
                        itemValue = self.options.itemValue(item),
                        itemText = self.options.itemText(item),
                        tagClass = self.options.tagClass(item);

                // Update tag's class and inner text
                $tag.attr('class', null);
                $tag.addClass('tag ' + htmlEncode(tagClass));
                $tag.contents().filter(function () {
                    return this.nodeType == 3;
                })[0].nodeValue = htmlEncode(itemText);

                if (self.isSelect) {
                    var option = $('option', self.$element).filter(function () {
                        return $(this).data('item') === item;
                    });
                    option.attr('value', itemValue);
                }
            });
        },

        /**
         * Returns the items added as tags
         */
        items: function () {
            return this.itemsArray;
        },

        /**
         * Assembly value by retrieving the value of each item, and set it on the
         * element.
         */
        pushVal: function () {
            var self = this,
                    val = $.map(self.items(), function (item) {
                        return self.options.itemValue(item).toString();
                    });

            self.$element.val(val, true).trigger('change');
        },

        /**
         * Initializes the tags input behaviour on the element
         */
        build: function (options) {
            var self = this;

            self.options = $.extend({}, defaultOptions, options);
            // When itemValue is set, freeInput should always be false
            if (self.objectItems)
                self.options.freeInput = false;

            makeOptionItemFunction(self.options, 'itemValue');
            makeOptionItemFunction(self.options, 'itemText');
            makeOptionFunction(self.options, 'tagClass');

            // Typeahead Bootstrap version 2.3.2
            if (self.options.typeahead) {
                var typeahead = self.options.typeahead || {};

                makeOptionFunction(typeahead, 'source');

                self.$input.typeahead($.extend({}, typeahead, {
                    source: function (query, process) {
                        function processItems(items) {
                            var texts = [];

                            for (var i = 0; i < items.length; i++) {
                                var text = self.options.itemText(items[i]);
                                map[text] = items[i];
                                texts.push(text);
                            }
                            process(texts);
                        }

                        this.map = {};
                        var map = this.map,
                                data = typeahead.source(query);

                        if ($.isFunction(data.success)) {
                            // support for Angular callbacks
                            data.success(processItems);
                        } else if ($.isFunction(data.then)) {
                            // support for Angular promises
                            data.then(processItems);
                        } else {
                            // support for functions and jquery promises
                            $.when(data)
                                    .then(processItems);
                        }
                    },
                    updater: function (text) {
                        self.add(this.map[text]);
                        return this.map[text];
                    },
                    matcher: function (text) {
                        return (text.toLowerCase().indexOf(this.query.trim().toLowerCase()) !== -1);
                    },
                    sorter: function (texts) {
                        return texts.sort();
                    },
                    highlighter: function (text) {
                        var regex = new RegExp('(' + this.query + ')', 'gi');
                        return text.replace(regex, "<strong>$1</strong>");
                    }
                }));
            }

            // typeahead.js
            if (self.options.typeaheadjs) {
                var typeaheadConfig = null;
                var typeaheadDatasets = {};

                // Determine if main configurations were passed or simply a dataset
                var typeaheadjs = self.options.typeaheadjs;
                if ($.isArray(typeaheadjs)) {
                    typeaheadConfig = typeaheadjs[0];
                    typeaheadDatasets = typeaheadjs[1];
                } else {
                    typeaheadDatasets = typeaheadjs;
                }

                self.$input.typeahead(typeaheadConfig, typeaheadDatasets).on('typeahead:selected', $.proxy(function (obj, datum) {
                    if (typeaheadDatasets.valueKey)
                        self.add(datum[typeaheadDatasets.valueKey]);
                    else
                        self.add(datum);
                    self.$input.typeahead('val', '');
                }, self));
            }

            // self.$container.on('click', $.proxy(function(event) {
            //   if (! self.$element.attr('disabled')) {
            //     self.$input.removeAttr('disabled');
            //   }
            //   self.$input.focus();
            // }, self));

            if (self.options.addOnBlur && self.options.freeInput) {
                self.$input.on('focusout', $.proxy(function (event) {
                    // HACK: only process on focusout when no typeahead opened, to
                    //       avoid adding the typeahead text as tag
                    if ($('.typeahead, .twitter-typeahead', self.$container).length === 0) {
                        self.add(self.$input.val());
                        self.$input.val('');
                    }
                }, self));
            }


            // self.$container.on('keydown', 'input', $.proxy(function(event) {
            //   var $input = $(event.target),
            //       $inputWrapper = self.findInputWrapper();
            //
            //   if (self.$element.attr('disabled')) {
            //     self.$input.attr('disabled', 'disabled');
            //     return;
            //   }
            //
            //   // switch (event.which) {
            //   //   // BACKSPACE
            //   //   case 8:
            //   //     if (doGetCaretPosition($input[0]) === 0) {
            //   //       var prev = $inputWrapper.prev();
            //   //       if (prev.length) {
            //   //         self.remove(prev.data('item'));
            //   //       }
            //   //     }
            //   //     break;
            //   //
            //   //   // DELETE
            //   //   case 46:
            //   //     if (doGetCaretPosition($input[0]) === 0) {
            //   //       var next = $inputWrapper.next();
            //   //       if (next.length) {
            //   //         self.remove(next.data('item'));
            //   //       }
            //   //     }
            //   //     break;
            //   //
            //   //   // LEFT ARROW
            //   //   case 37:
            //   //     // Try to move the input before the previous tag
            //   //     var $prevTag = $inputWrapper.prev();
            //   //     if ($input.val().length === 0 && $prevTag[0]) {
            //   //       $prevTag.before($inputWrapper);
            //   //       $input.focus();
            //   //     }
            //   //     break;
            //   //   // RIGHT ARROW
            //   //   case 39:
            //   //     // Try to move the input after the next tag
            //   //     var $nextTag = $inputWrapper.next();
            //   //     if ($input.val().length === 0 && $nextTag[0]) {
            //   //       $nextTag.after($inputWrapper);
            //   //       $input.focus();
            //   //     }
            //   //     break;
            //   //  default:
            //   //      // ignore
            //   //  }
            //
            //   // Reset internal input's size
            //   var textLength = $input.val().length,
            //       wordSpace = Math.ceil(textLength / 5),
            //       size = textLength + wordSpace + 1;
            //   $input.attr('size', Math.max(this.inputSize, $input.val().length));
            // }, self));



            // Remove icon clicked
            self.$container.on('click', '[data-role=remove]', $.proxy(function (event) {
                if (self.$element.attr('disabled')) {
                    return;
                }
                self.remove($(event.target).closest('.tag').data('item'));
            }, self));

            // Only add existing value as tags when using strings as tags
            if (self.options.itemValue === defaultOptions.itemValue) {
                if (self.$element[0].tagName === 'INPUT') {
                    self.add(self.$element.val());
                } else {
                    $('option', self.$element).each(function () {
                        self.add($(this).attr('value'), true);
                    });
                }
            }
        },

        /**
         * Removes all tagsinput behaviour and unregsiter all event handlers
         */
        destroy: function () {
            var self = this;

            // Unbind events
            self.$container.off('keypress', 'input');
            self.$container.off('click', '[role=remove]');

            self.$container.remove();
            self.$element.removeData('tagsinput');
            self.$element.show();
        },

        /**
         * Sets focus on the tagsinput
         */
        focus: function () {
            this.$input.focus();
        },

        /**
         * Returns the internal input element
         */
        input: function () {
            return this.$input;
        },

        /**
         * Returns the element which is wrapped around the internal input. This
         * is normally the $container, but typeahead.js moves the $input element.
         */
        findInputWrapper: function () {
            var elt = this.$input[0],
                    container = this.$container[0];
            while (elt && elt.parentNode !== container)
                elt = elt.parentNode;

            return $(elt);
        }
    };

    /**
     * Register JQuery plugin
     */
    $.fn.tagsinput = function (arg1, arg2, arg3) {
        var results = [];

        this.each(function () {
            var tagsinput = $(this).data('tagsinput');
            // Initialize a new tags input
            if (!tagsinput) {
                tagsinput = new TagsInput(this, arg1);
                $(this).data('tagsinput', tagsinput);
                results.push(tagsinput);

                if (this.tagName === 'SELECT') {
                    $('option', $(this)).attr('selected', 'selected');
                }

                // Init tags from $(this).val()
                $(this).val($(this).val());
            } else if (!arg1 && !arg2) {
                // tagsinput already exists
                // no function, trying to init
                results.push(tagsinput);
            } else if (tagsinput[arg1] !== undefined) {
                // Invoke function on existing tags input
                if (tagsinput[arg1].length === 3 && arg3 !== undefined) {
                    var retVal = tagsinput[arg1](arg2, null, arg3);
                } else {
                    var retVal = tagsinput[arg1](arg2);
                }
                if (retVal !== undefined)
                    results.push(retVal);
            }
        });

        if (typeof arg1 == 'string') {
            // Return the results from the invoked function calls
            return results.length > 1 ? results : results[0];
        } else {
            return results;
        }
    };

    $.fn.tagsinput.Constructor = TagsInput;

    /**
     * Most options support both a string or number as well as a function as
     * option value. This function makes sure that the option with the given
     * key in the given options is wrapped in a function
     */
    function makeOptionItemFunction(options, key) {
        if (typeof options[key] !== 'function') {
            var propertyName = options[key];
            options[key] = function (item) {
                return item[propertyName];
            };
        }
    }
    function makeOptionFunction(options, key) {
        if (typeof options[key] !== 'function') {
            var value = options[key];
            options[key] = function () {
                return value;
            };
        }
    }
    /**
     * HtmlEncodes the given value
     */
    var htmlEncodeContainer = $('<div />');
    function htmlEncode(value) {
        if (value) {
            return htmlEncodeContainer.text(value).html();
        } else {
            return '';
        }
    }

    /**
     * Returns the position of the caret in the given input field
     * http://flightschool.acylt.com/devnotes/caret-position-woes/
     */
    function doGetCaretPosition(oField) {
        var iCaretPos = 0;
        if (document.selection) {
            oField.focus();
            var oSel = document.selection.createRange();
            oSel.moveStart('character', -oField.value.length);
            iCaretPos = oSel.text.length;
        } else if (oField.selectionStart || oField.selectionStart == '0') {
            iCaretPos = oField.selectionStart;
        }
        return (iCaretPos);
    }


    /**
     * Returns boolean indicates whether user has pressed an expected key combination.
     * @param object keyPressEvent: JavaScript event object, refer
     *     http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
     * @param object lookupList: expected key combinations, as in:
     *     [13, {which: 188, shiftKey: true}]
     */
    // function keyCombinationInList(keyPressEvent, lookupList) {
    //     var found = false;
    //     $.each(lookupList, function (index, keyCombination) {
    //         if (typeof (keyCombination) === 'number' && keyPressEvent.which === keyCombination) {
    //             found = true;
    //             return false;
    //         }
    //
    //         if (keyPressEvent.which === keyCombination.which) {
    //             var alt = !keyCombination.hasOwnProperty('altKey') || keyPressEvent.altKey === keyCombination.altKey,
    //                 shift = !keyCombination.hasOwnProperty('shiftKey') || keyPressEvent.shiftKey === keyCombination.shiftKey,
    //                 ctrl = !keyCombination.hasOwnProperty('ctrlKey') || keyPressEvent.ctrlKey === keyCombination.ctrlKey;
    //             if (alt && shift && ctrl) {
    //                 found = true;
    //                 return false;
    //             }
    //         }
    //     });
    //
    //     return found;
    // }

    /**
     * Initialize tagsinput behaviour on inputs and selects which have
     * data-role=tagsinput
     */
    $(function () {
        $("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();
    });
})(window.jQuery);
// --- end ---

// --- map distributor input dropdown ---
$(document).ready(function () {

    $('.in-div').click(function (event) {
        $(this).next().removeClass('d-none');
    });
    $(document).click(function (event) {
        var your_element = $(".right-section");
        if (!your_element.is(event.target) && your_element.has(event.target).length === 0)
        {
            $('.tag-list').addClass('d-none');
        }
    });

    $(document).on('click', '.list-group-item', function () {
        var $already_exist = 0;
        var dist_id = $(this).attr('data_id');
        var selectedText = $(this).children('p').text();
        var htmlelement = $(this).parent().siblings('.in-div').find('.inputTag');
        //console.log(htmlelement);
        $(htmlelement.siblings()).each(function (index) {
            if ($(this).text() == selectedText) {
                $already_exist++;
            }
        });

        if (!$already_exist) {
            $(htmlelement).before('<span  title=' + selectedText + ' class="position-relative tag">' + selectedText + '<span onclick="remove_ids(' + dist_id + ')" class="remove" data-role="remove"></span></span>');
        }
        $('.remove').click(function () {
            $(this).parent().remove();

        });
    });

    $("#searach_distributor").keyup(function () {
        update_list("distributor_list");
    });
    $("#search_btn").keyup(function () {
        update_list("distributor_list");
    });

});
// end
function li_click(id)
{
    var ind = list_of_element.indexOf(id);

    if (ind == -1) {
        list_of_element.push(id);
    }
}
function remove_ids(id) {

    var ind = list_of_element.indexOf(id);
    list_of_element.splice(ind, 1);

}


// admin dashboard graph -nitish
