'use strict';

const moment = require('moment');
const path = require('path');
const fs = require('fs');
//const helper = require('./helper');

class Socket{

    constructor(socket){
        this.io = socket;
    }

    socketEvents(){
        this.io.on('connection', (socket) => {
            console.log('connect.....')

            socket.on('retailers_click',function(){
                console.log('retailers_click');
                //socket.broadcast.emit('usersReload');
                socket.emit('usersReload');
            });


            socket.on('disconnect', async () => {

                console.log('disconnect');
        	});
        });
    }



    socketConfig(){
        this.io.use( async (socket, next) => {
            // let userId = socket.request._query['id'];
            // let userSocketId = socket.id;
            // const response = await helper.addSocketId( userId, userSocketId);
            // if(response &&  response !== null){
                next();
            // }else{
            //     console.error(`Socket connection failed, for  user Id ${userId}.`);
            // }
        });
        this.socketEvents();
    }
}
module.exports = Socket;
