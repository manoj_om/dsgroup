<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiSkuLists extends Model
{
    //
    protected $fillable = ['api_id','sku_id','description','unit_cost',
    'units','units_delivered','amount_collected','status','api_order_id',
    'packing_type'];



}
