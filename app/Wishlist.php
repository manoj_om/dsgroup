<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    //
    protected $table = 'wishlist';
    protected $fillable = ['user_id','product_id'];


    public function products(){

        return $this->belongsTo('App\Product','product_id','id')->select('id','product_lists_id','short_description','package_unit',
        'packaging','quantity','category','sub_category','mrp','rlp',
        'brand_name','manufacturer','max_order_quantity','product_unit_weight',
        'avtar');
    }



}
