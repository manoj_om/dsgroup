<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
class validateUsers extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request) {
        return [
            'user_name' => 'required',
            'user_email' => 'required|email|unique:users,email,'.$request->editid.'',
            'user_mobile' => 'required|digits:10|max:10|min:10|unique:users,mobile_number,'.$request->editid.'',
            'role' => 'required',
//            'username' => 'required',
            'user_designation' => 'required',
        ];
    }

}
