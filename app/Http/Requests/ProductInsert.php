<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductInsert extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'product_name' => 'required',
            'brand_name' => 'required',
            'avtar' => 'required|image64:jpeg,jpg,png',
            'product_category' => 'required',
            'product_subcategory' => 'required',
            'product_sku' => 'required',
            'packaging' => 'required',
            'quantity' => 'required',
            'product_package_unit' => 'required',
            'mrp' => 'required',
            'dlp' => 'required',
            'rlp' => 'required',
            'product_unit_weight' => 'required',
        ];
    }

    public function messages() {
        return [
            'avtar.required' => 'Product Image is required',
        ];
    }

}
