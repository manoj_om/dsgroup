<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class validateSchemeOrderVolume extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request) {
        return [
            'scheme_name' => 'required',
            'fromdate' => 'required',
            'todate' => 'required|date|after_or_equal:fromdate',
            'promocode' => 'required|unique:order_volume_schemes,promocode,' . $request->currentid . '|unique:new_product_launches|unique:ordered_item_schemes|unique:ordered_items_discounts|unique:sku_line_sold_per_call_schemes',
            'order_count' => 'required',
            'minimum_order_value' => 'required',
            'max_applicability' => 'required',
            'free_ds_rlp' => 'required',
            'free_ds_mrp' => 'required',
            'shop_type' => 'required',
            'retailer_type' => 'required',
            'channel_type' => 'required',
        ];
    }

    public function messages() {
        return [
            'retailer_type.required' => 'Retailer Type 2 is required',
            'shop_type.required' => 'Retailer Type 1 is required',
        ];
    }

}
