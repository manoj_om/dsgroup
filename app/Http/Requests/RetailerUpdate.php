<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RetailerUpdate extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request) {
        
//        print_r($request->all());exit;
        return [
           'full_name' => 'required',
            'has_smart_phone' => 'required|boolean',
            'primary_phone' => 'required|max:10|min:10|digits:10|unique:retailers,primary_mobile_number,'.$request->edit_id.'',
            'whatsapp_number' => "nullable|digits:10|max:10|min:10|unique:retailers,whats_app_number,$request->edit_id",
            'whatsapp_number' => "required_if:has_smart_phone,==,true",
            'shop_type' => 'required',
            'shop_name' => 'required',
            'device' => "required_if:has_smart_phone,true",
            'ds_qrcode' => 'required|unique:retailers,scan_code,'.$request->edit_id.'',
            'shop_address' => 'required',
            'lattitude' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'primary_contact_image' => 'nullable|image64:jpeg,jpg,png',
            'shop_image' => 'nullable|image64:jpeg,jpg,png',
            'alternate_phone' => "nullable|digits:10|max:10|min:10|different:primary_phone|different:whatsapp_number|different:secondary_phone_number",
            'secondary_phone_number' => "nullable|digits:10|max:10|min:10|different:primary_phone|different:whatsapp_number|different:alternate_phone",
            'secondary_contact_image' => 'nullable|image64:jpeg,jpg,png',
            'gst_image' => 'nullable|image64:jpeg,jpg,png',
            'pan_image' => 'nullable|image64:jpeg,jpg,png',
            'adhar_image' => 'nullable|image64:jpeg,jpg,png',
            'gst' => 'nullable|max:15|min:15',
            'pan_number' => 'nullable|max:10|min:10',
            'adhar_number' => 'nullable|digits:12||max:12|min:12',
        ];
    }
  public function messages()
{
    return [
        'full_name.required' => 'A Full Name is required',
        'primary_phone.required'  => 'A Primary Number is required',
        'whatsapp_number.required'  => 'A Whatsapp Number is required',
        'ds_qrcode.required'  => 'A DS QR Code is required',
        'latitude.required'  => 'A Latitude is required',
        'longitude.required'  => 'A Longitude is required',
       
      
    ];
}
}
