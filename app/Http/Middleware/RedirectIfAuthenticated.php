<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
//        echo $guard;exit;
        
       
        if (Auth::guard($guard)->check()) {
            if($guard == 'agent'){
                return redirect()->route('agent-dashboard');
            }else if($guard == 'hub'){
                return redirect()->route('hub-dashboard');
            }
             return redirect()->route('dashboard');
        }
        
        

        return $next($request);
    }

}
