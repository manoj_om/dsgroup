<?php

namespace App\Http\Middleware;

use Closure;
use App\Repository\RetailerRepository;

class CheckHubForRetailer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $userId = request()->user()->id;
        $mapDistributor = new \App\MapDistributor;

        $userDetail = $mapDistributor->where('retailer_id',$userId)->first();
        if(empty($userDetail))
            return response()->json(['status' => false,'message' => 'You have no hub assigned'], __('api/responseCode.logout'));

        $distributorId = $userDetail->distributor_id;

        $isActiveDistributor = \App\Distributor::where('id',$distributorId)->where('status',1)->first();

        if(empty($isActiveDistributor))
            return response()->json(['status' => false,'message' => 'Your assigned hub is not active'], __('api/responseCode.logout'));
        return $next($request);
    }
}
