<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class adminRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        print_r($request->all());exit;
        
        if(Auth::user()->role != 1){
          return redirect()->route('call-center-queue'); 
       }
        return $next($request);
    }
}
