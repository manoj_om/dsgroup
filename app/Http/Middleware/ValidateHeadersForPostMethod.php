<?php

namespace App\Http\Middleware;

use Closure;

class ValidateHeadersForPostMethod
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->ismethod('POST')){

            if (0 !== strpos($request->headers->get('Content-Type'), 'multipart/form-data')){
                return response()->json(['status' => false,'message' => 'Header content type is not accepted'],__('api/responseCode.unsupported_type'));
            }
            //$header = explode(';',$request->header('content-type'));
            /*if(is_array($header) && $header[0] != 'multipart/form-data'){
                return response()->json(['status' => false,'message' => 'Header content type is modified'],415);
            }*/
        }
        return $next($request);
    }
}
