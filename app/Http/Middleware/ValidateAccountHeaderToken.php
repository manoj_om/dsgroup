<?php

namespace App\Http\Middleware;

use Closure;

class ValidateAccountHeaderToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!$request->headers->get('authorization')){
            return response()->json(['status' => false,'message' => 'Unauthorised access'],401);
        }else{
            return response()->json($request->headers->get('authorization'));
        }

        return $next($request);
    }
}
