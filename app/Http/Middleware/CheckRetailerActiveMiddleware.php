<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Repository\RetailerRepository;
class CheckRetailerActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {
        // if(Auth::guard($guard)->check()){
        //     $userId = Auth::gaurd($guard)->user()->id;
        //     $retailerRepository =  new RetailerRepository(new \App\Retailer);
        //     $retailerRepository->getById($userId);
        //     if($retailerRepository->getById($userId)->status != '0')
        //     {
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'Your account has been deactivated.'],
        //             __('api/responseCode.logout'));
        //     }

        // }
        return $next($request);
    }
}
