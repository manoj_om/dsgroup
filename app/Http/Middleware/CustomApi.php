<?php

namespace App\Http\Middleware;
use Api\Models\AgentsOtpModel;
use Closure;

class CustomApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      //echo "hi....";die();

       if($request->header('token')==config('api.token_key_api'))
       {
            if($request->user_key){

                $agentUser = AgentsOtpModel::where(['token_key' => $request->user_key])->first();
                if(!$agentUser){
                    return Response()->json([
                        'status' => 0,
                        'message' => "Sorry! something went wrong"
                    ], 503);
                }
                $activeAgent = \App\AgentsOnboardings::active()->where('id',$agentUser->agent_id)->first();
                if(!$activeAgent){
                    return Response()->json([
                        'status' => 0,
                        'message' => "You are blocked by Admin. Please contact."
                    ], 503);
                }

            }
            return $next($request);
       } else {

            return Response()->json([
                'status' => 0,
                'message' => "Auth error"
            ], 503);
       }

    }
}
