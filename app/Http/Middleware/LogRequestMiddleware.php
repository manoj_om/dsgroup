<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class LogRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        return $next($request);
    }


    public function terminate($request, $response){

        $fileName = "response-";
        if($request->url() == url('api/order/update/status')){
        config(['logging.channels.apiRequest.path' => storage_path('logs/api-request/'.$fileName.date('d-M-Y-H-i-s').'.log')]);
        Log::channel('apiRequest')->info('Api-response.', [
            'request' => $request,
            'requestInArray' => $request->all(),
            'request-url' => $request->url(),
            'response' => $response->getContent()]);
        }else{
            config(['logging.channels.apiRequest.path' => storage_path('logs/api/'.$fileName.date('d-M-Y-H-i-s').'.log')]);
            Log::channel('apiRequest')->info('Api-response.', [
                'request' => $request,
                'requestInArray' => $request->all(),
                'request-url' => $request->url(),
                'response' => $response->getContent()]);
        }

    }
}
