<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\HomeSliderBanner;
use App\SchemeAndPromotion;
use Intervention\Image\Facades\Image as Image;
use App\OrderStatusSetting;
use App\CallCenterScript;

class CMSController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        return view('pages.cms.index');
    }

    public function scheme() {

        return view('pages.cms.scheme');
    }

    public function callscript() {

        return view('pages.cms.callscript');
    }
    public function callScriptTreeView($id) {
        $data = CallCenterScript::with('shop','call')->where('id',$id)->first();
       
         return view('pages.cms.callscripttree')->with(array('data' =>$data));
    }

    public function orderStatusSettingView() {

        return view('pages.cms.status_setting');
    }

    public function addSlider(Request $request) {

        $request->validate([
            'avtar' => 'required|image64:jpeg,jpg,png',
            'title' => 'required',
           
            'type' => 'required'
        ]);

        $avtar = '';
        if ($request->get('avtar')) {
            $image = $request->get('avtar');
            $avtar = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('avtar'))->save(public_path('images/banners/') . $avtar);
        }

        $add = HomeSliderBanner::create([
                    'title' => $request->title,
                    'url' => 'https://www.dsgroup.com/',
                    'banner_type' => $request->type,
                    'avatar' => $avtar,
                    'created_by' => Auth::user()->id,
        ]);

        if ($add) {

            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function setOrderStatusSetting(Request $request) {

        $request->validate([
            'order_to_packing' => 'required',
            'packing_to_dispatch' => 'required',
            'dispatch_to_delivered' => 'required'
        ]);

         $OrderStatusSetting = OrderStatusSetting::first();
        if ($OrderStatusSetting) {
            $add = OrderStatusSetting::where('id',$OrderStatusSetting->id)->update([
                        'packing_to_dispatch' => $request->packing_to_dispatch,
                        'order_to_packing' => $request->order_to_packing,
                        'dispatch_to_delivered' => $request->dispatch_to_delivered,
                        'created_by' => Auth::user()->id,
            ]);
        } else {
            $add = OrderStatusSetting::create([
                        'packing_to_dispatch' => $request->packing_to_dispatch,
                        'order_to_packing' => $request->order_to_packing,
                        'dispatch_to_delivered' => $request->dispatch_to_delivered,
                        'created_by' => Auth::user()->id,
            ]);
        }


        if ($add) {

            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function getOrderStatusSetting(Request $request) {
        $OrderStatusSetting = OrderStatusSetting::first();
        if ($OrderStatusSetting) {
            return response()->json(array('status' => true, 'list' => $OrderStatusSetting));
        } else {
            return response()->json(array('status' => false));
        }
    }

    public function addScheme(Request $request) {

        $request->validate([
            'avtar' => 'required|image64:jpeg,jpg,png',
            'scheme_name' => 'required',
            'fromdate' => 'required',
            'todate' =>  'required|date|after_or_equal:fromdate',
            'description' => 'required',
            'eligibility' => 'required',
        ]);
        $avtar = '';
        if ($request->get('avtar')) {
            $image = $request->get('avtar');
            $avtar = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('avtar'))->save(public_path('images/scheme/') . $avtar);
        }

        $insert = SchemeAndPromotion::create([
                    'scheme_name' => $request->scheme_name,
                    'to_date' => date('Y-m-d H:i:s', strtotime($request->todate)),
                    'from_date' => date('Y-m-d H:i:s', strtotime($request->fromdate)),
                    'avatar' => $avtar,
                    'eligibility' => $request->eligibility,
                    'description' => $request->description,
                    'created_by' => Auth::user()->id,
        ]);

        if ($insert) {
            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function getSlider(Request $request) {
        $data = HomeSliderBanner::orderBy('id', 'DESC');
        $data->where('is_delete', 0);

        if ($request->type != '') {
            $data->Where('banner_type', $request->type);
        }
        if ($request->key != '') {

            $data->where(function($query) use ($request) {
                return $query->orWhere('title', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('url', 'LIKE', '%' . $request->key . '%');
            });
        }
        $data = $data->paginate(10);
        if (count($data) > 0) {
            return json_encode(array('code' => 200, 'list' => $data));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function getScheme(Request $request) {
        $data = SchemeAndPromotion::orderBy('id', 'DESC');
        $data->where('is_delete', 0);
        if ($request->key != '') {

            $data->where(function($query) use ($request) {
                return $query->orWhere('scheme_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('eligibility', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('description', 'LIKE', '%' . $request->key . '%');
            });
        }
        $data = $data->paginate(10);
        if (count($data) > 0) {
            return json_encode(array('code' => 200, 'list' => $data));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function changeSliderStatus(Request $request) {
        $data = HomeSliderBanner::where('id', $request->sid)
                ->update([
            'status' => $request->status
        ]);

        if ($data) {
            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function changeSchemeStatus(Request $request) {
        $data = SchemeAndPromotion::where('id', $request->sid)
                ->update([
            'status' => $request->status
        ]);

        if ($data) {
            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function deleteSlider(Request $request) {
        $data = HomeSliderBanner::where('id', $request->sid)
                ->update([
            'is_delete' => 1
        ]);

        if ($data) {
            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function deleteScheme(Request $request) {
        $data = SchemeAndPromotion::where('id', $request->sid)
                ->update([
            'is_delete' => 1
        ]);

        if ($data) {
            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

}
