<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\BuildingScheme;
use App\OrderVolumeScheme;
use App\OrderedItemScheme;
use App\SkuLineSoldPerCallScheme;
use App\NewProductLaunch;
use Api\Models\RetailersModel;
use App\OrderedItemsDiscount;
use App\Http\Requests\validateSchemeOrderVolume;
use App\Http\Requests\validateSchemeOrderItems;
use App\Http\Requests\validateSchemeSkuLine;
use App\Http\Requests\validateSchemeNewLaunch;
use App\Http\Requests\validateSchemeOrderItemsDiscount;

class SchemeController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        return view('pages.scheme.index');
    }

    public function orderVolumeView() {

        return view('pages.scheme.order_volume');
    }

    public function orderItemView() {

        return view('pages.scheme.order_item');
    }

    public function skuCallSchemeView() {

        return view('pages.scheme.sku_scheme');
    }

    public function newProductLaunchView() {

        return view('pages.scheme.new_product');
    }

    public function orderedItemsDiscountView() {

        return view('pages.scheme.ordered_discount');
    }

    public function addSchemeBuilding(Request $request) {
        $insert = BuildingScheme::create([
                    'scheme_name' => $request->scheme_name,
                    'from_date' => $this->dateFormat($request->fromdate),
                    'to_date' => $this->dateFormat($request->todate),
                    'primary_product_1' => $request->primary_prod_one,
                    'primary_product_2' => $request->primary_prod_two,
                    'secondary_product' => $request->secondary_prod,
                    'apply_discount' => $request->discount,
                    'fixed_amount' => $request->amount_percentage,
                    'promo_code' => $request->promocode,
                    'applied_location' => $request->location,
        ]);

        if ($insert) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function addOrderVolume(validateSchemeOrderVolume $request) {
        $insert = OrderVolumeScheme::create([
                    'scheme_name' => $request->scheme_name,
                    'from_date' => $this->dateFormat($request->fromdate),
                    'to_date' => $this->dateFormat($request->todate),
                    'promocode' => $request->promocode,
                    'applied_location' => $request->location,
                    'frequency' => $request->frequency,
                    'order_count' => $request->order_count,
                    'minimum_order_value' => $request->minimum_order_value,
                    'max_time_applicability' => $request->max_applicability,
                    'free_ds_product_worth_rlp' => $request->free_ds_rlp,
                    'free_ds_product_worth_mrp' => $request->free_ds_mrp,
                    'shop_type' => $request->shop_type,
                    'retailer_type' => $request->retailer_type,
                    'channel_type' => $request->channel_type,
        ]);

        if ($insert) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function updateOrderVolume(validateSchemeOrderVolume $request) {
        $insert = OrderVolumeScheme::where('id', $request->currentid)->update([
            'scheme_name' => $request->scheme_name,
            'from_date' => $this->dateFormat($request->fromdate),
            'to_date' => $this->dateFormat($request->todate),
            'promocode' => $request->promocode,
            'applied_location' => $request->location,
            'frequency' => $request->frequency,
            'order_count' => $request->order_count,
            'minimum_order_value' => $request->minimum_order_value,
            'max_time_applicability' => $request->max_applicability,
            'free_ds_product_worth_rlp' => $request->free_ds_rlp,
            'free_ds_product_worth_mrp' => $request->free_ds_mrp,
            'shop_type' => $request->shop_type,
            'retailer_type' => $request->retailer_type,
            'channel_type' => $request->channel_type,
        ]);

        if ($insert) {
            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function addOrderedItemScheme(validateSchemeOrderItems $request) {
        $insert = OrderedItemScheme::create([
                    'scheme_name' => $request->scheme_name,
                    'from_date' => $this->dateFormat($request->fromdate),
                    'to_date' => $this->dateFormat($request->todate),
                    'free_secondary_product' => $request->secondary_prod,
                    'promocode' => $request->promocode,
                    'applied_location' => $request->location,
                    'frequency' => $request->frequency,
                    'free_product_count' => $request->free_product_count,
                    'product_count' => $request->product_count,
                    'primary_product' => $request->primary_product,
                    'max_time_applicability' => $request->max_applicability,
                    'shop_type' => $request->shop_type,
                    'retailer_type' => $request->retailer_type,
                    'channel_type' => $request->channel_type,
        ]);

        if ($insert) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function updateOrderedItemScheme(validateSchemeOrderItems $request) {
        $insert = OrderedItemScheme::where('id', $request->currentid)->update([
            'scheme_name' => $request->scheme_name,
            'from_date' => $this->dateFormat($request->fromdate),
            'to_date' => $this->dateFormat($request->todate),
            'free_secondary_product' => $request->secondary_prod,
            'promocode' => $request->promocode,
            'applied_location' => $request->location,
            'frequency' => $request->frequency,
            'free_product_count' => $request->free_product_count,
            'product_count' => $request->product_count,
            'primary_product' => $request->primary_product,
            'max_time_applicability' => $request->max_applicability,
            'shop_type' => $request->shop_type,
            'retailer_type' => $request->retailer_type,
            'channel_type' => $request->channel_type,
        ]);

        if ($insert) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function addSkuLineSoldPerCallScheme(validateSchemeSkuLine $request) {
        $insert = SkuLineSoldPerCallScheme::create([
                    'scheme_name' => $request->scheme_name,
                    'from_date' => $this->dateFormat($request->fromdate),
                    'to_date' => $this->dateFormat($request->todate),
                    'free_secondary_product' => $request->free_secondary_prod,
                    'promocode' => $request->promocode,
                    'applied_location' => $request->location,
                    'fixed_amount_percentage' => $request->fixed_amount_percentage,
                    'minimum_order_value' => $request->minimum_order_value,
                    'apply_discount' => $request->apply_discount,
                    'shop_type' => $request->shop_type,
                    'max_time_applicability' => $request->max_applicability,
                    'frequency' => $request->frequency,
                    'retailer_type' => $request->retailer_type,
                    'channel_type' => $request->channel_type,
        ]);

        if ($insert) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function updateSkuLineSoldPerCallScheme(validateSchemeSkuLine $request) {
        $insert = SkuLineSoldPerCallScheme::where('id', $request->currentid)->update([
            'scheme_name' => $request->scheme_name,
            'from_date' => $this->dateFormat($request->fromdate),
            'to_date' => $this->dateFormat($request->todate),
            'free_secondary_product' => $request->free_secondary_prod,
            'promocode' => $request->promocode,
            'applied_location' => $request->location,
            'fixed_amount_percentage' => $request->fixed_amount_percentage,
            'minimum_order_value' => $request->minimum_order_value,
            'apply_discount' => $request->apply_discount,
            'shop_type' => $request->shop_type,
            'max_time_applicability' => $request->max_applicability,
            'frequency' => $request->frequency,
            'retailer_type' => $request->retailer_type,
            'channel_type' => $request->channel_type,
        ]);

        if ($insert) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function addNewProductLaunch(validateSchemeNewLaunch $request) {
        $insert = NewProductLaunch::create([
                    'scheme_name' => $request->scheme_name,
                    'from_date' => $this->dateFormat($request->fromdate),
                    'to_date' => $this->dateFormat($request->todate),
                    'promocode' => $request->promocode,
                    'applied_location' => $request->location,
                    'fixed_amount_percentage' => $request->fixed_amount_percentage,
                    'minimum_order_value' => $request->minimum_order_value,
                    'shop_type' => $request->shop_type,
                    'max_time_applicability' => $request->max_applicability,
                    'frequency' => $request->frequency,
                    'retailer_type' => $request->retailer_type,
                    'channel_type' => $request->channel_type,
        ]);

        if ($insert) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function updateNewProductLaunch(validateSchemeNewLaunch $request) {
        $insert = NewProductLaunch::where('id', $request->currentid)->update([
            'scheme_name' => $request->scheme_name,
            'from_date' => $this->dateFormat($request->fromdate),
            'to_date' => $this->dateFormat($request->todate),
            'promocode' => $request->promocode,
            'applied_location' => $request->location,
            'fixed_amount_percentage' => $request->fixed_amount_percentage,
            'minimum_order_value' => $request->minimum_order_value,
            'shop_type' => $request->shop_type,
            'max_time_applicability' => $request->max_applicability,
            'frequency' => $request->frequency,
            'retailer_type' => $request->retailer_type,
            'channel_type' => $request->channel_type,
        ]);

        if ($insert) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function addOrderedItemsDiscount(validateSchemeOrderItemsDiscount $request) {
        $insert = OrderedItemsDiscount::create([
                    'scheme_name' => $request->scheme_name,
                    'promocode' => $request->promocode,
                    'frequency' => $request->frequency,
                    'shop_type' => $request->shop_type,
                    'primary_product' => $request->primary_product,
                    'primary_product_count' => $request->product_count,
                    'free_secondary_product' => $request->secondary_prod,
                    'free_secondary_product_count' => $request->free_product_count,
                    'apply_discount' => $request->discount,
                    'fixed_percentage' => $request->fixed_amount_percentage,
                    'max_time_applicability' => $request->max_applicability,
                    'applied_location' => $request->location,
                    'created_by' => Auth::user()->id,
                    'from_date' => $this->dateFormat($request->fromdate),
                    'to_date' => $this->dateFormat($request->todate),
                    'retailer_type' => $request->retailer_type,
                    'channel_type' => $request->channel_type,
        ]);

        if ($insert) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function updateOrderedItemsDiscount(validateSchemeOrderItemsDiscount $request) {
        $insert = OrderedItemsDiscount::where('id', $request->currentid)->update([
            'scheme_name' => $request->scheme_name,
            'promocode' => $request->promocode,
            'frequency' => $request->frequency,
            'shop_type' => $request->shop_type,
            'primary_product' => $request->primary_product,
            'primary_product_count' => $request->product_count,
            'free_secondary_product' => $request->secondary_prod,
            'free_secondary_product_count' => $request->free_product_count,
            'apply_discount' => $request->discount,
            'fixed_percentage' => $request->fixed_amount_percentage,
            'max_time_applicability' => $request->max_applicability,
            'applied_location' => $request->location,
            'created_by' => Auth::user()->id,
            'from_date' => $this->dateFormat($request->fromdate),
            'to_date' => $this->dateFormat($request->todate),
            'retailer_type' => $request->retailer_type,
            'channel_type' => $request->channel_type,
        ]);

        if ($insert) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function getSchemeBuilding(Request $request) {
        $BuildingScheme = BuildingScheme::orderBy('id', 'DESC');
        if ($request->key != '') {
            $BuildingScheme->where('scheme_name', 'LIKE', '%' . $request->key . '%');
            $BuildingScheme->orWhere('apply_discount', 'LIKE', '%' . $request->key . '%');
            $BuildingScheme->orWhere('fixed_amount', 'LIKE', '%' . $request->key . '%');
        }

        $data = $BuildingScheme->paginate(10);

        if (count($data) > 0) {
            return json_encode(array('code' => 200, 'list' => $data));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function getOrderedDiscount(Request $request) {
        $BuildingScheme = OrderedItemsDiscount::orderBy('id', 'DESC');
        if ($request->status != '') {
            $BuildingScheme->where('status', $request->status);
        }
        if ($request->channel != '') {
            $BuildingScheme->where('channel_type', $request->channel);
        }

        if ($request->key != '') {
            $BuildingScheme->where(function($query) use ($request) {
                return $query->orwhere('scheme_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('apply_discount', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('fixed_percentage', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('promocode', 'LIKE', '%' . $request->key . '%');
            });
        }

        $data = $BuildingScheme->paginate(15);

        if (count($data) > 0) {
            return json_encode(array('code' => 200, 'list' => $data));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function getOrderVolumeScheme(Request $request) {
        $OrderVolumeScheme = OrderVolumeScheme::orderBy('id', 'DESC');
        if ($request->status != '') {
            $OrderVolumeScheme->where('status', $request->status);
        }
        
        if ($request->channel != '') {
            $OrderVolumeScheme->where('channel_type', $request->channel);
        }
        if ($request->key != '') {
            $OrderVolumeScheme->where(function($query) use ($request) {
                return $query->where('scheme_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('promocode', 'LIKE', '%' . $request->key . '%');
            });
        }

        $data = $OrderVolumeScheme->paginate(15);

        if (count($data) > 0) {
            return json_encode(array('code' => 200, 'list' => $data));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function getOrderedItemScheme(Request $request) {
        $OrderedItemScheme = OrderedItemScheme::orderBy('id', 'DESC');
        if ($request->status != '') {
            $OrderedItemScheme->where('status', $request->status);
        }
        if ($request->channel != '') {
            $OrderedItemScheme->where('channel_type', $request->channel);
        }
        if ($request->key != '') {
            $OrderedItemScheme->where(function($query) use ($request) {
                return $query->where('scheme_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('promocode', 'LIKE', '%' . $request->key . '%');
            });
        }

        $data = $OrderedItemScheme->paginate(15);

        if (count($data) > 0) {
            return json_encode(array('code' => 200, 'list' => $data));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function getSkuLineSoldPerCallScheme(Request $request) {
        $SkuLineSoldPerCallScheme = SkuLineSoldPerCallScheme::orderBy('id', 'DESC');

        if ($request->status != '') {
            $SkuLineSoldPerCallScheme->where('status', $request->status);
        }
        if ($request->channel != '') {
            $SkuLineSoldPerCallScheme->where('channel_type', $request->channel);
        }
        if ($request->key != '') {
            $SkuLineSoldPerCallScheme->where(function($query) use ($request) {
                return $query->where('scheme_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('promocode', 'LIKE', '%' . $request->key . '%');
            });
        }
        $data = $SkuLineSoldPerCallScheme->paginate(15);

        if (count($data) > 0) {
            return json_encode(array('code' => 200, 'list' => $data));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function getNewProductLaunch(Request $request) {
        $NewProductLaunch = NewProductLaunch::orderBy('id', 'DESC');

        if ($request->status != '') {
            $NewProductLaunch->where('status', $request->status);
        }
        if ($request->channel != '') {
            $NewProductLaunch->where('channel_type', $request->channel);
        }
        if ($request->key != '') {
            $NewProductLaunch->where(function($query) use ($request) {
                return $query->where('scheme_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('promocode', 'LIKE', '%' . $request->key . '%');
            });
        }


        $data = $NewProductLaunch->paginate(15);

        if (count($data) > 0) {
            return json_encode(array('code' => 200, 'list' => $data));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function changeSchemeBuildingStatus(Request $request) {
        $change = BuildingScheme::whereIn('id', $request->list)->update([
            'status' => $request->status
        ]);
        if ($change) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function changeOrderedDiscount(Request $request) {
        $change = OrderedItemsDiscount::whereIn('id', $request->list)->update([
            'status' => $request->status
        ]);
        if ($change) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function changeOrderCountStatus(Request $request) {
        $change = OrderVolumeScheme::whereIn('id', $request->list)->update([
            'status' => $request->status
        ]);
        if ($change) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function changeOrderedItemScheme(Request $request) {
        $change = OrderedItemScheme::whereIn('id', $request->list)->update([
            'status' => $request->status
        ]);
        if ($change) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function changeSkuLineSoldPerCallScheme(Request $request) {
        $change = SkuLineSoldPerCallScheme::whereIn('id', $request->list)->update([
            'status' => $request->status
        ]);
        if ($change) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function changeNewProductLaunch(Request $request) {
        $change = NewProductLaunch::whereIn('id', $request->list)->update([
            'status' => $request->status
        ]);
        if ($change) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function dateFormat($date) {

        return date('Y-m-d H:i:s', strtotime($date));
    }

    public function getRetailerCity(Request $request) {

        $data_city = RetailersModel::where('is_fill', 1)->where('city', '!=', null)->select('city')->groupBy('city')->get();
        if (count($data_city) > 0) {
            return json_encode(array('code' => 200, 'list' => $data_city));
        } else {
            return json_encode(array('code' => 500));
        }
    }

}
