<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Traits\CurlRequestTrait;
class DialerApiController extends Controller
{
    //
    use CurlRequestTrait;
    public $endpoint = "http://api.tracko.cardekho.com/api/dcp/connect-dcp-call";

    public function apiIntegrate(Request $request)
    {
        # code...
        $agent = !empty($request->agent) ? $request->agent : "" ;
        $customer = !empty($request->customer) ? $request->customer : "6378335459" ;
        $requestArr = '{
            "agent_number" : "'.$agent.'",
            "virtual_number" : "7375917862",
            "customer_number" : "'.$customer.'"
        }';

        // dd($requestArr);
        $options = array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer NlsCjSGhj_MOj6NfRI65PYgtVTqW8dr0z0eLhdbRp1rzkblAIN-oIsQn3a9BAhYiaXoCfeKXy2jhz2_wjbDHI6vkeiVdeeVbDtiVqPPqXl3AgBstlvSW41BgTuV08GiI'
            ),
            'body' => $requestArr
        );

        $requestFromApi = $this->requestFromCurl($options,'POST');
        dd($requestFromApi);

    }
}
