<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image as Image;
use App\Http\Requests\validateUsers;

class UsersController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function manageUsersView(Request $request) {

        return view('pages.users.index');
    }

    public function viewProfile(Request $request) {

        return view('pages.users.profile');
    }

    public function getUsers(Request $request) {
        $users = User::where('status', $request->status)->paginate(6);

        if (count($users) > 0) {
            echo json_encode(array('code' => 200, 'list' => $users));
        } else {
            echo json_encode(array('code' => 500));
        }
        exit;
    }

    public function disableUsers(Request $request) {
        $users = User::whereIn('id', $request->list)
                ->update([
            'status' => 1
        ]);
        if ($users) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'User not disabled successfully'));
        }

        exit;
    }

    public function enableUsers(Request $request) {
        $users = User::whereIn('id', $request->list)
                ->update([
            'status' => 0
        ]);
        if ($users) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'User not enable successfully'));
        }

        exit;
    }

    public function updateUser(validateUsers $request) {
        $users = User::where('id', $request->editid)
                ->update([
            'name' => $request->user_name,
            'email' => $request->user_email,
            'mobile_number' => $request->user_mobile,
            'role' => $request->role,
            'username' => $request->username,
            'designation' => $request->user_designation,
        ]);
        if ($users) {
            return response()->json(['success' => 'You have successfully updated User'], 200);
        } else {
            return response()->json(['success' => 'You have npt successfully updated User'], 500);
        }
    }

    public function addUser(validateUsers $request) {



        $insert = User::Create([
                    'name' => $request->user_name,
                    'email' => $request->user_email,
                    'mobile_number' => $request->user_mobile,
                    'role' => $request->role,
                    'username' => $request->username,
                    'designation' => $request->user_designation,
                    'password' => Hash::make(Str::random(8)),
        ]);

        if ($insert) {

            return response()->json(['success' => 'You have successfully added User'], 200);
        } else {
            return response()->json(['success' => 'You have npt successfully added User'], 500);
        }
    }

    public function getCurrentUsers() {
        $currentUser = Auth::user();
        echo json_encode($currentUser);
        exit;
    }


    public function uploadImage(Request $request) {
        $name = '';
        if ($request->get('avtar')) {
            $image = $request->get('avtar');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('avtar'))->save(public_path('images/users/') . $name);
        }

        $users = User::where('id', Auth::user()->id)
                ->update([
            'avtar' => $name
        ]);
        if ($users) {
            return json_encode(array('code' => 200, 'msg' => 'User profile updated successfully'));
        } else {
            return json_encode(array('code' => 500, 'msg' => 'User profile not updated successfully'));
        }
    }

    public function searchUsers(Request $request) {
        $user_id = Auth::user()->id;
        $data = User::orderBy('created_at', 'DESC');
        if (isset($request->status)) {
            $data->where('status', $request->status);
        }
        if ($request->key != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('email', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('mobile_number', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('designation', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('username', 'LIKE', '%' . $request->key . '%');
            });
        }
        if ($request->role != '') {
            $data->Where('role', $request->role);
        }
        $result = $data->paginate(6);
        if (count($result) > 0) {
            echo json_encode(array('code' => 200, 'list' => $result));
        } else {
            echo json_encode(array('code' => 500, 'list' => $result));
        }
        exit;
    }

}
