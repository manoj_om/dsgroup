<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\OrderRepository;
use App\Repository\RetailerRepository;
use App\Orders;
use App\CallingAgent;
use App\Retailer;
use App\Distributor;

class ReportController extends Controller {

    public $retailerType1 = array(
        '0' => 'Not selected',
        '1' => 'General A',
        '2' => 'General B',
        '3' => 'General C',
        '4' => 'Chemist A',
        '5' => 'Chemist B',
        '6' => 'Chemist C',
        '7' => 'Cosmetic',
        '8' => 'Paan A',
        '9' => 'Paan B',
        '10' => 'Paan C'
    );
    public $retailerType2 = array(
        '0' => 'Not selected',
        '1' => 'Prime',
        '2' => 'Non Prime',
        '3' => 'Regular',
    );

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        return view('pages.report.index');
    }

    public function orderreportview() {

        return view('pages.report.orderreport');
    }

    public function callcenterview() {

        return view('pages.report.call-center');
    }

    public function mobileappreportview() {

        return view('pages.report.mobile-app-report');
    }

    public function hubreportview() {

        return view('pages.report.hub-report');
    }

    public function riderreportview() {

        return view('pages.report.rider-report');
    }

    public function schemereportview() {

        return view('pages.report.scheme-report');
    }

    public function summary() {

        return view('pages.report.summary');
    }

    public function order(Request $request, OrderRepository $orderRepository) {
        # code...
//        $orderLists = $orderRepository->getAll();
        $list = Orders::orderBy('created_at', 'ASC');

        if ($request->date1 != '' && $request->date2 && isset($request->date1) && isset($request->date2)) {
            $from = date('Y-m-d', strtotime($request->date1));
            $to = date('Y-m-d', strtotime($request->date2));
            $list->where(function($query) use ($request, $from, $to) {
                $query->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to);
            });
        }
        $orderLists = $list->get();

        $reportHeading = array(
            'Date', 'Order Number', 'Retailer Name', 'Retailer Id', 'Retailer Type-1', 'Retailer Type-2',
            'Hub details', 'Area', 'Mobile', 'App/ Call Center', 'Callcenter Detail', 'Rider Detail',
            'S.No. of SKU', 'Item Name', 'Item Price', 'Unit type', 'Order Qty', 'Discount', 'Discount Value',
            'Order Value', 'Order Time', 'Packed Quantity', 'Packed time', 'Dispatched time', 'Delivered Value',
            'Delivered Qty', 'Delivered Time', 'Order Status', 'SKU Status', 'Distributor Name', 'API Order No.',
            'Order to delivery time', 'Overall rating'
        );
        $fileName = time() . "-order-report.csv";
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=' . $fileName . '');
        $file = fopen('php://output', 'w');
        fputcsv($file, $reportHeading);
        $reportValue = array();
        foreach ($orderLists as $key => $orderList) {
            foreach ($orderList->orderItems as $orderItem) {
                $reportValue = array(
                    'Date' => \Carbon\Carbon::parse($orderList->created_at)->format('d-m-Y'),
                    'Order Number' => $orderList->order_number,
                    'Retailer Name' => $orderList->retailer->full_name,
                    'Retailer Id' => $orderList->user_id,
                    'Retailer Type-1' => $this->retailerType1[$orderList->retailer->user_type],
                    'Retailer Type-2' => $this->retailerType2[$orderList->retailer->is_prime],
                    // 'Hub details' => $orderList->retailer,
                    'Hub details' => 'Hub',
                    'Area' => $orderList->retailer->shop_address,
                    'Mobile' => $orderList->retailer->primary_mobile_number,
                    'App/ Call Center' => '',
                    'Callcenter Detail' => '',
                    'Rider Detail' => isset($orderList->apiorder->rider_name) ? $orderList->apiorder->rider_name : '',
                    'S.No. of SKU' => $orderItem->product_id,
                    'Item Name' => $orderItem->products->name,
                    'Item Price' => $orderItem->products->rlp,
                    'Unit type' => '',
                    'Order Qty' => $orderItem->quantity,
                    'Discount' => $orderItem->promocode,
                    'Discount Value' => $orderItem->discount_amount,
                    'Order Value' => $orderList->total_amount,
                    'Order Time' => \Carbon\Carbon::parse($orderList->created_at)->format('H:i:s'),
                    'Packed Quantity' => $orderItem->packed_quantity,
                    'Packed time' => '',
                    'Dispatched time' => '',
                    'Delivered Value' => '',
                    'Delivered Qty' => '',
                    'Delivered Time' => '',
                    'Order Status' => $orderList->status,
                    'SKU Status' => '',
                    'Distributor Name' => isset($orderList->hubs->distributor_name) ? $orderList->hubs->distributor_name : '',
                    'API Order No.' => isset($orderList->apiorder->api_order_id) ? $orderList->apiorder->api_order_id : '',
                    'Order to delivery time' => '',
                    'Overall rating' => ''
                );
                fputcsv($file, $reportValue);
            }
        }

        fclose($file);
        exit;
    }

    public function mobileApp(Request $request, RetailerRepository $retailerRepository, OrderRepository $orderRepository) {
        # code...
//        $retailers = $retailerRepository->getAll();
        
        $list = Retailer::orderBy('created_at', 'ASC');

        if ($request->date1 != '' && $request->date2 && isset($request->date1) && isset($request->date2)) {
            $from = date('Y-m-d', strtotime($request->date1));
            $to = date('Y-m-d', strtotime($request->date2));
            $list->where(function($query) use ($request, $from, $to) {
                $query->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to);
            });
        }
        $retailers = $list->get();
        
        $fileName = time() . "-mobile-report.csv";
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=' . $fileName . '');
        $file = fopen('php://output', 'w');

        $retailersOrder = $orderRepository->getTotalOrdersOfRetailer()->pluck('total', 'user_id');

        $reportHeading = array(
            'Date',
            'Retailer ID',
            'Area',
            'Mobile',
            'Retailer Type-1',
            'Retailer Type-2',
            'Average visit time',
            'Order value (RLP)',
            'Delivery value (RLP)',
            'SKU spread',
            'screen views'
        );
        fputcsv($file, $reportHeading);
        foreach ($retailers as $retailer) {
            $reportValue = array(
                'Date' => \Carbon\Carbon::parse($retailer->created_at)->format('d-m-Y'),
                'Retailer ID' => $retailer->id,
                'Area' => $retailer->shop_address,
                'Mobile' => $retailer->primary_number,
                'Retailer Type-1' => $this->retailerType1[$retailer->user_type],
                'Retailer Type-2' => $this->retailerType2[$retailer->is_prime],
                'Average visit time' => '',
                'Order value (RLP)' => isset($retailersOrder[$retailer->id]) ? $retailersOrder[$retailer->id] : 0,
                'Delivery value (RLP)' => '',
                'SKU spread' => '',
                'screen views' => ''
            );
            fputcsv($file, $reportValue);
        }
        fclose($file);
        exit;
    }

    public function callAgent(Request $request) {
        # code...
        $fileName = time() . "-callagent-report.csv";



        $list = CallingAgent::orderBy('created_at', 'ASC');

        if ($request->date1 != '' && $request->date2 && isset($request->date1) && isset($request->date2)) {
            $from = date('Y-m-d', strtotime($request->date1));
            $to = date('Y-m-d', strtotime($request->date2));
            $list->where(function($query) use ($request, $from, $to) {
                $query->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to);
            });
        }
        
        $agents = $list->get();

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=' . $fileName . '');
        $file = fopen('php://output', 'w');
        $reportHeading = array(
            'Date',
            'Executive ID',
            'Executive name',
            'Executive email',
            'Present/ Absent',
            'Total number of calls',
            'Number of connected calls',
            'Number of Orders',
            'Conversion ratio',
            'Total order value',
            'Total deliver value',
            'Delivery %',
            'Average Order value',
            'Total offline time',
            'Average SKU spread',
            'Average call duration',
        );
        fputcsv($file, $reportHeading);
        foreach ($agents as $agent) {
            $reportValue = array(
                'Date' => \Carbon\Carbon::parse($agent->created_at)->format('d-m-Y'),
                'Executive ID' => $agent->serial_number,
                'Executive name' => $agent->supervisor_name,
                'Executive email' => $agent->email,
                'Present/ Absent' => '',
                'Total number of calls' => '',
                'Number of connected calls' => '',
                'Number of Orders' => '',
                'Conversion ratio' => '',
                'Total order value' => '',
                'Total deliver value' => '',
                'Delivery %' => '',
                'Average Order value' => '',
                'Total offline time' => '',
                'Average SKU spread' => '',
                'Average call duration' => '',
            );
            fputcsv($file, $reportValue);
        }
        fclose($file);
        exit;
    }

    public function hub(Request $request, OrderRepository $orderRepository) {
        # code...
     
        $list = Distributor::orderBy('created_at', 'ASC');

        if ($request->date1 != '' && $request->date2 && isset($request->date1) && isset($request->date2)) {
            $from = date('Y-m-d', strtotime($request->date1));
            $to = date('Y-m-d', strtotime($request->date2));
            $list->where(function($query) use ($request, $from, $to) {
                $query->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to);
            });
        }
        
        $hubs = $list->get();
        
        
        $hubStock = new \App\Repository\HubStockRepository;
        $fileName = time() . "-hub-report.csv";
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=' . $fileName . '');
        $file = fopen('php://output', 'w');
        $openingInventoryDate = \Carbon\Carbon::now()->subDay(1)->format('Y-m-d');

        $reportHeading = array(
            'Date',
            'Hub Detail',
            'SS/ Distributor',
            'Location/ Area',
            'Number of retailers',
            'hub manager',
            'Opening inventory',
            'Inbound inventory',
            'Inventory write off',
            'Closing inventory',
            'Number of orders',
            'Total Order value (DLP)',
            'Total packed value (DLP)',
            'Total delivered value (DLP)',
            'Total delivered value (RLP)',
            'Total  returns (RLP)',
            'Total Lost sales (RLP)',
            'Total Returns (DLP)',
            'Scheme Value (RLP)',
            'Scheme Value (DLP)',
            'Avg time to pack',
            'Avg tme to dispatch'
        );
        fputcsv($file, $reportHeading);
        foreach ($hubs as $hub) {

            $retailers = array_column($hub->retailers->toArray(), 'id');
            $totalOrders = ($retailers) ? $orderRepository->getTotalOrders($retailers) : 0;
            $closingInventory = $hubStock->getClosingInvenory($hub->id);
            $openingInventory = $hubStock->getOpeningInvenory($hub->id, $openingInventoryDate);
            $dlpPrice = 0;
            if ($totalOrders) {
                foreach ($totalOrders as $totalOrder) {
                    foreach ($totalOrder->orderItems as $orderItem) {
                        $dlpPrice = + $orderItem->products->dlp;
                    }
                }
            }
            $reportValue = array(
                'Date' => \Carbon\Carbon::parse($hub->created_at)->format('d-m-Y'),
                'Hub Detail' => $hub->distributor_name,
                'SS/ Distributor' => '',
                'Location/ Area' => $hub->address,
                'Number of retailers' => $hub->distributor_code,
                'hub manager' => $hub->distributor_name,
                'Opening inventory' => $openingInventory, //calculate all the remain qty of product end of the day
                'Inbound inventory' => '', //
                'Inventory write off' => '', //
                'Closing inventory' => $closingInventory, //
                'Number of orders' => $totalOrders ? count($totalOrders) : 0,
                'Total Order value (DLP)' => $dlpPrice,
                'Total packed value (DLP)' => '',
                'Total delivered value (DLP)' => '',
                'Total delivered value (RLP)' => '',
                'Total  returns (RLP)' => '',
                'Total Lost sales (RLP)' => '',
                'Total Returns (DLP)' => '',
                'Scheme Value (RLP)' => '',
                'Scheme Value (DLP)' => '',
                'Avg time to pack' => '',
                'Avg tme to dispatch' => ''
            );
            fputcsv($file, $reportValue);
        }
        fclose($file);
        exit;
    }

    public function rider(Request $request) {
        # code...

        $fileName = time() . "-rider-report.csv";
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=' . $fileName . '');
        $file = fopen('php://output', 'w');

        $reportHeading = array(
            'Date',
            'Rider Detail',
            'Assigned Hub',
            'Delivery partner details',
            'Number of trips',
            'Number of orders',
            'Orders per trip',
            'Full deliveries',
            'Partial deliveries',
            'Full returns',
            'Total Dispatch value',
            'Total delivery value',
            'Value of returns',
            'Delivery value/ Dispatch value',
            'Average TAT',
            'Average rating'
        );
        fputcsv($file, $reportHeading);
    }

    public function scheme(Request $request) {
        # code...
        $fileName = time() . "-rider-report.csv";
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=' . $fileName . '');
        $file = fopen('php://output', 'w');

        $reportHeading = array(
            'Retailer Details',
            'Retailer Type-1',
            'Retailer type-2',
            'Area',
            'Geography',
            'Hub Details',
            'Scheme Type',
            'Scheme Details (Value/ Volume)',
            'Scheme start date',
            'Scheme end date',
            'Scheme type',
            'Qualification criteria',
            'Benefit Value',
            'Qualification status',
            'Scheme fulfillment status',
            'Order No'
        );
        fputcsv($file, $reportHeading);
    }

}
