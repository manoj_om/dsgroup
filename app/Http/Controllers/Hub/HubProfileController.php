<?php

namespace App\Http\Controllers\Hub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class HubProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
//        $this->middleware('auth:agent');
    }

    public function viewhubProfile(){

      return view('pages.hub.hub-profile');
    }
}
