<?php

namespace App\Http\Controllers\Hub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\CurlRequestTrait;
class StockController extends Controller
{
     use CurlRequestTrait;
    public function __construct() {
      $this->middleware('auth:hub');
    }

    public function index(){

      return view('pages.hub.stock.index');
    }
    
    public function sendThreshNotification(Request $request) {
       $stock =  $request->prods;
       $type = $request->type;
       
       $this->sendThresholdNotification($type, $stock);
    }
    
    
}
