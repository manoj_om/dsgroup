<?php

namespace App\Http\Controllers\Hub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MapDistributor;
use App\Distributor;
use App\Orders;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\SessionGuard;
use App\OrderItems;
use App\Traits\PromocodeTrait;
use App\Traits\CurlRequestTrait;

class OrderController extends Controller {

    public function __construct() {
        $this->middleware('auth:hub');
    }

    use PromocodeTrait,
        CurlRequestTrait;

    public function index() {


        return view('pages.hub.order.index');
    }

    public function view($orderid) {
        $data = Orders::where('id', $orderid)->with('apiorder', 'retailer', 'orderItemsView', 'hubs', 'ratings');
        $data->where(function($query) {
            $query->where('hub_id', Auth::guard('hub')->user()->id);
        });
        $data = $data->first()->toArray();
        if ($data['promocode'] != '') {
            $data['promodetail'] = $this->discountAgentPanel($data['promocode_key'], $data['promocode']);
        }
        return view('pages.hub.order.view')->with(array('data' => $data));
    }

    public function getOrders(Request $request) {
        $data = Orders::with('apiorder', 'retailer', 'orderItemsView', 'hubs', 'ratings')->orderBy('created_at', 'DESC');

        $data->where(function($query) use ($request) {
            $query->where('hub_id', Auth::guard('hub')->user()->id);
        });


        if ($request->searchFromDate != '' && $request->searchToDate && isset($request->searchFromDate) && isset($request->searchToDate)) {
            $from = date('Y-m-d', strtotime($request->searchFromDate));
            $to = date('Y-m-d', strtotime($request->searchToDate));

            $data->where(function($query) use ($request, $from, $to) {
                return $query->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to);
            });
        }
        if ($request->deliveryFromTime != '' && $request->deliveryToTime && isset($request->deliveryFromTime) && isset($request->deliveryToTime)) {
            $from = date('Y-m-d', strtotime($request->deliveryFromTime));
            $to = date('Y-m-d', strtotime($request->deliveryToTime));

            $data->where(function($query) use ($request, $from, $to) {
                return $query->whereDate('delivery_time', '>=', $from)->whereDate('delivery_time', '<=', $to);
            });
        }

        if (isset($request->orderBy) && $request->orderBy != '') {
            $data->where(function($query) use ($request) {
                return $query->where('order_by', $request->orderBy);
            });
        }
        if (isset($request->orderStatus) && $request->orderStatus != '') {
            $data->where(function($query) use ($request) {
                return $query->where('status', $request->orderStatus);
            });
        }
        if ($request->key != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('order_number', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('total_amount', 'LIKE', '%' . $request->key . '%')
                                ->orWhereHas('retailer', function($q) use($request) {
                                    $q->Where('full_name', 'LIKE', '%' . $request->key . '%');
                                    $q->orWhere('primary_mobile_number', 'LIKE', '%' . $request->key . '%');
                                    $q->orWhere('city', 'LIKE', '%' . $request->key . '%');
                                });
            });
        }
        $data = $data->paginate($request->pagesize);
        if (count($data) > 0) {
            return json_encode(array('code' => 200, 'list' => $data));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function updatePackedQuantity(Request $request) {
        $update = OrderItems::where('id', $request->itemId)->update([
            'packed_quantity' => $request->quantity,
        ]);

        if ($update) {
            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function changeStatus(Request $request) {
        $update = Orders::where('id', $request->orderid)->first();
        if ($update->status == 0) {
            
             return json_encode(array('code' => 300));
        }
        
        if ($request->type == 2) {
            $notify = "PACKED_ORDER";
            $update->status = "$request->type";
            $update->packed_time = date('Y-m-d H:i:s');
        } else if ($request->type == 3) {
            $notify = "DISPATCH_ORDER";
            $update->status = "$request->type";
            $update->dispatch_time = date('Y-m-d H:i:s');
        } else {
            $notify = "CANCEL_ORDER";
            $update->status = "$request->type";
        }


        if ($update->save()) {

            /*             * *
             *
             * send message & push notification after successfully placed order
             *
             */

            $message = __('api/responseCode.pushNotification.' . $request->type, ['number' => $update->order_number]);

            $this->sendSms($message, $update->user_id);

            $title = __('api/responseCode.title.' . $request->type);

            $this->sendPushNotification($message, $title, $update->user_id);

            $this->sendNotification($notify, $update);

            $orderLogRepository = new \App\Repository\OrdersLogRepository;
            $orderLogRepository->manageLog($update);

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

}
