<?php

namespace App\Http\Controllers\Hub;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductCategory;
use App\ProductSubCategory;
use App\Product;
use App\HubStock;
use App\Distributor;
use Intervention\Image\Facades\Image as Image;
use App\Repository\OrderRepository;
use App\HubStockLog;
use App\HubStockDateCheck;
use App\Repository\OrdersLogRepository;
use App\Repository\ProductRepository;
use App\Traits\CurlRequestTrait;

class DashboardController extends Controller {

    use CurlRequestTrait;
    public function __construct() {

        $this->middleware('auth:hub');
    }

    public function index(OrderRepository $orderRepository) {

        $date = \Carbon\Carbon::now()->format('Y-m-d');

        $gaurd = Auth::getDefaultDriver();
        $userId = Auth::guard($gaurd)->user()->id;

        $orders = $orderRepository->fromDate($date,$userId);
        $orderCount = $orders->count();
        $totalPrice = $orders->sum('total_amount');
        $todayorderProcessed = 0;
        $collectedAmount = 0;
        foreach($orders as $order){

            if($order->status == '5'){
                $todayorderProcessed++;
                $collectedAmount = $collectedAmount+$order->total_amount;
            }
        }

        $getAllOrders = $orderRepository->getAll();
        $helper = new \App\Helpers\Helpers;
        $totalOrdersPrice = $helper->convertRupeeToLac($getAllOrders->sum('total_amount'));

        $totalProcessed = $orderProcessed = 0;
        foreach($getAllOrders as $order){

            if($order->status == '5'){
                $orderProcessed++;
                $totalProcessed = $totalProcessed+$order->total_amount;
            }
        }


        $totalPrice = $helper->convertRupeeToLac($totalPrice);

        $retailersAssigned =  \App\MapDistributor::where('distributor_id',$userId)->count();

        return view('pages.hub.dashboard.index',
            compact('orderCount','totalPrice','orderProcessed','collectedAmount',
            'retailersAssigned','totalProcessed','todayorderProcessed','totalOrdersPrice','todayorderProcessed'));
    }

    public function getGuardInfo() {
        return json_encode(Auth::guard('hub')->user());
    }

    public function getCategory(Request $request) {

        $data = ProductCategory::orderBy('created_at', 'DESC');
        if ($request->searchkey != '') {
            $data->where('name', 'LIKE', '%' . $request->searchkey . '%');
        }

        $data = $data->get()->toArray();

        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'no category found'));
        }
        exit;
    }

    public function getSubCategory(Request $request) {
        $data = ProductSubCategory::with('category')->orderBy('created_at', 'DESC');
        if ($request->cat != '') {
            $data->where('category', $request->cat);
        }
        if ($request->key != '') {
            $data->where('name', 'LIKE', '%' . $request->key . '%');
        }

        $data = $data->get()->toArray();
        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'no category found'));
        }
        exit;
    }

    public function inserInHubLog($param) {
        HubStockLog::create([
            'stock_id' => $param->id,
            'product_id' => $param->product_id,
            'distributor_id' => $param->distributor_id,
            'product_quantity' => $param->product_quantity,
            'pre_created_at' => $param->created_at,
            'out_stock' => $param->in_stock,
            'in_stock' => $param->out_stock,
        ]);

        HubStock::where('id', $param->id)->update([
            'product_quantity' => ($param->product_quantity + $param->in_stock) - $param->out_stock,
            'out_stock' => 0,
            'in_stock' => 0,
        ]);
    }

    public function activeProducts(){
        HubStock::where('status',0);
    }

    public function getProducts(Request $request) {

        $hubid = Auth::guard('hub')->user()->id;

        $isRun = HubStockDateCheck::whereDate('created_at', date("Y-m-d"))->where('hub_id', $hubid)->first();
        if (!$isRun) {
            $yesterDayStock = HubStock::get();
            foreach ($yesterDayStock as $old => $val) {
                $this->inserInHubLog($val);
            }
            $dateInsert = HubStockDateCheck::create([
                        'total' => count($yesterDayStock),
                        'hub_id' => $hubid,
            ]);
        }

        $data = Product::orderBy('created_at', 'DESC');


        $data->where(function($query) use ($request) {
            return $query->orWhereHas('category', function ($query1) use($request) {
                        $query1->where('status', 0);
                    });
        });
        $data->where(function($query) use ($request) {
            return $query->orWhereHas('subcategory', function ($query1) {
                        $query1->where('status', 0);
                    });
        });
        $data->where(function($query) use ($request) {
            return $query->orWhereHas('productName', function ($query1) {
                        $query1->where('status', 0);
                    });
        });

        $data->where(function($query) use ($request) {
            return $query->where('status', 0);
        });


        if ($request->hub_status == 1) {
            $data->where(function($query) use ($request) {
                $query->whereHas('stock',function($q1) use($request){
                     $q1->where('status', $request->status);
                });
            });
        }
        if ($request->hub_status == 2) {
            $data->where(function($query) use ($request) {
                $query->doesnthave('stock');
            });
        }

        if ($request->thresh == 1) {
            $data->where(function($query) use ($request) {
                $query->whereHas('stock',function($q1) use($request){
                     $q1->whereRaw('hub_stocks.threshold_quantity_one >= ((hub_stocks.product_quantity+ hub_stocks.in_stock)- hub_stocks.out_stock)');
                });
            });
        }
        if ($request->thresh == 2) {
            $data->where(function($query) use ($request) {
                $query->whereHas('stock',function($q1) use($request){
                    $q1->whereRaw('hub_stocks.threshold_quantity_two >= (( hub_stocks.product_quantity+ hub_stocks.in_stock)- hub_stocks.out_stock)');
                });
            });
        }


//        $data->where(function($query) use ($request) {
//            return $query->WhereHas('stock', function ($query1) use($request) {
//                        $query1->where('status', $request->status);
//                    });
//        });

        if ($request->cat != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('category', $request->cat);
            });
        }
        if ($request->subcat != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('sub_category', $request->subcat);
            });
        }

        if ($request->key != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('short_description', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('sku_number', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('brand_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('mrp', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('rlp', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('dlp', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('quantity', 'LIKE', '%' . $request->key . '%')
                                ->orWhereHas('productName', function ($query1) use ($request) {
                                    $query1->where('name', 'LIKE', '%' . $request->key . '%');
                                })
                                ->orWhereHas('packingType', function ($query2) use ($request) {
                                    $query2->where('name', 'LIKE', '%' . $request->key . '%');
                                })
                                ->orWhereHas('weightType', function ($query3) use ($request) {
                                    $query3->where('name', 'LIKE', '%' . $request->key . '%');
                                });
            });
        }
        $data->with('category', 'subcategory', 'stock', 'productName', 'packingType', 'weightType', 'unitType');

        $data = $data->paginate(15);

        if (count($data) > 0) {
            return response()->json(['code' => 200, 'list' => $data]);
        } else {
            return response()->json(array('code' => 500, 'msg' => 'no category found'));
        }
        exit;
    }

    public function ChangeStatus(Request $request) {
        $count = 0;

        foreach ($request->list as $key => $value) {
            $count++;
            $checkProduct = HubStock::where('product_id', $value)->where('distributor_id', Auth::guard('hub')->user()->id)->get();
            if (count($checkProduct) > 0) {
                HubStock::where('product_id', $value)->where('distributor_id', Auth::guard('hub')->user()->id)->Update([
                    'status' => $request->arg,
                ]);
            } else {
                HubStock::Create([
                    'product_id' => $value,
                    'status' => $request->arg,
                    'distributor_id' => Auth::guard('hub')->user()->id
                ]);
            }
        }

        if ($count == count($request->list)) {
            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function productView($productId) {

        $data = Product::with('category', 'subcategory', 'stock', 'productName', 'packingType', 'weightType', 'unitType');

        if(!$data){
            return redirect()->back();
        }
        $data->where('id', $productId);
        $data = $data->first()->toArray();

        if ($data) {
            return view('pages.hub.stock.view')->with(array('product' => $data));
        } else {
            return view('error.401.blade');
        }
    }

    public function updateProductStock(Request $request,ProductRepository $productRepository) {

        try{
        $checkProduct = HubStock::where('product_id', $request->productId)->where('distributor_id', Auth::guard('hub')->user()->id)->get();

        if (count($checkProduct) > 0) {
            if ($request->type == 1) {
                if ($checkProduct[0]->in_stock != null) {
                    $col = array(
                        'in_stock' => $checkProduct[0]->in_stock + $request->quantity,
                    );
                } else {
                    $col = array(
                        'in_stock' => $request->quantity,
                    );
                }
            }
            if ($request->type == 2) {
                $col = array(
                    'threshold_quantity_one' => $request->quantity,
                );
            }
            if ($request->type == 3) {
                $col = array(
                    'threshold_quantity_two' => $request->quantity,
                );
            }
            $update = HubStock::where('product_id', $request->productId)->where('distributor_id', Auth::guard('hub')->user()->id);
            $update->Update($col);
            /**
             * added by niraj lal rahi
             * for sent notification
             */
            $notifyMe = \App\NotifyMe::where('product_id',$request->productId)->where('status','PENDING')->get()->toArray();
            if(count($notifyMe)){
                $retailers = array_column($notifyMe,'user_id');
                $retailersRepository = new \App\Repository\RetailerRepository(new \App\Retailer);
                $currentHubRetailers = $retailersRepository->getRetailersFromHub($retailers,Auth::guard('hub')->user()->id);
                $product = $productRepository->getById($request->productId);

                $message = $product->name." product has been added to the stock. Please check and purchase";
                $notification = [];
                if($currentHubRetailers){
                    foreach($currentHubRetailers as $retailers){
                        $this->sendSms($message,$retailers->id);

                        $this->sendPushNotification(
                            $message,$message,$retailers->id,'stock-update',$request->productId);
                        $notification[] = $retailers->id;
                    }
                    if(count($notification)){
                        $update = \App\NotifyMe::whereIn('user_id',$notification)->where('status','PENDING')->update(['status' =>  'SENT']);
                    }
                }


            }
        } else {
            if ($request->type == 1) {
                if (count($checkProduct) > 0 && $checkProduct[0]->in_stock != null) {
                    $col = array(
                        'in_stock' => $checkProduct[0]->in_stock + $request->quantity,
                        'product_id' => $request->productId,
                        'distributor_id' => Auth::guard('hub')->user()->id,
                    );
                } else {
                    $col = array(
                        'in_stock' => $request->quantity,
                        'product_id' => $request->productId,
                        'distributor_id' => Auth::guard('hub')->user()->id,
                    );
                }
            }
            if ($request->type == 2) {
                $col = array(
                    'threshold_quantity_one' => $request->quantity,
                    'product_id' => $request->productId,
                    'distributor_id' => Auth::guard('hub')->user()->id,
                );
            }
            if ($request->type == 3) {
                $col = array(
                    'threshold_quantity_two' => $request->quantity,
                    'product_id' => $request->productId,
                    'distributor_id' => Auth::guard('hub')->user()->id,
                );
            }
            $update = HubStock::Create($col);
        }


        if ($update) {

            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
        }
        catch(\Exception $exception){
            return json_encode(array('code' => 500,'message' => $exception->getMessage(),'user' => Auth::guard('hub')->user()->id));
        }
    }

    public function profile() {

        return view('pages.hub.hub-profile');
    }

    public function profileImageUpload(Request $request) {
        $name = '';
        if ($request->get('avtar')) {
            $image = $request->get('avtar');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('avtar'))->save(public_path('images/distributor/') . $name);
        }

        $users = Distributor::where('id', Auth::guard('hub')->user()->id)
                ->update([
            'avtar' => $name
        ]);
        if ($users) {
            return json_encode(array('code' => 200, 'msg' => 'User profile updated successfully'));
        } else {
            return json_encode(array('code' => 500, 'msg' => 'User profile not updated successfully'));
        }
    }

    public function orderPieChart(Request $request,OrdersLogRepository $ordersLogRepository) {

        $allOrders = $ordersLogRepository->getAll();
        $category = array(
            ['Placed order', 0, false],
            ['Packed', 0, false],
            ['Shipped', 0, false],
            ['Delivered', 0, false]);
        foreach ($allOrders as $orders) {
            if ($orders->status == '1')
                $category[0][1] = $category[0][1] + 1;
            if ($orders->status == '2')
                $category[1][1] = $category[1][1] + 1;
            if ($orders->status == '3')
                $category[2][1] = $category[2][1] + 1;
            if ($orders->status == '4')
                $category[3][1] = $category[3][1] + 1;
        }
        return response()->json($category);
    }

    public function lastSixDaysOrderPie(Request $request,OrderRepository $orderRepository) {

        $currentDate = \Carbon\Carbon::now()->addDay(1)->format('Y-m-d');
        $lastSixthDate = \Carbon\Carbon::now()->subDay(7)->format('Y-m-d');

        $gaurd = Auth::getDefaultDriver();
        $userId = Auth::guard($gaurd)->user()->id;

        $lists = $orderRepository->lastSixDaysOrderPie($lastSixthDate, $currentDate,$userId);

        $orderCount = array();
        $totalPrice = array();
        $dates = array();
        foreach ($lists as $list) {
            $orderCount[] = (int) $list->ordercount;
            $totalPrice[] = (int) $list->total;
            $dates[] = $list->date;
        }

        $response[] = array('name' => 'Orders', 'data' => $orderCount);
        $response[] = array('name' => 'Total Price', 'data' => $totalPrice, 'yAxis' => 1);

        return response()->json(array('yAxis' => $response, 'xAxis' => $dates));
    }

}
