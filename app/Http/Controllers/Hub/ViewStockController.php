<?php

namespace App\Http\Controllers\Hub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class ViewStockController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
//        $this->middleware('auth:agent');
    }

    public function view(){

      return view('pages.hub.stock.view');
    }
}
