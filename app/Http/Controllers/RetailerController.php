<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Retailer;
use App\AttentionRequired;
use App\DiscoveredPlace;
use App\MapDistributor;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Api\Models\RetailersModel;
use App\RetailerProduct;
use App\Product;
use App\Helpers;
use Api\Models\QrCodeModel;
use Intervention\Image\Facades\Image as Image;
use App\Http\Requests\RetailerInsert;
use App\Http\Requests\RetailerUpdate;
use App\Exports\RetailerExport;
use App\Imports\RetailerImport;
use Maatwebsite\Excel\Facades\Excel;

class RetailerController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function importView() {
       return view('pages.retailer.import');
    }

    public function import(Request $request) {
        
           $request->validate([
            'file' => ['required'],
        ]);
        
        if ($this->checkExcelFile($request->file('file')->getClientOriginalExtension())) {
            try {
               Excel::import(new RetailerImport, request()->file('file'));
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                $failures = $e->failures();
                 return view('pages.retailer.import', compact('failures'));
            }
            
        }else {
            $error = \Illuminate\Validation\ValidationException::withMessages([
                        'file' => array('file type must be csv xls xlsx'),
            ]);
            throw $error;
        }
            
        
        
//        Excel::import(new RetailerImport, request()->file('file'));
//
//        return back();
    }
    public function checkExcelFile($file_ext) {
        $valid = array(
            'csv', 'xls', 'xlsx' // add your extensions here.
        );
        return in_array($file_ext, $valid) ? true : false;
    }
    public function addRetailer(RetailerInsert $request) {
//        echo "<pre>"; print_r($request->all());  exit;

        if (!Helpers\Helpers::barcodeScanner($request->ds_qrcode)) {
            return Response()->json([
                        'status' => 1001,
                        'message' => "Invalid barcode",
            ]);
        }
        $retailerimage = '';
        if ($request->get('primary_contact_image')) {
            $image1 = $request->get('primary_contact_image');
            $retailerimage = "primary" . time() . '.' . explode('/', explode(':', substr($image1, 0, strpos($image1, ';')))[1])[1];
            Image::make($request->get('primary_contact_image'))->save(public_path('uploads/retailers/primary/') . $retailerimage);
        }
        $retailersecondaryimage = '';
        if ($request->get('secondary_contact_image')) {
            $image2 = $request->get('secondary_contact_image');
            $retailersecondaryimage = "second" . time() . '.' . explode('/', explode(':', substr($image2, 0, strpos($image2, ';')))[1])[1];
            Image::make($request->get('secondary_contact_image'))->save(public_path('uploads/retailers/secondary/') . $retailersecondaryimage);
        }
        $shopimage = '';
        if ($request->get('shop_image')) {
            $image3 = $request->get('shop_image');
            $shopimage = "shop_image" . time() . '.' . explode('/', explode(':', substr($image3, 0, strpos($image3, ';')))[1])[1];
            Image::make($request->get('shop_image'))->save(public_path('uploads/retailers/shop_image/') . $shopimage);
        }
        $gstimage = '';
        if ($request->get('gst_image')) {
            $image4 = $request->get('gst_image');
            $gstimage = "gst_image" . time() . '.' . explode('/', explode(':', substr($image4, 0, strpos($image4, ';')))[1])[1];
            Image::make($request->get('gst_image'))->save(public_path('uploads/retailers/kyc/') . $gstimage);
        }
        $panimage = '';
        if ($request->get('pan_image')) {
            $image5 = $request->get('pan_image');
            $panimage = "pan_image" . time() . '.' . explode('/', explode(':', substr($image5, 0, strpos($image5, ';')))[1])[1];
            Image::make($request->get('pan_image'))->save(public_path('uploads/retailers/kyc/') . $panimage);
        }
        $adharimage = '';
        if ($request->get('adhar_image')) {
            $image6 = $request->get('adhar_image');
            $adharimage = "adhar_image" . time() . '.' . explode('/', explode(':', substr($image6, 0, strpos($image6, ';')))[1])[1];
            Image::make($request->get('adhar_image'))->save(public_path('uploads/retailers/kyc/') . $adharimage);
        }

        $cityState = Helpers\Helpers::getCityState($request->lattitude, $request->longitude);
        
//        echo "<pre>";print_r($cityState);exit;
        if ($request->same_as_primary) {
            $request->whatsapp_number = $request->primary_phone;
        }


        $retailer = Retailer::create([
                    'user_id' => Auth::user()->id,
                    'full_name' => $request->full_name,
                    'primary_mobile_number' => $request->primary_phone,
                    'alternative_mobile_number' => $request->alternate_phone,
                    'whats_app_number' => $request->whatsapp_number,
                    'capture_retailer_image' => $retailerimage,
                    'secondary_full_name' => $request->secondary_name,
                    'secondary_mobile_number' => $request->secondary_phone_number,
                    'secondary_capture_retailer_image' => $retailersecondaryimage,
                    'shop_image' => $shopimage,
                    'shop_type' => $request->shop_type,
                    'shop_name' => $request->shop_name,
                    'dob' => $request->dob,
                    'anniversay' => $request->anniversary,
                    'first_kid_birthday' => $request->first_kid_dob,
                    'second_kid_birthday' => $request->sec_kid_dob,
                    'establishment_name' => $request->establishment_name,
                    'gst_number' => $request->gst,
                    'pan_number' => $request->pan_number,
                    'adhar_number' => $request->adhar_number,
                    'gst_number_file' => $gstimage,
                    'pan_number_file' => $panimage,
                    'adhar_number_file' => $adharimage,
                    'p_device_type' => $request->device,
                    'scan_code' => $request->ds_qrcode,
                    'shop_address' => $request->shop_address,
                    'latitudes' => $request->lattitude,
                    'longitude' => $request->longitude,
                    'city' => isset($cityState['city']) ? $cityState['city'] : '',
                    'state' => isset($cityState['state']) ? $cityState['state'] : '',
                    'name_as_per_id' => $request->name_id,
                    'user_type' => 2,
                    'is_fill' => 1,
        ]);
//        print_r($retailer);
        if ($retailer) {
            $QrCodeModel = QrCodeModel::create([
                        'user_id' => $retailer->id,
                        'paytm' => $request->paytm_qrcode,
                        'mobiqwik' => $request->mobiqwik_qrcode,
                        'gpay' => $request->gpay_qrcode,
                        'phone_pe' => $request->phonepe_qrcode,
                        'upi' => $request->upi_qrcode,
                        'bharat_qr' => $request->bharat_qrcode,
                        'ds_group' => $request->ds_qrcode,
            ]);
//            print_r($QrCodeModel);
        }

        return response()->json(['status' => 'true']);
    }

    public function getRetailerCityList(Request $request) {
        $city = Retailer::groupBy('city')->select('city')->get();
        if (count($city) > 0) {
            return response()->json(['status' => 'true', 'list' => $city]);
        } else {
            return response()->json(['status' => 'false']);
        }
    }

    public function updateRetailer(RetailerUpdate $request) {


        if (!Helpers\Helpers::barcodeScanner($request->ds_qrcode)) {
            return Response()->json([
                        'status' => 1001,
                        'message' => "Invalid barcode",
            ]);
        }
        $retailerimage = '';
        if ($request->get('primary_contact_image')) {
            $image1 = $request->get('primary_contact_image');
            $retailerimage = "pri" . time() . '.' . explode('/', explode(':', substr($image1, 0, strpos($image1, ';')))[1])[1];
            Image::make($request->get('primary_contact_image'))->save(public_path('uploads/retailers/primary/') . $retailerimage);
        }
        $retailersecondaryimage = '';
        if ($request->get('secondary_contact_image')) {
            $image2 = $request->get('secondary_contact_image');
            $retailersecondaryimage = "second" . time() . '.' . explode('/', explode(':', substr($image2, 0, strpos($image2, ';')))[1])[1];
            Image::make($request->get('secondary_contact_image'))->save(public_path('uploads/retailers/secondary/') . $retailersecondaryimage);
        }
        $shopimage = '';
        if ($request->get('shop_image')) {
            $image3 = $request->get('shop_image');
            $shopimage = "shp" . time() . '.' . explode('/', explode(':', substr($image3, 0, strpos($image3, ';')))[1])[1];
            Image::make($request->get('shop_image'))->save(public_path('uploads/retailers/shop_image/') . $shopimage);
        }
        $gstimage = '';
        if ($request->get('gst_image')) {

            $image4 = $request->get('gst_image');
            $gstimage = "gst-" . time() . '.' . explode('/', explode(':', substr($image4, 0, strpos($image4, ';')))[1])[1];

            Image::make($request->get('gst_image'))->save(public_path('uploads/retailers/kyc/') . $gstimage);
        }
        $panimage = '';
        if ($request->get('pan_image')) {

            $image5 = $request->get('pan_image');
            $panimage = "pan-" . time() . '.' . explode('/', explode(':', substr($image5, 0, strpos($image5, ';')))[1])[1];

            Image::make($request->get('pan_image'))->save(public_path('uploads/retailers/kyc/') . $panimage);
        }
        $adharimage = '';
        if ($request->get('adhar_image')) {

            $image6 = $request->get('adhar_image');
            $adharimage = "adhar-" . time() . '.' . explode('/', explode(':', substr($image6, 0, strpos($image6, ';')))[1])[1];

            Image::make($request->get('adhar_image'))->save(public_path('uploads/retailers/kyc/') . $adharimage);
        }

        $cityState = Helpers\Helpers::getCityState($request->lattitude, $request->longitude);
        if ($request->same_as_primary) {
            $request->whatsapp_number = $request->primary_phone;
        }

        $get = Retailer::where('id', $request->edit_id)->first();
        if ($retailerimage == '') {
            $retailerimage = $get->capture_retailer_image;
        }
        if ($retailersecondaryimage == '') {
            $retailersecondaryimage = $get->secondary_capture_retailer_image;
        }
        if ($shopimage == '') {
            $shopimage = $get->shop_image;
        }


        if ($gstimage == '' && $get->gst_number_file != '') {
            $gstimage = $get->gst_number_file;
        }
        if ($panimage == '' && $get->pan_number_file != '') {
            $panimage = $get->pan_number_file;
        }
        if ($adharimage == '' && $get->adhar_number_file != '') {
            $adharimage = $get->adhar_number_file;
        }

        $retailer = Retailer::where('id', $request->edit_id)->update([
            'user_id' => Auth::user()->id,
            'full_name' => $request->full_name,
            'primary_mobile_number' => $request->primary_phone,
            'alternative_mobile_number' => $request->alternate_phone,
            'whats_app_number' => $request->whatsapp_number,
            'capture_retailer_image' => $retailerimage,
            'secondary_full_name' => $request->secondary_name,
            'secondary_mobile_number' => $request->secondary_phone_number,
            'secondary_capture_retailer_image' => $retailersecondaryimage,
            'shop_image' => $shopimage,
            'shop_type' => $request->shop_type,
            'shop_name' => $request->shop_name,
            'dob' => $request->dob,
            'anniversay' => $request->anniversary,
            'first_kid_birthday' => $request->first_kid_dob,
            'second_kid_birthday' => $request->sec_kid_dob,
            'establishment_name' => $request->establishment_name,
            'gst_number' => $request->gst,
            'pan_number' => $request->pan_number,
            'adhar_number' => $request->adhar_number,
            'gst_number_file' => $gstimage,
            'pan_number_file' => $panimage,
            'adhar_number_file' => $adharimage,
            'p_device_type' => $request->device,
            'scan_code' => $request->ds_qrcode,
            'shop_address' => $request->shop_address,
            'latitudes' => $request->lattitude,
            'longitude' => $request->longitude,
            'city' => isset($cityState['city']) ? $cityState['city'] : '',
            'state' => isset($cityState['state']) ? $cityState['state'] : '',
            'name_as_per_id' => $request->name_id,
            'user_type' => 2,
            'is_fill' => 1,
        ]);
//        print_r($retailer);
        if ($retailer) {
            $QrCodeModel = QrCodeModel::where('user_id', $request->edit_id)->update([
                'user_id' => $request->edit_id,
                'paytm' => $request->paytm_qrcode,
                'mobiqwik' => $request->mobiqwik_qrcode,
                'gpay' => $request->gpay_qrcode,
                'phone_pe' => $request->phonepe_qrcode,
                'upi' => $request->upi_qrcode,
                'bharat_qr' => $request->bharat_qrcode,
                'ds_group' => $request->ds_qrcode,
            ]);
//            print_r($QrCodeModel);
        }

        return response()->json(['status' => 'true']);
    }

    public function index() {
        $data = RetailersModel::get();

        $city = "<option value=''>Select City</option>";
        $state = "<option value=''>Select State</option>";
        $location = "<option value=''>Select location</option>";
        $data_city = RetailersModel::select('city')->groupBy('city')->get();


        $data_state = RetailersModel::select('state')->groupBy('state')->get();
        $data_shop_address = RetailersModel::select('shop_address')->groupBy('shop_address')->get();
        foreach ($data_city as $re_value) {
            if (isset($re_value->city) && $re_value->city != '') {
                $city .= "<option value='" . $re_value->city . "'>" . $re_value->city . "</option>";
            }
        }
        foreach ($data_state as $re_value) {
            if (isset($re_value->state) && $re_value->state != '') {
                $state .= "<option value='" . $re_value->state . "'>" . $re_value->state . "</option>";
            }
        }
        foreach ($data_shop_address as $re_value) {
            if (isset($re_value->shop_address) && $re_value->shop_address != '') {
                $location .= "<option value='" . $re_value->shop_address . "'>" . $re_value->shop_address . "</option>";
            }
        }
        return view('pages.retailer.index', ['data' => $data, 'city' => $city, 'state' => $state, 'location' => $location]);
    }

    public function getRetailer(Request $request) {
        $data = Retailer::with('agantdata', 'otherQrcode', 'MapDistributor', 'MapAgent')->orderBy('created_at', 'DESC')->where('is_fill', 1)->where('status', 0);
        if (isset($request->userid)) {
            $data->where('user_id', $request->userid);
        }

        $data = $data->paginate(15);
        echo json_encode($data);
        exit;
    }

    public function getSearchRetailer(Request $request) {

        $user_id = Auth::user()->id;
        $data = Retailer::with('agantdata', 'otherQrcode', 'MapDistributor', 'MapAgent')->orderBy('created_at', 'DESC')->where('is_fill', 1);
        if ($request->key != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('scan_code', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('full_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('primary_mobile_number', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('shop_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('city', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('state', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('created_at', 'LIKE', '%' . $request->key . '%');
            });
        }
        if (isset($request->userid)) {
            $data->where('user_id', $request->userid);
        }
        if (isset($request->city)) {
            $data->Where('city', 'LIKE', '%' . $request->city . '%');
        }
        if ($request->shop_type != '') {
            $data->Where('shop_type', $request->shop_type);
        }
        if ($request->hub_status == 1) {
            $data->has('MapDistributor');
            if ($request->hub != '') {
                $data->whereHas('MapDistributor', function($q) use($request) {
                    $q->where('distributor_id', $request->hub);
                });
            }
        }
        if ($request->hub_status == 2) {
            $data->doesnthave('MapDistributor');
        }
        if ($request->retailer_type != '') {
            $data->Where('is_prime', $request->retailer_type);
        }
        if ($request->status != '') {
            $data->Where('status', $request->status);
        }
        if ($request->source != '') {
            $data->Where('user_type', $request->source);
        }
        $data = $data->paginate(15);

        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'list' => $data));
        }
        exit;
    }

    public function getSearchRetailerAttentionRequired(Request $request) {
        if ($request->status != 1) {
            $request->status = 0;
        }


        $key = $request->key;
        $user_id = Auth::user()->id;
//        DB::enableQueryLog();
        $data = AttentionRequired::with('getRetailer', 'feedback')->orderBy('created_at', 'DESC');
        $data->where('status', $request->status);
        $data->has('getRetailer');
        if ($request->created_date != '' && isset($request->created_date)) {
            $from = date('Y-m-d', strtotime($request->created_date));
            $data->where(function($query) use ($request, $from) {
//                 $query->whereBetween('created_at', [$from, $to]);
                return $query->whereDate('created_at', $from);
            });
        }
        if ($request->key != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('reason', 'LIKE', '%' . $request->key . '%')
                                ->orWhereHas('getRetailer', function ($query1) use ($request) {
                                    $query1->where('scan_code', 'LIKE', '%' . $request->key . '%')
                                    ->orWhere('full_name', 'LIKE', '%' . $request->key . '%')
                                    ->orWhere('primary_mobile_number', 'LIKE', '%' . $request->key . '%')
                                    ->orWhere('shop_name', 'LIKE', '%' . $request->key . '%')
                                    ->orWhere('city', 'LIKE', '%' . $request->key . '%')
                                    ->orWhere('latitudes', 'LIKE', '%' . $request->key . '%')
                                    ->orWhere('longitude', 'LIKE', '%' . $request->key . '%');
                                })
                                ->orWhereHas('feedback', function ($query2) use ($request) {
                                    $query2->where('remark', 'LIKE', '%' . $request->key . '%');
                                });
            });
        }


        $result = $data->paginate(10);
//          dd(DB::getQueryLog());
        if (count($result) > 0) {
            echo json_encode(array('code' => 200, 'list' => $result));
        } else {
            echo json_encode(array('code' => 500, 'list' => $result));
        }
        exit;
    }

    public function getSearchAgentDiscoverPlaces(Request $request) {


        $user_id = Auth::user()->id;
        $data = DiscoveredPlace::where('status', $request->status)->orderBy('created_at', 'DESC');
        if ($request->key != '') {
            $data->Where('location', 'LIKE', '%' . $request->key . '%');
            $data->orWhere('city', 'LIKE', '%' . $request->key . '%');
            $data->orWhere('state', 'LIKE', '%' . $request->key . '%');
            $data->orWhere('remarks', 'LIKE', '%' . $request->key . '%');
            $data->orWhere('lattitude', 'LIKE', '%' . $request->key . '%');
            $data->orWhere('longitude', 'LIKE', '%' . $request->key . '%');
        }
        if ($request->created != '' && isset($request->created)) {
            $from = date('Y-m-d', strtotime($request->created));
            $data->where(function($query) use ($request, $from) {
//                 $query->whereBetween('created_at', [$from, $to]);
                return $query->whereDate('created_at', $from);
            });
        }

        $data = $data->paginate(10);

        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'list' => $data));
        }


        exit;
    }

    public function view($id) {
        $data = Retailer::where('id', $id)->first();

        if (!$data)
            return redirect()->back();

        if ($data->user_type == 2) {
            $data = Retailer::with('admin', 'otherQrcode', 'MapDistributor', 'MapAgent')->where('id', $id)->first();
        } else {
            $data = Retailer::with('agantdata', 'otherQrcode', 'MapDistributor', 'MapAgent')->where('id', $id)->first();
        }

//       echo "<pre>";print_r($data);exit;
        $mapObj = MapDistributor::with('distributorData')->where(['retailer_id' => $id])->get();



        //die();
        return view('pages.retailer.view')->with(array('data' => $data, 'mapObj' => $mapObj));
    }

    public function findRetailer(Request $request) {
        $data = Retailer::with('orderCalling')->where('id', $request->rid)->first();
        if ($data) {
            return response(json_encode(array('code' => 200, 'data' => $data)));
        } else {
            return response(json_encode(array('code' => 500)));
        }
    }

    public function attentionRequiredView() {
        return view('pages.retailer.attentionView');
    }

    public function discoveredPlacesView() {

        return view('pages.retailer.discoverView');
    }

    public function orderCallingView($id) {
        return view('pages.retailer.order_calling_setting')->with(array('id' => $id));
    }

    public function assignProductView($id) {
        return view('pages.retailer.assign_product')->with(array('id' => $id));
    }

    public function getAllAttentionList(Request $request) {
        if ($request->status != 1) {
            $request->status = 0;
        }

        $data = AttentionRequired::with('getRetailer', 'feedback')->orderBy('created_at', 'DESC')->where('status', $request->status)->has('getRetailer')->paginate(10);
        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'list' => $data));
        }
        exit;
    }

    public function getAllDiscovedPlacesList(Request $request) {
        $data = DiscoveredPlace::with('getAgent')->where('status', $request->status)->orderBy('created_at', 'DESC')->paginate(10);
        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'list' => $data));
        }

        exit;
    }

    public function removeFromAttentionList(Request $request) {
        $data = AttentionRequired::whereIn('id', $request->list)->update([
            'status' => 1,
            'resolution' => $request->resolution
        ]);
        if ($data) {
            $requestData = AttentionRequired::whereIn('id', $request->list)->get();
            foreach ($requestData as $attention) {
                $message = 'Your request has been resolved.';
                $this->sendSms($message, $attention->user_id, 'agent');
            }

            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500));
        }

        exit;
    }

    public function removeFromDiscoveplaces(Request $request) {
        $data = DiscoveredPlace::whereIn('id', $request->list)->update([
            'status' => 1,
            'resolution' => $request->resolution
        ]);
        if ($data) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500));
        }

        exit;
    }

    public function disableRetailer(Request $request) {
        $data = Retailer::whereIn('id', $request->list)->update([
            'status' => 1
        ]);
        if ($data) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500));
        }

        exit;
    }

    public function makeNonPrimeRetailer(Request $request) {
        $data = Retailer::whereIn('id', $request->list)->update([
            'is_prime' => $request->isPrime
        ]);
        if ($data) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500));
        }

        exit;
    }

    public function enableRetailer(Request $request) {
        $data = Retailer::whereIn('id', $request->list)->update([
            'status' => 0
        ]);
        if ($data) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500));
        }

        exit;
    }

    public function makePrimeRetailer(Request $request) {
        $data = Retailer::whereIn('id', $request->list)->update([
            'is_prime' => $request->isPrime
        ]);
        if ($data) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500));
        }

        exit;
    }

    public function barcodeScanner($barcode) {

        if (strlen($barcode) == 15) {
            $divideF = array(36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36);
            $placeValueFactor = array(1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2);
            $digit = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
            $weight = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35);
            $combine = array_combine($digit, $weight);


            $rid = str_split($barcode);
            $arrayPop = array_pop($rid);



            $ridPlaces = $this->convertInNumber($rid);

            $ridWeights = $this->getWeights($rid, $combine);

            //c*d
            $valueOfweightAndPlaceValue = $this->getPlaceValueMultiplyWithWeight($ridWeights, $placeValueFactor);

            //e/f
            $getQuotient = $this->getQuotient($divideF, $valueOfweightAndPlaceValue);

            $remider = $this->getReminders($valueOfweightAndPlaceValue, $getQuotient);

            $sumOfTotal = $this->sumOfTotal($getQuotient, $remider);

            $reminderFrom36 = ($sumOfTotal % 36);

            $remindeminusBy36 = (36 - $reminderFrom36);

            if ($remindeminusBy36 > 35) {
                return 0;
            }
            $isItO = $digit[$remindeminusBy36];
            if ($arrayPop == $isItO) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
        exit;
    }

    public function getReminders($valueOfweightAndPlaceValue, $getQuotient) {

        $remider = array();
        foreach ($valueOfweightAndPlaceValue as $k => $v) {
            $remider[] = $v - ($getQuotient[$k] * 36);
        }

        return $remider;
    }

    public function sumOfTotal($getQuotient, $remider) {

        $sumofTotal = array();
        foreach ($getQuotient as $k => $v) {
            $sumofTotal[] = $v + $remider[$k];
        }

        return array_sum($sumofTotal);
    }

    public function getQuotient($divideF, $valueOfweightAndPlaceValue) {
        $quotient = array();
        foreach ($valueOfweightAndPlaceValue as $key => $value) {
            $quotient[] = floor($value / $divideF[$key]);
        }
        return $quotient;
    }

    public function getPlaceValueMultiplyWithWeight($ridWeights, $placeValueFactor) {

        $values = array();
        foreach ($ridWeights as $k => $v) {
            $values[] = $v * $placeValueFactor[$k];
        }
        return $values;
    }

    public function convertInNumber($param) {


        $places = array();
        foreach ($param as $key => $value) {
            $places[] = $key + 1;
        }
        return $places;
    }

    public function getWeights($param, $combine) {

        $ridWeight = array();
        foreach ($param as $key => $val) {
            $ridWeight[] = $combine[$val];
        }
        return $ridWeight;
    }

    public function searchRetailersInMap(Request $request) {
        $cityHtml = "<option value=''>Select City</option>";
        $locationHtml = "<option value=''>Select location</option>";
        $city = $request->city ? $request->city : NULL;
        $state = $request->state ? $request->state : NULL;
        $location = $request->location ? $request->location : NULL;
        $data = new RetailersModel();

        if (isset($city) && trim($city)) {

            $data = $data->where(['city' => $city]);
        }
        if (isset($state) && trim($state)) {
            $data = $data->where(['state' => $state]);
        }
        if (isset($location) && trim($location)) {
            $data = $data->where(['shop_address' => $location]);
        }
        $data = $data->get();

        foreach ($data as $re_value) {
            if (isset($re_value->city) && $re_value->city != '') {
                $cityHtml .= "<option value='" . $re_value->city . "'>" . $re_value->city . "</option>";
            }
        }
        foreach ($data as $re_value) {
            if (isset($re_value->shop_address) && $re_value->shop_address != '') {
                $locationHtml .= "<option value='" . $re_value->shop_address . "'>" . $re_value->shop_address . "</option>";
            }
        }
        return json_encode(array('code' => 200, 'list' => $data->toArray(), 'cityHtml' => $cityHtml, 'locationHtml' => $locationHtml));
    }

    public function updateRetailerAssignedProducts(Request $request) {

        foreach ($request->list as $key => $val) {
            RetailerProduct::updateOrCreate([
                'retailer_id' => $request->rid,
                'product_id' => $val
            ]);
        }

        RetailerProduct::where('retailer_id', $request->rid)->whereNotIn('product_id', $request->list)->delete();

        return json_encode(array('code' => 200, 'msg' => 'Retailer product updated successfully'));
    }

    public function getAssingedProductList(Request $request) {
        $key = $request->searchKey;
        $cat = $request->cat;
        $products = Product::with('productName')->orderBy('id', 'DESC');

        $products->where(function ($query) use($cat) {
            $query->where('status', 0);
            if ($cat != '') {
                $query->where('category', $cat);
            }
        });
        if ($request->searchKey != '') {
            $products->orWhere(function($query) use($key) {
                $query->where('name', 'LIKE', '%' . $key . '%');
                $query->orWhere('sku_number', 'LIKE', '%' . $key . '%');
                $query->orWhere('mrp', 'LIKE', '%' . $key . '%');
                $query->orWhere('rlp', 'LIKE', '%' . $key . '%');
                $query->orWhere('dlp', 'LIKE', '%' . $key . '%');
            });
        }



        $products = $products->paginate(20);

        if (count($products) > 0) {
            foreach ($products as $key => $value) {
                $products[$key]['isAssigned'] = $this->checkIsAssignedProduct($value['id'], $request->rid);
            }
        } else {
            return json_encode(array('code' => 'Products Not Available'));
        }

        return json_encode(array('code' => 200, 'list' => $products));
    }

    public function checkIsAssignedProduct($productId, $rid) {

        $RetailerProduct = RetailerProduct::where('retailer_id', $rid)->where('product_id', $productId)->get();
        if (count($RetailerProduct) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function export(Request $request) {


        $user_id = Auth::user()->id;
        $data = Retailer::orderBy('created_at', 'DESC')->where('is_fill', 1);
        if ($request->key != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('scan_code', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('full_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('primary_mobile_number', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('shop_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('city', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('state', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('created_at', 'LIKE', '%' . $request->key . '%');
            });
        }
        if ($request->type1 != '') {
            $data->Where('shop_type', $request->type1);
        }
        if ($request->type2 != '') {
            $data->Where('is_prime', $request->type2);
        }
        if ($request->status != '') {
            $data->Where('status', $request->status);
        }
        if (isset($request->userid)) {
            $data->Where('user_id', $request->userid);
        }
        $data = $data->get();
        return Excel::download(new RetailerExport($data), 'retailer.xlsx');
    }

    public function exportOnboardByagent(Request $request) {
//        print_r($request->all());exit;

        $user_id = Auth::user()->id;
        $data = Retailer::orderBy('created_at', 'DESC')->where('is_fill', 1)->where('user_id', $request->user_id);

        $data = $data->get();
        return Excel::download(new RetailerExport($data), 'retailer.xlsx');
    }

}
