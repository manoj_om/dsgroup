<?php

namespace App\Http\Controllers\Api\Retailer;

use App\Http\Controllers\Controller;

use App\Traits\WishListTrait;
use Illuminate\Http\Request;
use App\Traits\CartTrait;
use App\Traits\OrdersTrait;
use App\Traits\ProfileTrait;
use App\Traits\CurlRequestTrait;
use App\Traits\PromocodeTrait;
use App\Traits\ProductReviewTrait;

class MyAccountController extends Controller
{
    //
    use CartTrait,OrdersTrait,ProfileTrait,CurlRequestTrait,WishListTrait,
    PromocodeTrait,ProductReviewTrait;

    private $endpoint,$sessionToken;
    private $storeId = 'DU001';
    private $gaurd = '';

    private $promocodeKey =['orderVolume','orderItems','skuLine','productLaunch'];

    public function __construct()
    {
        $this->endpoint = "http://delivery.now.bike/api/v2/";
        $this->sessionToken = "W269ndsormentvj2wf";
    }


    public function listPromocode(Request $request){
        try{

            $promocodes = $this->getPromocodes();
            return $this->apiResponse([
                'status'=> true,
                'promocodes' => $promocodes,
            ],__('api/responseCode.success'));
        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Internal server error please try again.',
                'error' => 'Error : '. $exception->getMessage(),
            ],__('api/responseCode.server_error'));
        }
    }
    public function logout(Request $request)
    {
        try {
            $user = \Auth::user()->token();
            $user->revoke();

            return $this->apiResponse([
                'status' => true,
                'message' => 'User logged out successfully'
            ], __('api/responseCode.success'));
        } catch (\Exception $exception) {
            return $this->apiResponse([
                'status' => false,
                'message' => 'Failed to update profile',
                'error' => 'Error : ' . $exception->getMessage(),
            ], __('api/responseCode.server_error'));
        }
    }





}
