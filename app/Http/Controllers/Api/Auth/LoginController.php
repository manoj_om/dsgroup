<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\UserRepository;
use App\Repository\RetailerRepository;
use App\Helpers;

class LoginController extends Controller
{
    //
    /* added by niraj lal rahi
     * login is used to user authenticate with certain parameters
     * @params
     * Request
     * UserRepository
     * */
    public function login(Request $request,RetailerRepository $retailerRepository){
        try{

            $requestData = $request->post();
            $validator = \validator($requestData,[
                //'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|digits:10'
                'mobile_number' => 'required|digits:10'
            ]);

            //validate request
            if($validator->fails()){
                return $this->apiResponse([
                    'status'=>false,
                    'message' => $validator->errors()
                ],__('api/responseCode.bad_request'));
            }
            //generate otp and save to database
            $send_otp = $retailerRepository->sendOtpOnMobile($requestData['mobile_number']);

            if($send_otp){

                //third party integration to send otp on mobile number
                $message_body= urlencode("Your verification code is : ".$send_otp->otp);
                $message="&send_to=".$requestData['mobile_number']."&msg=$message_body";

                Helpers\Helpers::sendSMS(config('api.smsurl').$message);

                return $this->apiResponse([
                    'status'=> true,
                    'message' => 'Otp sent to the registered mobile number.'
                ]);
            }
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Invalid credentials.'
            ],__('api/responseCode.failed'));

        }catch(\Exception $exception){

            return $this->apiResponse([
                'status'=> false,
                'message' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }

    }

    /*
     * added by niraj lal rahi
     * verifyOtp is used to authenticate user with mobile number with certain params
     * @param
     * Request
     * UserRepository
     * */

    public function verifyOtp(Request $request,RetailerRepository $retailerRepository){
        try{

            $requestData = $request->post();
            $validator = \Validator($requestData,[
                'mobile_number' => 'required|digits:10',
                'otp' => 'required|digits:6'
            ]);

            if($validator->fails()){
                return $this->apiResponse([
                    'status'=>false,
                    'message' => $validator->errors()
                ],__('api/responseCode.bad_request'));
            }

            $credentials = [
                'primary_mobile_number' => $requestData['mobile_number'],
                'otp' => $requestData['otp']
            ];
            $verify_user = $retailerRepository->verifyUserOtp($credentials);

            if($verify_user){

                \Auth::login($verify_user); //create session

                $user = $request->user();

                $tokenResult = $user->createToken('O2R Personal Token'); //create tokens
                $token = $tokenResult->accessToken;

                if($request->post('device_token')){
                    /**
                     * check if user have already token
                     */
                    $isAlredyExist = \App\DeviceFcmToken::where(['user_id' => request()->user()->id])->first();

                    if($isAlredyExist)
                        $isAlredyExist->delete();
                    \App\DeviceFcmToken::create([
                        'user_id' => request()->user()->id,
                        'token' => $request->post('device_token')
                    ]);
                }
                //empty otp and attempt fields
                $retailerRepository ->emptyOtpAndAttemptCount($user->id);

                return $this->apiResponse([
                    'status'=> true,
                    'message' => 'success',
                    'data' => $user,
                    'token' => 'Bearer '.$token
                ],__('api/responseCode.success'));

            }

            return $this->apiResponse([
                'status'=> false,
                'message' => 'Credentials is not valid'
            ],__('api/responseCode.server_error'));



        }catch (\Exception $exception){

            return $this->apiResponse([
                'status'=> false,
                'message' => 'Sorry verification failed',
                'error' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }
    }



}
