<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repository\UnitWeightRepository;
use Illuminate\Http\Request;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\BannerRepository;
use App\Repository\SchemeAndPromotionRepository;
use App\Repository\ProductListRepository;
class IndexController extends Controller
{

    /*
     * added by niraj lal rahi
     *
     * index function
     * @param
     * Request
     * CategoryRepository
     * */
    public  function index(Request $request,
                           CategoryRepository $categoryRepository,
                           BannerRepository $bannerRepository,
                           ProductRepository $productRepository){

        try{
            // category list
            $response['categories'] = $categoryRepository->getCategoryWithSubCategory();
            //banner list code goes here
            $response['banners'] = $bannerRepository->getActiveBanner();

            $userId = request()->user()->id;
            $mapDistributor = new \App\MapDistributor;

            $userDetail = $mapDistributor->where('retailer_id',$userId)->first();
            if(empty($userDetail)){
                $response['featured_product'] = [];

                return $this->apiResponse([
                    'status' => true,
                    'message' => 'Data loaded successfully',
                    'data' => $response
                ]);
            }

            $distributorId = $userDetail->distributor_id;

            $isActiveDistributor = \App\Distributor::where('id',$distributorId)->where('status',1)->first();
            if(empty($isActiveDistributor)){
                $response['featured_product'] = [];

                return $this->apiResponse([
                    'status' => true,
                    'message' => 'Data loaded successfully',
                    'data' => $response
                ]);
            }


            $response['featured_product'] = $productRepository->list(true,$distributorId);


            return $this->apiResponse([
                'status' => true,
                'message' => 'Data loaded successfully',
                'data' => $response
            ]);

        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }

    }

    /*
     * added by niraj lal rahi
     * search function is used for search product by name
     * @param
     * Request
     * ProductRepository
     * */

    public function search(Request $request,ProductRepository $productRepository,
    ProductListRepository $productListRepository){
        try{
            $requestData = $request->post();
            $validator = \Validator($requestData,[
                'keyword' => 'required'
            ]);

            if($validator->fails()){

                return $this->apiResponse([
                    'status'=>false,
                    'message' => $validator->errors()
                ],__('api/responseCode.bad_request'));
            }

            $userId = request()->user()->id;
            $mapDistributor = new \App\MapDistributor;

            $userDetail = $mapDistributor->where('retailer_id',$userId)->first();
            $distributorId = $userDetail->distributor_id;

            $searchProduct = $productRepository->search($distributorId,$requestData['keyword']);

            return $this->apiResponse([
                'status' => true,
                'message' => count($searchProduct).' Records found',
                'data' => $searchProduct
            ]);

        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }
    }

    /*
     * added by niraj lal rahi
     * getProductByCategory function is used for search product by category id
     * @param
     * Request
     * ProductRepository
     * */

    public function getProductByCategory(Request $request,
    ProductRepository $productRepository,
    ProductListRepository $productListRepository){

        try{
            $requestData = $request->post();
            $validator = \Validator($requestData,[
                'category' => 'required|numeric',
                'sub_category' => 'numeric|nullable'
            ],[
                'category.numeric' => 'Invalid category type',
                'sub_category.numeric' => 'Invalid sub category type'
            ]);

            if($validator->fails()){

                return $this->apiResponse([
                    'status'=>false,
                    'message' => $validator->errors()
                ],__('api/responseCode.bad_request'));
            }
            $requestData['subCategory'] = (isset($requestData['sub_category']) and !empty($requestData['sub_category'])) ? $requestData['sub_category'] : false;

            $userId = request()->user()->id;
            $mapDistributor = new \App\MapDistributor;

            $userDetail = $mapDistributor->where('retailer_id',$userId)->first();
            $requestData['distributor_id'] = $userDetail->distributor_id;

            $getProduct = $productRepository->getProductFromCategory($requestData);




            return $this->apiResponse([
                'status' => true,
                'message' => count($getProduct).' Records found',
                'data' => $getProduct
            ]);


        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }
    }

    public function schemeAndPromotionsList(Request $request,SchemeAndPromotionRepository $schemeAndPromotionRepository){
        try{
            $list = $schemeAndPromotionRepository->getAllPromotionList();
            return $this->apiResponse([
                'status' => true,
                'message' => count($list).' Records found',
                'data' => $list
            ]);

        }catch (\Exception $exception){

            return $this->apiResponse([
                'status'=> false,
                'message' => 'Sorry something went wrong',
                'error' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }
    }

    public function getProductDetails($id,ProductRepository $productRepository,
                                      UnitWeightRepository $unitWeightRepository){
        try{
            $userId = request()->user()->id;
            $mapDistributor = new \App\MapDistributor;

            $distributorDetail = $mapDistributor->where('retailer_id',$userId)->first();
            $detail = $productRepository->getById($id)->load('inWishlist');

            $where = array(
                'products.product_lists_id'=>$detail->product_lists_id,
                'products.packaging' => $detail->packaging
            );

            $associateProducts = $productRepository->getAllCondition($where,$distributorDetail->distributor_id);
            $detail->associate_product = $associateProducts;
            $attributes = array();
            //$attributes['packingType'] = $packingTypeRepository->getWhere(array('status' => 1));
            //$attributes['unit_types'] = $unitTypeRepository->getAll();
            $attributes['unit_weights'] = $unitWeightRepository->getAll();

            if(!empty($detail))
                return $this->apiResponse([
                    'status' => true,
                    'message' => 'Records found',
                    'data' => $detail,
                    'attributes' => $attributes,
                    //'associate_products' => $associateProducts
                ]);
            else
                return $this->apiResponse([
                    'status' => false,
                    'message' => 'Product does not exist',
                ]);
        }catch (\Exception $exception){

            return $this->apiResponse([
                'status'=> false,
                'message' => 'Product not found',
                'error' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }
    }

}
