<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AgentsOnboardings;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use App\Helpers;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $user_id = Auth::user()->id;
        $city = "<option value=''>Select City</option>";
        $state = "<option value=''>Select State</option>";
        $location = "<option value=''>Select location</option>";
        $data_city = AgentsOnboardings::select('city')->groupBy('city')->get();
        $data_state = AgentsOnboardings::select('state')->groupBy('state')->get();
        $data_shop_address = AgentsOnboardings::select('address')->groupBy('address')->get();

        foreach ($data_city as $re_value) {
            if (isset($re_value->city) && $re_value->city != '') {
                $city .= "<option value='" . $re_value->city . "'>" . $re_value->city . "</option>";
            }
        }
        foreach ($data_state as $re_value) {
            if (isset($re_value->state) && $re_value->state != '') {
                $state .= "<option value='" . $re_value->state . "'>" . $re_value->state . "</option>";
            }
        }
        foreach ($data_shop_address as $re_value) {
            if (isset($re_value->address) && $re_value->address != '') {
                $location .= "<option value='" . $re_value->address . "'>" . $re_value->address . "</option>";
            }
        }


        $data = AgentsOnboardings::orderBy('is_approve', 'ASC')->orderBy('created_at', 'DESC')->get();

//        print_r($data);exit;
        return view('home', ['data' => $data, 'city' => $city, 'state' => $state, 'location' => $location]);
    }

    public function location($id, Request $request) {
        # code...
        $date = $request->date;
        $query = \Api\Models\AgentsStartMyDayModel::where('user_id', $id);
        if ($date)
            $query->whereDate('created_at', $date);

        $getLocations = $query->get();
        return response()->json(['status' => true, 'list' => $getLocations], 200);
    }

    public function getAllOnBoardingAgent(Request $request) {
        $user_id = Auth::user()->id;
        $data = AgentsOnboardings::orderBy('is_approve', 'ASC')->orderBy('created_at', 'DESC')->where('activate', 0)->where('is_approve', 1)->paginate(15);
        $city = AgentsOnboardings::select('city')->groupBy('city')->get();
//        echo "<pre>";
//        print_r($data);
//        exit;

        if (count($data) > 0) {

            echo json_encode(array('code' => 200, 'list' => $data, 'city' => $city));
        } else {
            echo json_encode(array('code' => 500));
        }
        exit;
    }

    public function getAllOnBoardingAgentsMap(Request $request) {
        $user_id = Auth::user()->id;
        $data = AgentsOnboardings::orderBy('is_approve', 'ASC')->orderBy('created_at', 'DESC')->get();


        if (count($data) > 0) {

            return response()->json(['code' => 200, 'list' => $data], 200);
        } else {
            return response()->json(['code' => 500,], 500);
        }
    }

    public function approvedAgentView($id) {

        $data = AgentsOnboardings::with('retailadd')->where('id', $id)->first();
        if (!$data) {
            return redirect()->back();
        }
        return view('agent_approved_page')->with(array('data' => $data));
    }

    public function approveAgents($aid) {
        $app = AgentsOnboardings::where('id', $aid)->update([
            'is_approve' => 1
        ]);
        $result = AgentsOnboardings::where('id', $aid)->first();

        $message_body = urlencode("Your request Id: 00" . $aid . " is approved for O2R APP");
        $message = "&send_to=" . $result->mobile_number . "&msg=$message_body";
        Helpers\Helpers::sendSMS(config('api.smsurl') . $message);
        if ($app) {
            echo json_encode(array('code' => 200, 'msg' => 'agent approved'));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'agent not approved'));
        }
        exit;
    }

    public function approveAllAgents(Request $request) {
        foreach ($request->agentlist as $key => $val) {
            $app = AgentsOnboardings::where('id', $val)->update([
                'is_approve' => 1
            ]);
            $result = AgentsOnboardings::where('id', $val)->first();

            $message_body = urlencode("Your request Id: 00" . $val . " is approved for O2R APP");
            $message = "&send_to=" . $result->mobile_number . "&msg=$message_body";
            Helpers\Helpers::sendSMS(config('api.smsurl') . $message);
        }
        return response()->json(['code' => 200]);
    }

    public function rejectAgents($aid) {
        $app = AgentsOnboardings::where('id', $aid)->update([
            'is_approve' => 2
        ]);
        if ($app) {
            echo json_encode(array('code' => 200, 'msg' => 'agent approved'));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'agent not approved'));
        }
        exit;
    }

    public function rejectAllAgents(Request $request) {
        foreach ($request->agentlist as $key => $val) {
            $app = AgentsOnboardings::where('id', $val)->update([
                'is_approve' => 2
            ]);
        }
        return response()->json(['code' => 200]);
    }

    public function deactivateAgent(Request $request) {
        $app = AgentsOnboardings::whereIn('id', $request->list)->update([
            'activate' => 1
        ]);
        if ($app) {
            echo json_encode(array('code' => 200, 'msg' => 'agent deactivated successfully'));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'agent not deactivated successfully'));
        }
        exit;
    }

    public function activateAgents(Request $request) {
        $app = AgentsOnboardings::whereIn('id', $request->list)->update([
            'activate' => 0
        ]);
        if ($app) {
            echo json_encode(array('code' => 200, 'msg' => 'agent Activated successfully'));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'agent not Activated successfully'));
        }
        exit;
    }

    public function searchInAgent(Request $request) {

        $key = $request->searchkey;
        $user_id = Auth::user()->id;
        $data = AgentsOnboardings::orderBy('is_approve', 'ASC')->orderBy('created_at', 'DESC');

        if ($request->city != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('city', $request->city);
            });
        }

        if ($key != '') {
            $data->where(function ($query) use($key) {
                $query->Where('name', 'LIKE', '%' . $key . '%');
                $query->orWhere('email', 'LIKE', '%' . $key . '%');
                $query->orWhere('mobile_number', 'LIKE', '%' . $key . '%');
                $query->orWhere('adhar_number', 'LIKE', '%' . $key . '%');
                $query->orWhere('agency_name', 'LIKE', '%' . $key . '%');
            });
        }
        if (trim($request->is_approve) == "all") {
            $data = $data->whereIn('is_approve', [0, 1, 2]);
        } elseif ($request->is_approve == 3) {
            $data->where('activate', 1);
        } elseif ($request->is_approve == 1) {
            $data->where('activate', 0)->where('is_approve', 1);
        } else {
            $data = $data->where('is_approve', '=', $request->is_approve);
        }
        $data = $data->paginate(15);


        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500));
        }
        exit;
    }

    public function searchtAllOnBoardingAgent(Request $request) {

//        print_r($request->all());exit;
        $columns = Schema::getColumnListing('agents_onboardings');
        $key = $request->searchkey;
        $user_id = Auth::user()->id;
        $data = AgentsOnboardings::orderBy('is_approve', 'ASC')->orderBy('created_at', 'DESC');

        if (trim($request->is_approve) == "all") {

            $data = $data->whereIn('is_approve', [0, 1, 2]);
        } else {

            $data = $data->where('is_approve', '=', $request->is_approve);
        }
        if ($key != '') {
            $data->where(function ($query) use($key, $columns) {

                foreach ($columns as $value) {
                    if ($value != 'is_approve')
                        $query = $query->orWhere($value, 'LIKE', '%' . $key . '%');
                }
            });
        }
        $data = $data->paginate(8);


        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500));
        }
        exit;
    }

    public function agentMap() {
        $user_id = Auth::user()->id;
        $data = RetailersModel::get();
        // echo "hi...";die();
//     print_r($data);exit;
        return view('map')->with(array('data' => $data));
    }

    public function export(Request $request) {

//         echo 'in';exit;
        # code...
//        $orderLists = $orderRepository->getAll();
        $key = $request->searchkey;
        $user_id = Auth::user()->id;
        $data = AgentsOnboardings::orderBy('is_approve', 'ASC')->orderBy('created_at', 'DESC');

        if ($request->city != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('city', $request->city);
            });
        }

        if ($key != '') {
            $data->where(function ($query) use($key) {
                $query->Where('name', 'LIKE', '%' . $key . '%');
                $query->orWhere('email', 'LIKE', '%' . $key . '%');
                $query->orWhere('mobile_number', 'LIKE', '%' . $key . '%');
                $query->orWhere('adhar_number', 'LIKE', '%' . $key . '%');
                $query->orWhere('agency_name', 'LIKE', '%' . $key . '%');
            });
        }
        if (trim($request->is_approve) == "all") {
            $data = $data->whereIn('is_approve', [0, 1, 2]);
        } elseif ($request->is_approve == 3) {
            $data->where('activate', 1);
        } elseif ($request->is_approve == 1) {
            $data->where('activate', 0)->where('is_approve', 1);
        } else {
            $data = $data->where('is_approve', '=', $request->is_approve);
        }
        $data = $data->get();
//       echo "<pre>";
//       print_r($data);
//       exit;
        $reportHeading = array(
            'Name', 'Email', 'Mobile Number', 'Employee code', 'Adhaar Number', 'Agency Name', 'State', 'City', 'Address', 'Status', 'Approved/Rejected', 'Created_at'
        );
        $fileName = time() . "-onboardingagent.csv";
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=' . $fileName . '');
        $file = fopen('php://output', 'w');
        fputcsv($file, $reportHeading);
        $reportValue = array();
        foreach ($data as $key => $value) {
            if ($value->is_approve == 0)
                $type = 'Pending';
            if ($value->is_approve == 1)
                $type = 'Approved';
            if ($value->is_approve == 2)
                $type = 'Rejected';
            $reportValue = array(
                'Name' => $value->name,
                'Email' => $value->email,
                'Mobile Number' => $value->mobile_number,
                'Employee code' => $value->employee_code,
                'Adhaar Number' => $value->adhar_number,
                'Agency Name' => $value->agency_name,
                'State' => $value->state,
                'City' => $value->city,
                'Address' => $value->address,
                'Status' => $value->activate == 0 ? 'Active' : 'InActive',
                'Approved/Rejected' => $type,
                'Created_at' => \Carbon\Carbon::parse($value->created_at)->format('d-m-Y'),
            );
            fputcsv($file, $reportValue);
        }

        fclose($file);
        exit;
    }

}
