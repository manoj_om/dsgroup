<?php

namespace App\Http\Controllers;

use App\Repository\OrderRepository;
use App\Repository\RetailerRepository;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct() {

        $this->middleware('auth');
//        $this->middleware('auth:agent');
    }

    public function index(RetailerRepository $retailerRepository,
    OrderRepository $orderRepository){
        
        $activeCallingAgent = \App\CallingAgent::active()->count();
        $totalDistributor = \App\Distributor::active()->count();
        $totalRetailers = $retailerRepository->activeRetailers()->count();
        $totalOnboard = \App\AgentsOnboardings::active()->count();
        $totalSales = $orderRepository->getTotalSales()->sum('total_amount');

        /**
         *  next 6 days prediction except today
         */
        $retailerOrderCallingRepository = new \App\Repository\RetailerOrderCallingRepository;
        $listsOfQueues = $retailerOrderCallingRepository->getAll();

        $nextDays = array();
        for($i=1;$i<=7;$i++){

            $day = \Carbon\Carbon::now()->addDay($i)->format('D');
            if($day !== 'Sun')
                $nextDays[$day] = 0;
        }

        $dbDaysNumber = array(
             '1' =>  'Mon' ,
             '2' =>  'Tue' ,
             '3' =>  'Wed' ,
             '4' =>  'Thu' ,
             '5' =>  'Fri' ,
             '6' =>  'Sat' ,

        );

        $completeDays = array(
            'Mon'=>   'Monday' ,
            'Tue'=>  'Tuesday' ,
            'Wed'=> 'Wednesday' ,
            'Thu'=> 'Thursday' ,
            'Fri'=>   'Friday' ,
            'Sat'=> 'Saturday' ,

       );


        foreach($listsOfQueues as $key=> $listsOfQueue){
            $callingDays = explode(',',$listsOfQueue->calling_days);
            foreach($callingDays as $callingDay){
                $nextDays[$dbDaysNumber[$callingDay]] =$nextDays[$dbDaysNumber[$callingDay]]+1;
            }


        }

        return view('pages.dashboard.index',compact('totalDistributor','totalRetailers',
        'totalOnboard','totalSales','nextDays','completeDays','activeCallingAgent'));
    }

    public function graph(Request $request,OrderRepository $orderRepository){
        $currentDate = \Carbon\Carbon::now()->addDay(1)->format('Y-m-d');
        $lastSixthDate = \Carbon\Carbon::now()->subDay(7)->format('Y-m-d');

        $lists = $orderRepository->lastSixDaysOrderPie($lastSixthDate, $currentDate);
        $callCenterOrder = $orderRepository->getOrdersPlaceByCall($lastSixthDate, $currentDate);
        $callCenterOrderDelivered = $orderRepository->getOrdersPlaceByCall($lastSixthDate, $currentDate,4);
        /**
         * shop type graph
         *
         */
        $shopTypes = $orderRepository->getGraphByShopType();
        $shopType = array(
            '1'  => 'General A',
            '2' => 'General B' ,
            '3' => 'General C',
            '4' => 'Chemist A',
            '5' => 'Chemist B',
            '6' => 'Chemist C',
            '7' => 'Cosmetic',
            '8' => 'Paan A' ,
            '9' => 'Paan B' ,
            '10' => 'Paan C'

        );
        $match = array();
        foreach($shopTypes as $data ){
            $shopTypeGraph[] = array($shopType[$data->shopType],(int)$data->total,false);
            unset($shopType[$data->shopType]);
        }
        foreach($shopType as $shop){
            $shopTypeGraph[] = array($shop,0,false);
        }
        $deliveredArray = array();


        $orderCount = array();
        $totalPrice = array();
        $dates = array();
        foreach ($lists as $list) {
            $orderCount[] = (int) $list->ordercount;
            $totalPrice[] = (int) $list->total;
            $dates[] = $list->date;
        }
        $pending = $pendingTotal = $pendingDates = [];

        foreach ($callCenterOrder as $list) {
            $pending[] = (int) $list->ordercount;
            $pendingTotal[] = (int) $list->total;
            $pendingDates[] = $list->date;
        }
        $delivered = $deliveredTotal = $deliveredDates = [];
        foreach($callCenterOrderDelivered as $delivered){
            $delivered[] = (int) $list->ordercount;
            $deliveredTotal[] = (int) $list->total;
            $deliveredDates[] = $list->date;
        }

        $orderRatio[] = array(
            'name' =>  'Call to Order Ratio',
            'data' => $totalPrice
        );


        $response['delivery'] =  [
            array('name' => 'Orders', 'data' => $delivered),
            array('name' => 'Delivery Ticket Size', 'data' => $deliveredTotal)
        ];
        $response['all'] = [
            array('name' => 'Call', 'data' => $pending),
            array('name' => 'Orders', 'data' => $pendingTotal)
        ];
        // [{
        //     name: 'Call',
        //     data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0]

        // }, {
        //     name: 'Order',
        //     data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5]

        // }]
        return response()->json(
            array(
                'xAxis' => $dates,
                'orderRatio' => $orderRatio,
                'shopTypeGraph' => $shopTypeGraph,
                'call' => $response,
                'pending' => $pendingDates,
                'delivered' => $deliveredDates
            )
        );
    }
}
