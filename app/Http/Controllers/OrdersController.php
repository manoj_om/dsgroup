<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Orders;
use App\RetailerOrderCalling;
use App\Traits\PromocodeTrait;

class OrdersController extends Controller {

    use PromocodeTrait;

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('pages.orders.index');
    }

//    public function getOrders(Request $request) {
//        $key = $request->key;
//        $retailerId = $request->retailerId;
//
////            DB::enableQueryLog();
//        $orders = Orders::with('orderItems', 'retailer');
//        if ($request->searchDate != '') {
//            $orders->whereDate('created_at', date('Y-m-d', strtotime($request->searchDate)));
//        }
//        if (isset($request->retailerId) && $request->retailerId != '') {
//            $orders->orwhereHas('retailer', function($query) use($retailerId) {
//                $query->Where('id', $retailerId);
//            });
//        }
//        if ($request->key != '') {
//            $orders->where(function($query) use ($request) {
//                return $query->orWhere('order_number', 'LIKE', '%' . $request->key . '%')
//                                ->orWhere('total_amount', 'LIKE', '%' . $request->key . '%')
//                                ->orWhereHas('retailer', function($q) use($request) {
//                                    $q->Where('full_name', 'LIKE', '%' . $request->key . '%');
//                                    $q->orWhere('primary_mobile_number', 'LIKE', '%' . $request->key . '%');
//                                    $q->orWhere('city', 'LIKE', '%' . $request->key . '%');
//                                });
//            });
//        }
//
//
//
//
//
//        $data = $orders->orderBy('id', 'DESC')->paginate(15);
////        dd(DB::getQueryLog());
//        if (count($data) > 0) {
//            return response()->json(array('status' => 200, 'list' => $data));
//        } else {
//            return response()->json(array('status' => 500, 'list' => $data));
//        }
//    }

    public function getOrders(Request $request) {


        $data = Orders::with('apiorder', 'retailer', 'orderItemsView', 'hubs','ratings','mappedhub')->orderBy('created_at', 'DESC');

           $retailerId = $request->retailerId;
        if ($request->searchFromDate != '' && $request->searchToDate && isset($request->searchFromDate) && isset($request->searchToDate)) {
            $from = date('Y-m-d', strtotime($request->searchFromDate));
            $to = date('Y-m-d', strtotime($request->searchToDate));

            $data->where(function($query) use ($request, $from, $to) {
                return $query->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to);
            });
        }
        if (isset($request->retailerId) && $request->retailerId != '') {
            $data->orwhereHas('retailer', function($query) use($retailerId) {
                $query->Where('id', $retailerId);
            });
        }

        if (isset($request->orderBy) && $request->orderBy != '') {
            $data->where(function($query) use ($request) {
                return $query->where('order_by', $request->orderBy);
            });
        }
        if (isset($request->hub) && $request->hub != '') {
            $data->where(function($query) use ($request) {
                return $query->where('hub_id', $request->hub);
            });
        }
        if (isset($request->orderStatus) && $request->orderStatus != '') {
            $data->where(function($query) use ($request) {
                return $query->where('status', $request->orderStatus);
            });
        }
        if ($request->key != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('order_number', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('total_amount', 'LIKE', '%' . $request->key . '%')
                                ->orWhereHas('retailer', function($q) use($request) {
                                    $q->Where('full_name', 'LIKE', '%' . $request->key . '%');
                                    $q->orWhere('primary_mobile_number', 'LIKE', '%' . $request->key . '%');
                                    $q->orWhere('city', 'LIKE', '%' . $request->key . '%');
                                });
            });
        }
        $data = $data->paginate($request->pagesize);
        if (count($data) > 0) {
            return json_encode(array('code' => 200, 'list' => $data));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function changeStatus(Request $request) {
//       DB::enableQueryLog();

        $order = Orders::where('id', $request->oid)->update([
            'status' => "$request->status"
        ]);
//     dd(DB::getQueryLog());
        if ($order) {
            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function detailView($orderid) {



        $orders = Orders::with('orderItemsView', 'retailer', 'apiorder','ratings')->where('id', $orderid)->first()->toArray();
        if(empty($orders))
            return redirect()->back();
         if($orders['promocode'] != ''){
            $orders['promodetail'] =  $this->discountAgentPanel($orders['promocode_key'],$orders['promocode']);
         }
        return view('pages.orders.view')->with(array('orders' => $orders));
    }

    public function saveRetailerOrderCalling(Request $request) {

        $request->validate([
            'days' => 'required',
            'orderLimit' => 'required',
            'startTime' => 'required',
            'endTime' => 'required',

        ]);


        $isExist = RetailerOrderCalling::where('retailer_id', $request->retailerId)->get();
        if (count($isExist) > 0) {
            $set = RetailerOrderCalling::where('retailer_id', $request->retailerId)->update([
                'calling_days' => $request->days,
                'order_limit' => $request->orderLimit,
                'start_time' => $request->startTime,
                'end_time' => $request->endTime,
                'calling_option' => 1,
                'source' => 1,

            ]);
        } else {
            $set = RetailerOrderCalling::create([
                        'retailer_id' => $request->retailerId,
                        'calling_days' => $request->days,
                        'order_limit' => $request->orderLimit,
                        'start_time' => $request->startTime,
                        'end_time' => $request->endTime,
                        'calling_option' => 1,
                        'source' => 1,

            ]);
        }

        return response()->json(['status' => 'true']);
    }

}
