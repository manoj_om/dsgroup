<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App;
use App\City;
use App\Distributor;
use App\MapDistributor;
use Illuminate\Routing\Controller as BaseController;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportDistributor;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\validateSchemeDistributor;
use App\CallingAgent;
use App\MapAgent;
use App\Traits\CurlRequestTrait;

class DistributorController extends BaseController {

    use CurlRequestTrait;

    public function index() {

        return view('pages.distributor.index_distributor');
    }

    public function __construct() {
        $this->middleware('auth');
    }

//    public function deliveryPartner() {
//
//        return view('pages.product.product_deliver_partner');
//    }
//    public function distributorPage() {
//
//        return view('pages.product.product_distributor');
//    }
    public function manageHubView(Request $request) {
        return view('pages.users.hub');
    }

    public function store(Request $request) {
        //echo $request->id;die();

        if (isset($request->id)) {
            $request->validate([
                'distributor_name' => 'required|string|max:60',
                'distributor_code' => 'required|string|max:20',
                'mobile' => 'required|int',
                'email' => 'required|email|max:60',
                'pan_card' => 'required|string|max:10',
                'gst_no' => 'required|string|max:20',
                'aadhar_no' => 'required|string|max:15',
                'website' => 'required|required|string|max:60',
                'state' => 'required|required',
                'city' => 'required',
                'latitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'longitude' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                'address' => 'required',
                    //pan_card website state
            ]);
        } else {
            $request->validate([
                'distributor_name' => 'required|string|max:60',
                'distributor_code' => 'required|string|max:20|unique:distributors',
                'mobile' => 'required|int|unique:distributors',
                'email' => 'required|email|max:60|unique:distributors',
                'pan_card' => 'required|string|max:10|unique:distributors',
                'gst_no' => 'required|string|max:20|unique:distributors',
                'aadhar_no' => 'required|string|max:15|unique:distributors',
                'website' => 'required|string|max:60',
                'state' => 'required',
                'city' => 'required',
                'latitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'longitude' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                'address' => 'required',
            ]);
        }

        if ($request->has('redirectForm')) {
            $redirectForm = $request->get('redirectForm');
            $request->request->remove('redirectForm');
        }
        if ($request->has('accept_toc')) {
            $request->request->remove('accept_toc');
        }
        if (isset($request->id)) {
            $data = $request;
            $bjDist = Distributor::where(['id' => $request->id])->first();
            $allreadyExist = Distributor::where('id', "!=", $request->id)->where(function ($query) use($data) {

                        $query->Where('distributor_code', '=', $data->distributor_code)
                                ->orWhere('mobile', '=', $data->mobile)
                                ->orWhere('email', '=', $data->email);
                    })->first();
            if (isset($allreadyExist->mobile) or isset($allreadyExist->distributor_code) or isset($allreadyExist->email)) {
                return redirect('distributor')->withErrors('All ready in used ' . $data->distributor_code . "," . $allreadyExist->mobile . "," . $allreadyExist->email);
            }
        } else {

//           DB::enableQueryLog();
            $bjDist = new Distributor();
        }

        $pass = Str::random(8);
        $bjDist->distributor_name = $request->distributor_name;
        $bjDist->distributor_code = $request->distributor_code;
        $bjDist->mobile = $request->mobile;
        $bjDist->email = $request->email;
        $bjDist->pan_card = $request->pan_card;
        $bjDist->gst_no = $request->gst_no;
        $bjDist->aadhar_no = $request->aadhar_no;
        $bjDist->website = $request->website;
        $bjDist->state = $request->state;
        $bjDist->city = $request->city;
        $bjDist->latitude = $request->latitude;
        $bjDist->longitude = $request->longitude;
        $bjDist->address = trim($request->address);
        $bjDist->password = Hash::make($pass);
        // $bjDist->save();
//          dd(DB::getQueryLog());
        if ($bjDist->save()) {
            $message_body = urlencode("You have been successfully added as Hub user on DS Group portal.
             Please use this password $pass to login on portal " . url('/'));
            $message = "&send_to=" . $request->mobile . "&msg=$message_body";
            \App\Helpers\Helpers::sendSMS(config('api.smsurl') . $message);
            return redirect('distributor')->with('success', 'Record Saved');
        }
//        dd(DB::getQueryLog());
        // return redirect('distributor/create')->with('success', 'Record Saved');
    }

    public function create() {
        $result_state = DB::table('states')->get();

        return view('pages.distributor.add_distributor', ['states' => $result_state]);
    }

    public function edit($id) {
        $result_state = DB::table('states')->get();
        $obj = Distributor::find($id);
        if (!isset($obj->id)) {
            return redirect('distributor')->with('success', 'Id not found');
        }
        return view('pages.distributor.edit_distributor', ['states' => $result_state, 'model' => $obj]);
    }

    public function getDistributor(Request $request) {
        $Distributor = Distributor::orderBy('id', 'DESC');

        if ($request->status != '') {
            $Distributor->where('status', $request->status);
        }
        if ($request->searchkey != '') {
            $Distributor->where(function($query) use ($request) {
                return $query->where('distributor_name', 'like', "%" . $request->searchkey . "%")
                                ->orWhere('mobile', 'like', "%" . $request->searchkey . "%")
                                ->orWhere('email', '=', $request->searchkey)
                                ->orWhere('gst_no', 'like', "%" . $request->searchkey . "%")
                                ->orWhere('city', 'like', "%" . $request->searchkey . "%")
                                ->orWhere('distributor_code', 'like', "%" . $request->searchkey . "%");
            });
        }
        $Distributor = $Distributor->paginate(15);

        foreach ($Distributor as $row) {
            if (isset($row->city)) {
                $obj = City::where(["id" => $row->city])->first();
                $row->city = $obj->city;
            }
        }
        $url_str = '';
        if ($_SERVER['HTTP_HOST'] == 'localhost')
            $url_str = "public/";

        echo json_encode(array('list' => $Distributor, 'siteUrl' => config('app.url') . $url_str));
        exit;
    }

    public function getcity(Request $request) {


        $str_cities = "<option value=''>Select City</option>";
        if (trim($request->state)) {
            $result_state = DB::table('cities')->where(['state_id' => $request->state])->get();
            foreach ($result_state as $val) {
                $flage_str = '';
                if (isset($request->city_id)) {
                    if ($val->id == $request->city_id) {

                        $flage_str = " selected = '' ";
                    }
                }
                $str_cities .= "<option value='" . $val->id . "' $flage_str >" . $val->city . "</option>";
            }
        }
        echo $str_cities;
        die();
    }

    public function changeStatus(Request $request) {
        if (count($request->ids)) {
            $flage = 0;
            foreach ($request->ids as $row_val) {
                $resultObj = Distributor::where(['id' => $row_val])->first();
                $resultObj->status = $request->status;
                if ($resultObj->save()) {
                    $flage = 1;
                }
            }
            if ($flage == 1) {
                echo json_encode(array('status' => 1));
                die();
            }
            echo json_encode(array('status' => 0));
            die();
        }
    }

    public function addExcel() {

        return view('pages.distributor.import_excel');
    }

    public function checkExcelFile($file_ext) {
        $valid = array(
            'csv', 'xls', 'xlsx' // add your extensions here.
        );
        return in_array($file_ext, $valid) ? true : false;
    }

    public function import(Request $request) {


        $request->validate([
            'excel_file' => ['required'],
        ]);

        if ($this->checkExcelFile($request->file('excel_file')->getClientOriginalExtension())) {
            try {
                $test = Excel::import(new ImportDistributor, request()->file('excel_file'));
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                $failures = $e->failures();
            }
            if (count($failures) > 0) {
                return redirect('distributor')->with(['bug' => 'Error : Please download sample format of hub and all row must match']);
            } else {
                return redirect('distributor')->with('success', 'Record Imported');
            }
        } else {
            $error = \Illuminate\Validation\ValidationException::withMessages([
                        'excel_file' => array('file type must be csv xls xlsx'),
            ]);
            throw $error;
        }



        return redirect('distributor')->with('success', 'Record Saved');
    }

    public function getDistributorList(Request $request) {
        $list = Distributor::orderBy('distributor_name', 'ASC');
        if ($request->status != '') {
            $list->where('status', $request->status);
        }
        if ($request->searchkey != '') {
            $list->where(function($query) use ($request) {
                return $query->where('distributor_name', 'like', "%" . $request->searchkey . "%")
                                ->orWhere('distributor_code', 'like', "%" . $request->searchkey . "%");
            });
        }
        $data = $list->get();
        if (count($data)) {
            return response()->json(array('status' => true, 'list' => $data));
        } else {
            return response()->json(array(['status' => false, 'msg' => 'No Distributor exist']));
        }
    }

    public function getAgentList(Request $request) {
        $list = CallingAgent::orderBy('name', 'ASC');
        if ($request->status != '') {
            $list->where('status', $request->status);
        }
        if ($request->searchkey != '') {
            $list->where(function($query) use ($request) {
                return $query->where('name', 'like', "%" . $request->searchkey . "%")
                                ->orWhere('executive_code', 'like', "%" . $request->searchkey . "%");
            });
        }
        $data = $list->get();
        if (count($data)) {
            return response()->json(array('status' => true, 'list' => $data));
        } else {
            return response()->json(array(['status' => false, 'msg' => 'No Distributor exist']));
        }
    }

    public function ajaxlist(Request $request) {
        if (isset($request->searach_distributor) && trim($request->searach_distributor)) {
            $Distributor = Distributor::where('distributor_name', 'like', "%" . $request->searach_distributor . "%")
                    ->orWhere('mobile', 'like', "%" . $request->searach_distributor . "%")->orWhere('email', 'like', "%" . $request->searach_distributor . "%")
                    ->orWhere('pan_card', 'like', "%" . $request->searach_distributor . "%")
                    ->orWhere('gst_no', 'like', "%" . $request->searach_distributor . "%")
                    ->orWhere('aadhar_no', 'like', "%" . $request->searach_distributor . "%")
                    ->orWhere('website', 'like', "%" . $request->searach_distributor . "%")
                    ->orWhere('address', 'like', "%" . $request->searach_distributor . "%")
                    ->get();
        } else {
            $Distributor = Distributor::get();
        }
        $str = '';
        foreach ($Distributor as $row) {
            $str .= '<li class="list-group-item" data_id=' . $row->id . ' onclick="li_click(' . $row->id . ')" >
                                            <p class="m-0">' . $row->distributor_name . '</p>
                                            <small class=" text-grey-dark font-weight-light">' . $row->distributor_code . '</small>
                                          </li>';
        }
        return $str;

        //die();
    }

    public function mapDistributor(Request $request) {
        $retailer = MapDistributor::where('retailer_id', $request->retail_id)->get();
        if (count($retailer) > 0) {
            MapDistributor::where('retailer_id', $request->retail_id)->update([
                'distributor_id' => $request->dist,
            ]);
        } else {
            MapDistributor::create([
                'retailer_id' => $request->retail_id,
                'distributor_id' => $request->dist,
            ]);
        }
        $message = 'Congratulations! We have started to delivering to your area. Please download from the playstore.';
        $this->sendSms($message, $request->retail_id);
        return response()->json(['status' => true]);
    }

    public function mapMultipleRetailer(Request $request) {
        foreach ($request->retail_id as $key => $val) {
            $retailer = MapDistributor::where('retailer_id', $val)->get();
            if (count($retailer) > 0) {
                MapDistributor::where('retailer_id', $val)->update([
                    'distributor_id' => $request->dist,
                ]);
            } else {
                MapDistributor::create([
                    'retailer_id' => $val,
                    'distributor_id' => $request->dist,
                ]);
            }
            $message = 'Congratulations! We have started to delivering to your area. Please download from the playstore.';
            $this->sendSms($message, $val);
        }
        return response()->json(['status' => true]);
    }

    public function mapAgent(Request $request) {
        $retailer = MapAgent::where('retailer_id', $request->retail_id)->get();
        if (count($retailer) > 0) {
            MapAgent::where('retailer_id', $request->retail_id)->update([
                'agent_id' => $request->dist,
            ]);
        } else {
            MapAgent::create([
                'retailer_id' => $request->retail_id,
                'agent_id' => $request->dist,
            ]);
        }

        return response()->json(['status' => true]);
    }

    public function removeMapDistributor(Request $request) {
        $retailer = MapDistributor::where('retailer_id', $request->retail_id)->delete();

        return response()->json(['status' => true]);
    }

    public function removeMapAgent(Request $request) {
        $retailer = MapAgent::where('retailer_id', $request->retail_id)->delete();

        return response()->json(['status' => true]);
    }

}
