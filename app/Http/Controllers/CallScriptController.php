<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShopType;
use App\CallType;
use App\ScriptLevelType;
use App\EventType;
use App\CallCenterScript;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CallScript as Script;

class CallScriptController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function getTypes(Request $request) {
        $shopType = ShopType::all();
        $callType = CallType::all();
        $levelType = ScriptLevelType::all();
        $eventType = EventType::all();

        return response()->json(['shoptype' => $shopType, 'calltype' => $callType, 'leveltype' => $levelType, 'eventtype' => $eventType]);
    }
    
    public function addShopType(Request $request) {
        $request->validate([
            'type' => "required|unique:shop_types,name",
        ]);
        $insert = ShopType::create([
                    'name' => $request->type
        ]);
        if ($insert) {
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function addCallType(Request $request) {
        $request->validate([
            'type' => 'required|unique:call_types,name',
        ]);
        $insert = CallType::create([
                    'name' => $request->type
        ]);
        if ($insert) {
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function addLevelType(Request $request) {
        $request->validate([
            'type' => 'required|unique:script_level_types,name',
        ]);
        $insert = ScriptLevelType::create([
                    'name' => $request->type
        ]);
        if ($insert) {
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function addEventType(Request $request) {
        $request->validate([
            'type' => 'required|unique:event_types,name',
        ]);
        $insert = EventType::create([
                    'name' => $request->type
        ]);
        if ($insert) {
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function getAllScripts(Request $request) {
        $callScript = CallCenterScript::with('shop', 'call');


        if ($request->calltype != '') {
            $callScript->where(function($query) use ($request) {
                return $query->where('call_type', $request->calltype);
            });
        }
        if ($request->retailerType2 != '') {
            $callScript->where(function($query) use ($request) {
                return $query->where('retailer_type', $request->retailerType2);
            });
        }
        if ($request->retailerType1 != '') {
            $callScript->where(function($query) use ($request) {
                return $query->where('shop_type', $request->retailerType1);
            });
        }

        $data = $callScript->paginate(15);
        return response()->json(['list' => $data]);
    }

    public function addCallScript(Script $request) {
        $authid = Auth::user()->id;
        $where = array(
        'shop_type' => $request->shopType,
        'retailer_type' => $request->reatilerType,
        'call_type' => $request->callType
        );
        $isExist = CallCenterScript::where($where)->get()->first();
        if ($isExist)
            return response()->json(array('code' => 500, 'message' => "Script already added"));

        $insert = CallCenterScript::create([
                    'shop_type' => $request->shopType,
                    'retailer_type' => $request->reatilerType,
                    'created_by' => $authid,
                     'status' => 0,
                    'call_type' => $request->callType,
                    'script' => $request->scriptText,
        ]);

        return response()->json(['status' => true]);
    }
    public function saveScriptTree(Request $request) {   
        $insert = CallCenterScript::where('id', $request->scritpid)->update([ 
            'script_data' => json_encode($request->treeData),
        ]);
         return response()->json(['status' => true]);
    }
    public function updateCallScript(Script $request) {
          $authid = Auth::user()->id;
        $where = array(
            'shop_type' => $request->shopType,
            'retailer_type' => $request->reatilerType,
            'call_type' => $request->callType,
        );
        $isExist = CallCenterScript::where($where)->where('id', '!=', $request->editid)->get();

//        print_r($isExist);exit;

        if (count($isExist) > 0)
            return response()->json(array('code' => 500, 'message' => "Script already added"));

        $insert = CallCenterScript::where('id', $request->editid)->update([
            'shop_type' => $request->shopType,
            'retailer_type' => $request->reatilerType,
            'call_type' => $request->callType,
            'created_by' => $authid,
            'script' => $request->scriptText,
        ]);

        return response()->json(['status' => true]);
    }

}
