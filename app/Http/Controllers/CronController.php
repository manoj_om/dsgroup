<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image as Image;
use App\Http\Requests\validateUsers;
use App\HubStock;
use App\HubStockDateCheck;
use App\HubStockLog;
use App\Product;

class CronController extends Controller {

    public function cronHubStockNotifications(Request $request) {
        
       $distributorlist = \App\Distributor::where('status', 1)->get();

        foreach ($distributorlist as $d => $dv) {
                $yesterDayStock = HubStock::where('distributor_id', $dv->id)->get();
                if (count($yesterDayStock) > 0) {
                    foreach ($yesterDayStock as $old => $val) {
                        if((($val->product_quantity + $val->in_stock) - $val->out_stock) < $val->threshold_quantity_one){
                            $this->notificationsend('THRESH_ONE',$val);
                        }
                        if((($val->product_quantity + $val->in_stock) - $val->out_stock) < $val->threshold_quantity_two){
                             $this->notificationsend('THRESH_TWO',$val);
                        }
                    }
                    
                }
        }
        
    }
    
    public function notificationsend($type,$data) {
        $notificationRepository = new  \App\Repository\NotificationRepository;
        $notification =
        [
            'type' => $type,
            'user_id' => $data->distributor_id,
            'action_by' => $data->distributor_id,
            'action_user_gaurd' => 'hub',
            'data' => json_encode($data),
            'admin_message' => __('api/notification.webNotification.hub.stock.'.$type,['product_name' => $this->getProductName($data->product_id)]),
            'hub_message' => __('api/notification.webNotification.hub.stock.'.$type,['product_name' => $this->getProductName($data->product_id)]),
            'retailer_message' => __('api/notification.webNotification.hub.stock.'.$type,['product_name' => $this->getProductName($data->product_id)]),
        ];

        return $notificationRepository->store($notification);
    }

    public function inserInHubLog($param) {
        HubStockLog::create([
            'stock_id' => $param->id,
            'product_id' => $param->product_id,
            'distributor_id' => $param->distributor_id,
            'product_quantity' => $param->product_quantity > 0 ? $param->product_quantity : 0 ,
            'pre_created_at' => $param->created_at,
            'out_stock' => $param->in_stock,
            'in_stock' => $param->out_stock,
        ]);

        HubStock::where('id', $param->id)->update([
            'product_quantity' => (($param->product_quantity + $param->in_stock) - $param->out_stock) > 0 ? (($param->product_quantity + $param->in_stock) - $param->out_stock) : 0,
            'out_stock' => 0,
            'current_quantity' => (($param->product_quantity + $param->in_stock) - $param->out_stock) > 0 ? (($param->product_quantity + $param->in_stock) - $param->out_stock) : 0,
            'in_stock' => 0,
        ]);
    }

    public function hubCron(Request $request) {

      
        $distributorlist = \App\Distributor::where('status', 1)->get();

        foreach ($distributorlist as $d => $dv) {

            $isRun = HubStockDateCheck::whereDate('created_at', date("Y-m-d"))->where('hub_id', $dv->id)->first();
            if (!$isRun) {
                $yesterDayStock = HubStock::where('distributor_id', $dv->id)->get();
                if (count($yesterDayStock) > 0) {
                    foreach ($yesterDayStock as $old => $val) {
                        $this->inserInHubLog($val);
                    }
                    $dateInsert = HubStockDateCheck::create([
                                'total' => count($yesterDayStock),
                                'hub_id' => $dv->id,
                    ]);
                }
            }
        }
    }
    public function getProductName($param) {
        
        $name = Product::with('productName','packingType','weightType','unitType')->where('id',$param)->first();
        
        return $name['productName']['name'] . ' ' . $name['packingType']['name'] . ' ' . $name['unitType']['name']. ' ' . $name['unitType']['name'];
        
     }

}
