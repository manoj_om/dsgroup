<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExportAttentionRequestController extends Controller
{
    //
    
    public function __construct() {
        $this->middleware('auth');
    }

    public function export($id)
    {
        
       
        # code...
        $lists = \App\AttentionRequired::has('getRetailer')->where('status',$id)->get();
        $reportHeading = array(
            'Date','Retailer Name','Scan code','Reason','Shop Name','City','State','Geo Location','Resolution','Requested By','Status','Source'
        );

       
        
        $fileName = time() . "-attention-request.csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=' . $fileName . '');
        header('Cache-Control: max-age=0');
        $file = fopen('php://output', 'w');
        fputcsv($file, $reportHeading);
        foreach($lists as $list){
            if($list->user_type == 1) {
                $list->source = 'Mobile App';
                $name = $list->onboardingAgent->name;
            }else if($list->user_type == 2){
                 $list->source = 'Agent Call Center';
                $feed = \App\FeedbackRemark::where('id',$list->feedback_id)->first();
                $list->reason = $feed->remark; 
                $name = $list->callingAgent->name;
            }else{
                 $list->source = 'Admin';
                $name = $list->admin->name;
            }

            $reportHeading = array(
                'Date' => \Carbon\Carbon::parse($list->created_at)->format('d-M-Y'),
                'Retailer Name' => $list->getRetailer->full_name,
                'Scan code' => $list->getRetailer->scan_code,
                'Reason' => $list->reason,
                'Shop Name' => $list->getRetailer->shop_name,
                'City' => $list->getRetailer->city,
                'State' => $list->getRetailer->state,
                'Geo Location' => $list->getRetailer->latitudes . ' - ' .$list->getRetailer->longitude,
                'Resolution' => $list->resolution,
                'Requested By' => $name,
                'Status' => $list->status ? 'Resolved' : 'Pending',
                'Source' => $list->source,
            );
            fputcsv($file, $reportHeading);
        }
        fclose($file);
        exit;
    }
}
