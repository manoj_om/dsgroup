<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ProductCategory;
use App\ProductSubCategory;
use App\Product;
use Illuminate\Support\Str;
use App\Imports\ImportProducts;
use Maatwebsite\Excel\Facades\Excel;
use App\ProductList;
use Intervention\Image\Facades\Image as Image;
use App\Http\Requests\ProductInsert;
use App\Http\Requests\ProductUpdate;
use App\PackingType;
use App\ProductUnitType;
use App\ProductUnitWeight;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        return view('pages.product.index');
    }

    public function deliveryPartner() {

        return view('pages.product.product_deliver_partner');
    }

    public function distributorPage() {

        return view('pages.product.product_distributor');
    }

    public function addProduct() {

        return view('pages.product.add_product');
    }

    public function addCategory(Request $request) {
        $request->validate([
            'image' => 'required|image64:jpeg,jpg,png',
            'category' => 'required'
        ]);


        $avtar = '';
        if ($request->get('image')) {
            $image = $request->get('image');
            $avtar = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('image'))->resize(320, 240)->save(public_path('images/product/categories/') . $avtar);
        }

        $create = ProductCategory::Create([
                    'name' => $request->category,
                    'avtar' => $avtar,
                    'created_by' => Auth::user()->id,
        ]);

        return response()->json(['status' => 'true']);
    }

    public function addProductList(Request $request) {
        $request->validate([
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'name' => 'required'
        ]);

        $create = ProductList::Create([
                    'name' => $request->name,
                    'category_id' => $request->category_id,
                    'subcategory_id' => $request->subcategory_id,
                    'created_by' => Auth::user()->id,
        ]);

        return response()->json(['status' => 'true']);
    }

    public function updateProductList(Request $request) {
        $request->validate([
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'name' => 'required',
            'editId' => 'required'
        ]);
        $create = ProductList::where('id', $request->editId)->update([
            'name' => $request->name,
            'category_id' => $request->category_id,
            'subcategory_id' => $request->subcategory_id,
            'created_by' => Auth::user()->id,
        ]);

        return response()->json(['status' => 'true']);
    }

    public function updateCategory(Request $request) {
        $request->validate([
            'image' => 'nullable|image64:jpeg,jpg,png',
            'category' => 'required'
        ]);


        $avtar = '';
        if ($request->get('image')) {
            $image = $request->get('image');
            $avtar = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('image'))->resize(320, 240)->save(public_path('images/product/categories/') . $avtar);
        } else {
            $avtara = ProductCategory::where('id', $request->categoryid)->first();
            if ($avtara->avtar != null || $avtara->avtar != "") {
                $avtar = $avtara->avtar;
            }
        }

        $create = ProductCategory::where('id', $request->categoryid)->Update([
            'name' => $request->category,
            'avtar' => $avtar,
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500));
        }
        exit;
    }

    public function disableProductCategory(Request $request) {
        $create = ProductCategory::whereIn('id', $request->list)->Update([
            'status' => 1,
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'Product Disabled Successfully'));
        }
        exit;
    }

    public function disableProductList(Request $request) {
        $create = ProductList::whereIn('id', $request->list)->Update([
            'status' => 1,
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'Product Disabled Successfully'));
        }
        exit;
    }

    public function enableProductList(Request $request) {
        $create = ProductList::whereIn('id', $request->list)->Update([
            'status' => 0,
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'Product Disabled Successfully'));
        }
        exit;
    }

    public function enableProductCategory(Request $request) {
        $create = ProductCategory::whereIn('id', $request->list)->Update([
            'status' => 0,
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'Product Not Enabled Successfully'));
        }
        exit;
    }

    public function disableProductSubCategory(Request $request) {
        $create = ProductSubCategory::whereIn('id', $request->list)->Update([
            'status' => 1,
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'Product Disabled Successfully'));
        }
        exit;
    }

    public function enableProductSubCategory(Request $request) {
        $create = ProductSubCategory::whereIn('id', $request->list)->Update([
            'status' => 0,
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'Product Not Enabled Successfully'));
        }
        exit;
    }

    public function disableProduct(Request $request) {
        $create = Product::whereIn('id', $request->list)->Update([
            'status' => 1,
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'Product Disabled Successfully'));
        }
        exit;
    }

    public function enableProduct(Request $request) {
        $create = Product::whereIn('id', $request->list)->Update([
            'status' => 0,
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'Product Not Enabled Successfully'));
        }
        exit;
    }

    public function addUnitType(Request $request) {
        $request->validate([
            'name' => 'required'
        ]);

        ProductUnitType::create([
            'name' => $request->name
        ]);
        return response()->json(['status' => 'true']);
    }

    public function addPackingType(Request $request) {
        $request->validate([
            'name' => 'required'
        ]);

        PackingType::create([
            'name' => $request->name
        ]);

        return response()->json(['status' => 'true']);
    }

    public function addWeightType(Request $request) {
        $request->validate([
            'name' => 'required'
        ]);

        ProductUnitWeight::create([
            'name' => $request->name
        ]);

        return response()->json(['status' => 'true']);
    }
    public function updateUnitType(Request $request) {
        $request->validate([
            'name' => 'required'
        ]);

        ProductUnitType::where('id',$request->editid)->update([
            'name' => $request->name
        ]);
        return response()->json(['status' => 'true']);
    }

    public function updatePackingType(Request $request) {
        $request->validate([
            'name' => 'required'
        ]);

        PackingType::where('id',$request->editid)->update([
            'name' => $request->name
        ]);

        return response()->json(['status' => 'true']);
    }

    public function updateWeightType(Request $request) {
        $request->validate([
            'name' => 'required'
        ]);

        ProductUnitWeight::where('id',$request->editid)->update([
            'name' => $request->name
        ]);

        return response()->json(['status' => 'true']);
    }

    public function getProductType(Request $request) {
        $PackingType = PackingType::get()->toArray();
        $unitType = ProductUnitType::get()->toArray();
        $weightType = ProductUnitWeight::get()->toArray();

        return response()->json(['status' => 'true', 'type' => $PackingType, 'unit' => $unitType, 'weight' => $weightType]);
    }

    public function addProducts(ProductInsert $request) {

        $where = array(
            'product_lists_id' => $request->product_name,
            'product_unit_weight' => $request->product_unit_weight,
            'package_unit' => $request->product_package_unit,
            'packaging' => $request->packaging
        );
        $isProductExist = Product::where($where)->get()->first();
        if ($isProductExist)
            return response()->json(array('code' => 500, 'message' => "Product already added"));


        $avtar = '';
        if ($request->get('avtar')) {
            $image = $request->get('avtar');
            $avtar = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('avtar'))->resize(320, 240)->save(public_path('images/product/') . $avtar);
        }


        $create = Product::Create([
                    'serial_number' => Str::random(5) . time(),
                    'product_lists_id' => $request->product_name,
                    'short_description' => $request->short_description,
                    'sku_number' => $request->product_sku,
                    'package_unit' => $request->product_package_unit,
                    'packaging' => $request->packaging,
                    'quantity' => $request->quantity,
                    'category' => $request->product_category,
                    'mrp' => $request->mrp,
                    'rlp' => $request->rlp,
                    'sub_category' => $request->product_subcategory,
                    'created_by' => Auth::user()->id,
                    'avtar' => $avtar,
                    'brand_name' => $request->brand_name,
                    'manufacturer' => $request->manufacturer,
                    'hsn_code' => $request->hsn_code,
                    'ean_code' => $request->ean_code,
                    'dlp' => $request->dlp,
                    'max_order_quantity' => $request->max_quantity,
                    'product_length' => $request->product_length,
                    'product_width' => $request->product_width,
                    'product_height' => $request->product_height,
                    'shelf_life' => $request->shelf_life,
                    'custmer_target' => $request->custmor_target,
                    'master_carton_gross_weight' => $request->master_carton_gross_weight,
                    'master_carton_net_weight' => $request->master_carton_net_weight,
                    'master_carton_length' => $request->master_carton_length,
                    'master_carton_width' => $request->master_carton_width,
                    'master_carton_height' => $request->master_carton_height,
                    'product_unit_weight' => $request->product_unit_weight,
                    'is_featured' => $request->is_featured,
        ]);

        if ($create) {
            return response()->json(array('code' => 200));
        } else {
            return response()->json(array('code' => 500));
        }
        exit;
    }

    public function updateProducts(ProductUpdate $request) {

        $where = array(
            'product_lists_id' => $request->product_name,
            'product_unit_weight' => $request->product_unit_weight,
            'package_unit' => $request->product_package_unit,
            'packaging' => $request->packaging
        );
        $isProductExist = Product::where($where)->where('id' ,'!=', $request->productid)->get();
        if (count($isProductExist) > 0)
            return response()->json(array('code' => 500, 'message' => "Product already added"));

        $avtar = '';
        if ($request->get('avtar')) {
            $image = $request->get('avtar');
            $avtar = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('avtar'))->resize(320, 240)->save(public_path('images/product/') . $avtar);
        } else {
            $avtara = Product::where('id', $request->productid)->first();
            if ($avtara->avtar != null || $avtara->avtar != "") {
                $avtar = $avtara->avtar;
            }
        }


        $create = Product::where('id', $request->productid)->Update([
            'product_lists_id' => $request->product_name,
            'short_description' => $request->short_description,
            'sku_number' => $request->product_sku,
            'package_unit' => $request->product_package_unit,
            'packaging' => $request->packaging,
            'quantity' => $request->quantity,
            'category' => $request->product_category,
            'sub_category' => $request->product_subcategory,
            'mrp' => $request->mrp,
            'rlp' => $request->rlp,
            'avtar' => $avtar,
            'brand_name' => $request->brand_name,
            'manufacturer' => $request->manufacturer,
            'hsn_code' => $request->hsn_code,
            'ean_code' => $request->ean_code,
            'dlp' => $request->dlp,
            'max_order_quantity' => $request->max_quantity,
            'product_length' => $request->product_length,
            'product_width' => $request->product_width,
            'product_height' => $request->product_height,
            'shelf_life' => $request->shelf_life,
            'custmer_target' => $request->custmor_target,
            'master_carton_gross_weight' => $request->master_carton_gross_weight,
            'master_carton_net_weight' => $request->master_carton_net_weight,
            'master_carton_length' => $request->master_carton_length,
            'master_carton_width' => $request->master_carton_width,
            'master_carton_height' => $request->master_carton_height,
            'product_unit_weight' => $request->product_unit_weight,
            'is_featured' => $request->is_featured,
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500));
        }
        exit;
    }

    public function addSubCategory(Request $request) {
        $request->validate([
            'avtar' => 'required|image64:jpeg,jpg,png',
            'category' => 'required',
            'subcategory' => 'required'
        ]);
        $avtar = '';
        if ($request->get('avtar')) {
            $image = $request->get('avtar');
            $avtar = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('avtar'))->resize(320, 240)->save(public_path('images/product/subcategories/') . $avtar);
        }
        $create = ProductSubCategory::Create([
                    'category' => $request->category,
                    'name' => $request->subcategory,
                    'created_by' => Auth::user()->id,
                    'avtar' => $avtar
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500));
        }
        exit;
    }

    public function updateSubCategory(Request $request) {

        $request->validate([
            'avtar' => 'nullable|image64:jpeg,jpg,png',
            'category' => 'required',
            'subcategory' => 'required'
        ]);

        $avtar = '';
        if ($request->get('avtar')) {
            $image = $request->get('avtar');
            $avtar = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('avtar'))->resize(320, 240)->save(public_path('images/product/subcategories/') . $avtar);
        } else {
            $avtara = ProductSubCategory::where('id', $request->editId)->first();
            if ($avtara->avtar != null || $avtara->avtar != "") {
                $avtar = $avtara->avtar;
            }
        }

        $create = ProductSubCategory::where('id', $request->editId)->update([
            'category' => $request->category,
            'name' => $request->subcategory,
            'avtar' => $avtar
        ]);

        if ($create) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500));
        }
        exit;
    }

    public function getCategory(Request $request) {

        $data = ProductCategory::orderBy('created_at', 'DESC')->where('status', 0);
        if ($request->searchkey != '') {
            $data->where('name', 'LIKE', '%' . $request->searchkey . '%');
        }

        $data = $data->get()->toArray();

        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'no category found'));
        }
        exit;
    }

    public function ProductList(Request $request) {

        $data = ProductCategory::orderBy('created_at', 'DESC');
        $data->where('status', 0);
        if ($request->searchkey != '') {
            $data->where('name', 'LIKE', '%' . $request->searchkey . '%');
        }

        $data = $data->get()->toArray();

        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'no category found'));
        }
        exit;
    }

    public function getCategories(Request $request) {

        $data = ProductCategory::orderBy('created_at', 'DESC');

        if ($request->searchkey != '') {
            $data->where('name', 'LIKE', '%' . $request->searchkey . '%');
        }
        if ($request->status != '') {
            $data->where('status', $request->status);
        }

        $data = $data->paginate(15);

        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'no category found'));
        }
        exit;
    }

    public function getProductListName(Request $request) {

        $data = ProductList::with('category', 'subcategory')->orderBy('created_at', 'DESC');
        $data->WhereHas('category', function ($query1) use ($request) {
            $query1->where('status', 0);
        });

        $data->WhereHas('subcategory', function ($query3) use ($request) {
            $query3->where('status', 0);
        });
        if ($request->status != '') {
            $data->where('status', $request->status);
        }
        if ($request->searchkey != '') {
            $data->where('name', 'LIKE', '%' . $request->searchkey . '%');
        }
        if ($request->productKey != '') {
            $data->where('category_id', $request->productKey);
        }
        if ($request->subProductKey != '') {
            $data->where('subcategory_id', $request->subProductKey);
        }

        $data = $data->paginate(15);
        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'no category found'));
        }
        exit;
    }

    public function getProductNameList(Request $request) {

        $data = ProductList::with('category', 'subcategory')->where('status', 0)->orderBy('created_at', 'DESC')->get()->toArray();

        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'no product found'));
        }
        exit;
    }

    public function getSubCategory(Request $request) {
        $data = ProductSubCategory::with('category')->orderBy('created_at', 'DESC')->where('status', 0);
        if ($request->cat != '') {
            $data->where('category', $request->cat);
        }
        if ($request->key != '') {
            $data->where('name', 'LIKE', '%' . $request->key . '%');
        }

        $data = $data->get()->toArray();
        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'no category found'));
        }
        exit;
    }

    public function getSubCategories(Request $request) {
        $data = ProductSubCategory::with('category')->orderBy('created_at', 'DESC');

        $data->orWhereHas('category', function ($query1) use ($request) {
            $query1->where('status', 0);
        });
        if ($request->cat != '') {
            $data->where('category', $request->cat);
        }
        if ($request->status != '') {
            $data->where('status', $request->status);
        }
        if ($request->key != '') {
            $data->where('name', 'LIKE', '%' . $request->key . '%');
        }

        $data = $data->paginate(15);
        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'no category found'));
        }
        exit;
    }

    public function getProductList(Request $request) {
        $data = Product::with('category', 'subcategory', 'productName', 'packingType', 'unitType', 'weightType')->orderBy('created_at', 'DESC');
        $data->Where('status', 0);
        $data->WhereHas('category', function ($query1) use ($request) {
            $query1->where('status', 0);
        });
        $data->WhereHas('subcategory', function ($query1) use ($request) {
            $query1->where('status', 0);
        });
        $data->WhereHas('productName', function ($query1) use ($request) {
            $query1->where('status', 0);
        });
        if ($request->cat != '') {
            $data->Where('category', $request->cat);
        }
        if ($request->key != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('short_description', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('sku_number', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('brand_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('mrp', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('rlp', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('dlp', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('quantity', 'LIKE', '%' . $request->key . '%')
                                ->orWhereHas('productName', function ($query1) use ($request) {
                                    $query1->where('name', 'LIKE', '%' . $request->key . '%');
                                })
                                ->orWhereHas('packingType', function ($query2) use ($request) {
                                    $query2->where('name', 'LIKE', '%' . $request->key . '%');
                                })
                                ->orWhereHas('weightType', function ($query3) use ($request) {
                                    $query3->where('name', 'LIKE', '%' . $request->key . '%');
                                });
            });
        }
        $data = $data->get()->toArray();
        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'no category found'));
        }
        exit;
    }

    public function getProducts(Request $request) {
        $data = Product::with('category', 'subcategory', 'productName', 'packingType', 'unitType', 'weightType')->orderBy('created_at', 'DESC');

        $data->WhereHas('category', function ($query1) use($request) {
            $query1->where('status', 0);
        });
        $data->WhereHas('subcategory', function ($query1) {
            $query1->where('status', 0);
        });
        $data->WhereHas('productName', function ($query1) {
            $query1->where('status', 0);
        });
        if ($request->status != '') {
            $data->where('status', $request->status);
        }
        if ($request->cat != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('category', $request->cat);
            });
        }

        if ($request->key != '') {
            $data->where(function($query) use ($request) {
                return $query->orWhere('short_description', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('sku_number', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('brand_name', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('mrp', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('rlp', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('dlp', 'LIKE', '%' . $request->key . '%')
                                ->orWhere('quantity', 'LIKE', '%' . $request->key . '%')
                                ->orWhereHas('productName', function ($query1) use ($request) {
                                    $query1->where('name', 'LIKE', '%' . $request->key . '%');
                                })
                                ->orWhereHas('packingType', function ($query2) use ($request) {
                                    $query2->where('name', 'LIKE', '%' . $request->key . '%');
                                })
                                ->orWhereHas('weightType', function ($query3) use ($request) {
                                    $query3->where('name', 'LIKE', '%' . $request->key . '%');
                                });
            });
        }
        $data = $data->paginate(15);
        if (count($data) > 0) {
            echo json_encode(array('code' => 200, 'list' => $data));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'no category found'));
        }
        exit;
    }

    public function paginateProduct(Request $request) {


        $users = ProductCategory::paginate(5);

        return response()->json($users);
    }

    public function import(Request $request) {

//        print_r($request->all());exit;
        
        $this->validate($request, [
            'excelfile' => ['required'],
        ]);

        if ($this->checkExcelFile($request->file('excelfile')->getClientOriginalExtension())) {
            Excel::import(new ImportProducts, request()->file('excelfile'));
            return back();
        } else {
            $error = \Illuminate\Validation\ValidationException::withMessages([
                        'excelfile' => array('file type must be csv xls xlsx'),
            ]);
            throw $error;
        }
    }

    public function checkExcelFile($file_ext) {
        $valid = array(
            'csv', 'xls', 'xlsx' // add your extensions here.
        );
        return in_array($file_ext, $valid) ? true : false;
    }

}
