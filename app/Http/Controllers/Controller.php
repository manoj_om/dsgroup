<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //added by niraj lal rahi
    /*
    * @params
    * $data in array, $statusCode,
    */
    public function apiResponse(array  $data,$statusCode = 200,$type = 'application/json '){

        return response()->json($data, $statusCode)->withHeaders([
            'Content-Type' => $type
        ]);
    }
}
