<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\CallingAgent;
use App\Retailer;
use Intervention\Image\Facades\Image as Image;
use App\Product;
use App\ProductCategory;
use App\Orders;
use App\OrderItems;
use App\SchemeAndPromotion;
use App\FeedbackCategory;
use App\FeedbackRemark;
use App\RetailerFeedback;
use App\AttentionRequired;
use App\CallScript;
use Illuminate\Support\Facades\DB;
use App\SkuLineSoldPerCallScheme;
use App\OrderVolumeScheme;
use App\OrderedItemScheme;
use App\OrderedItemsDiscount;
use App\NewProductLaunch;
use App\Traits\CartTrait;
use App\Traits\OrdersTrait;
use App\Traits\ProfileTrait;
use App\Traits\CurlRequestTrait;
use App\Traits\PromocodeTrait;
use App\Traits\ProductReviewTrait;
use Illuminate\Validation\Rule;
use App\Helpers\Helpers;
use App\Repository\OrderRepository;
use App\Repository\RetailerOrderCallingRepository;
use App\ProductReview;
use App\RetailerOrderCalling;
use App\Distributor;
use App\CallCenterScript;
use Illuminate\Support\Facades\Session;
class AgentController extends Controller {

    use AuthenticatesUsers,
        CartTrait,
        OrdersTrait,
        ProfileTrait,
        CurlRequestTrait,
        PromocodeTrait,
        ProductReviewTrait;

    private $endpoint, $sessionToken;
    private $storeId = 'DU001';
    private $promocodeKey = ['orderVolume', 'orderItems', 'skuLine', 'productLaunch'];

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'agent/';

    public function __construct() {
        $this->middleware('auth:agent', ['except' => 'getRetailersQueueList']);
        $this->endpoint = "http://delivery.now.bike/api/v2/";
        $this->sessionToken = "W269ndsormentvj2wf";
    }

    public function listPromocode(Request $request) {
        try {

            $promocodes = $this->getPromocodesAgentPanel($request->id);
            return $this->apiResponse([
                        'status' => true,
                        'promocodes' => $promocodes,
                            ], __('api/responseCode.success'));
        } catch (\Exception $exception) {
            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Internal server error please try again.',
                        'error' => 'Error : ' . $exception->getMessage(),
                            ], __('api/responseCode.server_error'));
        }
    }

    public function getPromocodeDetail(Request $request) {
        try {

            $validate = \Validator($request->post(), [
                'promocode_key' => ['required', Rule::in($this->promocodeKey)],
                'promocode' => ['required']
            ]);
            if ($validate->fails()) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => $validate->errors()->first(),
                                ], __('api/responseCode.bad_request'));
            }

            $promocodeKey = $request->post('promocode_key');
            $promocode = $request->post('promocode');

            $promocodes = $this->getPromocodesAgentPanel($request->id);
            if (empty($promocodes))
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Promocode is not valid'
                                ], __('api/responseCode.failed'));

            if (!empty($promocodes)) {

                $match = false;
                foreach ($promocodes as $value) {
                    if ($value['key'] == $promocodeKey && $value['value'] == $promocode) {
                        $match = true;
                        break;
                    }
                }
                if (!$match)
                    return $this->apiResponse([
                                'status' => false,
                                'message' => 'Promocode is not valid',
                                'promocode' => $promocodes
                                    ], __('api/responseCode.failed'));
            }

            $discountAmount = $this->discountAgentPanel($promocodeKey, $promocode);

            return $this->apiResponse([
                        'status' => true,
                        'discount' => $discountAmount,
                        'message' => 'Promocode applied successfully'
                            ], __('api/responseCode.success'));
        } catch (\Exception $exception) {
            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Internal server error please try again.',
                        'error' => 'Error : ' . $exception->getMessage(),
                            ], __('api/responseCode.server_error'));
        }
    }

    public function getPromocode(Request $request) {

        $sku = SkuLineSoldPerCallScheme::where('status', 0)->get();
        $vol = OrderVolumeScheme::where('status', 0)->get();
        $ordered = OrderedItemScheme::where('status', 0)->get();
        $discount = OrderedItemsDiscount::where('status', 0)->get();
        $new = NewProductLaunch::where('status', 0)->get();
        $list = array(
            'sku' => $sku,
            'volume' => $vol,
            'ordered' => $ordered,
            'discount' => $discount,
            'new' => $new,
        );

        return response()->json(['list' => $list]);
    }

    public function schemeAndPromotions(Request $request) {
        $data = SchemeAndPromotion::where('status', 0)->where('is_delete', 0)->whereDate('to_date', '>=', date('Y-m-d'))->get()->toArray();
        if (count($data) > 0) {
            return response()->json(array('status' => true, 'scheme' => $data));
        } else {
            return response()->json(array('status' => false));
        }
    }

    public function agentPlaceOrder(Request $request) {

        $request->validate([
            'totalAmount' => 'required',
            'carts' => 'required',
        ]);

        $order = Orders::create([
                    'user_id' => $request->reatilerId,
                    'order_number' => substr(sha1(time()), -10),
                    'total_amount' => $request->totalAmount,
                    'order_by' => 2,
        ]);

        if ($order) {
            foreach ($request->carts as $key => $value) {
                $orderitems = OrderItems::create([
                            'order_id' => $order->id,
                            'product_id' => $value['id'],
                            'quantity' => $value['rate'],
                ]);
            }
        }

        return response()->json(['status' => 'true']);
    }

    public function addFeedbackCategory(Request $request) {

        $request->validate([
            'name' => 'required|unique:feedback_categories',
        ]);

        $order = FeedbackCategory::create([
                    'created_by' => Auth::guard('agent')->user()->id,
                    'name' => $request->name,
        ]);


        return response()->json(['status' => 'true']);
    }

    public function addRetailerFeedback(Request $request) {

        $request->validate([
            'retailer_id' => 'required',
            'feedback_id' => 'required',
        ]);

        $order = AttentionRequired::create([
                    'retailer_id' => $request->retailer_id,
                    'user_type' => 2,
                    'user_id' => Auth::guard('agent')->user()->id,
                    'feedback_id' => $request->feedback_id,
        ]);


        return response()->json(['status' => 'true']);
    }

    public function getFeedback(Request $request) {
        $category = FeedbackCategory::with('remarks')->get()->toArray();

        return response()->json(['status' => 'true', 'list' => $category]);
    }

    public function addFeedbackRemarks(Request $request) {

        $request->validate([
            'name' => 'required',
            'feedback_category_id' => 'required',
        ]);

        $order = FeedbackRemark::create([
                    'created_by' => Auth::guard('agent')->user()->id,
                    'remark' => $request->name,
                    'feedback_category_id' => $request->feedback_category_id,
        ]);
        if ($order) {
            $attention = AttentionRequired::create([
                        'retailer_id' => $request->retailer_id,
                        'user_type' => 2,
                        'user_id' => Auth::guard('agent')->user()->id,
                        'feedback_id' => $order->id,
            ]);
        }


        return response()->json(['status' => 'true']);
    }

    public function index() {
        $helper = new Helpers;
        $day = $helper->getNumberFromDay();
        $retailerOrderCalling = new RetailerOrderCallingRepository;
        $userId = Auth::guard('agent')->user()->id;
        $orderRepository = new OrderRepository(new \App\Orders);
        $orders = $orderRepository->getOrderPlacedByAgent($userId)->total;
        $connectedCalls = $retailerOrderCalling->getQueueRetailers($day)->count();
        if ($orders) {
            $helper = new \App\Helpers\Helpers;
            $orders = $helper->convertRupeeToLac($orders);
        }

        return view('pages.callcenter.dashboard', compact('connectedCalls', 'orders'));
    }

    public function agentPanel($id) {
//       print_r(Auth::guard('agent')->user());exit;
//         echo "in1sss11";exit;
        return view('pages.callcenter.agent_panel')->with(array('rid' => $id));
    }

    public function getGuardInfo() {
        return json_encode(Auth::guard('agent')->user());
//        return redirect('/agent/login');
    }

    public function callindex() {

        return view('pages.agent.index');
    }

    public function call_queue() {

        return view('pages.agent.call_queue');
    }

    public function agent_status() {

        return view('pages.agent.agent_status');
    }

    public function profile() {

        return view('pages.agent.profile');
    }

    public function profileImageUpload(Request $request) {
        $name = '';
        $request->validate([
            'avtar' => 'required|image64:jpeg,jpg,png',
        ]);

        if ($request->get('avtar')) {
            $image = $request->get('avtar');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('avtar'))->save(public_path('images/callingAgent/') . $name);
        }

        $users = CallingAgent::where('id', Auth::user()->id)
                ->update([
            'avtar' => $name
        ]);
        if ($users) {
            return json_encode(array('code' => 200, 'msg' => 'User profile updated successfully'));
        } else {
            return json_encode(array('code' => 500, 'msg' => 'User profile not updated successfully'));
        }
    }

    public function getCallingAgents(Request $request) {

        $agents = CallingAgent::orderBy('created_at', 'DESC');
        if (isset($request->key) && $request->key != '') {
            $agents->orWhere('name', 'LIKE', '%' . $request->key . '%');
            $agents->orWhere('email', 'LIKE', '%' . $request->key . '%');
            $agents->orWhere('mobile_number', 'LIKE', '%' . $request->key . '%');
            $agents->orWhere('executive_code', 'LIKE', '%' . $request->key . '%');
        }
        $agents = $agents->paginate(8);


        if (count($agents) > 0) {

            return response()->json(['code' => 200, 'list' => $agents]);
        } else {

            return response()->json(['code' => 500]);
        }
    }

    public function changeAgentLoginStatus(Request $request) {

        $update = CallingAgent::where('id', $request->aid)->update([
            'login_status' => $request->arg
        ]);

        if ($update) {
            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500));
        }
    }

    public function panelView($id) {
       $data = Retailer::with('agantdata', 'otherQrcode','orderCalling')->has('orderCalling')->where('id', $id)->first();
        if($data){
            return json_encode(array('profile' => $data,'nocalling' => true)); 
        }else{
             $data1 = Retailer::with('agantdata', 'otherQrcode')->where('id', $id)->first();
        }
        return json_encode(array('profile' => $data1 , 'nocalling' => false));
    }

    public function checkIsHubActive($param) {
        $isActive = Distributor::where('id', $param)->where('status', 1)->first();
        if ($isActive) {
            return false;
        } else {
            return true;
        }
    }

    public function productList(Request $request) {

        $retail = Retailer::with('StockMapDistributorpanel')->where('id', $request->id);

        $retail->Where(function($query) use ($request) {
            $query->whereHas('StockMapDistributorpanel', function ($query1) use ($request) {
                $query1->where('retailer_id', $request->id);
            });
        });

        $result = $retail->first();
         
        if ($result['StockMapDistributorpanel']) {
            $hubId = $result['StockMapDistributorpanel']['distributor_id'];
            if($this->checkIsHubActive($result['StockMapDistributorpanel']['distributor_id'])){
                  return response()->json(array('code' => '404', 'msg' => 'Mapped Hub not active'));
            }else{
               Session::put('currentHub',$hubId);
            }
        } else{
             return response()->json(array('code' => '404', 'msg' => 'Hub not mapped to retailer'));
        }
        $product = ProductCategory::with('products', 'subCategory');
        $product->WhereHas('products', function ($query1) use ($request) {
            $query1->where('status', 0);
        });
        $product->has('products.stockagent');
        $product->where(function($query) use ($request) {
                $query->where('status', 0);
            });

        $product->WhereHas('subCategory', function ($query1) use ($request) {
            $query1->where('status', 0);
        });


        if (isset($request->key) && $request->key != '') {

            $product->where(function($query) use ($request) {
                $query->orwhere('name', 'LIKE', '%' . $request->key . '%');
            });

            $product->orWhereHas('products.productName', function ($query1) use ($request) {
                $query1->where('name', 'LIKE', '%' . $request->key . '%');
            });
        }
        $data = $product->orderBy('created_at', 'DESC')->get()->toArray();
            if(count($data) > 0){
                return json_encode(array('list' => $data));
                
            }
           return json_encode(array('code' => 500));
    }

    public function orderList(Request $request) {

        $orders = Orders::with('orderItems', 'apiorder', 'ratings')->where('user_id', $request->retailerId)->whereDate('created_at', date('Y-m-d', strtotime($request->searchDate)))->orderBy('created_at', 'DESC')->get()->toArray();
        return response()->json(array('list' => $orders));
    }

    public function frequentOrderList(Request $request) {
        $frequent = DB::table('orders')
                ->join('order_items', 'order_items.order_id', '=', 'orders.id')
                ->join('products', 'products.id', '=', 'order_items.product_id')
                ->join('product_lists', 'product_lists.id', '=', 'products.product_lists_id')
                ->select('products.id', DB::raw('COUNT(products.id) AS frequent'))
                ->groupBy('products.id')
                ->orderBy('frequent', 'DESC')
                ->where('user_id', $request->retailerId)
                ->limit(10)
                ->get();

        $order = array();
        if (count($frequent)) {
            foreach ($frequent as $key => $val) {
                $prod = Product::with('productName')->where('id', $val->id)->first();
                $order['order'][] = array(
                    'id' => $val->id,
                    'count' => $val->frequent,
                    'name' => $prod->productName->name,
                    'mrp' => $prod->mrp,
                    'rlp' => $prod->rlp,
                );
            }
            return response()->json(array('status' => true, 'frequent' => $order));
        } else {
            $order['order'][] = array(
                'id' => '',
                'count' => '',
                'name' => '',
                'mrp' => '',
                'rlp' => '',
            );
            return response()->json(array('status' => false, 'frequent' => $order));
        }
    }

    public function callingScript($id) {
        $retailer = Retailer::where('id', $id)->first();
//        echo "<pre>";print_r($retailer);exit;
        $callScript = CallCenterScript::with('shop', 'call')->where('shop_type', $retailer->shop_type)->where('retailer_type', $retailer->is_prime)->first()->toArray();
        if (count($callScript) > 0) {
            return response()->json(['status' => true, 'list' => $callScript]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function retailerQueue(Request $request) {


        return view('pages.agent.queue-retailer');
    }

    public function getRetailersQueueList(Request $request) {
        # code...
        $helper = new Helpers;
        $day = $helper->getNumberFromDay();
        $retailerOrderCalling = new \App\Repository\RetailersQueueRepository;

        $listsOfQueues = $retailerOrderCalling->getQueueList($day);
        $lists = array();
        foreach ($listsOfQueues as $listsOfQueue) {
            $lists[] = array(
                'startTime' => $listsOfQueue->start_time,
                'endTime' => $listsOfQueue->end_time,
                'orderLimit' => $listsOfQueue->order_limit,
                'retailers' => $listsOfQueue->retailers,
            );
        }
        return response()->json(['status' => true, 'lists' => $lists], 200);
    }

    public function saveRetailerOrderCalling(Request $request) {
//     print_r($request->startTime);exit;
     
        $request->validate([
            'days' => 'required',
            'orderLimit' => 'required',
            'startTime' => 'required',
            'endTime' => 'required',
        ]);
          
          $time1 = $request->startTime['hh'] . ':' . $request->startTime['mm']. ':' . '00 ' . $request->startTime['a'] ;
          $time2 = $request->endTime['hh'] . ':' . $request->endTime['mm']. ':' . '00 ' . $request->endTime['a'] ;
         
        $isExist = RetailerOrderCalling::where('retailer_id', $request->retailerId)->get();
        if (count($isExist) > 0) {
            $set = RetailerOrderCalling::where('retailer_id', $request->retailerId)->update([
                'calling_days' => $request->days,
                'order_limit' => $request->orderLimit,
                'start_time' => date('h:m a',strtotime($time1)),
                'end_time' => date('h:m a',strtotime($time2)),
                'calling_option' => 1,
                'source' => 1,
            ]);
        } else {
            $set = RetailerOrderCalling::create([
                        'retailer_id' => $request->retailerId,
                        'calling_days' => $request->days,
                        'order_limit' => $request->orderLimit,
                        'start_time' => $request->startTime,
                        'end_time' => $request->endTime,
                        'calling_option' => 1,
                        'source' => 1,
            ]);
        }


        return response()->json(['status' => 'true']);
    }

}
