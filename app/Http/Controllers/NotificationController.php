<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\NotificationRepository;
class NotificationController extends Controller
{
    //

    /*
    *   this is for get notifications
    *
    **/
    public function getNotifications() {
        $notificationRepository = new NotificationRepository;
        $notifications = $notificationRepository->getUnreadNotifications();

        return response()->json([
            'status' => true,
            'count' => $notifications->count(),
            'list' => $notifications->splice(0,10)
        ]);
    }

    public function viewNotification(){
        $notificationRepository = new NotificationRepository;
        $notifications = $notificationRepository->updateAdminView();
       return response()->json([
           'status' => true
       ]);
    }

    public function getHubNotifications(){
        
        
        $gaurd = \Auth::getDefaultDriver();
        $userId = \Auth::guard($gaurd)->user()->id;
        $notificationRepository = new NotificationRepository;
        $notifications = $notificationRepository->getHubUnreadNotifications($userId);
        return response()->json([
            'status' => true,
            'count' => $notifications->count(),
            'list' => $notifications->splice(0,10)
        ]);

    }
    public function getHubThreshNotifications(){
        
        
        $gaurd = \Auth::getDefaultDriver();
        $userId = \Auth::guard($gaurd)->user()->id;
        $notificationRepository = new NotificationRepository;
        $notifications = $notificationRepository->getHubUnreadThresholdNotifications($userId);
//        echo "<pre>";
//        print_r($notifications);exit;
        return response()->json([
            'status' => true,
            'count' => $notifications->count(),
            'list' => $notifications->splice(0,10)
        ]);

    }

    public function viewHubNotification(){
       
        $gaurd = \Auth::getDefaultDriver();
        $userId = \Auth::guard($gaurd)->user()->id;
        $notificationRepository = new NotificationRepository;
        $notifications = $notificationRepository->getHubUnreadNotifications($userId)->toArray();
        $notificationsStock = $notificationRepository->getHubUnreadThresholdNotifications($userId)->toArray();
       
        if(!count($notifications) && !count($notificationsStock))
            return response()->json([
                'status' => true
            ]);
           if(count($notifications) > 0){
               $ids = array_column($notifications,'id');
               $update = $notificationRepository->updateHubView($ids);
           }
           if(count($notificationsStock) > 0){
               $ids = array_column($notificationsStock,'id');
               $update = $notificationRepository->updateHubView($ids);
           }
        
        
       return response()->json([
           'status' => true,
           'ids' => $ids
       ]);
    }
}
