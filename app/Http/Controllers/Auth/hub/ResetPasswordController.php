<?php

namespace App\Http\Controllers\Auth\hub;

use Auth;
use Password;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /**
     * This will do all the heavy lifting
     * for resetting the password.
     */
    use ResetsPasswords;

     /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Only guests for "admin" guard are allowed except
     * for logout.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:hub');
    }

    /**
     * Show the reset password form.
     * 
     * @param  \Illuminate\Http\Request $request
     * @param  string|null  $token
     * @return \Illuminate\Http\Response
     */
    public function showResetForm(Request $request, $token = null){
        return view('hub.passwords.reset',[
            'title' => 'Reset Hub Password',
            'passwordUpdateRoute' => 'hub.password.update',
            'token' => $token,
            'email' => $request->email,
        ]);
    }
    
    protected function sendResetResponse(Request $request) {
        return redirect($this->redirectPath())
                        ->with(['status' => 'Your password reset successfully','hub' => 'hub']);
    }

    protected function sendResetFailedResponse(Request $request) {
        return redirect()->back()
                        ->withInput($request->only('email'))
                        ->withErrors(['email' => 'Your password not reset successfully','hub' => 'hub']);
    }
    

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    protected function broker(){
        return Password::broker('hubs');
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard(){
        return Auth::guard('hub');
    }
}
