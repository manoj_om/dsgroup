<?php

namespace App\Http\Controllers\Auth\hub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VerificationController extends Controller
{
    /**
     * Create a controller instance.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:hub');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify','resend');
    }

    /**
     * Show the email verification notice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return $request->user('hub')->hasVerifiedEmail()
            ? redirect()->route('hub-dashboard')
            : view('hub.verify',[
                'resendRoute' => 'hub.verification.resend',
            ]);
    }

    /**
     * Verfy the user email.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify(Request $request)
    {
        if ($request->route('id') != $request->user('hub')->getKey()) {
            //id value doesn't match.
            return redirect()
                ->route('hub.verification.notice')
                ->with('error','Invalid user!');
        }

        if ($request->user('hub')->hasVerifiedEmail()) {
            return redirect()
                ->route('hub-dashboard');
        }

        $request->user('hub')->markEmailAsVerified();

        return redirect()
            ->route('hub-dashboard')
            ->with('status','Thank you for verifying your email!');
    }

    /**
     * Resend the verification email.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resend(Request $request)
    {
        if ($request->user('hub')->hasVerifiedEmail()) {
            return redirect()->route('hub-dashboard');
        }

        $request->user('hub')->sendEmailVerificationNotification();

        return redirect()
            ->back()
            ->with('status','We have sent you a verification email!');
    }

}
