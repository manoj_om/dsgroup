<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Route;
use App\CallingAgent;

class AgentLoginController extends Controller {

    public function __construct() {
//       echo "in";exit;
//        $this->middleware('auth:agent', ['except' => ['logout']]);
//         echo "i2222n";exit;
        $this->middleware('guest:agent', ['except' => ['logout']]);
    }

    public function showLoginForm() {

        return view('pages.callingagent.login');
    }

    public function login(Request $request) {
        // Validate the form data
        $validator = \validator($request->all(),[
            'email' => 'required|email|exists:calling_agents|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ],[
            'email.exists' => 'These credentials do not match our records.',
        ]);

        if($validator->fails()){
            return redirect()->back()->withInput($request->only('email', 'remember', 'password'))->withErrors(
                ['messege' => $validator->errors()->first(), 'agent' => 'agent']);
        }


        // Attempt to log the user in
        if (Auth::guard('agent')->attempt(['email' => $request->email, 'password' => $request->password, 'status' => 0], $request->remember)) {

            CallingAgent::where('id', Auth::guard('agent')->user()->id)->update([
                'login_status' => 1
            ]);

            return redirect()->route('agent-dashboard');
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withInput($request->only('email', 'remember', 'password'))->withErrors(['messege' => 'Your Account is inactive', 'agent' => 'agent']);
    }

    public function logout() {

        CallingAgent::where('id', Auth::guard('agent')->user()->id)->update([
            'login_status' => 0
        ]);

        Auth::guard('agent')->logout();
        return redirect('/login');
    }

    private function validator(Request $request) {
        //validation rules.
        $rules = [
            'email' => 'required|email|exists:calling_agents|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ];


        //custom validation error messages.
        $messages = [
            'email.exists' => 'These credentials do not match our records.',
        ];

        //validate the request.
        $request->validate($rules, $messages);
    }

    private function checkStatus(Request $request) {
        $isactive = CallingAgent::where('email', $request->email)->first();
        if ($isactive->status == 1) {
            return response()->json(['isActive' => 500, 'msg' => 'Your account is not active']);
        }
    }

}
