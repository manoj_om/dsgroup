<?php

namespace App\Http\Controllers\Auth\Agent;

use Auth;
use Password;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * Only guests for "admin" guard are allowed except
     * for logout.
     * 
     * @return void
     */
    public function __construct()
    {
     
        $this->middleware('guest:agent');
    }

    /**
     * Show the reset email form.
     * 
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm(){
        return view('agent.passwords.email',[
            'title' => 'Agent Password Reset',
            'passwordEmailRoute' => 'agent.password.email'
        ]);
    }
    
     public function sendResetLinkResponse(Request $request) {
       
        return back()->with('status', 'Your password is sent on your mobile number');
    }

    public function sendResetLinkFailedResponse(Request $request) {
        return back()->withInput($request->only('email'))
                        ->withErrors(['email' => 'Your Email is not correct']);
    }

    /**
     * password broker for admin guard.
     * 
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker(){
        return Password::broker('agents');
    }

    /**
     * Get the guard to be used during authentication
     * after password reset.
     * 
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    public function guard(){
        return Auth::guard('agent');
    }
}
