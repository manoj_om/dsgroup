<?php

namespace App\Http\Controllers\Auth\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VerificationController extends Controller
{
    /**
     * Create a controller instance.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:agent');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify','resend');
    }

    /**
     * Show the email verification notice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return $request->user('agent')->hasVerifiedEmail()
            ? redirect()->route('agent-dashboard')
            : view('agent.verify',[
                'resendRoute' => 'agent.verification.resend',
            ]);
    }

    /**
     * Verfy the user email.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify(Request $request)
    {
        if ($request->route('id') != $request->user('agent')->getKey()) {
            //id value doesn't match.
            return redirect()
                ->route('agent.verification.notice')
                ->with('error','Invalid user!');
        }

        if ($request->user('agent')->hasVerifiedEmail()) {
            return redirect()
                ->route('agent-dashboard');
        }

        $request->user('agent')->markEmailAsVerified();

        return redirect()
            ->route('agent-dashboard')
            ->with('status','Thank you for verifying your email!');
    }

    /**
     * Resend the verification email.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resend(Request $request)
    {
        if ($request->user('agent')->hasVerifiedEmail()) {
            return redirect()->route('agent-dashboard');
        }

        $request->user('agent')->sendEmailVerificationNotification();

        return redirect()
            ->back()
            ->with('status','We have sent you a verification email!');
    }

}
