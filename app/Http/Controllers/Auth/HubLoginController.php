<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Route;
use App\Distributor;


class HubLoginController extends Controller {

    protected $guard = 'hub';

    public function __construct() {

//        echo "hidd";exit;
//       echo "in";exit;
//        $this->middleware('auth:agent', ['except' => ['logout']]);
//         echo "i2222n";exit;
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm() {

        return view('pages.hub.login');
    }

    public function login(Request $request) {

        $validator = \validator($request->all(),[
            'email' => 'required|email|exists:distributors|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ],[
            'email.exists' => 'These credentials do not match our records.',
        ]);

        if($validator->fails()){
            return redirect()->back()->withInput($request->only('email', 'remember', 'password'))->withErrors(
                ['messege' => $validator->errors()->first(), 'hub' => 'hub']);
        }

        if (Auth::guard('hub')->attempt(['email' => $request->email, 'password' => $request->password,'status' => 1] ,$request->remember)) {
            // if successful, then redirect to their intended location
//             echo "in";exit;



            return redirect()->route('hub-dashboard');
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withInput($request->only('email', 'remember','password','status'))
        ->withErrors(['messege' => 'Your Hub Account is inactive','hub' => 'hub']);
    }

    private function checkStatus(Request $request)
    {
        $isactive = Distributor::where('email',$request->email)->first();
        if($isactive->status == 0){
            return response()->json(['isActive' => 500,'msg' => 'Your account is not active']);
        }

    }

    public function logout() {
        Auth::guard('hub')->logout();
        return redirect('/login');
    }

    private function validator(Request $request) {
        //validation rules.
        $rules = [
            'email' => 'required|email|exists:distributors|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ];

        //custom validation error messages.
        $messages = [
            'email.exists' => 'These credentials do not match our records.',
        ];

        //validate the request.
        $request->validate($rules, $messages);
    }

}
