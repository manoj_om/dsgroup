<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\CallingAgent;

class CallcenterController extends Controller {

    public function __construct() {
//        $this->middleware('auth:agent');
        $this->middleware('auth');
    }

    public function index(){

      return view('pages.callcenter.index');
    }
    public function call_queue(){

      return view('pages.callcenter.call_queue');
    }
    public function agent_status(){

      return view('pages.callcenter.agent_status');
    }
    public function changeAgentLoginStatus(Request $request){

    $update = CallingAgent::where('id',$request->aid)->update([
        'login_status' => $request->arg
     ]);

    if($update){
        return json_encode(array('code' => 200));
    }else{
         return json_encode(array('code' => 500));
    }
    }

    public function getRetailersQueueList(Request $request) {
        # code...
        $helper = new \App\Helpers\Helpers;
        $day = $helper->getNumberFromDay();
        $retailerOrderCalling = new \App\Repository\RetailersQueueRepository;

        $listsOfQueues = $retailerOrderCalling->getQueueList($day);
        $lists = array();
        foreach ($listsOfQueues as $listsOfQueue) {
            $lists[] = array(
                'startTime' => $listsOfQueue->start_time,
                'endTime' => $listsOfQueue->end_time,
                'orderLimit' => $listsOfQueue->order_limit,
                'retailers' => $listsOfQueue->retailers,
            );
        }
        return response()->json(['status' => true, 'lists' => $lists], 200);
    }

}
