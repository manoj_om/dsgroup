<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\CallingAgent;
use Intervention\Image\Facades\Image as Image;

class CallingagentController extends Controller {

    public function __construct() {
//        $this->middleware('auth');
    }

    public function index() {

        return view('pages.callingagent.index');
    }

    public function addCallcenterAgent(Request $request) {

        $request->validate([
            'avtar' => 'nullable|image64:jpeg,jpg,png',
//            'caller_type' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'supervisor_name' => 'required',
            'executive_email' => 'required|email|unique:calling_agents,email',
            'executive_mobile' => 'required|unique:calling_agents,mobile_number|max:10',
            'executive_code' => 'required|unique:calling_agents',
            'executive_name' => 'required',
        ]);
        $name = '';
        if ($request->get('avtar')) {
            $image = $request->get('avtar');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('avtar'))->save(public_path('images/callingAgent/') . $name);
        }
        $pass = Str::random(8);
        $image = new CallingAgent();
        $image->avtar = $name;
        $image->name = $request->executive_name;
        $image->executive_code = $request->executive_code;
        $image->serial_number = Str::random(5) . time();
        $image->mobile_number = $request->executive_mobile;
        $image->email = $request->executive_email;
        $image->supervisor_name = $request->supervisor_name;
        $image->user_name = 'test';
        $image->password = Hash::make($pass);
        $image->shift_start_time = $request->start_time;
        $image->shift_end_time = $request->end_time;
        $image->shift_end_time = $request->end_time;
        $image->caller_type = 1;
        $image->created_by = Auth::user()->id;

        $image->save();
        $message_body= urlencode("You have been successfully added as Calling Agent user on DS Group portal.
        Please use this password $pass to login on portal ".url('/'));
        $message="&send_to=".$request->mobile."&msg=$message_body";
       \App\Helpers\Helpers::sendSMS(config('api.smsurl').$message);
        return response()->json(['success' => 'You have successfully added calling agent'], 200);
    }

    public function getCallingAgents(Request $request) {

        $agents = CallingAgent::orderBy('created_at', 'DESC');
        if(isset($request->status)){
            $agents->where('status',$request->status);
        }
        if (isset($request->key) && $request->key != '') {
            $agents->where(function($query) use ($request) {
                return $query->orWhere('name', 'LIKE', '%' . $request->key . '%')
                ->orWhere('email', 'LIKE', '%' . $request->key . '%')
                ->orWhere('mobile_number', 'LIKE', '%' . $request->key . '%')
                ->orWhere('executive_code', 'LIKE', '%' . $request->key . '%')
                ->orWhere('supervisor_name', 'LIKE', '%' . $request->key . '%');
            });
        }
        $agents = $agents->paginate(15);


        if (count($agents) > 0) {

            return response()->json(['code' => 200, 'list' => $agents]);
        } else {

            return response()->json(['code' => 500]);
        }
    }

    public function disableCallingAgents(Request $request) {

        $users = CallingAgent::whereIn('id', $request->list)
                ->update([
            'status' => 1
        ]);
        if ($users) {
            echo json_encode(array('code' => 200));
        } else {
            echo json_encode(array('code' => 500, 'msg' => 'User not enable successfully'));
        }
    }

    public function enableCallingAgents(Request $request) {
        $users = CallingAgent::whereIn('id', $request->list)
                ->update([
            'status' => 0
        ]);
        if ($users) {
            return json_encode(array('code' => 200));
        } else {
            return json_encode(array('code' => 500, 'msg' => 'User not enable successfully'));
        }
    }

}
