<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceFcmToken extends Model
{
    //
    protected $table = 'device_fcm_tokens';

    protected $fillable = ['user_id','token','device_type'];
}
