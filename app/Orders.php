<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\PromocodeTrait;
use Carbon\Carbon;
use App\OrderStatusSetting;

class Orders extends Model {

    //
    protected $table = 'orders';
    //protected $date = ['delivery_time', 'created_at', 'updated_at'];
    protected $fillable = [
        'order_by', 'user_id', 'promocode',
        'discount_amount', 'total_amount', 'status',
        'order_number', 'promocode_key', 'delivery_time', 'created_at',
         'placed_by', 'packed_time', 'dispatch_time', 'delivered_time','hub_id','promocode_msg'
    ];

    protected $appends = ['delivery_slot'];



    use PromocodeTrait;

    public function orderItems() {

        return $this->hasMany('App\OrderItems', 'order_id', 'id')->with('products');
    }

    public function orderItemsView() {

        return $this->hasMany('App\OrderItems', 'order_id', 'id')->with('products_detail');
    }

    public function retailer() {

        return $this->hasOne('App\Retailer', 'id', 'user_id')->with('MapDistributor');
    }

    public function hub() {

        return $this->hasOne('App\Retailer', 'id', 'user_id')->with('StockMapDistributor');
    }

    public function hubs() {

        return $this->hasOne('App\MapDistributor', 'retailer_id', 'user_id');
    }
    public function mappedhub() {

        return $this->hasOne('App\Distributor', 'id', 'hub_id');
    }

    public function apiorder() {

        return $this->hasOne('App\ApiOrders', 'orders_id', 'id');
    }

    public function apiOrders() {
        return $this->hasOne('App\ApiOrders');
    }

    public function ratings() {
        return $this->hasOne('App\ProductReview', 'order_id', 'id');
    }

    public function findTimeSlot($param) {
        $time = Carbon::parse($param)->format('H:i');

        if($time <= Carbon::parse('12:00')->format('H:i') && $time >= Carbon::parse('09:00')->format('H:i')){
            return "09 AM - 12PM";
        }
       if($time <= Carbon::parse('15:00')->format('H:i') && $time >= Carbon::parse('12:00')->format('H:i')){
            return "12 PM- 03 PM";
        }
        if($time <= Carbon::parse('18:00')->format('H:i') && $time >= Carbon::parse('15:00')->format('H:i')){
            return "03 PM- 06 PM";
        }
        if($time <= Carbon::parse('12:00')->format('h:i') && $time >= Carbon::parse('09:00')->format('h:i')){
            return "09 AM - 12PM";
        }
       if($time <= Carbon::parse('15:00')->format('h:i') && $time >= Carbon::parse('12:00')->format('h:i')){
            return "12 PM- 03 PM";
        }
        if($time <= Carbon::parse('18:00')->format('h:i') && $time >= Carbon::parse('15:00')->format('h:i')){
            return "03 PM- 06 PM";
        }


    }
    public function timeDuration($changedate, $created, $type) {

        if ($created == null) {
            $created = date('d-m-Y H:i:s');
        }
        if ($changedate == null) {
            $changedate = date('d-m-Y H:i:s');
        }

        $setting = OrderStatusSetting::first();
          if($setting->order_to_packing == '' || $setting->packing_to_dispatch == ''|| $setting->dispatch_to_delivered == ''){
              $setting->order_to_packing = '00:00';
              $setting->packing_to_dispatch = '00:00';
              $setting->dispatch_to_delivered = '00:00';
          }
        if ($type == 1) {

            $exp = explode(":", $setting->order_to_packing);

            $h = $exp[0];
            $time = $exp[1];

            $d1 = Carbon::parse($created)->addHours($h)->addMinutes($time);
            $d2 = Carbon::parse($changedate);
//            return "created" . $created .' date1 ' .$d1.'  date2 ' .$d2 . 'diff '  . $setting->order_to_packing;
            if (strtotime($d1) > strtotime($d2)) {
                return false;
            } else {
                return true;
            }
        }

        if ($type == 2) {

            $exp = explode(":", $setting->packing_to_dispatch);

            $h = $exp[0];
            $time = $exp[1];

            $d1 = Carbon::parse($created)->addHours($h)->addMinutes($time);
            $d2 = Carbon::parse($changedate);


//            return 'date1 ' .strtotime($d1).'  date2 ' .strtotime($d2) . 'diff '  . $setting->packing_to_dispatch;
           if (strtotime($d1) > strtotime($d2)) {
               return false;
            } else {
                return true;
            }
        }
        if ($type == 3) {

            $exp = explode(":", $setting->dispatch_to_delivered);
            $h = $exp[0];
            $time = $exp[1];

            $d1 = Carbon::parse($created)->addHours($h)->addMinutes($time);
            $d2 = Carbon::parse($changedate);

//             return 'date1 ' .$d1.'  date2 ' .$d2 . 'diff '  . $setting->dispatch_to_delivered;
           if (strtotime($d1) > strtotime($d2)) {
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * Scope a query to only include active users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSales($query)
    {
        return $query->where('status', 1);
    }

    public function getDeliveryTimeAttribute($value)
    {
        # code...
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i');
    }

    public function getDeliverySlotAttribute(){
        $time = \Carbon\Carbon::parse($this->delivery_time)->format('H');
        if($time == '12')
            return '9AM-12PM';
        if($time == '15')
            return '12PM-3PM';
        if($time > '15' && $time <= '19')
            return '3PM-6PM';

        return 'No time slot available';
        // array('value' => '12:00','text' => '9AM-12PM'),
        // array('value' => '15:00', 'text' =>'12PM-3PM') ,
        // array('value' => '19:00','text' => '3PM-7PM')
        // return $this->delivery_time;
    }

}
