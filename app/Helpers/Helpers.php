<?php // Code within app\Helpers\Helper.php  use App\Helpers;

namespace App\Helpers;
use Illuminate\Support\Facades\Input;

class Helpers
{
    public static function getaddress($lat,$lng)
    {

        $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=true&key=AIzaSyDvGdNTcLTe333c_bdBk7gjXg2Yi7RSsBA';

        $json = @file_get_contents($url);
        $data=json_decode($json);
        $status = $data->status;

        if($status=="OK")
        {
            return $data->results[0]->formatted_address;
        }
        else
        {
            return 'google map api response not received';
        }
    }
    public static function getCityState($lat,$lng)
    {

        $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=true&key=AIzaSyDvGdNTcLTe333c_bdBk7gjXg2Yi7RSsBA';
//     $json = @file_get_contents($url);
//     $data=json_decode($json);
//     $status = $data->status;
//
//     if($status=="OK")
//     {
//       $array=array('city'=> isset($data->results[0]->address_components[4]->long_name)?$data->results[0]->address_components[4]->long_name:'','state'=>isset($data->results[0]->address_components[5]->long_name)?$data->results[0]->address_components[5]->long_name:'');
//       return $array;
//     }
//     else
//     {
//       return 'test';
//     }
        $file_contents = file_get_contents($url);
        $json_decode = json_decode($file_contents);
        
//        echo "<pre>";print_r($json_decode);exit;
        if(isset($json_decode->results[0])) {
            $response = array();
            foreach($json_decode->results[0]->address_components as $addressComponet) {
                if(in_array('administrative_area_level_1', $addressComponet->types)) {
                    $response['state'] = $addressComponet->long_name;
                }
                if(in_array('locality', $addressComponet->types)) {
                    $response['city'] = $addressComponet->long_name;
                }
            }
//              echo "<pre>";print_r($response);exit;
           return array('city'=> isset($response['city'])?$response['city']:'','state'=>isset($response['state'])?$response['state']:'');
            if(isset($response[0])){ $first  =  $response[0];  } else { $first  = 'null'; }
            if(isset($response[1])){ $second =  $response[1];  } else { $second = 'null'; }
            if(isset($response[2])){ $third  =  $response[2];  } else { $third  = 'null'; }
            if(isset($response[3])){ $fourth =  $response[3];  } else { $fourth = 'null'; }
            if(isset($response[4])){ $fifth  =  $response[4];  } else { $fifth  = 'null'; }
           

            if( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth != 'null' ) {
//        echo "<br/>Address:: ".$first;
//        echo "<br/>City:: ".$second;
//        echo "<br/>State:: ".$fourth;
//        echo "<br/>Country:: ".$fifth;
                return array('city'=> isset($second)?$second:'','state'=>isset($fourth)?$fourth:'');
            }
            else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth == 'null'  ) {
//        echo "<br/>Address:: ".$first;
//        echo "<br/>City:: ".$second;
//        echo "<br/>State:: ".$third;
//        echo "<br/>Country:: ".$fourth;
                return array('city'=> isset($second)?$second:'','state'=>isset($third)?$third:'');
            }
            else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth == 'null' && $fifth == 'null'  ) {
//        echo "<br/>City:: ".$first;
//        echo "<br/>State:: ".$second;
//        echo "<br/>Country:: ".$third;
                return array('city'=> isset($first)?$first:'','state'=>isset($second)?$second:'');
            }
            else if ( $first != 'null' && $second != 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'   ) {
//        echo "<br/>State:: ".$first;
//        echo "<br/>Country:: ".$second;
                return array('city'=> isset($first)?$first:'','state'=>isset($first)?$first:'');
            }
            else if ( $first != 'null' && $second == 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'   ) {
                //echo "<br/>Country:: ".$first;
                return array('city'=> isset($first)?$first:'','state'=>isset($first)?$first:'');
            }
        }
    }
    public static function sendSMS($smsurl){

        // $url="http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=9355111518&msg=%20We%20have%20received%20your%20registration%20form%20and%20it%20is%20under%20approval.%20Request%20Id%3A%20Id%20no-1234&msg_type=TEXT&userid=2000122649&auth_scheme=plain&password=9910800744&v=1.1&format=text&override_dnd=true";
        $json_data = @file_get_contents($smsurl);
        return 1;
    }
    public static function  barcodeScanner($barcode) {

        $obj=new Helpers();
        if (strlen($barcode) == 15) {
            $divideF = array(36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36);
            $placeValueFactor = array(1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2);
            $digit = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
            $weight = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35);
            $combine = array_combine($digit, $weight);


            $rid = str_split($barcode);
            $arrayPop = array_pop($rid);



            $ridPlaces = $obj->convertInNumber($rid);

            $ridWeights = $obj->getWeights($rid, $combine);

            //c*d
            $valueOfweightAndPlaceValue = $obj->getPlaceValueMultiplyWithWeight($ridWeights, $placeValueFactor);

            //e/f
            $getQuotient = $obj->getQuotient($divideF, $valueOfweightAndPlaceValue);

            $remider = $obj->getReminders($valueOfweightAndPlaceValue, $getQuotient);

            $sumOfTotal = $obj->sumOfTotal($getQuotient, $remider);

            $reminderFrom36 = ($sumOfTotal % 36);

            $remindeminusBy36 = (36 - $reminderFrom36);

            if ($remindeminusBy36 > 35) {
                return 0;
            }
            $isItO = $digit[$remindeminusBy36];
            if ($arrayPop == $isItO) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
        exit;
    }


    public function getReminders($valueOfweightAndPlaceValue, $getQuotient) {

        $remider = array();
        foreach ($valueOfweightAndPlaceValue as $k => $v) {
            $remider[] = $v - ($getQuotient[$k] * 36);
        }

        return $remider;
    }

    public function sumOfTotal($getQuotient, $remider) {

        $sumofTotal = array();
        foreach ($getQuotient as $k => $v) {
            $sumofTotal[] = $v + $remider[$k];
        }

        return array_sum($sumofTotal);
    }

    public function getQuotient($divideF, $valueOfweightAndPlaceValue) {
        $quotient = array();
        foreach ($valueOfweightAndPlaceValue as $key => $value) {
            $quotient[] = floor($value / $divideF[$key]);
        }
        return $quotient;
    }

    public function getPlaceValueMultiplyWithWeight($ridWeights, $placeValueFactor) {

        $values = array();
        foreach ($ridWeights as $k => $v) {
            $values[] = $v * $placeValueFactor[$k];
        }
        return $values;
    }

    public function convertInNumber($param) {


        $places = array();
        foreach ($param as $key => $value) {
            $places[] = $key + 1;
        }
        return $places;
    }

    public function getWeights($param, $combine) {

        $ridWeight = array();
        foreach ($param as $key => $val) {
            $ridWeight[] = $combine[$val];
        }
        return $ridWeight;
    }

    public static function generateOtp(){
        return mt_rand(100000, 999999);
    }
     public function getNumberFromDay()
     {
         # code...
         $days = array(
            'Mon' => '1' ,
            'Tue' => '2' ,
            'Wed' => '3' ,
            'Thu' => '4' ,
            'Fri' => '5' ,
            'Sat' => '6' ,

        );

            $day = \Carbon\Carbon::now()->format('D');
            if($day != 'Sun')
                return $days[$day];
            return 0;


        return $days;

    }

    public function count_digit($number) {
        return strlen($number);
    }

    public function divider($number_of_digits) {
          $tens="1";

        if($number_of_digits>8)
          return 10000000;

        while(($number_of_digits-1)>0)
        {
          $tens.="0";
          $number_of_digits--;
        }
        return $tens;
    }
    public function convertRupeeToLac(int $num){

        if($num == 0)
            return 0;
        $ext="";//thousand,lac, crore
        $number_of_digits = $this->count_digit($num); //this is call :)
        if($number_of_digits >3)
        {
            if($number_of_digits%2!=0)
                $divider=$this->divider($number_of_digits-1);
            else
                $divider=$this->divider($number_of_digits);
        }
        else
            $divider=1;

        $fraction=$num/$divider;
        $fraction=number_format($fraction,2);
        if($number_of_digits < 4)
            $ext= '';
        elseif($number_of_digits==4 ||$number_of_digits==5)
            $ext="k";
        elseif($number_of_digits==6 ||$number_of_digits==7)
            $ext="Lac";
        elseif($number_of_digits==8 ||$number_of_digits==9)
            $ext="Cr";
        else
            $ext="Cr";
        return $fraction." ".$ext;
    }


}

?>
