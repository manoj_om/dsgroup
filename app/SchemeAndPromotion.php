<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchemeAndPromotion extends Model {

    protected $attributes = [
        'status' => 0
    ];
    protected $fillable = [
        'scheme_name', 'to_date', 'from_date', 'avatar', 'eligibility', 'description', 'status', 'created_by'
    ];


}
