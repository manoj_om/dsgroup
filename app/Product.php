<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductCategory;
use App\ProductSubCategory;
use App\ProductList;
use App\PackingType;
use App\ProductUnitType;
use Illuminate\Support\Facades\Auth;
use App\ProductUnitWeight;
use Illuminate\Support\Facades\Session;
class Product extends Model {



    protected $attributes = [
        'status' => 0
    ];
    protected $appends = ['image_url','name','in_stock'];

    protected $fillable = [
      'is_featured','master_carton_height','master_carton_width','master_carton_length','master_carton_net_weight','master_carton_gross_weight', 'custmer_target','shelf_life','product_height','product_width','product_length','max_order_quantity','dlp','ean_code','hsn_code','manufacturer','brand_name','avtar','rlp','mrp','serial_number', 'product_lists_id', 'short_description', 'sku_number', 'package_unit', 'packaging', 'quantity','category', 'sub_category', 'created_by', 'status','product_unit_weight'
    ];


    public function unitType() {
        return $this->hasOne('App\ProductUnitType', 'id', 'package_unit');
    }

    public function weightType() {
        return $this->hasOne('App\ProductUnitWeight', 'id', 'product_unit_weight');
    }
    public function category() {
        return $this->hasOne('App\ProductCategory', 'id', 'category');
    }
    public function packingType() {
        return $this->hasOne('App\PackingType', 'id', 'packaging');
    }
    public function productName() {
        return $this->hasOne('App\ProductList', 'id', 'product_lists_id');
    }

    public function subcategory() {
        return $this->hasOne('App\ProductSubCategory', 'id', 'sub_category');
    }
    public function stock() {
        return $this->hasOne('App\HubStock', 'product_id', 'id')->where('distributor_id',Auth::guard('hub')->user()->id);
    }
    public function stockagent() {
        return $this->hasOne('App\HubStock', 'product_id', 'id')->where('distributor_id',Session::get('currentHub'));
    }

    public function findCategory($title) {
        if ($title != '') {
            $find = ProductCategory::where('name', $title)
                    ->orWhere('name', strtolower($title))
                    ->orWhere('name', 'LIKE', '%' . strtolower($title) . '%')
                    ->first();
            if ($find) {
                return $find->id;
            } else {
                $insert = ProductCategory::create([
                            'name' => $title,
                            'created_by' => Auth::user()->id,
                ]);
                if ($insert) {
                       return $insert->id;
                }
            }
        } else {
            return '';
        }
    }
    public function findPackingType($title) {
        if ($title != '') {
            $find = PackingType::where('name', $title)
                    ->orWhere('name', strtolower($title))
                    ->orWhere('name', 'LIKE', '%' . strtolower($title) . '%')
                    ->first();
            if ($find) {
                return $find->id;
            } else {
                $insert = PackingType::create([
                            'name' => $title,
                ]);
                if ($insert) {
                       return $insert->id;
                }
            }
        } else {
            return '';
        }
    }
    public function findUnitType($title) {
        if ($title != '') {
            $find = ProductUnitType::where('name', $title)
                    ->orWhere('name', strtolower($title))
                    ->orWhere('name', 'LIKE', '%' . strtolower($title) . '%')
                    ->first();
            if ($find) {
                return $find->id;
            } else {
                $insert = ProductUnitType::create([
                            'name' => $title,
                ]);
                if ($insert) {
                       return $insert->id;
                }
            }
        } else {
            return '';
        }
    }
    public function findUnitWeightType($title) {
        if ($title != '') {
            $find = ProductUnitWeight::where('name', $title)
                    ->orWhere('name', strtolower($title))
                    ->orWhere('name', 'LIKE', '%' . strtolower($title) . '%')
                    ->first();
            if ($find) {
                return $find->id;
            } else {
                $insert = ProductUnitWeight::create([
                            'name' => $title,
                ]);
                if ($insert) {
                       return $insert->id;
                }
            }
        } else {
            return '';
        }
    }
    public function findUnitWeightTypeName($title) {
        if ($title != '') {
            $find = ProductUnitWeight::where('name', $title)
                    ->orWhere('name', strtolower($title))
                    ->orWhere('name', 'LIKE', '%' . strtolower($title) . '%')
                    ->first();
            if ($find) {
                return $find->name;
            } else {
                $insert = ProductUnitWeight::create([
                            'name' => $title,
                ]);
                if ($insert) {
                       return $insert->id;
                }
            }
        } else {
            return '';
        }
    }
    public function findProductName($title,$cat,$subcat) {
        if ($title != '') {
            $find = ProductList::where('name', $title)
                    ->orWhere('name', strtolower($title))
                    ->orWhere('name', 'LIKE', '%' . strtolower($title) . '%')
                    ->first();
            if ($find) {
                return $find->id;
            } else {
                $insert = ProductList::create([
                            'name' => $title,
                            'category_id' => $cat,
                            'subcategory_id' => $subcat,
                            'created_by' => Auth::user()->id,
                ]);
                if ($insert) {
                       return $insert->id;
                }
            }
        } else {
            return '';
        }
    }
    public function findCategoryId($title) {
        if ($title != '') {
            $find = ProductCategory::where('name', $title)
                    ->orWhere('name', strtolower($title))
                    ->orWhere('name', 'LIKE', '%' . strtolower($title) . '%')
                    ->first();
            if ($find) {
                return $find->id;
            }
        }
    }

    public function findSubCategory($title,$productid) {
        if ($title != '') {
            $find = ProductSubCategory::where('name', $title)
                    ->orWhere('name', strtolower($title))
                    ->orWhere('name', 'LIKE', '%' . strtolower($title) . '%')
                    ->first();
            if ($find) {
                return $find->id;
            } else {
                $insert = ProductSubCategory::create([
                            'name'   => $title,
                            'category' => $productid,
                            'created_by' => Auth::user()->id,
                ]);
                if ($insert) {
                    return $insert->id;
                }
            }
        } else {
            return '';
        }
    }

    public function getImageUrlAttribute(){
        if(!$this->avtar)
            return NULL;
        if(file_exists('images/product/'.$this->avtar))
            return url('images/product/'.$this->avtar);
        return NULL;
    }

    public function getNameAttribute(){

        $productName = $this->hasOne('App\ProductList','id','product_lists_id')->select('name')->first();
        $name = $productName->name." ";
        if($this->package_unit != 1){
            $unitTypeRelation = $this->hasOne('App\ProductUnitType','id','package_unit')->first();
            $name  .= !empty($unitTypeRelation) ? $unitTypeRelation->name.' ' : '';
        }

        $packingType = $this->packingType()->first();
        if(!empty($packingType))
            $name .= $packingType->name;


        return  $name;
    }
    public function getInStockAttribute(){
        $stockQuantity =  $this->hasOne('App\HubStock', 'product_id', 'id')->first();
        if(!empty($stockQuantity))
            return (int) $stockQuantity->product_quantity+(int)$stockQuantity->in_stock-(int) $stockQuantity->out_stock;
        return 0;
    }

    public function associateProduct(){
        return $this->hasMany('App\ProductList','id','product_lists_id')->where('status',0);
    }

    public function inWishlist(){
        return $this->hasOne('App\Wishlist')->where('user_id',request()->user()->id);
    }

    public function unitWeight(){
        return $this->hasOne('App\ProductUnitWeight','id','product_unit_weight')->select('name');
    }


}
