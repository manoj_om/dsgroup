<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapAgent extends Model
{
    //
     protected $fillable = ['agent_id', 'retailer_id'];
    public function retailer() {
        return $this->hasOne('App\Retailer', 'id', 'retailer_id');
    }
    
    public function agentData() {
        return $this->hasOne('App\CallingAgent', 'id', 'agent_id');
    }
}
