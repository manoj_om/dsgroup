<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkuLineSoldPerCallScheme extends Model
{
    protected $fillable = [

       'channel_type', 'retailer_type', 'status','applied_location','free_secondary_product','scheme_name','from_date','to_date','promocode','minimum_order_value','apply_discount','fixed_amount_percentage','shop_type','max_time_applicability','frequency'

    ];

}
