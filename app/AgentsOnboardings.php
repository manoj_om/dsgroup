<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;


class AgentsOnboardings extends Model
{
    use Sortable;
    public $sortable = ['id', 'name', 'agency_name', 'created_at', 'updated_at'];

    public function retailadd()
    {
        return $this->hasMany('App\Retailer', 'user_id', 'id')->with('MapDistributor');
    }
    /**
     * Scope a query to only include active users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('activate', 0)->where('is_approve',1);
    }



}
