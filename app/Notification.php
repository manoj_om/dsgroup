<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //

    protected $table = 'notifications';

    protected $fillable = [
        'type','user_id','action_by','action_user_gaurd','data',
        'retailer_message','hub_message','admin_message',
        'has_admin_view','has_admin_read','has_retailer_view',
        'has_retailer_read','has_hub_view','has_hub_read'
    ];


    public function getTypeAttribute($value){
        return __('api/notification.code.'.$value);
    }

}
