<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HubStock extends Model
{



    protected $attributes = [
        'status' => 0,
        'product_quantity' => 0
    ];
    protected $fillable = [
        'product_id','distributor_id','product_quantity','status','in_stock','out_stock','threshold_quantity_one','threshold_quantity_two','current_quantity'
    ];




}
