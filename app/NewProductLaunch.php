<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewProductLaunch extends Model
{


    protected $fillable = [

        'channel_type','retailer_type', 'status','applied_location','scheme_name','from_date','to_date','promocode','minimum_order_value','fixed_amount_percentage','shop_type','frequency','max_time_applicability'

    ];


}
