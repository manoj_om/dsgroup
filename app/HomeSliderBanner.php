<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeSliderBanner extends Model
{


  protected $fillable = [
        'title', 'url', 'avatar','created_by','status','banner_type'
    ];
  protected $appends = [
    'banner_url'
  ];

    public  function getBannerUrlAttribute(){
        return url('images/banners/'.$this->avatar);
    }


}
