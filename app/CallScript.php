<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallScript extends Model
{
    protected $fillable = ['shop_type','retailer_type','call_type','script_level_type','event_type','script','status'];


    public function shop() {
        return $this->hasOne('App\ShopType', 'id', 'shop_type');
    }
    public function call() {
        return $this->hasOne('App\CallType', 'id', 'call_type');
    }
    public function event() {
        return $this->hasOne('App\EventType', 'id', 'event_type');
    }
    public function level() {
        return $this->hasOne('App\ScriptLevelType', 'id', 'script_level_type');
    }





}
