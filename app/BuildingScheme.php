<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuildingScheme extends Model
{
     protected $fillable = [
        'scheme_name','from_date','to_date', 'primary_product_1', 'primary_product_2', 'secondary_product', 'apply_discount', 'fixed_amount','promo_code','applied_location','status'
    ];

}
