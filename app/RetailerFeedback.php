<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RetailerFeedback extends Model
{


     protected $fillable = [
        'retailer_id','agent_id','feedback_id','status'
    ];

     public function retailer() {
        return $this->hasOne('App\Retailer', 'id', 'retailer_id');
    }
     public function feedback() {
        return $this->hasOne('App\FeedbackRemark', 'id', 'feedback_id');
    }
     public function agent() {
        return $this->hasOne('App\CallingAgent', 'id', 'agent_id');
    }


}
