<?php

namespace App\Traits;
use Illuminate\Http\Request;
use App\Repository\CartRepository;
use App\Repository\ProductRepository;
use App\Repository\UnitWeightRepository;

trait CartTrait
{

    /***
     * added by niraj lal rahi
     * @param  Illuminate\Http\Request, App\Repository\CartRepository
     * @return mixed
     * function is used to add product in cart
     * */

    public function addToCart(Request $request,CartRepository $cartRepository,ProductRepository $productRepository){

        try {
            $validator = $this->validateRequest($request);
            if ($validator->fails()) {

                return $this->apiResponse([
                    'status' => false,
                    'message' => $validator->errors(),
                ], __('api/responseCode.bad_request'));

            }
            $userId = request()->user()->id;

            $requestProduct = $request->post('product_id');
            $requestQty = $request->post('quantity');

            $mapDistributor = new \App\MapDistributor;
            $userDetail = $mapDistributor->where('retailer_id',$userId)->first();

            $checkIfProductExist = $productRepository->checkIfProductExist($requestProduct,$userDetail->distributor_id);
            if($checkIfProductExist->count() != count($requestProduct))
                return $this->apiResponse([
                    'status' => false,
                    'message' => 'Invalid product in the lists',
                ], __('api/responseCode.bad_request'));


            $createArray = array();
            foreach ($requestProduct as $key => $value){
                $requestData['product_id'] = $value;
                $requestData['quantity'] = $requestQty[$key];
                $requestData['user_id'] = $userId;
                $requestData['created_at'] = date('Y-m-d H:i:s');
                if($isAlreadyAdded = $cartRepository->validCartItemOfUser($where = array('product_id' => $value,'user_id' => $requestData['user_id'])))
                    $cartRepository->update($isAlreadyAdded->id,$requestData);
                else
                    $createArray[] = $requestData;

            }

            $addToCart = $cartRepository->insertBulk($createArray);
            $message = !empty($createArray) ? "Product successfully added to cart" : "Product already added to cart" ;

            if($addToCart)
                return $this->apiResponse([
                    'status'=> true,
                    'message' => $message,
                ],__('api/responseCode.success'));

            return $this->apiResponse([
                'status' => false,
                'message' => 'Failed to add into cart'
            ], __('api/responseCode.failed'));
        }catch (\Exception $exception){

            return $this->apiResponse([
                'status'=> false,
                'message' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }
    }

    /***
     * added by niraj lal rahi
     * @param  Illuminate\Http\Request
     * @return mixed
     * function is used to validateRequest for the product add to cart with quantity
     * */
    protected function validateRequest(Request $request){
        return \Validator($request->post(),[
            'product_id' => 'required | array ',
            'quantity' => 'required | array',
            'product_id.*' => 'required | numeric | min : 1',
            'quantity.*' => 'required | numeric | min : 1'
        ],[
            'product_id.required' => 'Product is not defined',
            'quantity.required' => 'Product quantity is not defined',
            'quantity.min' => 'Please enter valid quantity of product',
            'product_id.min' => 'Please enter valid product'
        ]);
    }

    /***
     * added by niraj lal rahi
     * @param  Illuminate\Http\Request,App\Repository\CartRepository
     * @return mixed
     * function is used to list all the products added into the cart of
     * */
    public function listCartItems(Request $request,CartRepository $cartRepository,UnitWeightRepository $unitWeightRepository){

        try{
            $userId = request()->user()->id;
            $lists = $cartRepository->getCartItemFromUserId($userId);
            $mrpTotal = 0;
            $rlpTotal = 0;
            foreach ($lists as $list){
                $mrpTotal += isset($list->products->mrp) ? $list->products->mrp*$list->quantity : 0;
                $rlpTotal += isset($list->products->rlp) ? $list->products->rlp*$list->quantity : 0;
            }
            $attributes = $unitWeightRepository->getAll();
            return $this->apiResponse([
                'status'=> true,
                'message' => count($lists).' Records found',
                'mrpTotal' =>  $mrpTotal,
                'rlpTotal' => $rlpTotal,
                'unitWeights' => $attributes,
                'data' => $lists
            ],__('api/responseCode.success'));

        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }

    }
    /***
     * added by niraj lal rahi
     * @param  Illuminate\Http\Request,App\Repository\CartRepository
     * @return mixed
     * function is used to update quantity of any product added into the cart
     * */
    public function updateQuantity(Request $request,CartRepository $cartRepository){
        try{

            $validator = $this->validateQuantity($request);
            if ($validator->fails()) {

                return $this->apiResponse([
                    'status' => false,
                    'message' => $validator->errors(),

                ], __('api/responseCode.bad_request'));

            }
            $requestData = array();
            $requestData['quantity'] = $request->post('quantity');
            $userId = request()->user()->id;
            $cartId = $request->post('cart_id');
            $isValidCartId = $cartRepository->validCartItemOfUser($where = array('id' => $cartId,'user_id' => $userId));

            if(!$isValidCartId)
                return $this->apiResponse([
                    'status'=> false,
                    'message' => 'Product not found',
                ],__('api/responseCode.server_error'));

            $updateQuantity = $cartRepository->update($cartId,$requestData);
            if($updateQuantity)
                return $this->apiResponse([
                    'status' => true,
                    'message' => 'Successfully updated item quantity'
                ], __('api/responseCode.success'));

            return $this->apiResponse([
                'status' => false,
                'message' => 'Failed to update'
            ], __('api/responseCode.failed'));

        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Product not found',
                'error' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }
    }
    /***
     * added by niraj lal rahi
     * @param  Illuminate\Http\Request
     * @return mixed
     * function is used to validate request for update of quantity added in cart
     * */
    private function validateQuantity(Request $request){

        return \Validator($request->post(),[
            'cart_id' => 'required | numeric | min : 1 ',
            'quantity' => 'required | numeric | min : 1'
        ],[
            'cart_id.required' => 'Cart item is not defined',
            'quantity.required' => 'Product quantity is not defined',
            'cart_id.min' => 'Please enter valid cart id',
            'quantity.min' => 'Please enter valid quantity of product'
        ]);
    }
    /***
     * added by niraj lal rahi
     * @param  Illuminate\Http\Request,App\Repository\CartRepository
     * @return mixed
     * function is used to remove/delete product from cart
     * */
    public function removeProductFromCart($id,Request $request,CartRepository $cartRepository){
        try{
            $requestData['cart_id'] = $id;
            $validator = $this->validateRemoveProductRequest($requestData);
            if ($validator->fails()) {

                return $this->apiResponse([
                    'status' => false,
                    'message' => $validator->errors()->first(),

                ], __('api/responseCode.bad_request'));

            }
            $cartId = $requestData['cart_id'];
            $userId = request()->user()->id;

            $validUser = $cartRepository->validCartItemOfUser($where = array('id' => $cartId,'user_id' => $userId));
            if($validUser) {
                $removeProduct = $cartRepository->destroy($cartId);
                if ($removeProduct)
                    return $this->apiResponse([
                        'status' => true,
                        'message' => 'Product removed successfully from cart'
                    ], __('api/responseCode.success'));

                return $this->apiResponse([
                    'status' => false,
                    'message' => 'Failed to remove product from cart'
                ], __('api/responseCode.failed'));
            }
            return $this->apiResponse([
                'status' => false,
                'message' => 'Item not found'
            ], __('api/responseCode.unauthorized'));
        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Product does not exist in cart',
                'error' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }
    }
    /***
     * added by niraj lal rahi
     * @param  Illuminate\Http\Request
     * @return mixed
     * function is used to validate request for remove product from cart
     * */
    private function validateRemoveProductRequest(array $inputs){
        return \Validator($inputs,[
            'cart_id' => 'required | numeric | min:1',
        ],[
            'cart_id.required' => 'Cart item is not defined',
            'cart_id.min' => 'Please enter valid cart id'
        ]);
    }
}
