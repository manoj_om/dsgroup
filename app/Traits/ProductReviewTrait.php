<?php

namespace App\Traits;
use Illuminate\Http\Request;
use App\Repository\ProductReviewRepository;
trait ProductReviewTrait
{
    //

    public function storeReview(Request $request,ProductReviewRepository $productReviewRepository)
    {
        # code...

        try{
            $validate = $this->validateReview($request);
            if($validate->fails()){
                return $this->apiResponse([
                    'status'=> false,
                    'message' => $validate->errors()->first(),
                ],__('api/responseCode.failed'));
            }
            $where = array(
                'order_id' => $request->post('order_id'),
                'user_id' => request()->user()->id
            );
            $hasAlreadyReview = $productReviewRepository->getByCondition($where);
            if(count($hasAlreadyReview))
                return $this->apiResponse([
                    'status'=> false,
                    'message' => 'You have already given your feedback',
                ],__('api/responseCode.failed'));

            $inputs = array(
                'order_id' => $request->post('order_id'),
                'stars' => $request->post('stars'),
                'comment' => $request->post('comment'),
                'user_id' => request()->user()->id
            );

            if($review = $productReviewRepository->store($inputs)){
                return $this->apiResponse([
                    'status'=> true,
                    'review' => $review,
                    'message' => 'Your review submitted successfully.'
                ],__('api/responseCode.success'));
            }

        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Internal server error please try again.',
                'error' => 'Error : '. $exception->getMessage(),
            ],__('api/responseCode.server_error'));
        }
    }
    public function storeReviewPanel(Request $request,ProductReviewRepository $productReviewRepository)
    {
        # code...

        try{
            $validate = $this->validateReview($request);
            if($validate->fails()){
                return $this->apiResponse([
                    'status'=> false,
                    'message' => $validate->errors()->first(),
                ],__('api/responseCode.failed'));
            }
            $where = array(
                'order_id' => $request->post('order_id'),
                'user_id' => $request->post('user_id')
            );
            $hasAlreadyReview = $productReviewRepository->getByCondition($where);
            if(count($hasAlreadyReview))
                return $this->apiResponse([
                    'status'=> false,
                    'message' => 'You have already given your feedback',
                ],__('api/responseCode.failed'));

            $inputs = array(
                'order_id' => $request->post('order_id'),
                'stars' => $request->post('stars'),
                'comment' => $request->post('comment'),
                'user_id' => $request->post('user_id')
            );

            if($review = $productReviewRepository->store($inputs)){
                return $this->apiResponse([
                    'status'=> true,
                    'review' => $review,
                    'message' => 'Your review submitted successfully.'
                ],__('api/responseCode.success'));
            }

        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Internal server error please try again.',
                'error' => 'Error : '. $exception->getMessage(),
            ],__('api/responseCode.server_error'));
        }
    }

    public function validateReview(Request $request)
    {
        # code...
        return \validator($request->post(),[
            'user_id' => 'required|integer|min:1',
            'order_id' => 'required | integer|min:1',
            'stars' => 'required',
            'comment' => 'required'
        ]);
    }
}
