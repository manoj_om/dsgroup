<?php

namespace App\Traits;

use App\Repository\NewProductLaunchRepository;
use App\Repository\OrderedItemSchemeRepository;
use App\Repository\OrderRepository;
use App\Repository\OrderVolumeSchemeRepository;
use App\Repository\ProductRepository;
use App\Repository\SkuLineSoldPerCallSchemeRepository;
use App\SkuLineSoldPerCallScheme;
use App\Repository\RetailerRepository;
use App\Retailer;
use Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

trait PromocodeTrait {

    //
    public function getPromocodes() {

        $retailerRepository = new RetailerRepository(new Retailer);
        $userDetail = $retailerRepository->getById(request()->user()->id);

        $where = array(
            'shop_type' => $userDetail->shop_type,
            'applied_location' => $userDetail->city,
            'retailer_type' => $userDetail->is_prime,
            'channel_type' => '1'
        );
        $promocodes = array();
        $orderVolumePromocode = $this->orderVolumePromocode($where);

        if (count($orderVolumePromocode)) {
            foreach ($orderVolumePromocode as $value) {
                $promocodes[] = array('key' => 'orderVolume', 'value' => $value);
            }
        }


        $orderedItemPromocode = $this->orderedItemPromocode($where);

        if (count($orderedItemPromocode)) {
            foreach ($orderedItemPromocode as $value) {
                $promocodes[] = array('key' => 'orderItems', 'value' => $value);
            }
        }

        $skuLinePromocode = $this->skuLinePromocode($where);
        if (count($skuLinePromocode)) {
            foreach ($skuLinePromocode as $value) {
                $promocodes[] = array('key' => 'skuLine', 'value' => $value);
            }
        }

        $newProductLauncPromocode = $this->newProductLauncPromocode($where);
        if (count($newProductLauncPromocode)) {
            foreach ($newProductLauncPromocode as $value) {
                $promocodes[] = array('key' => 'productLaunch', 'value' => $value);
            }
        }

        return $promocodes;
    }

    public function getPromocodesAgentPanel($id) {

        $retailerRepository = new RetailerRepository(new Retailer);
        $userDetail = $retailerRepository->getById($id);


        $where = array(
            'shop_type' => $userDetail->shop_type,
            'applied_location' => $userDetail->city,
            'user_id' => $id,
            'retailer_type' => $userDetail->is_prime,
            'channel_type' => '2'
        );
        $promocodes = array();
        $orderVolumePromocode = $this->orderVolumePromocodePanel($where);

        if (count($orderVolumePromocode)) {
            foreach ($orderVolumePromocode as $value) {
                $promocodes[] = array('key' => 'orderVolume', 'value' => $value);
            }
        }


        $orderedItemPromocode = $this->orderedItemPromocodePanel($where);

        if (count($orderedItemPromocode)) {
            foreach ($orderedItemPromocode as $value) {
                $promocodes[] = array('key' => 'orderItems', 'value' => $value);
            }
        }

        $skuLinePromocode = $this->skuLinePromocodePanel($where);
        if (count($skuLinePromocode)) {
            foreach ($skuLinePromocode as $value) {
                $promocodes[] = array('key' => 'skuLine', 'value' => $value);
            }
        }

        $newProductLauncPromocode = $this->newProductLauncPromocodePanel($where);
        if (count($newProductLauncPromocode)) {
            foreach ($newProductLauncPromocode as $value) {
                $promocodes[] = array('key' => 'productLaunch', 'value' => $value);
            }
        }

        return $promocodes;
    }

    public function newProductLauncPromocode(array $where) {
        $promocodes = array();

        $newProductLaunchRepository = new NewProductLaunchRepository(new \App\NewProductLaunch);
        $orderRepository = new OrderRepository(new \App\Orders);

        $newProductLaunchLists = $newProductLaunchRepository->promocode($where);

        if (count($newProductLaunchLists)) {
            foreach ($newProductLaunchLists as $newProductLaunchList) {
                $params = array(
                    'userId' => request()->user()->id,
                    'fromDate' => \Carbon\Carbon::parse($newProductLaunchList->from_date)->format('Y-m-d'),
                    'toDate' => \Carbon\Carbon::parse($newProductLaunchList->to_date)->format('Y-m-d'),
                    'maxApplicableTime' => $newProductLaunchList->max_time_applicability,
                    'minimumOrderValue' => $newProductLaunchList->minimum_order_value,
                    'promocode' => $newProductLaunchList->promocode
                );
                $isApplicablePromo = $orderRepository->newProductLauncPromocode($params);
                if ($isApplicablePromo)
                    $promocodes[] = $newProductLaunchList->promocode;
            }
        }
        return $promocodes;
    }
    public function newProductLauncPromocodePanel(array $where) {
        $promocodes = array();

        $newProductLaunchRepository = new NewProductLaunchRepository(new \App\NewProductLaunch);
        $orderRepository = new OrderRepository(new \App\Orders);

        $newProductLaunchLists = $newProductLaunchRepository->promocode($where);

        if (count($newProductLaunchLists)) {
            foreach ($newProductLaunchLists as $newProductLaunchList) {
                $params = array(
                    'userId' => $where['user_id'],
                    'fromDate' => \Carbon\Carbon::parse($newProductLaunchList->from_date)->format('Y-m-d'),
                    'toDate' => \Carbon\Carbon::parse($newProductLaunchList->to_date)->format('Y-m-d'),
                    'maxApplicableTime' => $newProductLaunchList->max_time_applicability,
                    'minimumOrderValue' => $newProductLaunchList->minimum_order_value,
                    'promocode' => $newProductLaunchList->promocode
                );
                $isApplicablePromo = $orderRepository->newProductLauncPromocode($params);

//                print_r($isApplicablePromo);exit;
                if ($isApplicablePromo)
                    $promocodes[] = $newProductLaunchList->promocode;
            }
        }
        return $promocodes;
    }

    public function skuLinePromocode(array $where) {

        $skuLineSoldPerCallSchemeRepository = new SkuLineSoldPerCallSchemeRepository(new \App\SkuLineSoldPerCallScheme);
        $orderRepository = new OrderRepository(new \App\Orders);

        $promocodes = array();
        $skuLineLists = $skuLineSoldPerCallSchemeRepository->promocode($where);

        if (count($skuLineLists)) {
            $cartRepository = new \App\Repository\CartRepository(new \App\Cart);
            $cartLists = $cartRepository->getCartItemFromUserId(request()->user()->id)->toArray();
            $cartProduct = array_column($cartLists,'product_id');
            foreach ($skuLineLists as $skuLineList) {

                if(in_array($skuLineList->free_secondary_product,$cartProduct) ){

                    $params = array(
                        'userId' => request()->user()->id,
                        'fromDate' => \Carbon\Carbon::parse($skuLineList->from_date)->format('Y-m-d'),
                        'toDate' => \Carbon\Carbon::parse($skuLineList->to_date)->format('Y-m-d'),
                        'maxApplicableTime' => $skuLineList->max_time_applicability,
                        'minimumOrderValue' => $skuLineList->minimum_order_value,
                        'promocode' => $skuLineList->promocode
                    );
                    $isApplicablePromo = $orderRepository->skuLineSoldPerCallPromocode($params);
                    if ($isApplicablePromo)
                        $promocodes[] = $skuLineList->promocode;
                }
            }
        }
        return $promocodes;
    }
    public function skuLinePromocodePanel(array $where) {

        $skuLineSoldPerCallSchemeRepository = new SkuLineSoldPerCallSchemeRepository(new \App\SkuLineSoldPerCallScheme);
        $orderRepository = new OrderRepository(new \App\Orders);

        $promocodes = array();
        $skuLineLists = $skuLineSoldPerCallSchemeRepository->promocode($where);
        if (count($skuLineLists)) {
            foreach ($skuLineLists as $skuLineList) {
                $params = array(
                    'userId' => $where['user_id'],
                    'fromDate' => \Carbon\Carbon::parse($skuLineList->from_date)->format('Y-m-d'),
                    'toDate' => \Carbon\Carbon::parse($skuLineList->to_date)->format('Y-m-d'),
                    'maxApplicableTime' => $skuLineList->max_time_applicability,
                    'minimumOrderValue' => $skuLineList->minimum_order_value,
                    'promocode' => $skuLineList->promocode
                );
                $isApplicablePromo = $orderRepository->skuLineSoldPerCallPromocode($params);
                if ($isApplicablePromo)
                    $promocodes[] = $skuLineList->promocode;
            }
        }
        return $promocodes;
    }

    public function orderVolumePromocode(array $where) {

        $orderVolumeSchemeRepository = new OrderVolumeSchemeRepository(new \App\OrderVolumeScheme);
        $orderRepository = new OrderRepository(new \App\Orders);
        $promocodes = array();
        $orderVolumeLists = $orderVolumeSchemeRepository->promocode($where);
        if (count($orderVolumeLists)) {
            foreach ($orderVolumeLists as $orderVolumeList) {

                $params = array(
                    'userId' => request()->user()->id,
                    'fromDate' => \Carbon\Carbon::parse($orderVolumeList->from_date)->format('Y-m-d'),
                    'toDate' => \Carbon\Carbon::parse($orderVolumeList->to_date)->format('Y-m-d'),
                    'maxApplicableTime' => $orderVolumeList->max_time_applicability,
                    'orderCount' => $orderVolumeList->order_count,
                    'minimumOrderValue' => $orderVolumeList->minimum_order_value,
                    'promocode' => $orderVolumeList->promocode
                );

                $isApplicablePromo = $orderRepository->orderVolumeSchemePromocode($params);
                if ($isApplicablePromo)
                    $promocodes[] = $orderVolumeList->promocode;
            }
        }
        return $promocodes;
    }

    public function orderVolumePromocodePanel(array $where) {

        $orderVolumeSchemeRepository = new OrderVolumeSchemeRepository(new \App\OrderVolumeScheme);
        $orderRepository = new OrderRepository(new \App\Orders);
        $promocodes = array();
        $orderVolumeLists = $orderVolumeSchemeRepository->promocode($where);
        if (count($orderVolumeLists)) {
            foreach ($orderVolumeLists as $orderVolumeList) {

                $params = array(
                    'userId' => $where['user_id'],
                    'fromDate' => \Carbon\Carbon::parse($orderVolumeList->from_date)->format('Y-m-d'),
                    'toDate' => \Carbon\Carbon::parse($orderVolumeList->to_date)->format('Y-m-d'),
                    'maxApplicableTime' => $orderVolumeList->max_time_applicability,
                    'orderCount' => $orderVolumeList->order_count,
                    'minimumOrderValue' => $orderVolumeList->minimum_order_value,
                    'promocode' => $orderVolumeList->promocode
                );

                $isApplicablePromo = $orderRepository->orderVolumeSchemePromocode($params);
                if ($isApplicablePromo)
                    $promocodes[] = $orderVolumeList->promocode;
            }
        }
        return $promocodes;
    }

    public function orderedItemPromocode(array $where) {

        $orderedItemSchemeRepository = new OrderedItemSchemeRepository(new \App\OrderedItemScheme);


        $orderedItemSchemeLists = $orderedItemSchemeRepository->promocode($where);

        $promocodes = array();
        if (count($orderedItemSchemeLists)) {

            $orderRepository = new OrderRepository(new \App\Orders);
            $productRepository = new \App\Repository\ProductRepository(new \App\Product );

            $mapDistributor = new \App\MapDistributor;
            $userDetail = $mapDistributor->where('retailer_id',request()->user()->id)->first();

            foreach ($orderedItemSchemeLists as $orderedItemSchemeList) {

                $checkProductActive = $productRepository->checkIfProductExist([$orderedItemSchemeList->primary_product],$userDetail->distributor_id);
                if($checkProductActive){
                    $params = array(
                        'userId' => request()->user()->id,
                        'fromDate' => \Carbon\Carbon::parse($orderedItemSchemeList->from_date)->format('Y-m-d'),
                        'toDate' => \Carbon\Carbon::parse($orderedItemSchemeList->to_date)->format('Y-m-d'),
                        'maxApplicableTime' => $orderedItemSchemeList->max_time_applicability,
                        'primaryProduct' => $orderedItemSchemeList->primary_product,
                        'productCount' => $orderedItemSchemeList->product_count,
                        'promocode' => $orderedItemSchemeList->promocode
                    );

                    $isApplicablePromo = $orderRepository->orderedItemSchemePromocode($params);

                    if ($isApplicablePromo)
                        $promocodes[] = $orderedItemSchemeList->promocode;
                }
            }
        }
        return $promocodes;
    }

    public function orderedItemPromocodePanel(array $where) {

        $orderedItemSchemeRepository = new OrderedItemSchemeRepository(new \App\OrderedItemScheme);
        $orderRepository = new OrderRepository(new \App\Orders);

        $orderedItemSchemeLists = $orderedItemSchemeRepository->promocode($where);

        $promocodes = array();
        if (count($orderedItemSchemeLists)) {


            $productRepository = new \App\Repository\ProductRepository(new \App\Product );

            $mapDistributor = new \App\MapDistributor;
            $userDetail = $mapDistributor->where('retailer_id',$where['user_id'])->first();
            foreach ($orderedItemSchemeLists as $orderedItemSchemeList) {
                $checkProductActive = $productRepository->checkIfProductExist([$orderedItemSchemeList->primary_product],$userDetail->distributor_id);
                if($checkProductActive){
                    $params = array(
                        'userId' => $where['user_id'],
                        'fromDate' => \Carbon\Carbon::parse($orderedItemSchemeList->from_date)->format('Y-m-d'),
                        'toDate' => \Carbon\Carbon::parse($orderedItemSchemeList->to_date)->format('Y-m-d'),
                        'maxApplicableTime' => $orderedItemSchemeList->max_time_applicability,
                        'primaryProduct' => $orderedItemSchemeList->primary_product,
                        'productCount' => $orderedItemSchemeList->product_count,
                        'promocode' => $orderedItemSchemeList->promocode
                    );

                    $isApplicablePromo = $orderRepository->orderedItemSchemePromocode($params);

                    if ($isApplicablePromo)
                        $promocodes[] = $orderedItemSchemeList->promocode;
                }
            }
        }
        return $promocodes;
    }

    public function discount($key, $promocode,$product= false) {

        switch ($key) {
            case 'skuLine' :
                $skuLineSoldPerCallSchemeRepository = new SkuLineSoldPerCallSchemeRepository(new SkuLineSoldPerCallScheme);
                $getDetailOfPromocode = $skuLineSoldPerCallSchemeRepository->getPromocodeDetail(
                                array('promocode' => $promocode)
                        )->first();

                if ($getDetailOfPromocode->apply_discount == '2') {
                    $discount = $getDetailOfPromocode->fixed_amount_percentage;
                    return array('message' => 'You have got ₹' . $discount . ' discount', 'discount' => $discount);
                } else {
                    $productRepository = new ProductRepository(new \App\Product);
                    $productDetail = $productRepository->getById($getDetailOfPromocode->free_secondary_product);
                    if (!empty($productDetail)) {
                        $mrp = $productDetail->rlp;
                        $percentage = $getDetailOfPromocode->fixed_amount_percentage;
                        $discount = round(($mrp * $percentage / 100) * 100) / 100;
                        return array('message' => 'You have got ₹' . $discount . ' discount', 'discount' => $discount);
                    }
                }
                throw new Exception('Invalid request for promocode');
                break;

            case 'productLaunch':
                $newProductLaunchRepository = new NewProductLaunchRepository(new \App\NewProductLaunch);
                $getDetailOfPromocode = $newProductLaunchRepository->getPromocodeDetail(
                                array('promocode' => $promocode)
                        )->first();
                if (!empty($getDetailOfPromocode))
                    return array('message' => 'You have got ₹' . $getDetailOfPromocode->fixed_amount_percentage . ' discount', 'discount' => $getDetailOfPromocode->fixed_amount_percentage);

                throw new Exception('Invalid request for promocode');
                break;
            case 'orderItems' :
                $orderedItemSchemeRepository = new OrderedItemSchemeRepository(new \App\OrderedItemScheme);
                $getDetailOfPromocode = $orderedItemSchemeRepository->getPromocodeDetail(
                                array('promocode' => $promocode)
                        )->first();
                if (!empty($getDetailOfPromocode)) {
                    $productRepository = new \App\Repository\ProductRepository(new \App\Product);
                    $getFreeProduct = $productRepository->getById($getDetailOfPromocode->free_secondary_product);
                    if($product)
                        return array('message' => 'You have got ' . $getDetailOfPromocode->free_product_count . ' ' . $getFreeProduct->name . ' ' . $getFreeProduct->unitWeight->name . ' free product',
                        'discount' => 0,
                        'product' =>  $getDetailOfPromocode->free_secondary_product,
                        'quantity' => $getDetailOfPromocode->free_product_count);

                    return array('message' => 'You have got ' . $getDetailOfPromocode->free_product_count . ' ' . $getFreeProduct->name . ' ' . $getFreeProduct->unitWeight->name . ' free product',
                    'discount' => 0);
                }
                throw new Exception('Invalid request for promocode');

                break;
            case 'orderVolume':
                $orderVolumeSchemeRepository = new OrderVolumeSchemeRepository(new \App\OrderVolumeScheme);
                $getDetailOfPromocode = $orderVolumeSchemeRepository->getPromocodeDetail(
                                array('promocode' => $promocode)
                        )->first();

                if (!empty($getDetailOfPromocode)) {
                    return array('message' => 'You have got worth ₹' . $getDetailOfPromocode->free_ds_product_worth_rlp . ' free DS product.', 'discount' => 0);
                }
                return array('message' => 'You have got ₹0 discount', 'discount' => 0);
                break;
            default:
                return array('message' => 'You have got ₹0 discount', 'discount' => 0);
        }
        return 0;
    }

    public function discountAgentPanel($key, $promocode,$product= false) {

        switch ($key) {
            case 'skuLine' :
                $skuLineSoldPerCallSchemeRepository = new SkuLineSoldPerCallSchemeRepository(new SkuLineSoldPerCallScheme);
                $getDetailOfPromocode = $skuLineSoldPerCallSchemeRepository->getPromocodeDetail(
                                array('promocode' => $promocode)
                        )->first();

                if ($getDetailOfPromocode->apply_discount == '2') {
                    $discount = $getDetailOfPromocode->fixed_amount_percentage;
                    return array('message' => 'You have got ₹' . $discount . ' discount', 'discount' => $discount);
                } else {
                    $productRepository = new ProductRepository(new \App\Product);
                    $productDetail = $productRepository->getById($getDetailOfPromocode->free_secondary_product);
                    if (!empty($productDetail)) {
                        $mrp = $productDetail->rlp;
                        $percentage = $getDetailOfPromocode->fixed_amount_percentage;
                        $discount = round(($mrp * $percentage / 100) * 100) / 100;
                        return array('message' => 'You have got ₹' . $discount . ' discount', 'discount' => $discount);
                    }
                }
                throw new Exception('Invalid request for promocode');
                break;

            case 'productLaunch':
                $newProductLaunchRepository = new NewProductLaunchRepository(new \App\NewProductLaunch);
                $getDetailOfPromocode = $newProductLaunchRepository->getPromocodeDetail(
                                array('promocode' => $promocode)
                        )->first();
                if (!empty($getDetailOfPromocode))
                    return array('message' => 'You have got ₹' . $getDetailOfPromocode->fixed_amount_percentage . ' discount', 'discount' => $getDetailOfPromocode->fixed_amount_percentage);

                throw new Exception('Invalid request for promocode');
                break;
            case 'orderItems' :
                $orderedItemSchemeRepository = new OrderedItemSchemeRepository(new \App\OrderedItemScheme);
                $getDetailOfPromocode = $orderedItemSchemeRepository->getPromocodeDetail(
                                array('promocode' => $promocode)
                        )->first();
                if (!empty($getDetailOfPromocode)) {
                    $productRepository = new \App\Repository\ProductRepository(new \App\Product);
                    $getFreeProduct = $productRepository->getById($getDetailOfPromocode->free_secondary_product);
                    if($product)
                        return array('message' => 'You have got ' . $getDetailOfPromocode->free_product_count . ' ' . $getFreeProduct->name . ' ' . $getFreeProduct->unitWeight->name . ' free product',
                        'discount' => 0,
                        'product' =>  $getDetailOfPromocode->free_secondary_product,
                        'quantity' => $getDetailOfPromocode->free_product_count);

                    return array('message' => 'You have got ' . $getDetailOfPromocode->free_product_count . ' ' . $getFreeProduct->name . ' ' . $getFreeProduct->unitWeight->name . ' free product',
                    'discount' => 0);
                }
                throw new Exception('Invalid request for promocode');

                break;
            case 'orderVolume':
                $orderVolumeSchemeRepository = new OrderVolumeSchemeRepository(new \App\OrderVolumeScheme);
                $getDetailOfPromocode = $orderVolumeSchemeRepository->getPromocodeDetail(
                                array('promocode' => $promocode)
                        )->first();

                if (!empty($getDetailOfPromocode)) {
                    return array('message' => 'You have got worth ₹' . $getDetailOfPromocode->free_ds_product_worth_rlp . ' free DS product.', 'discount' => 0);
                }
                return array('message' => 'You have got ₹0 discount', 'discount' => 0);
                break;
            default:
                return array('message' => 'You have got ₹0 discount', 'discount' => 0);
        }
        return 0;
    }

    public function getPromocodeDetail(Request $request) {
        try {

            $validate = \Validator($request->post(), [
                'promocode_key' => ['required', Rule::in($this->promocodeKey)],
                'promocode' => ['required']
            ]);
            if ($validate->fails()) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => $validate->errors()->first(),
                                ], __('api/responseCode.bad_request'));
            }

            $promocodeKey = $request->post('promocode_key');
            $promocode = $request->post('promocode');

            $promocodes = $this->getPromocodes();
            if (empty($promocodes))
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Promocode is not valid'
                                ], __('api/responseCode.failed'));

            if (!empty($promocodes)) {

                $match = false;
                foreach ($promocodes as $value) {
                    if ($value['key'] == $promocodeKey && $value['value'] == $promocode) {
                        $match = true;
                        break;
                    }
                }
                if (!$match)
                    return $this->apiResponse([
                                'status' => false,
                                'message' => 'Promocode is not valid',
                                'promocode' => $promocodes
                                    ], __('api/responseCode.failed'));
            }

            $discountAmount = $this->discount($promocodeKey, $promocode);

            return $this->apiResponse([
                        'status' => true,
                        'discount' => $discountAmount,
                        'message' => 'Promocode applied successfully'
                            ], __('api/responseCode.success'));
        } catch (\Exception $exception) {
            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Internal server error please try again.',
                        'error' => 'Error : ' . $exception->getMessage(),
                            ], __('api/responseCode.server_error'));
        }
    }

}
