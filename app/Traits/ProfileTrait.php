<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Repository\RetailerRepository;
use Illuminate\Support\Facades\File;
use App\Repository\RetailersQrCodeRepository;

trait ProfileTrait
{
    //


    public function getProfileDetail(Request $request,RetailerRepository $retailerRepository){
        try{
            $userId = request()->user()->id;
            $userDetail = $retailerRepository->getById($userId)->load('QrCode');
            $shopType = array(
                array('id' => '1', 'value' => 'General A'),
                array('id' =>  '2','value' => 'General B' ),
                array('id' =>  '3','value' => 'General C'),
                array('id' =>  '4','value' => 'Chemist A'),
                array('id' =>  '5','value' => 'Chemist B'),
                array('id' =>  '6','value' => 'Chemist C'),
                array('id' =>  '7','value' => 'Cosmetic'),
                array('id' =>  '8','value' => 'Paan A' ),
                array('id' =>  '9','value' => 'Paan B' ),
                array('id' => '10','value' => 'Paan C')

            );
            $memberType = array(

                array( 'id'=>'1','value' =>'Prime'),
                array( 'id'=>'2','value' => 'Non Prime'),
                array( 'id'=>'3','value' => 'Regular'),
            );
            return $this->apiResponse([
                'status' => true,
                'data' => $userDetail,
                'shopType' =>$shopType,
                'memberType' => $memberType,
                'message' => 'Profile list successfully',
            ],__('api/responseCode.success'));
        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Failed to update profile',
                'error' => 'Error : '. $exception->getMessage(),
            ],__('api/responseCode.server_error'));
        }
    }

    public function updateProfile(Request $request,RetailerRepository $retailerRepository){

        try {
            $validator = $this->validateProfileUpdateRequest($request);
            if ($validator->fails()) {
                return $this->apiResponse([
                    'status' => false,
                    'message' => $validator->errors()->first(),
                ], __('api/responseCode.bad_request'));
            }
            $imageExtensions = ['jpg','png','jpeg','gif'];
            if($request->hasFile('primary_profile')){
                $fileExtension = $request->primary_profile->getClientOriginalExtension();

                if(!in_array($fileExtension,$imageExtensions))
                    return response()->json([
                        'status' => false,
                        'message' => 'Invalid primary profile file type'
                    ]);
            }
            if($request->hasFile('secondary_profile')){
                $fileExtension = $request->secondary_profile->getClientOriginalExtension();

                if(!in_array($fileExtension,$imageExtensions))
                    return response()->json([
                        'status' => false,
                        'message' => 'Invalid secondary profile file type'
                    ]);
            }
            if($request->hasFile('shop_image')){
                $fileExtension = $request->shop_image->getClientOriginalExtension();

                if(!in_array($fileExtension,$imageExtensions))
                    return response()->json([
                        'status' => false,
                        'message' => 'Invalid shop profile file type'
                    ]);
            }

            $updateProfileData = $this->store($request,$retailerRepository);

            if ($updateProfileData->save()) {
//                $updateProfileData = $retailerRepository->getById($userId);
                return $this->apiResponse([
                    'status' => true,
                    'data' => $updateProfileData,
                    'message' => 'Profile updated successfully',
                ], __('api/responseCode.success'));
            }
            return $this->apiResponse([
                'status' => false,
                'message' => 'Your cart is empty'
            ], __('api/responseCode.failed'));
        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Failed to update profile',
                'error' => 'Error : '. $exception->getMessage(),
            ],__('api/responseCode.server_error'));
        }

    }
    private function validateProfileUpdateRequest(Request $request){

        $userId = request()->user()->id;
        return \Validator($request->all(),[
           'full_name' => 'required | max: 100',
//            'primary_mobile_number' => 'required | digits:10 | unique:retailers,primary_mobile_number,'.$userId,
            // 'alternate_mobile_number' => 'digits : 10 | nullable',
            // 'whatsapp_number' => 'required | digits :10',
            // 'secondary_full_name' => 'max : 100 | nullable',
            // 'secondary_mobile_number' => 'digits : 10 | nullable',
            // 'dob' => 'required | date_format:m-d-Y|before:today',
            // 'anniversary' => 'required | date_format:m-d-Y',
            // 'first_kid_birthday' => 'required | date_format:m-d-Y|before:today',
            // 'second_kid_birthday' => 'required | date_format:m-d-Y|before:today',
            // 'gst_number' => 'required',
            // 'pan_number' => 'required',
            // 'adhar_number' => 'required',
            'primary_profile' => 'image|mimes:jpeg,png,jpg|max:2048|nullable',
            'secondary_profile' => 'image|mimes:jpeg,png,jpg|max:2048|nullable',
            'shop_image' => 'image|mimes:jpeg,png,jpg|max:2048|nullable',
        ]);
    }

    public function store(Request $request,RetailerRepository $retailerRepository){


        $userId = request()->user()->id;
        $updateProfileData = $retailerRepository->getById($userId);

        $primaryImageName = $secondaryImageName =  $shopImageName = '';

        if($request->hasFile('primary_profile')){
            $primaryImageName = time().'_primary_profile_'.$userId.'.'.$request->primary_profile->getClientOriginalExtension();
            if(!$request->primary_profile->move(public_path('uploads/retailers/primary/'), $primaryImageName))
                $primaryImageName = '';
            else{
                if(!empty($updateProfileData->capture_retailer_image))
                    File::delete(public_path('uploads/retailers/primary/').$updateProfileData->capture_retailer_image);
            }

        }

        if($request->hasFile('secondary_profile')){

            $secondaryImageName = time().'_secondary_profile_'.$userId.'.'.$request->secondary_profile->getClientOriginalExtension();
            if(!$request->secondary_profile->move(public_path('uploads/retailers/secondary/'), $secondaryImageName))
                $secondaryImageName = '';
            else{
                if(!empty($updateProfileData->secondary_capture_retailer_image))
                    File::delete(public_path('uploads/retailers/primary/').$updateProfileData->secondary_capture_retailer_image);
            }
        }

        if($request->hasFile('shop_image')){

            $shopImageName = time().'shop_image'.$userId.'.'.$request->shop_image->getClientOriginalExtension();
            if(!$request->shop_image->move(public_path('uploads/retailers/shop_image/'), $shopImageName))
                $shopImageName = '';
            else{
                if(!empty($updateProfileData->shop_image))
                    File::delete(public_path('uploads/retailers/shop_image/').$updateProfileData->shop_image);
            }
        }


        $updateProfileData->full_name =  $request->post('full_name');
        $updateProfileData->alternative_mobile_number =  $request->post('alternate_mobile_number');
        $updateProfileData->whats_app_number =  $request->post('whatsapp_number');
        $updateProfileData->secondary_full_name =  $request->post('secondary_full_name');
        $updateProfileData->secondary_mobile_number =  $request->post('secondary_mobile_number');
        $updateProfileData->dob = ($request->post('dob')) ? \Carbon\Carbon::createFromFormat('d-m-Y',$request->post('dob')): NULL;
        $updateProfileData->anniversay = ($request->post('anniversary')) ? \Carbon\Carbon::createFromFormat('d-m-Y',$request->post('anniversary')) : NULL ;
        $updateProfileData->first_kid_birthday = ($request->post('first_kid_birthday')) ? \Carbon\Carbon::createFromFormat('d-m-Y',$request->post('first_kid_birthday')) : NULL;
        $updateProfileData->second_kid_birthday = ($request->post('second_kid_birthday')) ? \Carbon\Carbon::createFromFormat('d-m-Y',$request->post('second_kid_birthday')) : NULL;
        $updateProfileData->gst_number =  $request->post('gst_number');
        $updateProfileData->pan_number =  $request->post('pan_number');
        $updateProfileData->adhar_number =  $request->post('adhar_number');
        $updateProfileData->shop_address = $request->post('shop_address');
        if(!empty($secondaryImageName))
            $updateProfileData->secondary_capture_retailer_image = $secondaryImageName;
        if(!empty($primaryImageName))
            $updateProfileData->capture_retailer_image = $primaryImageName;
        if(!empty($shopImageName))
            $updateProfileData->shop_image = $shopImageName;

        $retailersQrCodeRepository = new RetailersQrCodeRepository(new \App\RetailersQrCode);
        $getUserQrCode = $retailersQrCodeRepository->getFromUserId($userId);
        if(empty($getUserQrCode)){
            $getUserQrCode = new \App\RetailersQrCode;
            $getUserQrCode->user_id = $userId;
        }
        $getUserQrCode->paytm = $request->post('paytm');
        $getUserQrCode->mobiqwik = $request->post('mobiqwik');
        $getUserQrCode->phone_pe = $request->post('phone_pe');
        $getUserQrCode->upi = $request->post('upi');
        $getUserQrCode->bharat_qr = $request->post('bharat_qr');
        $getUserQrCode->gpay = $request->post('gpay');
        $getUserQrCode->ds_group = $request->post('ds_group');
        $getUserQrCode->save();
        return $updateProfileData;

    }

    public function notifyMe(Request $request)
    {
        # code...
        try{
            $validator =  \validator($request->post(),[
                'product_id' => 'required | integer| min : 1'
            ]);

            if ($validator->fails()) {
                return $this->apiResponse([
                    'status' => false,
                    'message' => $validator->errors()->first(),
                ], __('api/responseCode.bad_request'));
            }

            $input = [
                'user_id' => request()->user()->id,
                'product_id' => $request->post('product_id'),
                'status' => 'PENDING'
            ];
            if($notify = \App\NotifyMe::create($input)){
                return $this->apiResponse([
                    'status' => true,
                    'data' => $notify,
                    'message' => 'You will be notified soon',
                ], __('api/responseCode.success'));
            }

            return $this->apiResponse([
                'status' => false,
                'message' => 'Sorry! please try again'
            ], __('api/responseCode.failed'));
        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Failed to add notify me',
                'error' => 'Error : '. $exception->getMessage(),
            ],__('api/responseCode.server_error'));
        }
    }

}
