<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Repository\CartRepository;
use App\Repository\OrderRepository;
use DB;
use App\Repository\OrderItemsRepository;
use App\Repository\ApiOrderRepository;
use Illuminate\Validation\Rule;
use App\Repository\ApiSkuListsRepository;
use Illuminate\Support\Facades\Auth;
use App\Repository\ProductRepository;

trait OrdersTrait {

    //
    /**
     *  added by niraj lal rahi
     * function is used to place order by retailer user
     * @param
     * Request
     * CartRepository,
     * OrderRepository,
     * OrderItemsRepository,
     * ApiOrderRepository
     *
     * @return
     * status (boolean)
     * message (string)
     * data (array)
     *
     * */
    public function placeOrder(Request $request, CartRepository $cartRepository, OrderRepository $orderRepository, OrderItemsRepository $orderItemsRepository, ApiOrderRepository $apiOrderRepository, ProductRepository $productRepository) {



        try {

            /**             *
             *  Begin transaction here
             *  place order
             * */
            $keys = ['place-order'];
            $current_time = \Carbon\Carbon::now()->subDay(1);

            $validate = \Validator($request->post(), [
                'action' => ['required', Rule::in($keys)],
                'promocode_key' => ['required_with:promocode', Rule::in($this->promocodeKey)],
                'promocode' => ['required_with:promocode_key'],
                'deliver_date' => ['required', 'date_format:Y-m-d', 'after_or_equal:' . $current_time],
                'delivery_time' => ['required', 'date_format:H:i']
                    ], [
                'deliver_date.after_or_equal' => 'Date must be greater equal or greater than today'
            ]);

            if ($validate->fails()) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => $validate->errors()->first(),
                                ], __('api/responseCode.bad_request'));
            }
            DB::beginTransaction();

            $userId = request()->user()->id;
            
            if($this->checkIsActiveRetailer(request()->user()->id)){
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Your account is inactive by admin'
                                ], __('api/responseCode.failed')); 
            }
            
            $promocode = $request->post('promocode');
            $promocodeKey = $request->post('promocode_key');

            $cartItems = $cartRepository->getCartItemFromUserId($userId);

            if (!count($cartItems)) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Your cart is empty'
                                ], __('api/responseCode.failed'));
            }

            $mapDistributor = new \App\MapDistributor;
            $userDetail = $mapDistributor->where('retailer_id', $userId)->first();

            $productIds = array_column($cartItems->toArray(), 'product_id');
            $checkActiveProducts = $productRepository->checkIfProductExist($productIds, $userDetail->distributor_id);

            if (count($productIds) != count($checkActiveProducts)) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Some of products is out of stock. Please add again.'
                                ], __('api/responseCode.failed'));
            }

            if (!empty($promocode)) {
                $promocodes = $this->getPromocodes();
                if (empty($promocodes))
                    return $this->apiResponse([
                                'status' => false,
                                'message' => 'Promocode is not valid'
                                    ], __('api/responseCode.failed'));
                if (!empty($promocodes)) {
                    $match = false;
                    foreach ($promocodes as $value) {
                        if ($value['key'] == $promocodeKey && $value['value'] == $promocode) {
                            $match = true;
                            break;
                        }
                    }
                    if (!$match)
                        return $this->apiResponse([
                                    'status' => false,
                                    'message' => 'Promocode is not valid',
                                    'promocode' => $promocodes
                                        ], __('api/responseCode.failed'));
                }
            }

            $total = 0;
            $discount = 0;
            $orderItems = array();
            $message = '';
            if (!empty($promocode) && $promocodeKey) {
                $getDiscount = $this->discount($promocodeKey, $promocode, true);
                if ($promocodeKey == 'orderItems') {
                    $orderItems[] = array(
                        'product_id' => $getDiscount['product'],
                        'quantity' => $getDiscount['quantity'],
                        'packed_quantity' => $getDiscount['quantity'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'nature' => 'FREE'
                    );
                }
                $discount = $getDiscount['discount'];
                $message = $getDiscount['message'];
            }



            $productSku = array();

            foreach ($cartItems as $cartItem) {

                $quantity = $cartItem->quantity;
                $mrp = isset($cartItem->products->rlp) ? $cartItem->products->rlp : 0;
                $total = $total + ($mrp * $quantity);

                $productSku[] = array(
                    "sku_id" => $cartItem->product_id,
                    "sku_units" => $cartItem->quantity,
                    "sku_unit_cost" => isset($cartItem->products->rlp) ? $cartItem->products->rlp : 0,
                    "sku_description" => isset($cartItem->products->short_description) ? $cartItem->products->short_description : 'N/A'
                );

                $orderItems[] = array(
                    'product_id' => $cartItem->product_id,
                    'quantity' => $cartItem->quantity,
                    'packed_quantity' => $cartItem->quantity,
                    'created_at' => date('Y-m-d H:i:s'),
                    'nature' => 'PURCHASED'
                );


                /**
                 * Stock update quantity
                 */
                $stockQuantity = \App\HubStock::where('distributor_id', $userDetail->distributor_id)
                                ->where('product_id', $cartItem->product_id)->first();
                
                if ($cartItem['quantity'] > (($stockQuantity->product_quantity + $stockQuantity->in_stock) - $stockQuantity->out_stock )) {

                        return $this->apiResponse([
                                    'status' => false,
                                    'message' => 'Some products are out of stock',
                                        ], __('api/responseCode.failed'));
                    }
                
                $stockQuantity->out_stock = $stockQuantity->out_stock + $cartItem->quantity;

                $stockQuantity->save();
            }


            /*             * *
             *  discount calculation pending here
             * */
            $tmpTotal = $total;
            $total = $total - $discount;

            if ($total < 0) {
                $discount = $tmpTotal;
                $total = 0;
                $message = 'You have got ₹' . $discount . ' discount.';
                // return $this->apiResponse([
                //     'status' => false,
                //     'message' => 'To apply this promocode minimum cart value should be '.($discount+100)
                //         ], __('api/responseCode.failed'));
            }

            /**             *
             *  discount calculation end here
             * */
            $orders = array(
                'user_id' => $userId,
                'total_amount' => $total,
                'promocode' => $promocode,
                'promocode_key' => $promocodeKey,
                'discount_amount' => $discount,
                'order_number' => substr(sha1(time()), -10),
                'hub_id' => $userDetail->distributor_id,
                'delivery_time' => \Carbon\Carbon::parse($request->post('deliver_date') . ' ' . $request->post('delivery_time'))->format('Y-m-d H:i:s'),
                'promocode_msg' => $message
            );

            /**
             *
             * store to local database
             * */
            $placeOrder = $orderRepository->store($orders);

            for ($i = 0; $i < count($orderItems); $i++) {
                $orderItems[$i]['order_id'] = $placeOrder->id;
            }

            /**
             * store order items as per order id
             * */
            $orderItemsRepository->placeOrder($orderItems);

            /**
             * place new order to third party api using GuzzleHttp
             * */
            $thirdPartyApiPlaceOrder = $this->placeOrderThirdParty($placeOrder, $productSku, $apiOrderRepository);

            /**
             *  after successfully order place remove item from users cart
             * */
            $deleteCartProduct = $cartRepository->deleteUserCartProducts($userId);

            /*             * *
             *
             * send message & push notification after successfully placed order
             *
             */
            $message = __('api/notification.pushNotification.12', ['number' => $placeOrder->order_number]);
            //$message = "Order successfully placed. We are pleased to confirm your order no #$placeOrder->order_number. Thank you for shopping with DS Group.";

            $this->sendSms($message, $userId);

            $title = __('api/responseCode.title.12');

            $this->sendPushNotification($message, $title, $userId, 'place-order');

            $this->sendNotification($type = 'PLACE_ORDER', $placeOrder);

            $orderLogRepository = new \App\Repository\OrdersLogRepository;
            $orderLogRepository->manageLog($placeOrder);

            DB::commit();

            return $this->apiResponse([
                        'status' => true,
                        'message' => 'Order placed successfully',
                        'data' => $placeOrder,
                            //'api' => $thirdPartyApiPlaceOrder
                            ], __('api/responseCode.success'));
        } catch (\Exception $exception) {
            /**
             *  rollback if order place have any error
             * */
            DB::rollback();

            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Order failed. Please try again',
                        'error' => 'Error : ' . $exception->getMessage(),
                            ], __('api/responseCode.server_error'));
        }
    }

    public function placeOrderAgentPanel(Request $request, CartRepository $cartRepository, OrderRepository $orderRepository, OrderItemsRepository $orderItemsRepository, ApiOrderRepository $apiOrderRepository, ProductRepository $productRepository) {


        try {
            /*             * *
             *  Begin transaction here
             *  place order
             * */



            $current_time = \Carbon\Carbon::now()->subDay(1);



            $current_time = \Carbon\Carbon::now()->subDay(1);

            $validate = \Validator($request->post(), [
                'promocode_key' => ['required_with:promocode', Rule::in($this->promocodeKey)],
                'promocode' => ['required_with:promocode_key'],
                'deliver_date' => ['required', 'date_format:Y-m-d', 'after_or_equal:' . $current_time],
                'delivery_time' => ['required', 'date_format:H:i'],
                'cart_items' => ['required'],
                'totalQuantity' => ['required'],
                'totalAmount' => ['required'],
                    ], [
                'deliver_date.after_or_equal' => 'Date must be greater equal or greater than today'
            ]);




            if ($validate->fails()) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => $validate->errors()->first(),
                                ], __('api/responseCode.bad_request'));
            }



            DB::beginTransaction();

            $userId = $request->id;
            if($this->checkIsActiveRetailer($request->id)){
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Your account is inactive by admin'
                                ], __('api/responseCode.failed')); 
            }
            $promocode = $request->post('promocode');
            $promocodeKey = $request->post('promocode_key');
            $cartItems = $request->cart_items;
            $mapDistributor = new \App\MapDistributor;
            $userDetail = $mapDistributor->where('retailer_id', $userId)->first();
//            echo "<pre>";
//            print_r($cartItems);

            foreach ($cartItems as $key => $value) {
                $productIds[] = $value['id'];
            }
//            $productIds = array_column($cartItems->toArray(), 'id');
            $checkActiveProducts = $productRepository->checkIfProductExist($productIds, $userDetail->distributor_id);

            if (count($productIds) != count($checkActiveProducts)) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Some of products is out of stock. Please add again.'
                                ], __('api/responseCode.failed'));
            }


            if (!empty($promocode)) {
                $promocodes = $this->getPromocodesAgentPanel($request->id);
                if (empty($promocodes))
                    return $this->apiResponse([
                                'status' => false,
                                'message' => 'Promocode is not valid'
                                    ], __('api/responseCode.failed'));
                if (!empty($promocodes)) {
                    $match = false;
                    foreach ($promocodes as $value) {
                        if ($value['key'] == $promocodeKey && $value['value'] == $promocode) {
                            $match = true;
                            break;
                        }
                    }
                    if (!$match)
                        return $this->apiResponse([
                                    'status' => false,
                                    'message' => 'Promocode is not valid',
                                    'promocode' => $promocodes
                                        ], __('api/responseCode.failed'));
                }
            }



            $total = 0;
            $discount = 0;
            $message = '';
            if (!empty($promocode) && $promocodeKey) {
                $getDiscount = $this->discount($promocodeKey, $promocode);
                $discount = $getDiscount['discount'];




                $getDiscount = $this->discount($promocodeKey, $promocode, true);
                if ($promocodeKey == 'orderItems') {
                    $orderItems[] = array(
                        'product_id' => $getDiscount['product'],
                        'quantity' => $getDiscount['quantity'],
                        'packed_quantity' => $getDiscount['quantity'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'nature' => 'FREE'
                    );
                }
                $discount = $getDiscount['discount'];
                $message = $getDiscount['message'];
            }


            $orderItems = array();

            if (count($cartItems)) {

                $productSku = array();
                foreach ($cartItems as $cartItem) {
                    if (isset($cartItem['free'])) {
                        continue;
                    }
                    $quantity = $cartItem['rate'];
                    $mrp = isset($cartItem['rlp']) ? $cartItem['rlp'] : 0;
                    $total = $total + ($mrp * $quantity);

                    $productSku[] = array(
//                        "sku_id" =>isset($cartItem->products->sku_number) ? $cartItem->products->sku_number : 0,
                        "sku_id" => $cartItem['id'],
                        "sku_units" => $cartItem['rate'],
                        "sku_unit_cost" => isset($cartItem['rlp']) ? $cartItem['rlp'] : 0,
                        "sku_description" => isset($cartItem['short_description']) ? $cartItem['short_description'] : 'N/A'
                    );

                    $orderItems[] = array(
                        'product_id' => $cartItem['id'],
                        'quantity' => $cartItem['rate'],
                        'packed_quantity' => $cartItem['rate'],
                    );
                    $stockQuantity = \App\HubStock::where('distributor_id', $userDetail->distributor_id)
                                    ->where('product_id', $cartItem['id'])->first();
                    
                    if ($cartItem['rate'] > (($stockQuantity->product_quantity + $stockQuantity->in_stock) - $stockQuantity->out_stock )) {

                        return $this->apiResponse([
                                    'status' => false,
                                    'message' => 'Some products are out of stock',
                                        ], __('api/responseCode.failed'));
                    }


                    $stockQuantity->out_stock = $stockQuantity->out_stock + $cartItem['rate'];

                    $stockQuantity->save();
                }

                /*                 * *
                 *  discount calculation pending here
                 * */
                $total = $total - $discount;

                if ($total < 0) {
                    return $this->apiResponse([
                                'status' => false,
                                'message' => 'To apply this promocode minimum cart value should be ' . ($discount + 100)
                                    ], __('api/responseCode.failed'));
                }

                /*                 * *
                 *  discount calculation end here
                 * */


                $mapDistributor = new \App\MapDistributor;
                $userDetail = $mapDistributor->where('retailer_id', $userId)->first();


                $orders = array(
                    'user_id' => $userId,
                    'total_amount' => $total,
                    'placed_by' => Auth::guard('agent')->user()->id,
                    'promocode' => $promocode,
                    'promocode_key' => $promocodeKey,
                    'order_by' => 2,
                    'discount_amount' => $discount,
                    'order_number' => substr(sha1(time()), -10),
                    'hub_id' => $userDetail->distributor_id,
                    'delivery_time' => \Carbon\Carbon::parse($request->post('deliver_date') . ' ' . $request->post('delivery_time'))->format('Y-m-d H:i:s')
                );
                //dd($orders);

                /**
                 *
                 * store to local database
                 * */
                $placeOrder = $orderRepository->store($orders);

                for ($i = 0; $i < count($orderItems); $i++) {
                    $orderItems[$i]['order_id'] = $placeOrder->id;
                }

                /**
                 * store order items as per order id
                 * */
                $orderItemsRepository->placeOrder($orderItems);

                /**
                 * place new order to third party api using GuzzleHttp
                 * */
                $thirdPartyApiPlaceOrder = $this->placeOrderThirdPartyPanel($placeOrder, $productSku, $apiOrderRepository, $userId);

                /**
                 *  after successfully order place remove item from users cart
                 * */
                $deleteCartProduct = $cartRepository->deleteUserCartProducts($userId);

                /*                 * *
                 *
                 * send message & push notification after successfully placed order
                 *
                 */
                $message = __('api/notification.pushNotification.12', ['number' => $placeOrder->order_number]);
                //$message = "Order successfully placed. We are pleased to confirm your order no #$placeOrder->order_number. Thank you for shopping with DS Group.";

                $orderLogRepository = new \App\Repository\OrdersLogRepository;
                $orderLogRepository->manageLog($placeOrder);

                $this->sendSms($message, $userId);

                $title = __('api/responseCode.title.12');

                $this->sendPushNotification($message, $title, $userId, 'place-order');

                $this->sendNotification($type = 'PLACE_ORDER', $placeOrder);
                DB::commit();

                return $this->apiResponse([
                            'status' => true,
                            'message' => 'Order placed successfully',
                            'data' => $placeOrder,
                                //'api' => $thirdPartyApiPlaceOrder
                                ], __('api/responseCode.success'));
            }

            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Your cart is empty'
                            ], __('api/responseCode.failed'));
        } catch (\Exception $exception) {
            /**
             *  rollback if order place have any error
             * */
            DB::rollback();

            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Order failed. Please try again',
                        'error' => 'Error : ' . $exception->getMessage(),
                            ], __('api/responseCode.server_error'));
        }
    }
    public function placeOrderRetailerPanel(Request $request, CartRepository $cartRepository, OrderRepository $orderRepository, OrderItemsRepository $orderItemsRepository, ApiOrderRepository $apiOrderRepository, ProductRepository $productRepository) {


        try {
            /*             * *
             *  Begin transaction here
             *  place order
             * */



            $current_time = \Carbon\Carbon::now()->subDay(1);



            $current_time = \Carbon\Carbon::now()->subDay(1);

            $validate = \Validator($request->post(), [
                'promocode_key' => ['required_with:promocode', Rule::in($this->promocodeKey)],
                'promocode' => ['required_with:promocode_key'],
                'deliver_date' => ['required', 'date_format:Y-m-d', 'after_or_equal:' . $current_time],
                'delivery_time' => ['required', 'date_format:H:i'],
                'cart_items' => ['required'],
                'totalQuantity' => ['required'],
                'totalAmount' => ['required'],
                    ], [
                'deliver_date.after_or_equal' => 'Date must be greater equal or greater than today'
            ]);




            if ($validate->fails()) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => $validate->errors()->first(),
                                ], __('api/responseCode.bad_request'));
            }



            DB::beginTransaction();

            $userId = $request->id;
            if($this->checkIsActiveRetailer($request->id)){
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Your account is inactive by admin'
                                ], __('api/responseCode.failed')); 
            }
            $promocode = $request->post('promocode');
            $promocodeKey = $request->post('promocode_key');
            $cartItems = $request->cart_items;
            $mapDistributor = new \App\MapDistributor;
            $userDetail = $mapDistributor->where('retailer_id', $userId)->first();
//            echo "<pre>";
//            print_r($cartItems);

            foreach ($cartItems as $key => $value) {
                $productIds[] = $value['id'];
            }
//            $productIds = array_column($cartItems->toArray(), 'id');
            $checkActiveProducts = $productRepository->checkIfProductExist($productIds, $userDetail->distributor_id);

            if (count($productIds) != count($checkActiveProducts)) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Some of products is out of stock. Please add again.'
                                ], __('api/responseCode.failed'));
            }


            if (!empty($promocode)) {
                $promocodes = $this->getPromocodesAgentPanel($request->id);
                if (empty($promocodes))
                    return $this->apiResponse([
                                'status' => false,
                                'message' => 'Promocode is not valid'
                                    ], __('api/responseCode.failed'));
                if (!empty($promocodes)) {
                    $match = false;
                    foreach ($promocodes as $value) {
                        if ($value['key'] == $promocodeKey && $value['value'] == $promocode) {
                            $match = true;
                            break;
                        }
                    }
                    if (!$match)
                        return $this->apiResponse([
                                    'status' => false,
                                    'message' => 'Promocode is not valid',
                                    'promocode' => $promocodes
                                        ], __('api/responseCode.failed'));
                }
            }



            $total = 0;
            $discount = 0;
            $message = '';
            if (!empty($promocode) && $promocodeKey) {
                $getDiscount = $this->discount($promocodeKey, $promocode);
                $discount = $getDiscount['discount'];




                $getDiscount = $this->discount($promocodeKey, $promocode, true);
                if ($promocodeKey == 'orderItems') {
                    $orderItems[] = array(
                        'product_id' => $getDiscount['product'],
                        'quantity' => $getDiscount['quantity'],
                        'packed_quantity' => $getDiscount['quantity'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'nature' => 'FREE'
                    );
                }
                $discount = $getDiscount['discount'];
                $message = $getDiscount['message'];
            }


            $orderItems = array();

            if (count($cartItems)) {

                $productSku = array();
                foreach ($cartItems as $cartItem) {
                    if (isset($cartItem['free'])) {
                        continue;
                    }
                    $quantity = $cartItem['rate'];
                    $mrp = isset($cartItem['rlp']) ? $cartItem['rlp'] : 0;
                    $total = $total + ($mrp * $quantity);

                    $productSku[] = array(
//                        "sku_id" =>isset($cartItem->products->sku_number) ? $cartItem->products->sku_number : 0,
                        "sku_id" => $cartItem['id'],
                        "sku_units" => $cartItem['rate'],
                        "sku_unit_cost" => isset($cartItem['rlp']) ? $cartItem['rlp'] : 0,
                        "sku_description" => isset($cartItem['short_description']) ? $cartItem['short_description'] : 'N/A'
                    );

                    $orderItems[] = array(
                        'product_id' => $cartItem['id'],
                        'quantity' => $cartItem['rate'],
                        'packed_quantity' => $cartItem['rate'],
                    );
                    $stockQuantity = \App\HubStock::where('distributor_id', $userDetail->distributor_id)
                                    ->where('product_id', $cartItem['id'])->first();
                    
                    if ($cartItem['rate'] > (($stockQuantity->product_quantity + $stockQuantity->in_stock) - $stockQuantity->out_stock )) {

                        return $this->apiResponse([
                                    'status' => false,
                                    'message' => 'Some products are out of stock',
                                        ], __('api/responseCode.failed'));
                    }


                    $stockQuantity->out_stock = $stockQuantity->out_stock + $cartItem['rate'];

                    $stockQuantity->save();
                }

                /*                 * *
                 *  discount calculation pending here
                 * */
                $total = $total - $discount;

                if ($total < 0) {
                    return $this->apiResponse([
                                'status' => false,
                                'message' => 'To apply this promocode minimum cart value should be ' . ($discount + 100)
                                    ], __('api/responseCode.failed'));
                }

                /*                 * *
                 *  discount calculation end here
                 * */


                $mapDistributor = new \App\MapDistributor;
                $userDetail = $mapDistributor->where('retailer_id', $userId)->first();


                $orders = array(
                    'user_id' => $userId,
                    'total_amount' => $total,
                    'placed_by' => Auth::user()->id,
                    'promocode' => $promocode,
                    'promocode_key' => $promocodeKey,
                    'order_by' => 3,
                    'discount_amount' => $discount,
                    'order_number' => substr(sha1(time()), -10),
                    'hub_id' => $userDetail->distributor_id,
                    'delivery_time' => \Carbon\Carbon::parse($request->post('deliver_date') . ' ' . $request->post('delivery_time'))->format('Y-m-d H:i:s')
                );
                //dd($orders);

                /**
                 *
                 * store to local database
                 * */
                $placeOrder = $orderRepository->store($orders);

                for ($i = 0; $i < count($orderItems); $i++) {
                    $orderItems[$i]['order_id'] = $placeOrder->id;
                }

                /**
                 * store order items as per order id
                 * */
                $orderItemsRepository->placeOrder($orderItems);

                /**
                 * place new order to third party api using GuzzleHttp
                 * */
                $thirdPartyApiPlaceOrder = $this->placeOrderThirdPartyPanel($placeOrder, $productSku, $apiOrderRepository, $userId);

                /**
                 *  after successfully order place remove item from users cart
                 * */
                $deleteCartProduct = $cartRepository->deleteUserCartProducts($userId);

                /*                 * *
                 *
                 * send message & push notification after successfully placed order
                 *
                 */
                $message = __('api/notification.pushNotification.12', ['number' => $placeOrder->order_number]);
                //$message = "Order successfully placed. We are pleased to confirm your order no #$placeOrder->order_number. Thank you for shopping with DS Group.";

                $orderLogRepository = new \App\Repository\OrdersLogRepository;
                $orderLogRepository->manageLog($placeOrder);

                $this->sendSms($message, $userId);

                $title = __('api/responseCode.title.12');

                $this->sendPushNotification($message, $title, $userId, 'place-order');

                $this->sendNotification($type = 'PLACE_ORDER', $placeOrder);
                DB::commit();

                return $this->apiResponse([
                            'status' => true,
                            'message' => 'Order placed successfully',
                            'data' => $placeOrder,
                                //'api' => $thirdPartyApiPlaceOrder
                                ], __('api/responseCode.success'));
            }

            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Your cart is empty'
                            ], __('api/responseCode.failed'));
        } catch (\Exception $exception) {
            /**
             *  rollback if order place have any error
             * */
            DB::rollback();

            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Order failed. Please try again',
                        'error' => 'Error : ' . $exception->getMessage(),
                            ], __('api/responseCode.server_error'));
        }
    }

//    public function placeOrderRetailerPanel(Request $request, CartRepository $cartRepository, OrderRepository $orderRepository, OrderItemsRepository $orderItemsRepository, ApiOrderRepository $apiOrderRepository, ProductRepository $productRepository) {
//
//
//        try {
//            /*             * *
//             *  Begin transaction here
//             *  place order
//             * */
//
//
//
//            $current_time = \Carbon\Carbon::now()->subDay(1);
//
//
//
//            $current_time = \Carbon\Carbon::now()->subDay(1);
//
//            $validate = \Validator($request->post(), [
//                'promocode_key' => ['required_with:promocode', Rule::in($this->promocodeKey)],
//                'promocode' => ['required_with:promocode_key'],
//                'deliver_date' => ['required', 'date_format:Y-m-d', 'after_or_equal:' . $current_time],
//                'delivery_time' => ['required', 'date_format:H:i'],
//                'cart_items' => ['required'],
//                'totalQuantity' => ['required'],
//                'totalAmount' => ['required'],
//                    ], [
//                'deliver_date.after_or_equal' => 'Date must be greater equal or greater than today'
//            ]);
//
//
//
//
//            if ($validate->fails()) {
//                return $this->apiResponse([
//                            'status' => false,
//                            'message' => $validate->errors()->first(),
//                                ], __('api/responseCode.bad_request'));
//            }
//
//
//
//            DB::beginTransaction();
//
//            $userId = $request->id;
//            $promocode = $request->post('promocode');
//            $promocodeKey = $request->post('promocode_key');
//            $cartItems = $request->cart_items;
//            $mapDistributor = new \App\MapDistributor;
//            $userDetail = $mapDistributor->where('retailer_id', $userId)->first();
////            echo "<pre>";
////            print_r($cartItems);
//
//            foreach ($cartItems as $key => $value) {
//                $productIds[] = $value['id'];
//            }
////            $productIds = array_column($cartItems->toArray(), 'id');
//            $checkActiveProducts = $productRepository->checkIfProductExist($productIds, $userDetail->distributor_id);
//
//            if (count($productIds) != count($checkActiveProducts)) {
//                return $this->apiResponse([
//                            'status' => false,
//                            'message' => 'Some of products is out of stock. Please add again.'
//                                ], __('api/responseCode.failed'));
//            }
//
//
//            if (!empty($promocode)) {
//                $promocodes = $this->getPromocodesAgentPanel($request->id);
//                if (empty($promocodes))
//                    return $this->apiResponse([
//                                'status' => false,
//                                'message' => 'Promocode is not valid'
//                                    ], __('api/responseCode.failed'));
//                if (!empty($promocodes)) {
//                    $match = false;
//                    foreach ($promocodes as $value) {
//                        if ($value['key'] == $promocodeKey && $value['value'] == $promocode) {
//                            $match = true;
//                            break;
//                        }
//                    }
//                    if (!$match)
//                        return $this->apiResponse([
//                                    'status' => false,
//                                    'message' => 'Promocode is not valid',
//                                    'promocode' => $promocodes
//                                        ], __('api/responseCode.failed'));
//                }
//            }
//
//
//
//            $total = 0;
//            $discount = 0;
//            $message = '';
//            if (!empty($promocode) && $promocodeKey) {
//                $getDiscount = $this->discount($promocodeKey, $promocode);
//                $discount = $getDiscount['discount'];
//
//
//
//
//                $getDiscount = $this->discount($promocodeKey, $promocode, true);
//                if ($promocodeKey == 'orderItems') {
//                    $orderItems[] = array(
//                        'product_id' => $getDiscount['product'],
//                        'quantity' => $getDiscount['quantity'],
//                        'packed_quantity' => $getDiscount['quantity'],
//                        'created_at' => date('Y-m-d H:i:s'),
//                        'nature' => 'FREE'
//                    );
//                }
//                $discount = $getDiscount['discount'];
//                $message = $getDiscount['message'];
//            }
//
//
//            $orderItems = array();
//
//            if (count($cartItems)) {
//
//                $productSku = array();
//                foreach ($cartItems as $cartItem) {
//                    if (isset($cartItem['free'])) {
//                        continue;
//                    }
//                    $quantity = $cartItem['rate'];
//                    $mrp = isset($cartItem['rlp']) ? $cartItem['rlp'] : 0;
//                    $total = $total + ($mrp * $quantity);
//
//                    $productSku[] = array(
////                        "sku_id" =>isset($cartItem->products->sku_number) ? $cartItem->products->sku_number : 0,
//                        "sku_id" => $cartItem['id'],
//                        "sku_units" => $cartItem['rate'],
//                        "sku_unit_cost" => isset($cartItem['rlp']) ? $cartItem['rlp'] : 0,
//                        "sku_description" => isset($cartItem['short_description']) ? $cartItem['short_description'] : 'N/A'
//                    );
//
//                    $orderItems[] = array(
//                        'product_id' => $cartItem['id'],
//                        'quantity' => $cartItem['rate'],
//                        'packed_quantity' => $cartItem['rate'],
//                    );
//
//                    $stockQuantity = \App\HubStock::where('distributor_id', $userDetail->distributor_id)
//                                    ->where('product_id', $cartItem['id'])->first();
//                    $stockQuantity->out_stock = $stockQuantity->out_stock + $cartItem['quantity'];
//
//                    $stockQuantity->save();
//                }
//
//                /*                 * *
//                 *  discount calculation pending here
//                 * */
//                $total = $total - $discount;
//
//                if ($total < 0) {
//                    return $this->apiResponse([
//                                'status' => false,
//                                'message' => 'To apply this promocode minimum cart value should be ' . ($discount + 100)
//                                    ], __('api/responseCode.failed'));
//                }
//
//                /*                 * *
//                 *  discount calculation end here
//                 * */
//
//
//                $mapDistributor = new \App\MapDistributor;
//                $userDetail = $mapDistributor->where('retailer_id', $userId)->first();
//
//
//                $orders = array(
//                    'user_id' => $userId,
//                    'total_amount' => $total,
//                    'placed_by' => Auth::user()->id,
//                    'promocode' => $promocode,
//                    'promocode_key' => $promocodeKey,
//                    'order_by' => 3,
//                    'discount_amount' => $discount,
//                    'order_number' => substr(sha1(time()), -10),
//                    'hub_id' => $userDetail->distributor_id,
//                    'delivery_time' => \Carbon\Carbon::parse($request->post('deliver_date') . ' ' . $request->post('delivery_time'))->format('Y-m-d H:i:s')
//                );
//                //dd($orders);
//
//                /**
//                 *
//                 * store to local database
//                 * */
//                $placeOrder = $orderRepository->store($orders);
//
//                for ($i = 0; $i < count($orderItems); $i++) {
//                    $orderItems[$i]['order_id'] = $placeOrder->id;
//                }
//
//                /**
//                 * store order items as per order id
//                 * */
//                $orderItemsRepository->placeOrder($orderItems);
//
//                /**
//                 * place new order to third party api using GuzzleHttp
//                 * */
//                $thirdPartyApiPlaceOrder = $this->placeOrderThirdPartyPanel($placeOrder, $productSku, $apiOrderRepository, $userId);
//
//                /**
//                 *  after successfully order place remove item from users cart
//                 * */
//                $deleteCartProduct = $cartRepository->deleteUserCartProducts($userId);
//
//                /*                 * *
//                 *
//                 * send message & push notification after successfully placed order
//                 *
//                 */
//                $message = __('api/notification.pushNotification.12', ['number' => $placeOrder->order_number]);
//                //$message = "Order successfully placed. We are pleased to confirm your order no #$placeOrder->order_number. Thank you for shopping with DS Group.";
//
//                $orderLogRepository = new \App\Repository\OrdersLogRepository;
//                $orderLogRepository->manageLog($placeOrder);
//
//                $this->sendSms($message, $userId);
//
//                $title = __('api/responseCode.title.12');
//
//                $this->sendPushNotification($message, $title, $userId, 'place-order');
//
//                $this->sendNotification($type = 'PLACE_ORDER', $placeOrder);
//                DB::commit();
//
//                return $this->apiResponse([
//                            'status' => true,
//                            'message' => 'Order placed successfully',
//                            'data' => $placeOrder,
//                                //'api' => $thirdPartyApiPlaceOrder
//                                ], __('api/responseCode.success'));
//            }
//
//            return $this->apiResponse([
//                        'status' => false,
//                        'message' => 'Your cart is empty'
//                            ], __('api/responseCode.failed'));
//        } catch (\Exception $exception) {
//            /**
//             *  rollback if order place have any error
//             * */
//            DB::rollback();
//
//            return $this->apiResponse([
//                        'status' => false,
//                        'message' => 'Order failed. Please try again',
//                        'error' => 'Error : ' . $exception->getMessage(),
//                            ], __('api/responseCode.server_error'));
//        }
//    }

    /**
     *  added by niraj lal rahi
     * function is used to place order by retailer user to the third party api
     * @param
     * $order (Object)
     * $productSku,
     * ApiOrderRepository,
     *
     * @return
     * @void
     *
     * */
    public function placeOrderThirdParty($order, $productSku, ApiOrderRepository $apiOrderRepository) {
        $post = array();
        /**
         *  prepare data format to post in third party api
         * */
        $retailerRepository = new \App\Repository\RetailerRepository(new \App\Retailer);
        $userDetail = $retailerRepository->getById(request()->user()->id);
        $post["order"] = array(
            "customer_contact_number" => $userDetail->primary_mobile_number,
            "locality" => $userDetail->shop_address,
            "customer_address" => "Noida",
            "to_approx_lat" => 1.00,
            "to_approx_lng" => 21.22,
            "amount" => $order->total_amount,
            "payment_type" => "cash",
            "customer_pic_url" => $userDetail->capture_retailer_image,
            "customer_name" => $userDetail->full_name,
            "vendor_order_id" => $order->id,
            "store_id" => $this->storeId,
            "sku_list" => $productSku
        );

        $options = array(
            'headers' => array(
                'Content-Type' => 'application/json'
            ),
            'body' => \GuzzleHttp\json_encode($post),
            'query' => array(
                'session_token' => $this->sessionToken
            )
        );
        /**
         * set endpoint for the api url to post the request
         * */
        $this->endpoint = $this->endpoint . "orders";
        /**
         * make request to the curl
         * */
        $response = $this->requestFromCurl($options, 'POST');
        if ($response['statusCode'] != 200)
            throw new Exception("Api error in place order");

        /**
         * if response success then store in database
         * */
        $this->storeApiResponse($response['body']->order, $apiOrderRepository);
        return $response;
    }

    public function placeOrderThirdPartyPanel($order, $productSku, ApiOrderRepository $apiOrderRepository, $userId) {
        $post = array();
        /**
         *  prepare data format to post in third party api
         * */
        $retailerRepository = new \App\Repository\RetailerRepository(new \App\Retailer);
        $userDetail = $retailerRepository->getById($userId);
        $post["order"] = array(
            "customer_contact_number" => $userDetail->primary_mobile_number,
            "locality" => $userDetail->shop_address,
            "customer_address" => "Noida",
            "to_approx_lat" => 1.00,
            "to_approx_lng" => 21.22,
            "amount" => $order->total_amount,
            "payment_type" => "cash",
            "customer_pic_url" => $userDetail->capture_retailer_image,
            "customer_name" => $userDetail->full_name,
            "vendor_order_id" => $order->id,
            "store_id" => $this->storeId,
            "sku_list" => $productSku
        );

        $options = array(
            'headers' => array(
                'Content-Type' => 'application/json'
            ),
            'body' => \GuzzleHttp\json_encode($post),
            'query' => array(
                'session_token' => $this->sessionToken
            )
        );
        /**
         * set endpoint for the api url to post the request
         * */
        $this->endpoint = $this->endpoint . "orders";
        /**
         * make request to the curl
         * */
        $response = $this->requestFromCurl($options, 'POST');
        if ($response['statusCode'] != 200)
            throw new Exception("Api error in place order");

        /**
         * if response success then store in database
         * */
        $this->storeApiResponse($response['body']->order, $apiOrderRepository);
        return $response;
    }

    /**
     *  added by niraj lal rahi
     * function is used to save api response after place order
     * @param
     * $input (Object)
     * ApiOrderRepository,
     *
     * @return
     * @void
     *
     * */
    public function storeApiResponse($input, ApiOrderRepository $apiOrderRepository) {
        $prepareInput = array(
            'api_order_id' => $input->id,
            'orders_id' => $input->vendor_order_id,
            'vendor_name' => $input->vendor_name,
            'vendor_contact_number' => $input->vendor_contact_number,
            'vendor_address' => $input->vendor_address,
            'status' => $input->status,
            'cancellation_reason' => $input->cancellation_reason,
            'order_time' => $input->order_time,
            'assigned_time' => $input->assigned_time,
            'accepted_time' => $input->accepted_time,
            'arrived_time' => $input->arrived_time,
            'dispatched_time' => $input->dispatched_time,
            'delivered_time' => $input->delivered_time,
            'cancelled_time' => $input->cancelled_time,
            'rider_name' => $input->rider_name,
            'rider_phone' => $input->rider_phone,
            'vendor_pin' => $input->vendor_pin,
            'customer_pin' => $input->customer_pin,
            'total_distance' => $input->total_distance,
            'total_cost' => $input->total_cost,
            'sku_list' => json_encode($input->sku_list),
            'has_skus' => $input->has_skus,
            'rider_id' => $input->rider_id
        );

        $skuInsert = $this->skuStore($input->sku_list);

        return $apiOrderRepository->store($prepareInput);
    }

    /**
     *  added by niraj lal rahi
     *  this function is used to store all the orders skus
     * @param
     * OrderRepository
     *
     * @return
     *
     * status(boolean)
     * message (string)
     * data (object)
     *
     * */
    public function skuStore(array $inputs) {
        # code...
        try {
            $multiInsert = array();
            foreach ($inputs as $input) {
                $multiInsert[] = array(
                    'api_id' => $input->id,
                    'sku_id' => $input->sku_id,
                    // 'description' => $input->sku_description'],
                    'unit_cost' => $input->sku_unit_cost,
                    'units' => $input->sku_units,
                    // 'units_delivered' => $input->id'],
                    // 'amount_collected' => $input->id'],
                    // 'status' => $input->id'],
                    'api_order_id' => $input->order_id,
                    'created_at' => date('Y-m-d H:i:s')
                        //'packing_type' => $input->id']
                );
            }
            $apiSkuListsRepository = new ApiSkuListsRepository;
            return $apiSkuListsRepository->insertBulk($multiInsert);
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage() . " store skulist");
        } catch (\Throwable $throwable) {
            /**
             *  rollback if order place have any error
             * */
            DB::rollback();

            throw new \Exception($throwable->getMessage() . " store skulist");
        }
        }



        /**
         *  added by niraj lal rahi
         *  this function is used to list down all the orders
         * @param
         * OrderRepository
         *
         * @return
         *
         * status(boolean)
         * message (string)
         * data (object)
         *
         * */
        public function list(OrderRepository $orderRepository){
        try {

            $userId = request()->user()->id;
            $orderList = $orderRepository->list($userId);
            $cancelReason = array(
                'Double Order Punched',
                'Order Cancelled By Consumer',
                'No Rider Assigned',
                'Rider Late For Pickup',
                'No Reason Specified',
                'Order Spoiled',
                'Wrong Location Selected'
            );
            return $this->apiResponse([
                        'status' => true,
                        'message' => count($orderList) . ' Order listed successfully',
                        'data' => $orderList,
                        'cancelReason' => $cancelReason
                            ], __('api/responseCode.success'));
        } catch (\Exception $exception) {
            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Order listing failed. Please try again',
                        'error' => 'Error : ' . $exception->getMessage(),
                            ], __('api/responseCode.server_error'));
        }
    }

    /**
     * added by niraj lal rahi
     * api is used to cancel order
     *
     * @param
     * Request
     * OrderRepository
     * ApiOrderRepository
     *
     *
     * @return
     *  status(boolean)
     *  message (string)
     *
     * */
    public function cancel(Request $request, OrderRepository $orderRepository, ApiOrderRepository $apiOrderRepository) {

        try {

            $cancelReason = array(
                'Double Order Punched',
                'Order Cancelled By Consumer',
                'No Rider Assigned',
                'Rider Late For Pickup',
                'No Reason Specified',
                'Order Spoiled',
                'Wrong Location Selected'
            );

            $validator = \Validator($request->post(), [
                'order_id' => 'required | numeric | min : 1',
                'cancellation_reason' => ['required', Rule::in($cancelReason)]
                    ], [
                'order_id.required' => 'Order is not defined',
                'order_id.numeric' => 'Not valid order details',
                'order_id.min' => 'Not valid order details',
                'cancellation_reason.required' => 'Please provide cancellation reason'
            ]);

            if ($validator->fails()) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => $validator->errors()->first(),
                                ], __('api/responseCode.bad_request'));
            }

            $userId = request()->user()->id;
            $orderId = $request->post('order_id');

            DB::beginTransaction();

            $validUserOrder = $orderRepository->validOrderOfUser($userId, $orderId);
            $orderNumber = $validUserOrder->order_number;
            if ($validUserOrder) {

                if ($validUserOrder->status == 0)
                    return $this->apiResponse([
                                'status' => true,
                                'message' => 'Order already cancelled',
                                    ], __('api/responseCode.success'));




                $update['status'] = 0;
                $cancelOrder = $orderRepository->update($orderId, $update);

                $apiCancelOrder = $this->cancelApiOrder(
                        $validUserOrder, $request->post('cancellation_reason'), $apiOrderRepository
                );

                if ($cancelOrder and $apiCancelOrder['statusCode'] == 200) {

                    $orderLogRepository = new \App\Repository\OrdersLogRepository;
                    $orderLogRepository->manageLog($validUserOrder);

                    $mapDistributor = new \App\MapDistributor;
                    $userDetail = $mapDistributor->where('retailer_id', $userId)->first();
                    /**
                     * Stock update quantity
                     */
                    foreach ($validUserOrder->orderItems as $items) {
                        $stockQuantity = \App\HubStock::where('distributor_id', $userDetail->distributor_id)
                                        ->where('product_id', $items->product_id)->first();
                        $stockQuantity->out_stock = $stockQuantity->out_stock - $items->quantity;
                        $stockQuantity->save();
                    }

                    $messageBody = __('api/responseCode.pushNotification.0', ['number' => $orderNumber]);

                    $this->sendSms($messageBody, $userId);
                    $title = __('api/responseCode.title.0');

                    $this->sendPushNotification($messageBody, $title, $userId, 'place-order');

                    $this->sendNotification($type = 'CANCEL_ORDER', $validUserOrder);


                    DB::commit();

                    return $this->apiResponse([
                                'status' => true,
                                'message' => 'Order cancelled successfully',
                                    ], __('api/responseCode.success'));
                }

                DB::rollback();
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Failed to cancel order. Please try again',
                                ], __('api/responseCode.failed'));
            }

            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Operation not permitted for the current user'
                            ], __('api/responseCode.unauthorized'));
        } catch (\Exception $exception) {

            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Order cancel failed. Please try again',
                        'error' => 'Error : Order not exist ',
                            ], __('api/responseCode.server_error'));
        }
    }

    /**
     *  added by niraj lal rahi
     *  function is used to cancel the order to change the api status
     * @param
     * $order
     * $reason
     * ApiOrderRepository
     *
     * @return
     * void()
     * */
    public function cancelApiOrder($order, $reason, ApiOrderRepository $apiOrderRepository) {

        if (empty($order))
            return false;

        if (!isset($order->apiOrders))
            return false;

        if (empty($order->apiOrders))
            return false;

        $post = array();
        $post["order"] = array(
            "cancellation_reason" => $reason //need to change in dynamic
        );

        $options = array(
            'headers' => array(
                'Content-Type' => 'application/json'
            ),
            'body' => \GuzzleHttp\json_encode($post),
            'query' => array(
                'session_token' => $this->sessionToken
            )
        );
        /**
         * set endpoint for the api url to post the request
         * */
        $this->endpoint = $this->endpoint . "orders/" . $order->apiOrders->api_order_id . "/cancel";

        $response = $this->requestFromCurl($options, 'PUT');

        if ($response['statusCode'] != 200)
            throw new Exception("Api error in place order");

        $this->updateApiOrderStatus($response['body']->order, $order->apiOrders->id, $apiOrderRepository);
        return $response;
    }

    /** added by niraj lal rahi
     * function is used to update api order status to local database
     * @param
     *
     * $inputs
     * $apiOrderId
     * ApiOrderRepository
     *
     * @return
     * void()
     * */
    public function updateApiOrderStatus($inputs, $apiOrderId, ApiOrderRepository $apiOrderRepository) {
        if (empty($apiOrderId))
            return false;

        $updateArray = array();
        $updateArray['cancellation_reason'] = $inputs->cancellation_reason;
        $updateArray['status'] = $inputs->status;
        $updateArray['cancelled_time'] = $inputs->cancelled_time;

        return $apiOrderRepository->update($apiOrderId, $updateArray);
    }

    /** added by niraj lal rahi
     * function is used to filter order api
     * @param
     *
     * Request
     * OrderRepository
     *
     * @return
     * status (boolean)
     * message (string)
     * data (array)
     * */
    public function filter(Request $request, OrderRepository $orderRepository) {
        try {

            $validateRequest = \Validator($request->all(), [
                'from_date' => 'required|date_format:Y-m-d',
                'to_date' => 'required|date_format:Y-m-d'
            ]);

            if ($validateRequest->fails()) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => $validateRequest->errors()->first(),
                                ], __('api/responseCode.bad_request'));
            }
            $userId = request()->user()->id;
            $fromDate = \Carbon\Carbon::parse($request->from_date)->format('Y-m-d');
            $toDate = \Carbon\Carbon::parse($request->to_date)->addDay(1)->format('Y-m-d');
            $getListByDate = $orderRepository->getListBetweenDates($fromDate, $toDate, $userId);

            return $this->apiResponse([
                        'status' => true,
                        'message' => count($getListByDate) . ' Order listed successfully',
                        'data' => $getListByDate
                            ], __('api/responseCode.success'));
        } catch (\Exception $exception) {

            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Internal server error please try again.',
                        'error' => 'Error : ' . $exception->getMessage(),
                            ], __('api/responseCode.server_error'));
        }
    }

    public function updateStatus(Request $request, ApiOrderRepository $apiOrderRepository, ApiSkuListsRepository $apiSkuListsRepository) {

        try {

            // if (0 !== strpos($request->headers->get('Content-Type'), 'application/json')){
            //     return response()->json(['status' => false,'message' => 'Header content type is not accepted'],__('api/responseCode.unsupported_type'));
            // }

            $serverKey = config('app.referrer_token');

            if ($request->session_token !== $serverKey) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Not valid request',
                                ], __('api/responseCode.unauthorized'));
            }

            if (!empty($request->order)) {

                if (isset($request->order['id'])) {

                    $findProduct = $apiOrderRepository->getByCondition(['api_order_id' => $request->order['id']])->first();
                    if (empty($findProduct))
                        return $this->apiResponse([
                                    'status' => false,
                                        ], __('api/responseCode.failed'));
                    $prepareInput = array(
                        'status' => $request->order['status'],
                        'cancellation_reason' => $request->order['cancellation_reason'],
                        'assigned_time' => $request->order['assigned_time'],
                        'accepted_time' => $request->order['accepted_time'],
                        'arrived_time' => $request->order['arrived_time'],
                        'dispatched_time' => $request->order['dispatched_time'],
                        'delivered_time' => $request->order['delivered_time'],
                        'cancelled_time' => $request->order['cancelled_time'],
                        'rider_name' => $request->order['rider_name'],
                        'rider_phone' => $request->order['rider_phone'],
                        'vendor_pin' => $request->order['vendor_pin'],
                        'customer_pin' => $request->order['customer_pin'],
                        'total_distance' => $request->order['total_distance'],
                        'total_cost' => $request->order['total_cost'],
                        'has_skus' => $request->order['has_skus'],
                            //'sku_list' => json_encode($request->order['sku_list'])
//                        'rider_id' => $request->order['rider_id']
                    );

                    $where = array(
                        'api_order_id' =>
                        $request->order['id']
                    );
                    //$findSku = $apiSkuListsRepository->getByCondition($where)->first();
                    DB::beginTransaction();
                    if ($apiOrderRepository->update($findProduct->id, $prepareInput) &&
                            $apiSkuListsRepository->updateSku($where, $request->order['sku_list'])) {
                        DB::commit();
                        return $this->apiResponse([
                                    'status' => true,
                                        ], __('api/responseCode.success'));
                    }
                    DB::rollback();
                    return $this->apiResponse([
                                'status' => false,
                                    ], __('api/responseCode.failed'));
                }

                return $this->apiResponse([
                            'status' => false
                                ], __('api/responseCode.success'));
            }
            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Operation not permitted for the current user'
                            ], __('api/responseCode.unauthorized'));
        } catch (\Exception $exception) {

            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Internal server error please try again.',
                        'error' => 'Error : ' . $exception->getMessage(),
                            ], __('api/responseCode.server_error'));
        }
    }

    public function reOrder(Request $request, CartRepository $cartRepository, OrderRepository $orderRepository, OrderItemsRepository $orderItemsRepository, ApiOrderRepository $apiOrderRepository
    ) {


        try {
            /*             * *
             *  Begin transaction here
             *  place order
             * */
            $keys = ['place-order'];

            $validate = \Validator($request->post(), [
                'order_id' => 'required',
                'deliver_date' => ['required', 'date_format:Y-m-d'],
                'delivery_time' => ['required', 'date_format:H:i']
            ]);

            if ($validate->fails()) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => $validate->errors()->first(),
                                ], __('api/responseCode.bad_request'));
            }

            DB::beginTransaction();

            $userId = request()->user()->id;
            $orderId = $request->post('order_id');

            $isValidOrder = $orderRepository->validOrderOfUser($userId, $orderId);

            if (!$isValidOrder) {
                return $this->apiResponse([
                            'status' => false,
                            'message' => 'Invalid request',
                                ], __('api/responseCode.unauthorized'));
            }


            $productSku = array();
            $total = $discount = 0;

            foreach ($isValidOrder->orderItems as $cartItem) {

                $quantity = $cartItem->quantity;
                $mrp = isset($cartItem->products->rlp) ? $cartItem->products->rlp : 0;
                $total = $total + ($mrp * $quantity);

                $productSku[] = array(
//                        "sku_id" =>isset($cartItem->products->sku_number) ? $cartItem->products->sku_number : 0,
                    "sku_id" => 2,
                    "sku_units" => $cartItem->quantity,
                    "sku_unit_cost" => isset($cartItem->products->rlp) ? $cartItem->products->rlp : 0,
                    "sku_description" => isset($cartItem->products->short_description) ? $cartItem->products->short_description : 'N/A'
                );

                $orderItems[] = array(
                    'product_id' => $cartItem->product_id,
                    'quantity' => $cartItem->quantity
                );
            }

            /*             * *
             *  discount calculation pending here
             * */
            $total = $total - $discount;
            /*             * *
             *  discount calculation end here
             * */
            $mapDistributor = new \App\MapDistributor;
            $userDetail = $mapDistributor->where('retailer_id', $userId)->first();
            $orders = array(
                'user_id' => $userId,
                'total_amount' => $total,
                'promocode' => '',
                'discount_amount' => $discount,
                'hub_id' => $userDetail->distributor_id,
                'order_number' => substr(sha1(time()), -10),
                'delivery_time' => \Carbon\Carbon::parse($request->post('deliver_date') . ' ' . $request->post('delivery_time'))->format('Y-m-d H:i:s'),
            );

            /**
             *
             * store to local database
             * */
            $placeOrder = $orderRepository->store($orders);

            for ($i = 0; $i < count($orderItems); $i++) {
                $orderItems[$i]['order_id'] = $placeOrder->id;
                $orderItems[$i]['created_at'] = date('Y-m-d H:i:s');
            }

            /**
             * store order items as per order id
             * */
            $orderItemsRepository->insertBulk($orderItems);

            /**
             * place new order to third party api using GuzzleHttp
             * */
            $thirdPartyApiPlaceOrder = $this->placeOrderThirdParty($placeOrder, $productSku, $apiOrderRepository);


            $this->sendNotification($type = 'PLACE_ORDER', $placeOrder);


            /*             * *
             *
             * send message & push notification after successfully placed order
             *
             */
            $message = __('api/responseCode.pushNotification.12', ['number' => $placeOrder->order_number]);
            //$message = "Order successfully placed. We are pleased to confirm your order no #$placeOrder->order_number. Thank you for shopping with DS Group.";

            $this->sendSms($message, $userId);

            $title = __('api/responseCode.title.12');
            $this->sendPushNotification($message, $title, $userId, 'place-order');

            $orderLogRepository = new \App\Repository\OrdersLogRepository;
            $orderLogRepository->manageLog($placeOrder);

            DB::commit();

            return $this->apiResponse([
                        'status' => true,
                        'message' => 'Order placed successfully',
                        'data' => $placeOrder
                            ], __('api/responseCode.success'));
        } catch (\Exception $exception) {
            /**
             *  rollback if order place have any error
             * */
            DB::rollback();

            return $this->apiResponse([
                        'status' => false,
                        'message' => 'Order failed. Please try again',
                        'error' => 'Error : ' . $exception->getMessage(),
                            ], __('api/responseCode.server_error'));
        }
    }
    
    public function checkIsActiveRetailer($param) {
       $data = \App\Retailer::where('id',$param)->first();
       if($data->status == 1){
           return true;
       }
        return false;
    }

}
