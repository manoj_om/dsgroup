<?php

namespace App\Traits;
use App\Repository\WishlistRepository;
use Illuminate\Http\Request;
use App\Repository\ProductRepository;
trait WishListTrait
{
    //
    public function addToWishList(Request $request,WishlistRepository $wishlistRepository,ProductRepository $productRepository){
        try {
            $validate = $this->validateWishListRequest($request->post());
            if ($validate->fails()) {

                return $this->apiResponse([
                    'status' => false,
                    'message' => $validate->errors()->first(),
                ], __('api/responseCode.bad_request'));
            }
            $isValidProduct = $productRepository->getById($request->post('product_id'));
            $requestData = $request->post();
            $requestData['user_id'] = request()->user()->id;


            $addToWishlist = $wishlistRepository->store($requestData);

            if ($addToWishlist)
                return $this->apiResponse([
                    'status' => true,
                    'message' => 'Successfully added to your wishlist',
                    'data' => $addToWishlist
                ], __('api/responseCode.success'));

            return $this->apiResponse([
                'status' => false,
                'message' => 'Failed to add into your wishlist'
            ], __('api/responseCode.failed'));

        }catch (\Exception $exception){

            return $this->apiResponse([
                'status'=> false,
                'message' => 'Invalid product'
            ],__('api/responseCode.server_error'));
        }

    }

    public function validateWishListRequest(array $data){
        return \Validator($data,[
            'product_id' => 'required | numeric | min : 1'
        ],[
            'product_id.required' => 'Please send the product you want to add on wishlist',
            'product_id.numeric' => 'Invalid product. Please add valid product',
            'product_id.min' => 'Invalid product. Please add valid product'
        ]);
    }

    public function getList(WishlistRepository $wishlistRepository){
        try{
            $userId = request()->user()->id;
            $list = $wishlistRepository->getItemFromUserId($userId);

            return $this->apiResponse([
                'status'=> true,
                'message' => count($list).' Records found',
                'data' => $list
            ],__('api/responseCode.success'));

        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }

    }


    public function remove($id,WishlistRepository $wishlistRepository){
        try{
            $validateId = \Validator(['product_id' =>  $id],[
                'product_id' => 'required|numeric'
            ],[
             'product_id.required' => 'Please enter product you wish to add',
             'product_id.numeric' => 'Please enter valid product'
            ]);

            if($validateId->fails())
                return $this->apiResponse([
                    'status' => false,
                    'message' => $validateId->errors()->first(),
                ], __('api/responseCode.bad_request'));

            $userId = request()->user()->id;
            $productId = $id;

            $getList = $wishlistRepository->getListFromProductIdAndUserId($userId,$productId);
            if(!empty($getList)){
                if($wishlistRepository->destroy($getList->id))
                    return $this->apiResponse([
                        'status'=> true,
                        'message' => 'Item removed successfully',
                    ],__('api/responseCode.success'));

                return $this->apiResponse([
                    'status'=> false,
                    'message' => 'Sorry! Please try again to remove whishlist item',
                ],__('api/responseCode.failed'));
            }
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Sorry! item not found',
            ],__('api/responseCode.failed'));

        }catch (\Exception $exception){
            return $this->apiResponse([
                'status'=> false,
                'message' => 'Internal server error',
                'errot' => 'Error : '. $exception->getMessage()
            ],__('api/responseCode.server_error'));
        }
    }


}
