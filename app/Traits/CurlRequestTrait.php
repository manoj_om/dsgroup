<?php

namespace App\Traits;
use Illuminate\Support\Facades\Auth;

trait CurlRequestTrait
{
    //

   /* public $options = array(
        'query' => $query //array
        'headers' => $headers //array
        'version' => '1.0'
        'body' $body //array
    );*/
    function requestFromCurl(array $options,$method = 'GET')
    {
        $endpoint = $this->endpoint;
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $response = $client->request($method, $endpoint, $options);
//        $response = $response->send();
//        $statusCode = $response->getStatusCode();
//        $content = $response->getBody();
        return $newRes = array(
            'body' =>  \GuzzleHttp\json_decode($response->getBody()->getContents()),
            'statusCode' => $response->getStatusCode()
        );
    }

    /***
     *  function to send notification and sms
     *
     */
    public function sendSms($messageBody,$userId,$to='retailer'){
        if($to=='retailer'){
            $retailerRepository = new \App\Repository\RetailerRepository(new \App\Retailer);
            $profileData = $retailerRepository->getById($userId);

            $message_body= urlencode($messageBody);
            $message="&send_to=".$profileData->primary_mobile_number."&msg=$message_body";
        }else{
            $agent = \App\AgentsOnboardings::find($userId);
            if(!$agent)
                return true;
            $message_body= urlencode($messageBody);
            $message="&send_to=".$agent->mobile_number."&msg=$message_body";
        }


        return \App\Helpers\Helpers::sendSMS(config('api.smsurl').$message);
    }

    /**
     *  send push notification to the user if order placed successfully
     *
     */
    private function sendPushNotification($message,$title=null,$userId=null,$key='test',$id = 'test'){

        $this->endpoint = "https://fcm.googleapis.com/fcm/send";
        $registrationIds = array();
        if($userId){
            if(is_array($userId)){
                $fcm = \App\DeviceFcmToken::whereIn('user_id',$userId)->get()->toArray();
                $registrationIds = !empty($fcm) ? array_column($fcm,'token')  : array();
            }else{
                $fcm = \App\DeviceFcmToken::where('user_id',$userId)->first();
                if($fcm)
                    $registrationIds = array($fcm->token);
            }

        }else{
            $allFcm = \App\DeviceFcmToken::get()->toArray();
            $registrationIds = !empty($allFcm) ? array_column($allFcm,'token')  : array();
        }

        if(empty($registrationIds))
            return true;
        $msg = array
        (
            'message' 	=> $message,
            'title'		=> !empty($title) ? $title  : $message,
            'subtitle'	=> 'Order placed successfully',
            'key' => $key,
            'id' => $id

        );
        $body = array
        (
            'registration_ids' 	=> $registrationIds,
            'data'			=> $msg
        );
        $fields = array(
            'headers' => array(
                'Authorization' =>  'key='.config('app.fcm_key'),
                'Content-Type' =>  'application/json'
            ),
            'body' => \GuzzleHttp\json_encode($body),

        );

        return $this->requestFromCurl($fields,'POST');
    }


    public function sendNotification($type,$data){

        $gaurd = Auth::getDefaultDriver();
        $action_by = Auth::guard($gaurd)->user()->id;

        $notificationRepository = new  \App\Repository\NotificationRepository;
        $notification =
        [
            'type' => $type,
            'user_id' => $data->user_id,
            'action_by' => $action_by,
            'action_user_gaurd' => $gaurd,
            'data' => json_encode($data),
            'retailer_message' => __('api/notification.webNotification.'.$gaurd.'.retailer.'.$type,['number' => $data->order_number]),
            'hub_message' => __('api/notification.webNotification.'.$gaurd.'.distributor.'.$type,['number' => $data->order_number]),
            'admin_message' => __('api/notification.webNotification.'.$gaurd.'.admin.'.$type,['number' => $data->order_number])
        ];

        return $notificationRepository->store($notification);
    }
    public function sendThresholdNotification($type,$data){



        $gaurd = Auth::getDefaultDriver();
        $action_by = Auth::guard($gaurd)->user()->id;

        $notificationRepository = new  \App\Repository\NotificationRepository;
        $notification =
        [
            'type' => $type,
            'user_id' => $data->user_id,
            'action_by' => $action_by,
            'action_user_gaurd' => $gaurd,
            'data' => json_encode($data),
            'hub_message' => __('api/notification.webNotification.'.$gaurd.'.distributor.'.$type,['stock' => $data->name]),
        ];

        return $notificationRepository->store($notification);
    }





}
