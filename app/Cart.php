<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Cart extends Model
{
    //
    protected $table = 'carts';

    protected $fillable = [
        'product_id', 'user_id', 'quantity',
    ];

    public function products(){

        return $this->belongsTo('App\Product','product_id','id')
        ->select('id','product_lists_id','short_description',
        'product_unit_weight as package_unit','packaging','quantity','category',
            'sub_category','mrp','rlp','brand_name','manufacturer',
            'max_order_quantity','product_unit_weight','avtar');
    }


}
