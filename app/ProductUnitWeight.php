<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductUnitWeight extends Model
{
   protected $fillable =['name','status'];

}
