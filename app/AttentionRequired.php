<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttentionRequired extends Model
{


    protected $fillable = [
        'user_id','retailer_id','scan_code','reason','sku_name','status','feedback_id','user_type','resolution'
    ];
    public function getRetailer() {
            return $this->hasOne('App\Retailer','id','retailer_id');
    }
    public function feedback() {
            return $this->hasOne('App\FeedbackRemark','id','feedback_id');
    }

    public function admin(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function onboardingAgent(){
        return $this->hasOne('App\AgentsOnboardings','id','user_id');
    }

    public function callingAgent()
    {
        # code...
        return $this->hasOne('App\CallingAgent','id','user_id');
    }


}
