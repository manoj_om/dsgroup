<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedbackRemark extends Model
{


   protected $fillable = [
        'remark','created_by','status','feedback_category_id'
    ];

     public function feedbackCategory() {
        return $this->hasOne('App\FeedbackCategory', 'id', 'feedback_category_id');
    }


}
