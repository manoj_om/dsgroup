<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSubCategory extends Model
{

    protected $attributes = [
        'status' => 0
    ];


    protected $fillable = [
        'name', 'created_by','status','category','avtar'
    ];
    protected $appends = ['image_url'];

    public function category(){
        return $this->hasOne('App\ProductCategory','id','category');
    }

    public function getImageUrlAttribute(){
        if(file_exists('images/product/subcategories/'.$this->avtar))
            return url('images/product/subcategories/'.$this->avtar);
        return NULL;
    }

}
