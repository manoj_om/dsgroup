<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HubStockDateCheck extends Model
{
    protected $fillable = ['total','hub_id'];
}
