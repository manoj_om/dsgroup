<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderedItemsDiscount extends Model
{
     protected $fillable = [
         'scheme_name','from_date','to_date','promocode','frequency','shop_type','primary_product','primary_product_count','free_secondary_product','free_secondary_product_count',
         'apply_discount','fixed_percentage','max_time_applicability','applied_location','status','created_by','retailer_type','channel_type'
     ];

}
