<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersLog extends Model
{
    //

    protected $table = "orders_logs";

    protected $fillable = ['order_id','retailer_id','hub_id','total_amount',
    'action_by','action_user_gaurd','data','status'];



}
