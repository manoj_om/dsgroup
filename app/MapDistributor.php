<?php

namespace App;

//use App\Distributor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MapDistributor extends Model {




    public $table = 'map_distributors';
    protected $fillable = ['distributor_id', 'retailer_id'];

    public function distributorData() {
        return $this->hasOne('App\Distributor', 'id', 'distributor_id');
    }
    public function stockDistributorData() {
        return $this->hasOne('App\Distributor', 'id', 'distributor_id');
    }
    public function retailer() {
        return $this->hasOne('App\Retailer', 'id', 'retailer_id');
    }
    public function orders() {
        return $this->hasMany('App\Order', 'user_id', 'retailer_id');
    }



}
