<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RetailersQueue extends Model
{
    //

    public function retailers(){
        
        return $this->hasOne('App\Retailer','id','retailer_id')->where('status',0);
    }
}
