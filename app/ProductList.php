<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductList extends Model
{
    protected $fillable = [
        'name','status','created_by','category_id','subcategory_id'
    ];


    public function category() {
        return $this->hasOne('App\ProductCategory', 'id', 'category_id')->where('status',0);
    }

    public function subcategory() {
        return $this->hasOne('App\ProductSubCategory', 'id', 'subcategory_id')->where('status',0);
    }

    public function associateProduct(){
        return $this->hasMany('App\Product','product_lists_id','id')->select('id','product_lists_id','short_description','package_unit',
            'packaging','quantity','category','sub_category','mrp','rlp','brand_name','manufacturer','max_order_quantity','product_unit_weight')->where('status',0);
    }






}
