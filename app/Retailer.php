<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\AgentsOnboardings;
use Carbon\Carbon;
class Retailer extends Authenticatable {

    use HasApiTokens;

    protected $hidden = ['otp'];

    protected $fillable = [
      'user_id','full_name','primary_mobile_number','alternative_mobile_number','whats_app_number','capture_retailer_image','secondary_full_name',
      'secondary_mobile_number','secondary_capture_retailer_image','shop_image','shop_type','shop_name','dob','anniversay','first_kid_birthday','second_kid_birthday',
       'establishment_name','gst_number','pan_number','adhar_number','gst_number_file','pan_number_file','adhar_number_file','p_device_type','scan_code','shop_address',
       'latitudes','longitude','city','state','name_as_per_id','user_type','is_fill','is_prime','attempt_count','status'
        ,'otp'];

    protected $appends = [
        'primary_image',
        'secondary_image',
        'shops_image'
    ];


    public function getStatus($param) {
        if($param == 0){
            return "Active";
        }
        if($param == 1){
            return "Inactive";
        }
    }
    public function getUserType($param) {
        if($param == 1){
            return "Agent";
        }
        if($param == 2){
            return "Admin";
        }
    }
    public function getUserName($type,$id) {
        if($type == 1){
             $user = AgentsOnboardings::where('id',$id)->first();
             return $user->name;
        }
        if($type == 2){
             $user = User::where('id',$id)->first();
             return $user->name;
        }
    }

    public function getPrimaryImageAttribute(){
        if(!empty($this->capture_retailer_image))
            return url('uploads/retailers/primary/'.$this->capture_retailer_image);
        return NULL;
    }
    public function getSecondaryImageAttribute(){
        if(!empty($this->secondary_capture_retailer_image))
            return url('uploads/retailers/secondary/'.$this->secondary_capture_retailer_image);
        return NULL;
    }
    public function getShopsImageAttribute(){
        if(!empty($this->shop_image))
            return url('uploads/retailers/shop_image/'.$this->shop_image);
        return NULL;
    }

    public function agantdata() {
        return $this->hasOne('App\AgentsOnboardings', 'id', 'user_id');
    }
    public function admin() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function MapDistributor() {
        return $this->hasOne('App\MapDistributor', 'retailer_id', 'id')->with('distributorData');
    }
    public function MapAgent() {
        return $this->hasOne('App\MapAgent', 'retailer_id', 'id')->with('agentData');
    }
    public function StockMapDistributor() {
        return $this->hasOne('App\MapDistributor', 'retailer_id', 'id')->with('stockDistributorData')->where('distributor_id',Auth::guard('hub')->user()->id);
    }
    public function StockMapDistributorpanel() {
        return $this->hasOne('App\MapDistributor', 'retailer_id', 'id');
    }
    public function orderCalling() {
        return $this->hasOne('App\RetailerOrderCalling', 'retailer_id', 'id');
    }

    public function otherQrcode() {
        return $this->hasOne('Api\Models\QrCodeModel', 'user_id', 'id');
    }

    public function getRetailerType($param) {
        if ($param == 1) {
            return 'General A';
        } else if ($param == 2) {
            return 'General B';
        } else if ($param == 3) {
            return 'General C';
        } else if ($param == 4) {
            return 'Chemist A';
        } else if ($param == 5) {
            return 'Chemist B';
        } else if ($param == 6) {
            return 'Chemist C';
        } else if ($param == 7) {
            return 'Cosmetic';
        } else if ($param == 8) {
            return 'Paan A';
        } else if ($param == 9) {
            return 'Paan B';
        } else if ($param == 10) {
            return 'Paan C';
        } else {
            return " ";
        }
    }
    public function getRetailerTypeId($param) {
        if ($param == 'General A') {
            return 1;
        } else if ($param == 'General B') {
            return 2;
        } else if ($param == 'General C') {
            return 3;
        } else if ($param == 'Chemist A') {
            return 4;
        } else if ($param == 'Chemist B') {
            return 5;
        } else if ($param == 'Chemist C') {
            return 6;
        } else if ($param == 'Cosmetic') {
            return 7;
        } else if ($param == 'Paan A') {
            return 8;
        } else if ($param == 'Paan B') {
            return 9;
        } else if ($param == 'Paan C') {
            return 10;
        } else {
            return 1;
        }
    }
    public function deviceType($param) {
        if ($param == 1) {
            return 'Android';
        } else if ($param == 2) {
            return 'iOS';
        } else if ($param == 3) {
            return 'KaiOS';
        }  else {
            return "None";
        }
    }
    public function RetailerTypeTwo($param) {
         if ($param == 1) {
            return 'Prime';
        } else if ($param == 2) {
            return 'Non Prime';
        } else if ($param == 3) {
            return 'Regular';
        }  else {
            return "None";
        }
    }
    public function RetailerTypeTwoiD($param) {
         if ($param == 'Prime') {
            return 1;
        } else if ($param == 'Non Prime') {
            return 2;
        } else if ($param == 'Regular') {
            return 3;
        }  else {
            return "None";
        }
    }
    public function dateToExcel($param) {
        return Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($param));
    }
    public function QrCode(){
        return $this->hasOne('App\RetailersQrCode','user_id','id');
    }


    /**
     * Scope a query to only include active users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 0);
    }

}
