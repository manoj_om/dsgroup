<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HubStockLog extends Model
{
     protected $fillable = [
       'stock_id','product_id','distributor_id','product_quantity','pre_created_at','out_stock','in_stock'
    ];
}
