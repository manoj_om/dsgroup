<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderVolumeScheme extends Model
{
    protected $attributes = [
        'status' => 0
    ];

     protected $fillable = [
      'channel_type', 'retailer_type','scheme_name','from_date','to_date','promocode','applied_location','status','frequency','order_count','minimum_order_value','shop_type','free_ds_product_worth_mrp','free_ds_product_worth_rlp','max_time_applicability'
    ];


}
