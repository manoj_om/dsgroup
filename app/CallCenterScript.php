<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallCenterScript extends Model
{
   protected $fillable = ['shop_type','retailer_type','call_type','script','status','created_by','script_data'];


    public function shop() {
        return $this->hasOne('App\ShopType', 'id', 'shop_type');
    }
    public function call() {
        return $this->hasOne('App\CallType', 'id', 'call_type');
    }
    
}
