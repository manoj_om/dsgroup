<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Auth\Notifications\ResetPassword;
use App\Notifications\AdminResetPasswordNotification as Notification;

class CallingAgent extends Authenticatable implements MustVerifyEmail {

    protected $guard = 'agent';

    use Notifiable;

    protected $fillable = [
        'passord_test', 'avtar', 'name', 'executive_code', 'serial_number', 'mobile_number', 'email', 'supervisor_name', 'user_name', 'shift_start_time', 'shift_end_time', 'caller_type', 'created_by', 'login_status'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    //  public function sendPasswordResetNotification($token)
    // {
    //     $this->notify(new Notification($token));
    // }

    // /**
    //  * Send email verification notice.
    //  *
    //  * @return void
    //  */
    // public function sendEmailVerificationNotification()
    // {
    //     $this->notify(new AdminEmailVerificationNotification);
    // }

    public function sendPasswordResetNotification($token)
    {

        $email = $this->getEmailForPasswordReset();
        $phone = $this->getMobileForPasswordReset($email);
        $message_body= urlencode("Please reset your password by click on this link ".route('agent.password.reset',['token' => $token,'email' => $email]).". Thank you.");
        $message="&send_to=$phone&msg=$message_body";
        \App\Helpers\Helpers::sendSMS(config('api.smsurl').$message);
        return true;
        //$this->notify(new Notification($token));

    }

    public function getMobileForPasswordReset($email){

        return $this->where('email',$email)->first()->mobile_number;

    }
    public function scopeActive($query)
    {
        return $query->where('login_status', 1);
    }


}
