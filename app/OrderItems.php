<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    //
    protected $table = 'order_items';
    protected $fillable = ['order_id','product_id','quantity','status','packed_quantity','created_at','nature'];


    public function products(){

        return $this->belongsTo('App\Product','product_id','id')->select('id','dlp','product_lists_id','short_description','product_unit_weight as package_unit',
            'packaging','quantity','category','sub_category','mrp','rlp','brand_name','manufacturer','max_order_quantity','product_unit_weight','avtar');
    }

    public function products_detail() {

        return $this->belongsTo('App\Product','product_id','id')->with('unitType','weightType','packingType','productName');
    }


}
