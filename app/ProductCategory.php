<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{

    protected $attributes = [
        'status' => 0
    ];
    protected $fillable = [
        'name', 'created_by','status','avtar'
    ];

    protected $appends = ['image_url'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function subCategory(){
        return $this->hasMany('App\ProductSubCategory','category','id')->where('status',0);
    }
    public function products(){
        return $this->hasMany('App\Product','category','id')->with('subcategory','productName','weightType','packingType','unitType','stockagent');
    }

    public function getImageUrlAttribute(){
        if(empty($this->avtar))
            return NULL;
        if(file_exists('images/product/categories/'.$this->avtar))
            return url('images/product/categories/'.$this->avtar);
        return NULL;
    }



}
