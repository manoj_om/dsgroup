<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiOrders extends Model
{
    //
    protected $table = "api_orders";
    protected $hidden = ['sku_list'];
    protected $fillable = [
        'api_order_id','orders_id',
        'vendor_name',
        'vendor_contact_number',
        'vendor_address',
        'status',
        'cancellation_reason',
        'order_time',
        'assigned_time',
        'accepted_time',
        'arrived_time',
        'dispatched_time',
        'delivered_time',
        'cancelled_time',
        'rider_name',
        'rider_phone',
        'vendor_pin',
        'customer_pin',
        'total_distance',
        'total_cost',
        'sku_list',
        'has_skus',
        'rider_id'
    ];

    protected $appends = ['sku_lists'];

    public function getSkuListsAttribute($value){
        return \GuzzleHttp\json_decode($this->sku_list);
    }


}
