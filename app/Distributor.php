<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Auth\Notifications\ResetPassword;
use App\Notifications\HubResetPasswordNotification as Notification;

class Distributor extends Authenticatable implements MustVerifyEmail {

    use Notifiable;

    protected $guard = 'hub';
    protected $attributes = [
        'status' => 1
    ];
    public $table = 'distributors';
    protected $fillable = ['distributor_name', 'distributor_code', 'mobile', 'email', 'pan_card', 'gst_no', 'aadhar_no', 'website', 'state'
        , 'city', 'latitude', 'longitude', 'address', 'password'];

    public function cityname() {
        return $this->hasOne('City', 'city', 'id');
    }

    public function distributorData() {
        return $this->hasOne('App\MapDistributor', 'distributor_id', 'id')->with('retailer');
    }

    public function retailers() {
        return $this->hasMany('\App\Retailer', 'user_id', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $email = $this->getEmailForPasswordReset();
        $phone = $this->getMobileForPasswordReset($email);
        $message_body= urlencode("Please reset your password by click on this link ".route('hub.password.reset',['token' => $token,'email' => $email]).". Thank you.");
        $message="&send_to=$phone&msg=$message_body";
        \App\Helpers\Helpers::sendSMS(config('api.smsurl').$message);
        return true;
        //$this->notify(new Notification($token));

    }

    public function getMobileForPasswordReset($email){

        return $this->where('email',$email)->first()->mobile;

    }

    /**
     * Send email verification notice.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new AdminEmailVerificationNotification);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

}
