<?php

namespace App\Imports;

use App\Retailer;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\WithValidation;
use App\Helpers\Helpers as help;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Carbon\Carbon;
use App\Helpers;

class RetailerImport implements ToModel, WithHeadingRow, WithValidation {

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row) {
        $retailer = new Retailer();
//         if (!Helpers\Helpers::barcodeScanner($row['scan_code'])) {
//            return Response()->json([
//                        'status' => 1001,
//                        'message' => "Invalid barcode",
//            ]);
//        }
           
        return new Retailer([
            'user_id' => Auth::user()->id,
            'full_name' => $row['full_name'],
            'primary_mobile_number' => $row['primary_mobile_number'],
            'alternative_mobile_number' => $row['alternative_mobile_number'],
            'whats_app_number' => $row['whats_app_number'],
            'secondary_full_name' => $row['secondary_full_name'],
            'secondary_mobile_number' => $row['secondary_mobile_number'],
            'shop_type' => $retailer->getRetailerTypeId($row['shop_type']),
            'shop_name' => $row['shop_name'],
            'dob' => $retailer->dateToExcel($row['dob']),
            'anniversay' => $retailer->dateToExcel($row['anniversay']),
            'first_kid_birthday' => $retailer->dateToExcel($row['first_kid_birthday']),
            'second_kid_birthday' => $retailer->dateToExcel($row['second_kid_birthday']),
            'third_kid_birthday' => $retailer->dateToExcel($row['third_kid_birthday']),
            'establishment_name' => $row['establishment_name'],
            'gst_number' => $row['gst_number'],
            'pan_number' => $row['pan_number'],
            'adhar_number' => $row['adhar_number'],
            'scan_code' => $row['scan_code'],
            'shop_address' => $row['shop_address'],
            'latitudes' => $row['latitudes'],
            'longitude' => $row['longitude'],
            'city' => $row['city'],
            'state' => $row['state'],
            'retailer_type' => $retailer->RetailerTypeTwoiD($row['retailer_type']),
            'name_as_per_id' => $row['name_as_per_id'],
            'user_type' => 2,
            'is_fill' => 1,
        ]);
    }

    public function rules(): array {
        return [
            'full_name' => 'required',
            'primary_mobile_number' => 'required|max:10|min:10|digits:10|unique:retailers,primary_mobile_number',
            'whats_app_number' => 'nullable|digits:10|max:10|min:10|unique:retailers,whats_app_number',
            'alternate_phone' => "nullable|digits:10|max:10|min:10|different:primary_phone|different:whatsapp_number|different:secondary_phone_number",
            'secondary_phone_number' => "nullable|digits:10|max:10|min:10|different:primary_phone|different:whatsapp_number|different:alternate_phone",
            'shop_type' => 'required',
            'shop_name' => 'required',
            'scan_code' => 'required',
            'shop_address' => 'required',
            'latitudes' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'city' => 'required',
            'state' => 'required',
            'scan_code' => 'required',
            'retailer_type' => 'required',
            'pan_number' => 'nullable|max:10|min:10',
            'adhar_number' => 'nullable|digits:12||max:12|min:12',
        ];
    }

}
