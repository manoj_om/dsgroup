<?php

namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\WithValidation;

class ImportProducts implements ToModel, WithHeadingRow,WithValidation
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row) {

        $product = new Product();

        return new Product([
            'serial_number' => Str::random(5).time(),
            'brand_name' => $row['brand_name'],
            'manufacturer' => $row['manufacturer'],
            'hsn_code' => $row['hsn_code'],
            'ean_code' => $row['ean_code'],
            'dlp' => $row['dlp'],
            'max_order_quantity' => $row['max_order_quantity'],
            'product_length' => $row['product_length'],
            'product_width' => $row['product_width'],
            'product_height' => $row['product_height'],
            'shelf_life' => $row['shelf_life'],
            'custmer_target' => $row['custmer_target'],
            'master_carton_gross_weight' => $row['master_carton_gross_weight'],
            'master_carton_net_weight' => $row['master_carton_net_weight'],
            'master_carton_length' => $row['master_carton_length'],
            'master_carton_width' => $row['master_carton_width'],
            'master_carton_height' => $row['master_carton_height'],
            'product_lists_id' => $product->findProductName($row['name'],$product->findCategory($row['category']),$product->findSubCategory($row['sub_category'],$product->findCategory($row['category']))),
            'short_description' => $row['short_description'],
            'sku_number' => $row['sku_number'],
            'package_unit' => $product->findUnitType($row['package_unit']),
            'packaging' => $product->findPackingType(($row['packing_type'])),
            'product_unit_weight' => $product->findUnitWeightType(($row['product_unit_weight'])),
            'quantity' => $row['quantity'],
            'mrp' => $row['mrp'],
            'rlp' => $row['rlp'],
            'category' => $product->findCategory(($row['category'])),
            'sub_category' => $product->findSubCategory($row['sub_category'],$product->findCategory(($row['category']))),
            'created_by' => Auth::user()->id
        ]);
    }
    
    public function rules(): array
    {
        return [
          
            'brand_name' => 'required',
            'manufacturer' => 'required',
            'hsn_code' => 'required',
            'ean_code' => 'required',
            'dlp' => 'required',
            'max_order_quantity' => 'required',
            'product_length' => 'required',
            'product_width' => 'required',
            'product_height' => 'required',
            'shelf_life' => 'required',
            'custmer_target' => 'required',
            'master_carton_gross_weight' => 'required',
            'master_carton_net_weight' => 'required',
            'master_carton_length' => 'required',
            'master_carton_width' => 'required',
            'master_carton_height' => 'required',
            'short_description' => 'required',
            'sku_number' => 'required',
            'package_unit' => 'required',
            'packing_type' => 'required',
            'product_unit_weight' => 'required',
            'quantity' => 'required',
            'mrp' => 'required',
            'rlp' => 'required',
            'category' => 'required',
            'sub_category' => 'required',

        ];
    }

}


