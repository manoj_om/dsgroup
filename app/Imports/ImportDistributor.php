<?php

namespace App\Imports;

use App\Distributor;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\City;
use App\State;
use Maatwebsite\Excel\Concerns\WithValidation;


class ImportDistributor implements ToModel, WithHeadingRow,WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //
       if(!isset($row['hub_name']) or !trim($row['hub_code']) or !trim($row['mobile'])  or !trim($row['email']) or
           !trim($row['email']) or !trim($row['pan_card'])  or !trim($row['gst_no']) or !trim($row['aadhar_no']) 
               or !trim($row['website']) or !trim($row['state']) or !trim($row['city'])  or !trim($row['latitude']) 
               or !trim($row['longitude'])){
             return -1;
           }
        $alreday_exist=Distributor::orWhere(['distributor_code'=>$row['hub_code']])
                
                ->orWhere(['mobile'=>$row['mobile']])->orWhere(['email'=>$row['email']])->first();
          $duplicate = 0;
        if(isset($alreday_exist->distributor_code) or isset($alreday_exist->mobile) or isset($alreday_exist->email))  {
             
           $duplicate++;  
        }else{    
           $pass = Str::random(8);
        return new Distributor([
            'distributor_name'=>$row['hub_name'],
            'distributor_code'=>$row['hub_code'],
            'mobile'=>$row['mobile'],
            'email'=>$row['email'],
            'pan_card'=>$row['pan_card'],
            'gst_no'=>$row['gst_no'],
            'aadhar_no'=>$row['aadhar_no'],
            'website'=>$row['website'],
            'state'=> $this->findOrInsertState($row['state']),
            'city'=> $this->findOrInsertCity($row['city']),
            'latitude'=>$row['latitude'],
            'longitude'=>$row['longitude'],
            'address'=>$row['address'],
            'password'=>$this->sendMessege($pass,$row['mobile'])
        ]);
        }
    }
    
    public function rules(): array
    {
        return [
           'hub_name' => 'required', 
           'hub_code' => 'required', 
           'mobile' => 'required', 
           'email' => 'required|email', 
           'pan_card' => 'required', 
           'gst_no' => 'required', 
           'aadhar_no' => 'required', 
           'website' => 'required', 
           'state' => 'required', 
           'city' => 'required', 
           'latitude' => 'required', 
           'longitude' => 'required', 
           'address' => 'required', 

        ];
    }
    
    public function sendMessege($pass,$mob) {
        
      $message_body= urlencode("You have been successfully added as Hub user on DS Group portal.
             Please use this password $pass to login on portal ".url('/'));
            $message="&send_to=".$mob."&msg=$message_body";
            \App\Helpers\Helpers::sendSMS(config('api.smsurl').$message);  
            
            return Hash::make($pass);
    }
    public function findOrInsertCity($city) {
        
      $result_state = City::where('city', '=', $city)->first();
            if($result_state){
                return $result_state->id;
            }else{
              return null;  
            }
    }
    public function findOrInsertState($state) {
        $result_state = State::where('name', '=', $state)->first();
            if($result_state){
                return $result_state->id;
            }else{
              return null;  
            }
            
            
    }
}
