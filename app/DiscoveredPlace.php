<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscoveredPlace extends Model
{

    protected $attributes = [
        'status' => 0
    ];
      protected $fillable = ['user_id','lattitude','longitude','resolution','location','city','state','remarks'];
    public function getAgent() {
         return $this->hasOne('App\AgentsOnboardings','id','user_id');
    }


}
