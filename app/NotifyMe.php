<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotifyMe extends Model
{
    //
    protected $table = 'notify_me';
    protected $fillable = ['user_id','status','product_id'];
}
