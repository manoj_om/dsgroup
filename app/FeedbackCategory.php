<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedbackCategory extends Model
{
   protected $fillable = [
        'name','created_by','status'
    ];

   public function remarks() {

        return $this->hasMany('App\FeedbackRemark', 'feedback_category_id', 'id');
    }
}
