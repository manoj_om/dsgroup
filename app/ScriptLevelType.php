<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScriptLevelType extends Model
{
    protected $fillable = ['name','status'];

}
