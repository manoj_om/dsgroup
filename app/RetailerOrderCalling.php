<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RetailerOrderCalling extends Model
{
    protected $fillable = ['retailer_id','calling_days','order_limit','start_time','end_time','calling_option','source','status'];

    public function retailers(){
        return $this->hasOne('App\Retailer','id','retailer_id');
    }

}
