<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderedItemScheme extends Model
{
    protected $attributes = [
        "status" => 0
    ];



    protected $fillable = [
        'channel_type','retailer_type','max_time_applicability','shop_type','scheme_name','from_date','to_date','frequency','promocode','free_secondary_product','primary_product','product_count','free_product_count','applied_location','status'
    ];


}
