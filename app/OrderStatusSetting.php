<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatusSetting extends Model
{
    protected $fillable = [
        'order_to_packing','packing_to_dispatch','dispatch_to_delivered','created_by'
    ];
}
