<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class TraitMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:trait {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to create new trait';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $folderPath='app/Traits';
    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Trait';

    public $newFilePath;

    public $newFileName;

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {

        return str_replace('DummyTrait', $this->argument('name'), $stub);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {

        return file_get_contents(__DIR__ . '/Stubs/trait.stub');
        //return  app_path().'/Console/Commands/Stubs/trait.stub';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->setInputsFromArtisan();

        if ( $this->makeTraitDirectory()->makeTraitFile() ) {

            $this->sendSuccessMessage();

            return;
        }
        $this->error('Oops, something went wrong!');

    }

    private function makeTraitDirectory()
    {
        if ( ! file_exists(base_path($this->folderPath)))
        {
            mkdir(base_path($this->folderPath));
        }
        return $this;
    }

    private function makeTraitFile()
    {
        $txt = $this->replaceClass($this->getStub(),$this->argument('name'));
        $handle = fopen($this->newFilePath, "w");
        fwrite($handle, $txt);
        fclose($handle);
        return $this;
    }

    private function sendSuccessMessage()
    {
        $this->info($this->newFileName . ' successfully created');
    }


    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Traits';
    }

    private function setInputsFromArtisan()
    {
        $this->newFileName = $this->argument('name'). '.php';
        $this-> newFilePath = base_path($this->folderPath) . '/' . $this->argument('name'). '.php';
    }
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the trait.'],
        ];
    }

}
