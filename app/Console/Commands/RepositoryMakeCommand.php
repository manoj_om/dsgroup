<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class RepositoryMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to create new repository';

    protected $folderPath='app/Repository';

    protected $type = 'Repository';

    public $newFilePath;

    public $newFileName;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /*
     * replace class name with argumentName
     *  @param stub file
     *  @param name
     * */

    protected function replaceClass($stub, $name)
    {
        return str_replace('DummyRepository', $this->argument('name'), $stub);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {

        return file_get_contents(__DIR__ . '/Stubs/repository.stub');
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->setInputsFromArtisan();

        if ( $this->makeTraitDirectory()->makeTraitFile() ) {

            $this->sendSuccessMessage();

            return;
        }
        $this->error('Oops, something went wrong!');

    }

    private function makeTraitDirectory()
    {
        if ( ! file_exists(base_path($this->folderPath)))
        {
            mkdir(base_path($this->folderPath));
        }
        return $this;
    }

    private function makeTraitFile()
    {

        $txt = $this->replaceClass($this->getStub(),$this->argument('name'));

        $handle = fopen($this->newFilePath, "w");
        fwrite($handle, $txt);
        fclose($handle);
        return $this;
    }

    private function sendSuccessMessage()
    {
        $this->info($this->newFileName . ' successfully created');
    }

    private function setInputsFromArtisan()
    {
        $this->newFileName = $this->argument('name'). '.php';
        $this-> newFilePath = base_path($this->folderPath) . '/' . $this->argument('name'). '.php';
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the trait.'],
        ];
    }
}
