<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RetailersQueueList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retailers:queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is used to list down all the retailers whom agent will place order by calling them.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $helper = new \App\Helpers\Helpers;
            $day = $helper->getNumberFromDay();
            $retailerOrderCallingRepository = new \App\Repository\RetailerOrderCallingRepository;
            $listsOfQueues = $retailerOrderCallingRepository->getQueueRetailers($day);
            $lists = $ifExist = array();
            foreach($listsOfQueues as $key => $listsOfQueue ){
                $where = array(
                    'status' => 0,
                    'retailer_id' => $listsOfQueue->retailer_id,
                );
                $ifExist = \App\RetailersQueue::where($where)->first();

                if(!$ifExist){

                    $lists[] = array(
                        'start_time' => \Carbon\Carbon::parse($listsOfQueue->start_time)->format('H:i:s a'),
                        'end_time' => \Carbon\Carbon::parse($listsOfQueue->end_time)->format('H:i:s a'),
                        'order_limit' => $listsOfQueue->order_limit,
                        'status' => 0,
                        'retailer_id' => $listsOfQueue->retailer_id,
                        'created_at' => date('Y-m-d H:i:s')
                    );

                }else{
                    $ifExist->start_time = \Carbon\Carbon::parse($listsOfQueue->start_time)->format('H:i:s a');
                    $ifExist->end_time = \Carbon\Carbon::parse($listsOfQueue->end_time)->format('H:i:s a');
                    $ifExist->order_limit = $listsOfQueue->order_limit;
                    $ifExist->save();
                }


                //'created_at' => date('Y-m-d H:i:s')
            }
            if(count($lists) > 0){
                \App\RetailersQueue::insert($lists);
            }

            $this->sendSuccessMessage();
    }
    private function sendSuccessMessage($msg=null)
    {
        $this->info('Retailers queue list generated successfully'.$msg);
    }
}
