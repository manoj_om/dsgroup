<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use  App\HubStock;
class HubStockRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new HubStock;
    }

    public function getClosingInvenory($distributorId)
    {
        # code...
        return $this->model->where('distributor_id',$distributorId)->sum('product_quantity');
    }

    public function getOpeningInvenory($distributorId,$date)
    {
        # code...
        return $this->model->where('distributor_id',$distributorId)
            ->whereDate('updated_at','<',$date)->sum('product_quantity');
    }


    public function getProductOfDistributor($distributorId)
    {
        # code...

        return $this->model->where('distributor_id',$distributorId)->get()->toArray();
    }


}
