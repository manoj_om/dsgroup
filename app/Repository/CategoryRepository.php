<?php


namespace App\Repository;
use App\ProductCategory;
use App\Repository\BaseRepository;

class CategoryRepository extends BaseRepository
{
    private $categoryRepository;

    public function __construct(ProductCategory $productCategory)
    {
        $this->categoryRepository = $productCategory;
    }

    public function getCategoryWithSubCategory(){
        return $this->categoryRepository->where('status', 0)->with('subCategory')->get()->toArray();
    }
}
