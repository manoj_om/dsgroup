<?php


namespace App\Repository;

use App\Repository\BaseRepository;

class NotificationRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new \App\Notification;
    }

    public function getNotifications(){
        return $this->model->all();
    }

    public function getUnreadNotifications($limit=null)
    {
        # code...
        $query = $this->model->where('has_admin_view',0);
        if($limit)
            $query->limit($limit);
        return $query->get();
    }

    public function updateAdminView(){
        return $this->model->where('has_admin_view',0)->update(['has_admin_view' => '1']);
    }


    public function getHubUnreadNotifications($userId){

        return $this->model->select('notifications.*')
        ->join('map_distributors','retailer_id','user_id')
        ->where('map_distributors.distributor_id',$userId)
        ->where('notifications.has_hub_view',0)->get();
    }
    public function getHubUnreadThresholdNotifications($userId){

        return $this->model->select('notifications.*')
        ->join('map_distributors','distributor_id','user_id')
        ->where('map_distributors.distributor_id',$userId)
        ->where('notifications.has_hub_view',0)->get();
    }

    public function updateHubView(array $ids){

        return $this->model->whereIn('id',$ids)->update(['has_hub_view' => '1']);
    }



}
