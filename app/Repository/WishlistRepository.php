<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\Wishlist;


class WishlistRepository extends BaseRepository
{

    public function __construct(Wishlist $wishlist)
    {
        $this->model = $wishlist;
    }

    public function getItemFromUserId(int $userId){

        return $this->model->where('user_id', $userId)->with('products')->get();
    }

    public function getListFromProductIdAndUserId(int $userId,int $productId){
        return $this->model->where(['user_id' => $userId,'product_id' => $productId])->first();
    }




}
