<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\ProductReview;
class ProductReviewRepository extends BaseRepository
{

    public function __construct(ProductReview $productReview)
    {
        $this->model = $productReview;
    }

    public function getByCondition(array $where){
        return $this->model->where($where)->get();
    }

}
