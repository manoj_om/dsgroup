<?php


namespace App\Repository;


use App\Repository\BaseRepository;
use App\Orders;
use DB;
class OrderRepository extends BaseRepository
{

    public function __construct(Orders $orders)
    {
        $this->model = $orders;
    }

    public function list(int $userId){
        return $this->model->where('user_id',$userId)
        ->orderBy('created_at','DESC')->with(['orderItems','apiorder'])->get();
    }

    public function validOrderOfUser(int $userId,int $orderId){
        return $this->model->where(['user_id' => $userId,'id' => $orderId])->with(['apiOrders','orderItems'])->first();
    }


    public function getListBetweenDates($fromDate,$toDate,$userId){
        return $this->model
        ->where('user_id',$userId)->whereBetween('created_at',[$fromDate,$toDate])
        ->orderBy('id','DESC')
        ->with('apiOrders','orderItems')->get();
    }

    public function orderVolumeSchemePromocode(array $where){

        /**
         * check if total orders fullfill the criteria
         * */
        $orderCriteria = $this->model->selectRaw('user_id')
            ->where('user_id',$where['userId'])
            ->where('created_at','>=',$where['fromDate'])
            ->where('created_at','<=',$where['toDate'])
            ->where('total_amount','>=',$where['minimumOrderValue'])
            ->where('status','<>',0)
            ->groupBy('user_id')
            ->havingRaw('count(user_id) >= ? ',[$where['orderCount']])
            ->get();

        if(count($orderCriteria)){
            /**
             * check if totalPromocode applicable times
             * */
            $promocodeCriteria = $this->model->selectRaw('promocode')
                ->where('user_id',$where['userId'])
                ->where('created_at','>=',$where['fromDate'])
                ->where('created_at','<=',$where['toDate'])
                ->where('total_amount','>=',$where['minimumOrderValue'])
                ->where('promocode',$where['promocode'])
                ->where('status','<>',0)
                ->groupBy('promocode')
                ->havingRaw('count(promocode) >= ?',[$where['maxApplicableTime']])
                ->get();
            if(!count($promocodeCriteria))
                return true;
            return false;
        }
        return false;
    }

    public function orderedItemSchemePromocode(array $params){
        $firstCriteria =  $this->model->selectRaw('count(oi.product_id) as count')
            ->join('order_items as oi','oi.order_id','orders.id','inner')
            ->where('orders.user_id',$params['userId'])
            ->whereDate('orders.created_at','>=',$params['fromDate'])
            ->whereDate('orders.created_at','<=',$params['toDate'])
            ->where('orders.status',1)
            ->where('oi.product_id',$params['primaryProduct'])
            ->groupBy('oi.product_id')
            ->havingRaw('count(oi.product_id) >= ?',[$params['productCount']])
            ->get();

        if(count($firstCriteria)){
            /**
             * check if totalPromocode applicable times
             * */
            $promocodeCriteria = $this->model->selectRaw('promocode')
                ->where('user_id',$params['userId'])
                ->where('created_at','>=',$params['fromDate'])
                ->where('created_at','<=',$params['toDate'])
                ->where('promocode',$params['promocode'])
                ->where('status','<>',0)
                ->groupBy('promocode')
                ->havingRaw('count(promocode) >= ?',[$params['maxApplicableTime']])
                ->get();
            if(!count($promocodeCriteria))
                return true;
            return false;
        }
        return false;


    }

    public function skuLineSoldPerCallPromocode(array $where){
        /**
         * check if total orders fullfill the criteria
         * */
        $orderCriteria = $this->model->selectRaw('user_id')
            ->where('user_id',$where['userId'])
            ->where('created_at','>=',$where['fromDate'])
            ->where('created_at','<=',$where['toDate'])
            ->where('total_amount','>=',$where['minimumOrderValue'])
            ->where('status','<>',0)
            ->get();

        if(count($orderCriteria)){
            /**
             * check if totalPromocode applicable times
             * */
            $promocodeCriteria = $this->model->selectRaw('promocode')
                ->where('user_id',$where['userId'])
                ->where('created_at','>=',$where['fromDate'])
                ->where('created_at','<=',$where['toDate'])
                ->where('promocode',$where['promocode'])
                ->where('status','<>',0)
                ->groupBy('promocode')
                ->havingRaw('count(promocode) >= ?',[$where['maxApplicableTime']])
                ->get();
            if(!count($promocodeCriteria))
                return true;
            return false;
        }
        return false;
    }


    public function newProductLauncPromocode(array $where){

        /**
         * check if total orders fullfill the criteria
         * */
        $orderCriteria = $this->model->selectRaw('user_id')
            ->where('user_id',$where['userId'])
            ->where('created_at','>=',$where['fromDate'])
            ->where('created_at','<=',$where['toDate'])
            ->where('total_amount','>=',$where['minimumOrderValue'])
            ->where('status','<>',0)
            ->get();

        if(count($orderCriteria)){
            /**
             * check if totalPromocode applicable times
             * */
            $promocodeCriteria = $this->model->selectRaw('promocode')
                ->where('user_id',$where['userId'])
                ->where('created_at','>=',$where['fromDate'])
                ->where('created_at','<=',$where['toDate'])
                ->where('promocode',$where['promocode'])
                ->where('status','<>',0)
                ->groupBy('promocode')
                ->havingRaw('count(promocode) >= ?',[$where['maxApplicableTime']])
                ->get();
            if(!count($promocodeCriteria))
                return true;
            return false;
        }
        return false;
    }


    public function fromDate($date,$userId)
    {
        # code...

        return $this->model->whereDate('created_at',$date)->where('hub_id',$userId)->get();
    }

    public function getOrderPlacedByAgent($agentId)
    {
        # code...
        return $this->model->selectRaw('SUM(total_amount) as total')->where('placed_by',$agentId)->first();
    }

    public function getTotalOrders(array $retailers){
        return $this->model->whereIn('user_id',$retailers)->with('orderItems')->get();
    }

    public function getTotalOrdersOfRetailer(){
        return $this->model->selectRaw('SUM(total_amount) as total,user_id')
            ->groupBy(['user_id'])->get();
    }


    public function lastSixDaysOrderPie($lastSixthDate, $currentDate,$userId=null)
    {
        # code...

        $query =  $this->model->selectRaw('count(id) as ordercount,sum(total_amount) as total,convert(varchar, created_at, 23) as date')
        ->whereBetween('created_at', [$lastSixthDate, $currentDate])
        ->where('status','!=',0);
        /**
         * if userid is null
         * then request is by admin
         */
        if($userId)
            $query->where('hub_id',$userId);
        /**
         *  order by 2 is check for
         *  order placed by call center
         */
        if(!$userId)
            $query->where('order_by','2');
        return $query->groupBy(\DB::raw('convert(varchar, created_at, 23)'))->get();
    }

    public function getTotalSales()
    {
        # code...
        return $this->model->sales();
    }

    public function getGraphByShopType(){

        return $this->model->selectRaw('count(orders.id) as ordercount,sum(orders.total_amount)
         as total,retailers.shop_type as shopType')
            ->join('retailers','orders.user_id','retailers.id')
            ->where('orders.order_by',2)
            ->groupBy('retailers.shop_type')->get();
    }


    public function getOrdersPlaceByCall($lastSixthDate, $currentDate,$status=null){
        $query =  $this->model->selectRaw('count(id) as ordercount,sum(total_amount) as total,convert(varchar, created_at, 23) as date')
        ->whereBetween('created_at', [$lastSixthDate, $currentDate]);

        $query->where('order_by','2');
        /**
         *  order by 2 is check for
         *  order placed by call center
         */

        if($status)
            $query->where('status',$status);

        return $query->groupBy(\DB::raw('convert(varchar, created_at, 23)'))->get();
    }




}
