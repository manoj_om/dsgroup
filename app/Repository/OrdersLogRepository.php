<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\OrdersLog;
use Illuminate\Support\Facades\Auth;

class OrdersLogRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new OrdersLog;
    }


    public function manageLog($order){

        $gaurd = Auth::getDefaultDriver();
        $action_by = Auth::guard($gaurd)->user()->id;

        $input = [
            'order_id' => $order->id,
            'retailer_id' => $order->user_id,
            'hub_id' => $order->hub_id,
            'total_amount' => $order->total_amount,
            'action_by' => $action_by ,
            'action_user_gaurd' => $gaurd,
            'data' => json_encode($order),
            'status' => $order->status ??1
        ];

        return $this->store($input);
    }



}
