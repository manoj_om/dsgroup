<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\NewProductLaunch;
class NewProductLaunchRepository extends BaseRepository
{

    public function __construct(NewProductLaunch $newProductLaunch)
    {
        $this->model = $newProductLaunch;
    }


    public function promocode(array $where){

        isset($where['channel_type']) ?? $where['channel_type'] = 2;

        return $this->model
            ->where(function ($date) use ($where){
                return $date->where('to_date','>=',date('Y-m-d'))
                    ->where('from_date','<=',date('Y-m-d'));

            })
            ->where(function ($location) use ($where){
                return $location->where('applied_location','LIKE','%'.$where['applied_location'].'%')
                    ->orWhere('applied_location','LIKE','%All%');

            })
            ->where(function ($shopType) use ($where){
                return $shopType->where('shop_type','LIKE','%11%')
                    ->orWhere('shop_type','LIKE','%'.$where['shop_type'].'%');
            })
            ->where(function ($channelType) use ($where){
                return $channelType->where('channel_type',$where['channel_type'])
                    ->orWhere('channel_type',11);
            })
            ->where(function ($retailerType) use ($where){
                return $retailerType->where('retailer_type',$where['retailer_type'])
                ->orWhere('retailer_type',11);
            })
            ->where('status',0)
            ->get();
    }
    public  function getPromocodeDetail(array $where){
        return $this->model->where($where)->get();
    }


}
