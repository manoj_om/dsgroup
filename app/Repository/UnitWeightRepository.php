<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\ProductUnitWeight;
class UnitWeightRepository extends BaseRepository
{

    public function __construct(ProductUnitWeight $productUnitWeight)
    {
        $this->model = $productUnitWeight;
    }


}
