<?php


namespace App\Repository;

use App\Repository\BaseRepository;

class ApiSkuListsRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new \App\ApiSkuLists;
    }
    public function getByCondition(array $where){
        return $this->model->where($where)->get();
    }

    public function updateSku($where,$inputs)
    {
        # code...
        foreach($inputs as $input){
            $where['api_id'] = $input['id'];

            $getSku = $this->getByCondition($where)->first();

            $getSku->description = $input['sku_description'];
            $getSku->unit_cost = $input['sku_unit_cost'];

            $getSku->units_delivered = $input['sku_units_delivered'];
            $getSku->amount_collected = $input['sku_amount_collected'];

            $getSku->status = $input['status'];
            $getSku->api_order_id = $input['order_id'];
            $getSku->packing_type = $input['sku_packing_type'];
            $getSku->save();
        }
        return true;
    }

}
