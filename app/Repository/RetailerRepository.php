<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\Retailer;
use App\Helpers;

class RetailerRepository extends BaseRepository
{


    public function __construct(Retailer $retailer)
    {
        $this->model = $retailer;
    }

    /*
     * added by niraj lal rahi
     * sendOtpOnMobile is used to generate 6 digit otp and save to users data record
     * @param
     * @mobile_number
     *
     * */
    public function sendOtpOnMobile($mobile_number){

        $user = $this->model->where(
            ['primary_mobile_number'=> $mobile_number,
            'status' => 0]
            )->first();
        if($user){
            $otp = Helpers\Helpers::generateOtp();
            $user->otp = $otp;
            $user->attempt_count = $user->attempt_count+1;
            if($user->save())
                return $user;
            else
                return false;
        }
        return false;
    }

    /*
     * added by niraj lal rahi
     * verifyUserOtp
     * @param $where is array and with key and value match with columns in database
     *
     * */

    public function verifyUserOtp($where = array()){
        if(!empty($where))
            return $this->model->select('full_name','id')->where($where)->first();
        return false;
    }

    /*
     * added by niraj lal rahi
     * emptyOtpAndAttemptCount
     * this function will be used to reset the otp field and and count attempt by user to login
     * @param
     * $user_id
     * */
    public function emptyOtpAndAttemptCount($user_id = null){

        if($user_id){
            $user = $this->model->find($user_id);
            if($user){
                $user->otp = NULL;
                $user->attempt_count = 0;
                return $user->save();
            }
        }
        return false;

    }

    public function activeRetailers()
    {
        # code...
        return $this->model->active();
    }

    public function getRetailersFromHub(array $ids,$distributor_id){
        return $this->model->select('retailers.id')
        ->leftJoin('map_distributors','retailers.id','map_distributors.retailer_id')
        ->whereIn('retailers.id',$ids)
        ->where('map_distributors.distributor_id',$distributor_id)->get();
    }


}
