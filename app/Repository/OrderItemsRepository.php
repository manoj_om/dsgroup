<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\OrderItems;

class OrderItemsRepository extends BaseRepository
{

    public function __construct(OrderItems $orderItems)
    {
        $this->model = $orderItems;
    }


    public function placeOrder(array $inputs){
        return $this->model->insert($inputs);
    }


}
