<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\ProductUnitType;

class UnitTypeRepository extends BaseRepository
{

    public function __construct(ProductUnitType $productUnitType)
    {
        $this->model = $productUnitType;
    }


}
