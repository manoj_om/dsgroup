<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\ApiOrders;
class ApiOrderRepository extends BaseRepository
{

    public function __construct(ApiOrders $apiOrders)
    {
        $this->model = $apiOrders;
    }


    public function getByCondition(array $where){
        return $this->model->where($where)->get();
    }


}
