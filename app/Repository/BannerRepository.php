<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\HomeSliderBanner;

class BannerRepository extends BaseRepository
{

    public function __construct(HomeSliderBanner $homeSliderBanner)
    {
        $this->model = $homeSliderBanner;
    }


    public function getActiveBanner(){
        return $this->model->where('is_delete',0)->where('status',0)->get();
    }


}
