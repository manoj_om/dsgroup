<?php


namespace App\Repository;

use App\Repository\BaseRepository;

class RetailersQueueRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new \App\RetailersQueue;
    }

    public function getQueueList()
    {
        # code...
        return $this->model->where('status',0)->orderBy('start_time','ASC')->get();
    }


}
