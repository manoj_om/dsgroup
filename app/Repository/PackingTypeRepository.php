<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\PackingType;
class PackingTypeRepository extends BaseRepository
{

    public function __construct(PackingType $packingType)
    {
        $this->model = $packingType;
    }

    public function getWhere(array $where){
        return $this->model->where($where)->get();
    }




}
