<?php


namespace App\Repository;
use App\Product;
use App\Repository\BaseRepository;

class ProductRepository extends BaseRepository
{


    public function __construct(Product $product)
    {

        $this->model = $product;
    }

    public function search($distributorId,$keywords){

        return $this->model->select('products.id','products.product_lists_id','products.short_description','products.package_unit',
        'packaging','products.quantity','products.category','products.sub_category','products.mrp','products.rlp',
        'brand_name','products.manufacturer','products.max_order_quantity','products.product_unit_weight',
        'products.avtar')
        ->join('hub_stocks','products.id','hub_stocks.product_id','left')
        ->whereIn('products.product_lists_id',function($query) use ($keywords){
            return $query->select('id')
            ->from('product_lists')
            ->where('name','like',"%$keywords%")
            ->get()->toArray();
        })
        ->where('hub_stocks.distributor_id',$distributorId)
        ->where('hub_stocks.status',0)
        ->where('products.status',0)
        ->get();
    }

    public function getProductFromCategory(array $params){

        return $this->model->select('id','product_lists_id','short_description','package_unit',
        'packaging','quantity','category','sub_category','mrp','rlp',
        'brand_name','manufacturer','max_order_quantity','product_unit_weight',
        'avtar')
        ->whereIn('id',function($subQuery) use ($params){
            $query =$subQuery->selectRaw('MAX(products.id) as id')
            ->from(with($this->model)->getTable())
            ->join('hub_stocks','products.id','hub_stocks.product_id','left');
                if($params['subCategory'])
                    $query->where('products.sub_category',$params['subCategory']);
                if($params['category'])
                    $query->where('products.category',$params['category']);
                $query->where('hub_stocks.distributor_id',$params['distributor_id']);
                $query->where('hub_stocks.status',0);
                $query->groupBy(['products.product_lists_id','products.packaging']);
                return $query->get()->toArray();
        })
        ->where('status',0)
        ->get();
    }

    public function list($isFeatured = false,$distributorId){
        if($isFeatured)
            return $this->model->select('id','is_featured','product_lists_id','short_description','package_unit',
                'packaging','quantity','category','sub_category','mrp','rlp','brand_name',
                'manufacturer','max_order_quantity','product_unit_weight','avtar')
                ->whereIn('id',function($subQuery) use ($distributorId){
                    $query =$subQuery->selectRaw('MAX(products.id) as id')
                    ->from(with($this->model)->getTable())
                    ->join('hub_stocks','products.id','hub_stocks.product_id','left');
                        $query->where('hub_stocks.distributor_id',$distributorId);
                        $query->where('hub_stocks.status',0);
                        $query->groupBy(['products.product_lists_id','products.packaging']);
                        return $query->get()->toArray();
                })
                ->where('status',0)
                ->where('is_featured',1)
                ->get();
        return $this->model->all();
    }

    public function checkIfProductExist(array $ids,$distributorId){
        return $this->model->select('products.id')
        ->join('hub_stocks','products.id','hub_stocks.product_id','left')
        ->where('hub_stocks.distributor_id',$distributorId)
        ->whereIn('products.id',$ids)
        ->where('hub_stocks.status',0)
        ->where('products.status',0)->get();
    }

    public function getAllCondition(array $where,$distributorId){

        return $this->model->select('products.id','products.product_lists_id','products.short_description','products.package_unit',
        'packaging','products.quantity','products.category','products.sub_category','products.mrp','products.rlp',
        'brand_name','products.manufacturer','products.max_order_quantity','products.product_unit_weight',
        'products.avtar')
        ->join('hub_stocks','products.id','hub_stocks.product_id','left')
        ->where('hub_stocks.distributor_id',$distributorId)
        ->where($where)
        ->where('hub_stocks.status',0)
        ->where('products.status',0)
        ->get();

    }


}
