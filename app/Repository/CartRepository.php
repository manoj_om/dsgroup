<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\Cart;

class CartRepository extends BaseRepository
{

    public function __construct(Cart $cart)
    {
        $this->model = $cart;
    }

    public function getCartItemFromUserId(int $userId){

        return $this->model->where('user_id', $userId)->with('products')->get();
    }

    public function deleteUserCartProducts(int $userId){

        return $this->getCartItemFromUserId($userId)->each->delete();

    }

    public function validCartItemOfUser(array $where){
        return $this->model->where($where)->first();
    }






}
