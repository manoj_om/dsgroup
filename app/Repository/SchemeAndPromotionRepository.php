<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\SchemeAndPromotion;
class SchemeAndPromotionRepository extends BaseRepository
{

    public function __construct(SchemeAndPromotion $schemeAndPromotion)
    {
        $this->model = $schemeAndPromotion;
    }

    public function getAllPromotionList(){
        
        return $this->model->select('*')
                    ->where('is_delete','0')
                    ->where('status','0')
                    ->whereDate('to_date', '>=', date('Y-m-d'))
                    ->get();
       
    }
}
