<?php


namespace App\Repository;
use App\User;
use App\Helpers;

class UserRepository
{
    protected $userRepository;

    public function __construct(User $user)
    {
        $this->userRepository = $user;
    }

    /*
     * added by niraj lal rahi
     * sendOtpOnMobile is used to generate 6 digit otp and save to users data record
     * @param
     * @mobile_number
     *
     * */
    public function sendOtpOnMobile($mobile_number){
        $user = $this->userRepository->where('mobile_number',$mobile_number)->first();
        if($user){
            $otp = Helpers\Helpers::generateOtp();
            $user->otp = $otp;
            $user->attempt_count = $user->attempt_count+1;
            if($user->save())
                return $user;
            else
                return false;
        }
        return false;
    }

    /*
     * added by niraj lal rahi
     * verifyUserOtp
     * @param $where is array and with key and value match with columns in database
     *
     * */

    public function verifyUserOtp($where = array()){
        if(!empty($where))
            return $this->userRepository->where($where)->first();
        return false;
    }

    /*
     * added by niraj lal rahi
     * emptyOtpAndAttemptCount
     * this function will be used to reset the otp field and and count attempt by user to login
     * @param
     * $user_id
     * */
    public function emptyOtpAndAttemptCount($user_id = null){

        if($user_id){
            $user = $this->userRepository->find($user_id);
            if($user){
                $user->otp = NULL;
                $user->attempt_count = 0;
                $user->save();
            }
        }

    }
}
