<?php


namespace App\Repository;

use App\Repository\BaseRepository;

class RetailerOrderCallingRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new \App\RetailerOrderCalling;
    }

    public function getQueueRetailers($day)
    {
        # code...
        return $this->model->where('calling_days','LIKE','%'.$day.'%')->with('retailers')->get();
    }

    public function getByCondition(array $where){
        return $this->model->where($where)->get();
    }


}
