<?php


namespace App\Repository;

use App\Repository\BaseRepository;
use App\RetailersQrCode;
class RetailersQrCodeRepository extends BaseRepository
{

    public function __construct(RetailersQrCode $retailersQrCode)
    {
        $this->model = $retailersQrCode;
    }


    public function getFromUserId(int $userId){
        return $this->model->where('user_id',$userId)->get()->first();
    }


}
