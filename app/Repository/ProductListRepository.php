<?php


namespace App\Repository;

use App\ProductList;
use App\Repository\BaseRepository;
class ProductListRepository extends BaseRepository
{

    public function __construct(ProductList $productList)
    {
        $this->model = $productList;
    }

    public function getProductFromCategory($categoryId,$subcategoryId){

        if($subcategoryId)
            return $this->model->select('id','name')->where(function ($query) use ($categoryId,$subcategoryId){
                return $query->where('category_id',$categoryId)->where('subcategory_id',$subcategoryId);
            })->get();

        return $this->model->select('id','name')->where('category_id',$categoryId)->get();
    }

    public function list(){

        return $this->model->select('product_lists.id','product_lists.name','products.mrp','poducts.rlp')
            ->join('products','product_lists.id','products.product_lists_id','left')
            ->where('products.is_featured',1)
            ->groupBy(['product_lists.id','product_lists.name','products.mrp'])
            ->get();
    }

    public function search($keywords){
        return $this->model->select('id')->where('name','like',"%$keywords%")->get()->toArray();
    }

    public function getProductFromCategorySubCategory($categoryId,$subcategoryId){

        $query = $this->model->select('id');
        if($subcategoryId)
            $query->where('subcategory_id',$subcategoryId);
        if($categoryId)
            $query->where('category_id',$categoryId);

        return $query->get()->toArray();
    }

}
