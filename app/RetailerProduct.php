<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RetailerProduct extends Model
{
     protected $fillable = [
       'retailer_id','product_id'
    ];

}
