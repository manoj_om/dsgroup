<?php

namespace App\Exports;

use App\Distributor;
use Maatwebsite\Excel\Concerns\FromCollection;

class ExportDistributor implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Distributor::all();
    }
}
