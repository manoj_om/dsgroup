<?php

namespace App\Exports;

use Illuminate\Http\Request;
use App\Retailer;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Carbon\Carbon;

class RetailerExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithEvents {

    /**
     * @return \Illuminate\Support\Collection
     */
    protected $retailer;

    public function __construct($retailer) {

        $this->retailer = $retailer;
    }

    public function collection() {
        return $this->retailer;
    }

    public function map($retailer): array {

        $model = New Retailer();
        return [
            $retailer->full_name,
            $retailer->primary_mobile_number,
            $retailer->alternative_mobile_number != 0 ? $retailer->alternative_mobile_number : '' ,
            $retailer->whats_app_number != 0 ? $retailer->whats_app_number : '',
            $retailer->secondary_full_name,
            $retailer->secondary_mobile_number,
            $model->getRetailerType($retailer->shop_type),
            $retailer->shop_name,
            $retailer->dob != '' ? Carbon::parse($retailer->dob)->format('m-d-Y') : '',
            $retailer->anniversay != '' ? Carbon::parse($retailer->anniversay)->format('m-d-Y') : '',
            $retailer->first_kid_birthday != '' ? Carbon::parse($retailer->first_kid_birthday)->format('m-d-Y') : '',
            $retailer->second_kid_birthday != '' ? Carbon::parse($retailer->second_kid_birthday)->format('m-d-Y') : '',
            $retailer->establishment_name,
            $retailer->gst_number,
            $retailer->pan_number,
            $retailer->adhar_number,
            $model->deviceType($retailer->p_device_type),
            $retailer->scan_code,
            $retailer->shop_address,
            $retailer->latitudes,
            $retailer->longitude,
            $retailer->city,
            $retailer->state,
            $model->RetailerTypeTwo($retailer->is_prime),
            $retailer['otherQrcode']['paytm'],
            $retailer['otherQrcode']['mobiqwik'],
            $retailer['otherQrcode']['gpay'],
            $retailer['otherQrcode']['phone_pe'],
            $retailer['otherQrcode']['upi'],
            $retailer['otherQrcode']['bharat_qr'],
            $model->getStatus($retailer->status),
            $model->getUserType($retailer->user_type),
            $model->getUserName($retailer->user_type, $retailer->user_id),
            Carbon::parse($retailer->created_at)->format('m-d-Y'),
        ];
    }

    public function headings(): array {
        return [
            'Full Name',
            'Primary Mobile Number',
            'Alternative Mobile Number',
            'Whatspp Number',
            'Secondary Full Name',
            'Secondary Mobile Number',
            'Shop Type',
            'Shop Name',
            'DOB',
            'Anniversay',
            'First Kid Birthday',
            'Second Kid Birthday',
            'Establishment Name',
            'GST Number',
            'PAN Number',
            'Aadhar Number',
            'Device Type',
            'QR Code',
            'Address',
            'Lattitude',
            'Longitude',
            'City',
            'State',
            'Retailer Type',
            'PayTm QR Code',
            'Mobiqwik QR Code',
            'Gpay QR Code',
            'PhonePe QR Code',
            'UPI QR Code',
            'Bharat QR Code',
            'Status',
            'Onboarded_By',
            'Added_by',
            'Created At',
        ];
    }

    public function registerEvents(): array {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:AH1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }

}
