<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Auth\Notifications\ResetPassword;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile_number','role','username','designation','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function onboardingAgents() {
        return $this->hasMany('App\AgentsOnboardings');
    }

    public function sendPasswordResetNotification($token)
    {

        $email = $this->getEmailForPasswordReset();
        $phone = $this->getMobileForPasswordReset($email);
        $message_body= urlencode("Please reset your password by click on this link ".route('password.reset',['token' => $token,'email' => $email]).". Thank you.");
        $message="&send_to=$phone&msg=$message_body";
        \App\Helpers\Helpers::sendSMS(config('api.smsurl').$message);
        return true;
        //$this->notify(new Notification($token));

    }

    public function getMobileForPasswordReset($email){

        return $this->where('email',$email)->first()->mobile_number;

    }



}
