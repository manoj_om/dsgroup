<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('agent/retailer-queue-list', 'AgentController@getRetailersQueueList')->name('retailer.queue.lists');
Route::prefix('agent')->name('agent.')->group(function() {
    Route::get('/password/reset', 'Auth\Agent\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/email', 'Auth\Agent\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

    //Reset Password Routes
    Route::get('/password/reset/{token}', 'Auth\Agent\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset', 'Auth\Agent\ResetPasswordController@reset')->name('password.update');

    // Email Verification Route(s)
    Route::get('email/verify', 'Auth\Agent\VerificationController@show')->name('verification.notice');
    Route::get('email/verify/{id}', 'Auth\Agent\VerificationController@verify')->name('verification.verify');
    Route::get('email/resend', 'Auth\Agent\VerificationController@resend')->name('verification.resend');
});
Route::prefix('hub')->name('hub.')->group(function() {
    Route::get('/password/reset', 'Auth\hub\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/email', 'Auth\hub\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

    //Reset Password Routes
    Route::get('/password/reset/{token}', 'Auth\hub\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset', 'Auth\hub\ResetPasswordController@reset')->name('password.update');

    // Email Verification Route(s)
    Route::get('email/verify', 'Auth\hub\VerificationController@show')->name('verification.notice');
    Route::get('email/verify/{id}', 'Auth\hub\VerificationController@verify')->name('verification.verify');
    Route::get('email/resend', 'Auth\hub\VerificationController@resend')->name('verification.resend');
});




Auth::routes();

Route::get('/dialer/api', 'DialerApiController@apiIntegrate')->name('dialer.api');
Route::get('/cron/hub/stock/notification', 'CronController@cronHubStockNotifications');
Route::get('/cron/hub/stock/reset', 'CronController@hubCron')->name('dialer.api');
Route::prefix('agent')->group(function() {
    Route::post('/login', 'Auth\AgentLoginController@login')->name('agentlogin');

    Route::group([
        'middleware' => 'auth:agent'
            ], function() {
        Route::post('/retailer/order/calling', 'AgentController@saveRetailerOrderCalling');
        Route::post('/login/guard/data', 'AgentController@getGuardInfo');
        Route::get('/logout/', 'Auth\AgentLoginController@logout');
        Route::get('/', 'AgentController@index');
        Route::get('/dashboard', 'AgentController@index')->name('agent-dashboard');
        Route::get('/call/script/{id}', 'AgentController@callingScript');
        Route::get('/panel/{id}', 'AgentController@agentPanel')->name('agent-panel');
        Route::get('/profile', 'AgentController@profile');
        Route::post('/upload/image', 'AgentController@profileImageUpload');
        Route::get('/call/center', 'AgentController@callindex')->name('agent-call-center');
        Route::get('/call/queue', 'AgentController@call_queue')->name('agent-call-queue');
        Route::get('/call/status', 'AgentController@agent_status')->name('agent-agent-status');
        Route::get('/product/list', 'AgentController@productList');
        Route::get('/order/list', 'AgentController@orderList');
        Route::get('/frequent/order/list', 'AgentController@frequentOrderList');
        Route::get('/calling/list', 'AgentController@getCallingAgents');
        Route::get('/view/retailer/data/panel/{id}', 'AgentController@panelView');
        Route::get('/search/calling/agent', 'AgentController@getCallingAgents');
        Route::post('/change/agent/login/status', 'AgentController@changeAgentLoginStatus');
        Route::post('/place/order', 'AgentController@placeOrderAgentPanel');
        Route::post('/get/schemes/promotions', 'AgentController@schemeAndPromotions');
        Route::get('/get/promocode', 'AgentController@listPromocode');
        Route::post('promocode-detail', 'AgentController@getPromocodeDetail');
        Route::post('/add/feedbacks', 'AgentController@addFeedbackRemarks');
        Route::post('/add/feedback/category', 'AgentController@addFeedbackCategory');
        Route::get('/get/feedbacks', 'AgentController@getFeedback');
        Route::post('/add/retailer/feedback', 'AgentController@addRetailerFeedback');
        Route::get('/retailer-queue', 'AgentController@retailerQueue')->name('retailer.queue');
        Route::post('/retailer/rating', 'AgentController@storeReviewPanel');
    });
});
Route::prefix('hub')->group(function() {
    Route::post('/login', 'Auth\HubLoginController@login')->name('hublogin');
    Route::group([
        'middleware' => 'auth:hub'
            ], function() {

        /**
         * hub notification
         */

        Route::get('/notifications', 'NotificationController@getHubNotifications');
        Route::get('/thresh/notifications', 'NotificationController@getHubThreshNotifications');
        Route::get('/view-notifications', 'NotificationController@viewHubNotification');

        Route::get('/', 'Hub\DashboardController@index')->name('hub-dashboard');
        Route::get('/order/pie-chart', 'Hub\DashboardController@orderPieChart')->name('hub-piechart');
        Route::get('/order/last-sixday-pie-chart', 'Hub\DashboardController@lastSixDaysOrderPie')->name('hub-six-day-piechart');
        Route::post('/login/guard/data', 'Hub\DashboardController@getGuardInfo');
        Route::post('/get/product/category', 'Hub\DashboardController@getCategory');
        Route::post('/get/product/subcategory', 'Hub\DashboardController@getSubCategory');
        Route::post('/change/staus/stock', 'Hub\DashboardController@ChangeStatus');
        Route::get('/get/products', 'Hub\DashboardController@getProducts');
        Route::get('/product/view/{productId}', 'Hub\DashboardController@productView');
        Route::post('/update/stock', 'Hub\DashboardController@updateProductStock');
        Route::get('/logout/', 'Auth\HubLoginController@logout');
        Route::get('/profile', 'Hub\DashboardController@profile');
        Route::post('/upload/avtar', 'Hub\DashboardController@profileImageUpload');
        //hub
        Route::get('/dashboard', 'Hub\DashboardController@index')->name('hub-dashboard');
        Route::get('/order', 'Hub\OrderController@index')->name('hub-order');
        Route::get('/get/orders', 'Hub\OrderController@getOrders');
        Route::get('/stock', 'Hub\StockController@index')->name('hub-stock');
        Route::post('/send/threshold/notification', 'Hub\StockController@sendThreshNotification');
        Route::get('/view', 'Hub\ViewStockController@view')->name('view');
        Route::get('/order/view/{orderid}', 'Hub\OrderController@view')->name('view');
        Route::get('/hub-profile', 'Hub\HubProfileController@viewhubProfile')->name('hub-profile');
        Route::post('/update/packed/quantity', 'Hub\OrderController@updatePackedQuantity');
        Route::post('/cancel/packed/order', 'Hub\OrderController@changeStatus');
    });
});

//auth

Route::group([
    'middleware' => 'auth'
        ], function() {

    Route::group([
        'middleware' => 'App\Http\Middleware\adminRoleMiddleware'
            ], function() {

        /**
         * admin notification
         */

        Route::get('/admin/notifications', 'NotificationController@getNotifications');
        Route::get('/admin/view-notifications', 'NotificationController@viewNotification');

        /**
         * attention request export
         *
         */
        Route::get('export/attention/required/{status}','Admin\ExportAttentionRequestController@export');
        Route::get('/export/onboarding/agent','HomeController@export');

        Route::get('/retailer/panel/{id}', 'PlaceOrderController@agentPanel')->name('retailer-panel');
        Route::get('/retailer/call/script/{id}', 'PlaceOrderController@callingScript');
        Route::post('/retailer/add/feedbacks', 'PlaceOrderController@addFeedbackRemarks');

        Route::post('/retailer/add/retailer/feedback', 'PlaceOrderController@addRetailerFeedback');
        Route::post('/retailer/add/feedback/category', 'PlaceOrderController@addFeedbackCategory');
        Route::get('/retailer/get/feedbacks', 'PlaceOrderController@getFeedback');
        Route::post('/retailer/place/order', 'PlaceOrderController@placeOrderRetailerPanel');
        Route::post('/retailer/promocode-detail', 'PlaceOrderController@getPromocodeDetail');
        Route::get('/retailer/get/promocode', 'PlaceOrderController@listPromocode');
        Route::get('/retailer/order/list', 'PlaceOrderController@orderList');
        Route::get('/retailer/frequent/order/list', 'PlaceOrderController@frequentOrderList');
        Route::get('/retailer/product/list', 'PlaceOrderController@productList');
        Route::get('/retailer/view/retailer/data/panel/{id}', 'PlaceOrderController@panelView');
        Route::post('/retailer/get/schemes/promotions', 'PlaceOrderController@schemeAndPromotions');
        Route::post('/retailer/retailer/rating', 'PlaceOrderController@storeReviewPanel');
        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::get('/graph', 'DashboardController@graph')->name('graph');
        //manage scheme
        Route::get('/scheme/setting', 'SchemeController@index')->name('scheme-index');
        Route::get('/order/volume', 'SchemeController@orderVolumeView')->name('order-volume');
        Route::get('/ordered/items', 'SchemeController@orderItemView')->name('order-item');
        Route::get('/sku/call/scheme', 'SchemeController@skuCallSchemeView')->name('sku-scheme');
        Route::get('/new/product/launch', 'SchemeController@newProductLaunchView')->name('new-product');
        Route::get('/add/ordered/items/discount', 'SchemeController@orderedItemsDiscountView')->name('ordered-discount');
        Route::post('/add/scheme/building', 'SchemeController@addSchemeBuilding');
        Route::post('/add/ordered/items/discount', 'SchemeController@addOrderedItemsDiscount');
        Route::post('/update/ordered/items/discount', 'SchemeController@updateOrderedItemsDiscount');
        Route::post('/add/order/volume', 'SchemeController@addOrderVolume');
        Route::post('/update/order/volume', 'SchemeController@updateOrderVolume');
        Route::post('/add/order/item', 'SchemeController@addOrderedItemScheme');
        Route::post('/update/order/item', 'SchemeController@updateOrderedItemScheme');
        Route::post('/add/sku/scheme', 'SchemeController@addSkuLineSoldPerCallScheme');
        Route::post('/update/sku/scheme', 'SchemeController@updateSkuLineSoldPerCallScheme');
        Route::post('/add/new/product', 'SchemeController@addNewProductLaunch');
        Route::post('/update/new/product', 'SchemeController@updateNewProductLaunch');
        Route::get('/get/schemes/building', 'SchemeController@getSchemeBuilding');
        Route::get('/get/ordered/items/discount', 'SchemeController@getOrderedDiscount');
        Route::get('/get/order/volume', 'SchemeController@getOrderVolumeScheme');
        Route::get('/get/order/item', 'SchemeController@getOrderedItemScheme');
        Route::get('/get/sku/scheme', 'SchemeController@getSkuLineSoldPerCallScheme');
        Route::get('/get/new/product', 'SchemeController@getNewProductLaunch');
        Route::post('/change/scheme/building/status', 'SchemeController@changeSchemeBuildingStatus');
        Route::post('/change/orderd/discount/status', 'SchemeController@changeOrderedDiscount');
        Route::post('/change/order/volume/status', 'SchemeController@changeOrderCountStatus');
        Route::post('/change/order/item/status', 'SchemeController@changeOrderedItemScheme');
        Route::post('/change/sku/scheme/status', 'SchemeController@changeSkuLineSoldPerCallScheme');
        Route::post('/change/new/product/status', 'SchemeController@changeNewProductLaunch');
        Route::get('/get/retailer/city', 'SchemeController@getRetailerCity');


        Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
//onboarding agent route
        Route::get('/onboarding/agents', 'HomeController@index')->name('onboarding');
        Route::post('/onboarding/agents/{id}', 'HomeController@location')->name('location');

        Route::get('/get/all/onboarding/agent-map', 'HomeController@getAllOnBoardingAgentsMap');
        Route::get('/get/all/onboarding/agent', 'HomeController@getAllOnBoardingAgent');
        Route::get('/search/all/onboarding/agent', 'HomeController@searchtAllOnBoardingAgent');
        Route::get('/approve/agent/view/{aid}', 'HomeController@approvedAgentView')->name('approveView');
        Route::get('/approve-agent/{aid}', 'HomeController@approveAgents');
        Route::post('/approve/all/agent', 'HomeController@approveAllAgents');
        Route::post('/reject/all/agent', 'HomeController@rejectAllAgents');
        Route::get('/reject-agent/{aid}', 'HomeController@rejectAgents');
        Route::post('/search/agent', 'HomeController@searchInAgent');
        Route::post('/deactivate/from/agent', 'HomeController@deactivateAgent');
        Route::post('/activate/from/agent', 'HomeController@activateAgents');

//retailer route
        Route::post('/add/retailer', 'RetailerController@addRetailer');
        Route::get('/export/retailer', 'RetailerController@export');
        Route::post('/export/retailer/agent', 'RetailerController@exportOnboardByagent')->name('onboarding-reatailer');
        Route::post('/update/retailer', 'RetailerController@updateRetailer');
        Route::get('/retailer', 'RetailerController@index')->name('retailer');
        Route::get('/get/retailer/data', 'RetailerController@getRetailer');
        Route::get('/search/retailer/data', 'RetailerController@getSearchRetailer');
        Route::get('/search/attention/required/data', 'RetailerController@getSearchRetailerAttentionRequired');
        Route::get('/search/discovered/places/data', 'RetailerController@getSearchAgentDiscoverPlaces');
        Route::get('/view/retailer/data/{id}', 'RetailerController@view');

        Route::post('/find/retailer', 'RetailerController@findRetailer');
        Route::post('/import/retailer', 'RetailerController@import')->name('retailer.import');
        Route::get('/import/retailer/view', 'RetailerController@importView')->name('retailer.import.view');
        Route::get('/retailer/attention/required', 'RetailerController@attentionRequiredView')->name('attention');
        Route::get('/retailer/discovered/place', 'RetailerController@discoveredPlacesView')->name('discovered');
        Route::get('/retailer/order/calling/{retailerid}', 'RetailerController@orderCallingView')->name('order_calling');
        Route::get('/retailer/assign/prodct/{retailerid}', 'RetailerController@assignProductView')->name('assign_product');
        Route::get('/get/attention/data', 'RetailerController@getAllAttentionList');
        Route::get('/get/discoved/places/data', 'RetailerController@getAllDiscovedPlacesList');
        Route::post('/remove/from/attentionlist', 'RetailerController@removeFromAttentionList');
        Route::post('/remove/from/discoveplaces', 'RetailerController@removeFromDiscoveplaces');
        Route::get('/barcode/scanner/{code}', 'RetailerController@barcodeScanner');
        Route::get('/agent-map', 'HomeController@agentMap')->name('retailer-map');
        Route::get('/retailer/search-retailers-in-map', 'RetailerController@searchRetailersInMap');
        Route::post('/disable/retailer', 'RetailerController@disableRetailer');
        Route::post('/make/non/prime/retailer', 'RetailerController@makeNonPrimeRetailer');
        Route::post('/make/prime/retailer', 'RetailerController@makePrimeRetailer');
        Route::post('/enable/retailer', 'RetailerController@enableRetailer');
        Route::post('/update/retailer/assigned/products', 'RetailerController@updateRetailerAssignedProducts');
        Route::post('/get/assigned/product/list', 'RetailerController@getAssingedProductList');
        Route::get('/get/retailer/city/list', 'RetailerController@getRetailerCityList');


//manage User

        Route::get('/manage/users', 'UsersController@manageUsersView')->name('manage-user');

        Route::get('/your/profile', 'UsersController@viewProfile')->name('profile');
        Route::post('/add/users/data', 'UsersController@addUser');
        Route::post('/update/users/data', 'UsersController@updateUser');
        Route::get('/get/users/data', 'UsersController@getUsers');
        Route::post('/get/login/user/data', 'UsersController@getCurrentUsers');
        Route::get('/search/users', 'UsersController@searchUsers');
        Route::post('/disable/users', 'UsersController@disableUsers');
        Route::post('/enable/users', 'UsersController@enableUsers');
        Route::post('/upload/user/avtar', 'UsersController@uploadImage');


//product master
        Route::get('/product/master', 'ProductController@index')->name('catalog');
        Route::get('/product/delivery/partner', 'ProductController@deliveryPartner')->name('catalog-delivery');
        Route::get('/distributor', 'ProductController@distributorPage')->name('catalog-distributor');
        Route::get('/add/product', 'ProductController@addProduct')->name('add-product');
        Route::get('/product/order', 'OrdersController@index')->name('orders');
        Route::get('/get/order/list', 'OrdersController@getOrders');
        Route::get('/order/view/{orderid}', 'OrdersController@detailView');
        Route::post('/change/order/status', 'OrdersController@changeStatus');
        Route::post('/save/retailer/order/calling', 'OrdersController@saveRetailerOrderCalling');
        Route::post('/retailer/retailer/order/calling', 'OrdersController@saveRetailerOrderCalling');

//agent
        Route::get('/calling/agent', 'CallingagentController@index')->name('calling-agent');
        Route::post('/add/calling/agent', 'CallingagentController@addCallcenterAgent');
        Route::get('/get/calling/agent', 'CallingagentController@getCallingAgents');
        Route::get('/search/calling/agent', 'CallingagentController@getCallingAgents');
        Route::get('/search/calling/agent', 'AgentController@getCallingAgents');
        Route::post('/disable/calling/agents', 'CallingagentController@disableCallingAgents');
        Route::post('/enable/calling/agents', 'CallingagentController@enableCallingAgents');

//callcenter
        Route::get('/call/center', 'CallcenterController@index')->name('call-center-queue');
        Route::get('/call/queue', 'CallcenterController@call_queue')->name('call-queue');
        Route::get('/call/queue-list', 'CallcenterController@call_queue')->name('call-queue');
        Route::get('/call/status', 'CallcenterController@agent_status')->name('agent-status');
        Route::post('/change/agent/login/status', 'CallcenterController@changeAgentLoginStatus');

//report
        Route::get('/report/list', 'ReportController@index')->name('list');
        Route::get('/report/summary', 'ReportController@summary')->name('summary');
        Route::get('/report/order-report', 'ReportController@order')->name('report');
        Route::get('/report/mobile-report', 'ReportController@mobileApp')->name('mobile.report');
        Route::get('/report/callagent-report', 'ReportController@callAgent')->name('callagent.report');
        Route::get('/report/hub-report', 'ReportController@hub')->name('hub.report');
        Route::get('/report/rider-report', 'ReportController@rider')->name('hub.report');
        Route::get('/report/scheme-report', 'ReportController@scheme')->name('hub.report');

        Route::get('/report/order-report-page', 'ReportController@orderreportview')->name('order-report');
        Route::get('/report/call-center-page', 'ReportController@callcenterview')->name('call-center');
        Route::get('/report/mobile-app-report-page', 'ReportController@mobileappreportview')->name('mobile-app-report');
        Route::get('/report/hub-report-page', 'ReportController@hubreportview')->name('hub-report');
        Route::get('/report/rider-report-page', 'ReportController@riderreportview')->name('rider-report');
        Route::get('/report/scheme-report-page', 'ReportController@schemereportview')->name('scheme-report');



// cms
        Route::get('/cms/index', 'CMSController@index')->name('index');
        Route::get('/cms/scheme', 'CMSController@scheme')->name('scheme');

        Route::get('/cms/callscript', 'CMSController@callscript')->name('call-script');
        Route::get('/call/script/tree/{id}', 'CMSController@callScriptTreeView');

        Route::post('/add/home/slider', 'CMSController@addSlider');
        Route::post('/save/script/tree', 'CallScriptController@saveScriptTree');
        Route::get('/get/home/slider', 'CMSController@getSlider');
        Route::post('/change/home/slider/status', 'CMSController@changeSliderStatus');
        Route::post('/change/scheme/status', 'CMSController@changeSchemeStatus');
        Route::post('/add/scheme', 'CMSController@addScheme');
        Route::post('/get/schemes', 'CMSController@getScheme');
        Route::post('/delete/home/slider', 'CMSController@deleteSlider');
        Route::post('/delete/scheme', 'CMSController@deleteScheme');
        Route::get('/cms/order/status/setting', 'CMSController@orderStatusSettingView')->name('order-status-setting');
        Route::post('/set/order/status/setting', 'CMSController@setOrderStatusSetting');
        Route::get('/get/order/status/setting', 'CMSController@getOrderStatusSetting');

        Route::post('/add/unit/type', 'ProductController@addUnitType');
        Route::post('/add/packing/type', 'ProductController@addPackingType');
        Route::post('/add/weight/type', 'ProductController@addWeightType');
        Route::post('/update/unit/type', 'ProductController@updateUnitType');
        Route::post('/update/packing/type', 'ProductController@updatePackingType');
        Route::post('/update/weight/type', 'ProductController@updateWeightType');
        Route::post('/add/product/category', 'ProductController@addCategory');
        Route::post('/add/product/list', 'ProductController@addProductList');
        Route::post('/update/product/category', 'ProductController@updateCategory');
        Route::post('/update/product/list', 'ProductController@updateProductList');
        Route::post('/get/product/category', 'ProductController@getCategory');
        Route::get('/get/product/list/name', 'ProductController@getProductListName');
        Route::post('/get/product/name/list', 'ProductController@getProductNameList');
        Route::get('/get/product/categories', 'ProductController@getCategories');
        Route::post('/get/product/subcategory', 'ProductController@getSubCategory');
        Route::get('/get/product/sub/categories', 'ProductController@getSubCategories');
        Route::post('/add/product/subcategory', 'ProductController@addSubCategory');
        Route::post('/update/product/subcategory', 'ProductController@updateSubCategory');
        Route::post('/add/product/data', 'ProductController@addProducts');
        Route::post('/get/products/packing/type', 'ProductController@getProductType');
        Route::post('/update/product/data', 'ProductController@updateProducts');
        Route::post('/get/product/list', 'ProductController@getProductList');
        Route::get('/get/products', 'ProductController@getProducts');
        Route::post('disable/product/category', 'ProductController@disableProductCategory');
        Route::post('enable/product/category', 'ProductController@enableProductCategory');
        Route::post('disable/product/list', 'ProductController@disableProductList');
        Route::post('enable/product/list', 'ProductController@enableProductList');
        Route::post('disable/product/subcategory', 'ProductController@disableProductSubCategory');
        Route::post('enable/product/subcategory', 'ProductController@enableProductSubCategory');
        Route::post('disable/product', 'ProductController@disableProduct');
        Route::post('enable/product', 'ProductController@enableProduct');
        Route::get('/agent-map', 'HomeController@agentMap')->name('retailer-map');



        Route::resource('distributor', 'DistributorController');
        //add
        Route::get('/getcity-by-state', 'DistributorController@getcity');
        Route::get('/get/distributor/data', 'DistributorController@getDistributor');
        Route::get('/get/distributor/list', 'DistributorController@getDistributorList');
        Route::get('/get/agent/list', 'DistributorController@getAgentList');
        Route::post('/get/distributor/update', 'DistributorController@update');
        Route::get('/manage/hub/users', 'DistributorController@manageHubView')->name('manage-hub-users');
        Route::post('/distributor-import', 'DistributorController@import');
        Route::get('/distributor-add-excel', 'DistributorController@addExcel');
        Route::post('/get/distributor/change-status', 'DistributorController@changeStatus');
        //get/distributor/map-distributor
        Route::get('/get/distributor/ajaxlist', 'DistributorController@ajaxlist');
        Route::post('/map/distributor', 'DistributorController@mapDistributor');
        Route::post('/map/multiple/retailer', 'DistributorController@mapMultipleRetailer');
        Route::post('/map/agent', 'DistributorController@mapAgent');
        Route::post('/remove/map/distributor', 'DistributorController@removeMapDistributor');
        Route::post('/remove/map/agent', 'DistributorController@removeMapAgent');
        Route::get('category/paginate', 'ProductController@paginateProduct');

        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        Route::post('import', 'ProductController@import');

        //call script

        Route::get('get/call/script/type', 'CallScriptController@getTypes');
        Route::get('get/call/script/list', 'CallScriptController@getAllScripts');
        Route::post('add/shop/type', 'CallScriptController@addShopType');
        Route::post('add/call/type', 'CallScriptController@addCallType');
        Route::post('add/level/type', 'CallScriptController@addLevelType');
        Route::post('add/event/type', 'CallScriptController@addEventType');
        Route::post('save/call/script', 'CallScriptController@addCallScript');
        Route::post('update/call/script', 'CallScriptController@updateCallScript');
    });


    Route::group([
        'middleware' => 'App\Http\Middleware\callcenterAdminMiddleware'
            ], function() {
        Route::get('/call/center', 'CallcenterController@index')->name('call-center-queue');
        Route::get('/call/queue', 'CallcenterController@call_queue')->name('call-queue');
        Route::get('/call/status', 'CallcenterController@agent_status')->name('agent-status');
        Route::post('/change/agent/login/status', 'CallcenterController@changeAgentLoginStatus');
        Route::get('/search/calling/agent', 'AgentController@getCallingAgents');
        Route::get('/search/calling/agent', 'CallingagentController@getCallingAgents');
        Route::get('/get/calling/agent', 'CallingagentController@getCallingAgents');
        Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
        Route::get('/your/profile', 'UsersController@viewProfile')->name('profile');
        Route::post('/upload/user/avtar', 'UsersController@uploadImage');
        Route::post('/get/login/user/data', 'UsersController@getCurrentUsers');
        Route::get('/call/queue-list', 'CallcenterController@call_queue')->name('call-queue');
    });
});
