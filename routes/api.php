<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('time',function(){
    $amPm = date('a');
    $time = (int) date('H');
    $today = array();
    $allDay = array(
        array('value' => '12:00','text' => '9AM-12PM'),
        array('value' => '15:00', 'text' =>'12PM-3PM') ,
        array('value' => '18:00','text' => '3PM-6PM')
    );
    if($time  < 9)
        $today[] = $allDay[0];

    if($time < 12)
        $today[] = $allDay[1];
    if($time < 15)
        $today[] = $allDay[2];

    return response()->json(['today' => $today,'otherDays' => $allDay ]);
});

Route::group(['namespace'=>'Api','name' => 'api.'],function($api){

    $api->get('home','IndexController@index')->middleware(['auth:api','activeRetailer'])->name('index.list');

    $api->middleware(['auth:api','activeHub','activeRetailer'])->group(function($home){

        $home->post('search','IndexController@search')->name('index.search');
        $home->get('product/{id}/detail','IndexController@getProductDetails')->name('index.product.detail');
        $home->post('category-product-list','IndexController@getProductByCategory')->name('index.category.product.list');
    });

    $api->get('scheme-promotion-list','IndexController@schemeAndPromotionsList')->name('scheme.promotion.list');

    $api->put('order/update/status','Retailer\MyAccountController@updateStatus')->name('order.status.update');

    $api->namespace('Auth')->group(function($auth){
        $auth->post('login','LoginController@login')->name('login');
        $auth->post('verify-otp','LoginController@verifyOtp')->name('verifyOtp');
    });

    $api->group([
        'namespace' => 'Retailer',
        'prefix' => 'my-account',
        'middleware' => ['auth:api','activeRetailer'],
        'name'=>'account.'
    ],function($retailer){
        $retailer->post('notify-me','MyAccountController@notifyMe');
        $retailer->get('promocode','MyAccountController@listPromocode');
        $retailer->post('promocode-detail','MyAccountController@getPromocodeDetail');

        $retailer->post('update-profile','MyAccountController@updateProfile')->name('update.profile');
        $retailer->get('user-detail','MyAccountController@getProfileDetail')->name('profile');

        $retailer->get('logout','MyAccountController@logout')->name('logout');


        $retailer->group(['prefix' => 'cart','name' => 'cart.', 'middleware' => ['activeHub']],function ($cart){

            $cart->post ('add-to-cart','MyAccountController@addToCart')->name('addToCart');
            $cart->get ('list-cart-item','MyAccountController@listCartItems')->name('cartList');

            $cart->post ('update-cart-item-quantity','MyAccountController@updateQuantity')->name('cartUpdateQuantity');
            $cart->post ('remove-cart-item','MyAccountController@removeProductFromCart')->name('removeProductFromCart');
            $cart->delete ('{id}/delete','MyAccountController@removeProductFromCart')->name('removeProductFromCart');

          });

        $retailer->group(['prefix' => 'order','name' => 'order.'],function ($order){

            $order->post('place','MyAccountController@placeOrder')->name('place');
            $order->get('list','MyAccountController@list')->name('list');

            $order->post('cancel','MyAccountController@cancel')->name('cancel');
            $order->get('filter','MyAccountController@filter')->name('filter');
            $order->post('re-order','MyAccountController@reOrder')->name('reorder');

        });

        $retailer->group(['prefix' => 'wishlist','name' => 'wishlist.', 'middleware' => ['activeHub']],function ($cart){

            $cart->post ('add','MyAccountController@addToWishList')->name('add');
            $cart->get ('list','MyAccountController@getList')->name('list');

            $cart->delete ('{id}/remove','MyAccountController@remove')->name('delete');
            $cart->post ('remove-wishlist-item','MyAccountController@removeProductFromCart')->name('removeProductFromCart');

        });
        $retailer->group(['prefix' => 'review','name' => 'review.'],function ($review){

            $review->post ('add','MyAccountController@storeReview')->name('store');


        });


    });
});

Route::fallback(function() {
    return response()->json([
      'status' => false,
      'message' => 'Page not found'
    ], 404
  );
});
